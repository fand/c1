package com.cardone.validator;

import lombok.Setter;
import org.apache.commons.lang3.StringUtils;

import java.util.regex.Pattern;

/**
 * Created by Administrator on 2014/11/8.
 */
public class MatchValidatorRule extends RequiredValidatorRule {
    @Setter
    private String regex;

    @Override
    public boolean validate(Object obj) {
        if (!(obj instanceof String)) {
            return false;
        }

        String objString = (String) obj;

        if (StringUtils.isBlank(objString)) {
            return false;
        }

        return Pattern.matches(regex, objString);
    }
}
