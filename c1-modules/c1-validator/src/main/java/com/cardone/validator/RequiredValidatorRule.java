package com.cardone.validator;

import lombok.*;
import org.apache.commons.collections.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * Created by Administrator on 2014/10/31.
 */
public class RequiredValidatorRule implements ValidatorRule {
    @Setter
    private String message;


    @Setter
    private String typeCode;


    @Setter
    private String code;

    @Override
    public boolean validate(Object obj) {
        if (obj == null) {
            return false;
        }

        if (obj instanceof String) {
            String objString = (String) obj;

            return StringUtils.isNotBlank(objString);
        } else if (obj instanceof Map) {
            Map<Object, Object> objMap = (Map<Object, Object>) obj;

            return MapUtils.isNotEmpty(objMap);
        } else if (obj instanceof java.util.List) {
            List<Object> objList = (List<Object>) obj;

            return CollectionUtils.isNotEmpty(objList);
        }

        return true;
    }

    @Override
    public String getMessage() {
        return this.message;
    }

    @Override
    public String getTypeCode() {
        return this.typeCode;
    }

    @Override
    public String getCode() {
        return this.code;
    }
}
