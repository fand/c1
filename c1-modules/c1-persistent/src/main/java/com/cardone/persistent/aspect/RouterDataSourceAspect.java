package com.cardone.persistent.aspect;

/**
 * 路由拦截
 *
 * @author yaoht
 */
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class RouterDataSourceAspect {
    private com.cardone.common.router.support.RouterKeySupport masterRouterKeySupport;
    private com.cardone.common.router.support.RouterKeySupport slaveRouterKeySupport;
    private com.cardone.common.router.support.RouterKeySupport determineCurrentLookupKeySupport;

    private java.util.Map<String, String> determineCurrentLookupKeyMap = com.google.common.collect.Maps.newConcurrentMap();

    private org.springframework.transaction.annotation.Propagation[] newDetermineCurrentLookupKeyPropagations = new org.springframework.transaction.annotation.Propagation[]
            {
                    org.springframework.transaction.annotation.Propagation.REQUIRES_NEW,
                    org.springframework.transaction.annotation.Propagation.NOT_SUPPORTED,
                    org.springframework.transaction.annotation.Propagation.NEVER
            };

    /**
     * 设置属性
     *
     * @param joinPoint 切片点
     */
    public void setDetermineCurrentLookupKey(final org.aspectj.lang.JoinPoint joinPoint) {
        if ((determineCurrentLookupKeySupport == null) || (masterRouterKeySupport == null) || (slaveRouterKeySupport == null)) {
            return;
        }

        final java.lang.reflect.Method method = ((org.aspectj.lang.reflect.MethodSignature) joinPoint.getSignature()).getMethod();

        org.springframework.transaction.annotation.Transactional transactional = org.springframework.core.annotation.AnnotationUtils.findAnnotation(method, org.springframework.transaction.annotation.Transactional.class);

        String determineCurrentLookupKey = com.cardone.context.ContextHolder.getDetermineCurrentLookupKey();

        if (org.apache.commons.lang3.StringUtils.isNotBlank(determineCurrentLookupKey)) {
            if (transactional == null) {
                return;
            }

            if (!transactional.readOnly() && !org.apache.commons.lang3.ArrayUtils.contains(newDetermineCurrentLookupKeyPropagations, transactional.propagation())) {
                return;
            }
        }

        String key = joinPoint.getTarget().getClass().getName() + "." + joinPoint.getSignature().getName();

        if (determineCurrentLookupKeyMap.containsKey(key)) {
            com.cardone.context.ContextHolder.setDetermineCurrentLookupKey(determineCurrentLookupKeyMap.get(key));

            return;
        }

        if (org.apache.commons.lang3.StringUtils.isNotBlank(determineCurrentLookupKey)) {
            return;
        }

        String groupId;

        if ((transactional == null) || !transactional.readOnly()) {
            groupId = masterRouterKeySupport.readKey(key);
        } else {
            groupId = slaveRouterKeySupport.readKey(key);
        }

        determineCurrentLookupKey = determineCurrentLookupKeySupport.readKeyByGroupId(groupId, key);

        com.cardone.context.ContextHolder.setDetermineCurrentLookupKey(determineCurrentLookupKey);

        determineCurrentLookupKeyMap.put(key, determineCurrentLookupKey);
    }
}
