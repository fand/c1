package com.cardone.persistent.support;

import com.cardone.common.dto.PaginationDto;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.List;
import java.util.Map;

/**
 * 简易dao接口
 *
 * @author yaohaitao
 */
public interface JdbcTemplateSupport {
    /**
     * 生成sql
     *
     * @param id    sql标识
     * @param model model
     * @return sql
     */
    String builderSql(final String id, final Object model);

    /**
     * 分页
     *
     * @param paginationArgs 分页参数
     * @param model          model
     * @return 分页对象
     */
    PaginationDto<Map<String, Object>> pagination(final PaginationArgs paginationArgs, final Map<String, Object> model);

    /**
     * 分页
     *
     * @param mappedClass    返回类型
     * @param paginationArgs 分页参数
     * @param model          model
     * @return 分页对象
     */
    <P> PaginationDto<P> pagination(final Class<P> mappedClass, final PaginationArgs paginationArgs, final Map<String, Object> model);

    /**
     * 获取
     *
     * @param id    sql标识
     * @param model model
     * @return 行字段值对象
     */
    Map<String, Object> find(final String id, final Map<String, Object> model);

    /**
     * 查询
     *
     * @param id    sql标识
     * @param model model
     * @return 行字段值对象列表
     */
    List<Map<String, Object>> findList(final String id, final Map<String, Object> model);

    /**
     * 查询
     *
     * @param id sql标识
     * @return 行字段值对象列表
     */
    List<Map<String, Object>> findList(final String id);

    /**
     * 获取
     *
     * @param mappedClass 返回类型
     * @param id          sql标识
     * @param model       model
     * @return 行字段值对象
     */
    <P> P find(final Class<P> mappedClass, final String id, final Map<String, Object> model);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param id          sql标识
     * @param model       model
     * @return 行字段值对象列表
     */
    <P> List<P> findList(final Class<P> mappedClass, final String id, final Map<String, Object> model);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param id          sql标识
     * @return 行字段值对象列表
     */
    <P> List<P> findList(final Class<P> mappedClass, final String id);

    /**
     * 读取
     *
     * @param requiredType 返回类型
     * @param id           sql标识
     * @param objectId     读取字段标识
     * @param model        model
     * @return 字段值
     */
    <R> R read(final Class<R> requiredType, final String id, final String objectId, final Map<String, Object> model);

    /**
     * 读取
     *
     * @param requiredType 返回类型
     * @param id           读取字段标识
     * @param model        model
     * @return 字段值
     */
    <R> R read(final Class<R> requiredType, final String id, final Map<String, Object> model);

    /**
     * 读取
     *
     * @param requiredType 返回类型
     * @param id           sql标识
     * @param objectId     读取字段标识
     * @return 字段值
     */
    <R> R read(final Class<R> requiredType, final String id, final String objectId);

    /**
     * 读取
     *
     * @param requiredType 返回类型
     * @param id           sql标识
     * @return 字段值
     */
    <R> R read(final Class<R> requiredType, final String id);

    /**
     * 读取
     *
     * @param elementType 返回类型
     * @param id          sql标识
     * @param objectId    读取字段标识
     * @param model       model
     * @return 字段值列表
     */
    <R> List<R> readList(final Class<R> elementType, final String id, final String objectId, final Map<String, Object> model);

    /**
     * 读取
     *
     * @param elementType 返回类型
     * @param id          sql标识
     * @param model       model
     * @return 字段值列表
     */
    <R> List<R> readList(final Class<R> elementType, final String id, final Map<String, Object> model);

    /**
     * 读取
     *
     * @param elementType 返回类型
     * @param id          sql标识
     * @param objectId    读取字段标识
     * @return 字段值列表
     */
    <R> List<R> readList(final Class<R> elementType, final String id, final String objectId);

    /**
     * 读取
     *
     * @param elementType 返回类型
     * @param id          sql标识
     * @return 字段值列表
     */
    <R> List<R> readList(final Class<R> elementType, final String id);

    /**
     * 更新
     *
     * @param id        sql标识
     * @param modelList modelList
     * @return 影响行数
     */
    int[] update(final String id, final List<Map<String, Object>> modelList);

    /**
     * 更新
     *
     * @param id    sql标识
     * @param model model
     * @return 影响行数
     */
    int update(final String id, final Map<String, Object> model);

    /**
     * 更新
     *
     * @param idList sql标识集合
     * @param model  model
     * @return 影响行数
     */
    int[] update(final List<String> idList, final Map<String, Object> model);

    /**
     * 更新
     *
     * @param id sql标识
     * @return 影响行数
     */
    int update(final String id);

    /**
     * 批量更新
     *
     * @param id
     * @param batchValueList model
     * @return 影响行数
     */
    int[] batchUpdate(final String id, final List<Map<String, Object>> batchValueList);

    /**
     * 批量更新
     *
     * @param id
     * @param batchValueList model
     * @return 影响行数
     */
    int[] batchUpdate(final String id, Map<String, Object> model, final List<Map<String, Object>> batchValueList);

    /**
     * 使用对应目标数据源的NamedParameterJdbcTemplateSupport
     *
     * @return NamedParameterJdbcTemplate
     */
    NamedParameterJdbcTemplate getNamedParameterJdbcTemplate();
}