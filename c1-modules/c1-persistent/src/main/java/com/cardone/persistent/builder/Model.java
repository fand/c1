package com.cardone.persistent.builder;

import java.util.*;

/**
 * 对象映射
 *
 * @author yaohaitao
 */
public interface Model extends Map<String, Object> {

    /**
     * 填充
     *
     * @param keys  键数组
     * @param value 值
     * @return 对象映射
     */
    Model put(final String[] keys, final Object value);

    /**
     * 充值扩展
     *
     * @param keyPrefix 键前缀名称
     * @param kvMap     键值映射
     * @param keySuffix 键后缀名称
     * @return 对象映射
     */
    Model putExtend(final String keyPrefix, final Map<String, Object> kvMap, final String keySuffix);

    /**
     * 充值扩展
     *
     * @param keyPrefix 键前缀名称
     * @param keyBase   键名称
     * @param keySuffix 键后缀名称
     * @param value     值
     * @return 对象映射
     */
    Model putExtend(final String keyPrefix, final String keyBase, final String keySuffix, final Object value);

    /**
     * 充值扩展
     *
     * @param keyPrefix 键前缀名称
     * @param keyBases  键名称集合
     * @param keySuffix 键后缀名称
     * @param value     值
     * @return 对象映射
     */
    Model putExtend(final String keyPrefix, final String[] keyBases, final String keySuffix, final Object value);

    /**
     * 填充类型或对象字段
     *
     * @param modelArgs 类型
     * @return 对象映射
     */
    Model putObjectField(final ModelArgs modelArgs);

    /**
     * 充值true值
     *
     * @param keys 键数组
     * @return 对象映射
     */
    Model putTrue(final String... keys);

    /**
     * 充值true值扩展
     *
     * @param keyPrefix 键前缀名称
     * @param keyBases  键基本名称
     * @return 对象映射
     */
    Model putTrueExtend(final String keyPrefix, final String... keyBases);

    /**
     * 移除扩展
     *
     * @param keys 键数组
     * @return 对象映射
     */
    Model remove(final String... keys);

    /**
     * 移除扩展
     *
     * @param keyPrefix 键前缀名称
     * @param keyBase   键基本名称
     * @param keySuffix 键后缀名称
     * @return 对象映射
     */
    Model removeExtend(final String keyPrefix, final String keyBase, final String keySuffix);

    /**
     * 移除扩展
     *
     * @param keyPrefix 键前缀名称
     * @param keyBases  键基本名称集合
     * @param keySuffix 键后缀名称
     * @return 对象映射
     */
    Model removeExtend(final String keyPrefix, final String[] keyBases, final String keySuffix);

    /**
     * 设置
     *
     * @param defaultKeyPrefix 默认键值前缀名称
     */
    Model setDefaultKeyPrefix(final String defaultKeyPrefix);

    /**
     * 设置
     *
     * @param defaultKeySuffix 默认键值后缀名称
     */
    Model setDefaultKeySuffix(final String defaultKeySuffix);

    /**
     * 设置
     *
     * @param defaultSpace 默认间隔
     */
    Model setDefaultSpace(final String defaultSpace);

    /**
     * 常量枚举
     *
     * @author yaohaitao
     */
    public enum Keys {
        /**
         * 全部
         */
        all,

        /**
         * 开始行数
         */
        beginRowNum("begin_row_num"),

        /**
         * 总合
         */
        count,

        /**
         * 默认键值前缀名称
         */
        defaultKeyPrefix("where_and_eq"),

        /**
         * 默认键值后缀名称
         */
        defaultKeySuffix("value"),

        /**
         * 默认间隔
         */
        defaultSpace("_"),

        /**
         * 删除
         */
        delete,

        /**
         * 结束行数
         */
        endRowNum("end_row_num"),

        /**
         * 创建
         */
        insert,

        /**
         * 最大
         */
        max,

        /**
         * 最小
         */
        min,

        /**
         * 读取字段标识
         */
        objectId("object_id"),

        /**
         * 排序
         */
        orderBy("order_by"),

        /**
         * 开始行数
         */
        paginationBeginRowNum("pagination_begin_row_num"),

        /**
         * 结束行数
         */
        paginationEndRowNum("pagination_end_row_num"),

        /**
         * 分页大小
         */
        paginationSize("pagination_size"),

        /**
         * 选择
         */
        select,

        /**
         * 默认忽略属性数组
         */
        serialVersionUID,

        /**
         * 大小
         */
        size("size"),

        /**
         * 更新
         */
        update,

        /**
         * 并且之间
         */
        whereAndBetween("where_and_between"),

        /**
         * 并且等于
         */
        whereAndEq("where_and_eq"),

        /**
         * 并且like
         */
        whereAndLike("where_and_like"),

        /**
         * 并且不等于
         */
        whereAndNq("where_and_nq"),

        /**
         * 或者等于
         */
        whereOrEq("where_or_eq"),

        /**
         * 或者like
         */
        whereOrLike("where_or_like"),

        /**
         * 或者不等于
         */
        whereOrNq("where_or_nq");

        /**
         * string值
         */
        private final String stringValue;

        /**
         * 构建
         */
        private Keys() {
            this.stringValue = this.name();
        }

        /**
         * 构建
         *
         * @param stringValue string值
         */
        private Keys(final String stringValue) {
            this.stringValue = stringValue;
        }

        /**
         * 获取string值
         *
         * @return string值
         */
        public String stringValue() {
            return this.stringValue;
        }
    }
}