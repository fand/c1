package com.cardone.persistent.support;

import com.cardone.common.dto.*;
import com.cardone.context.*;
import lombok.*;
import lombok.experimental.*;
import org.apache.commons.collections.*;

import java.util.*;

/**
 * 分页参数
 *
 * @author yaohaitao
 */

public class PaginationArgs implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 7711818834200928202L;

    /**
     * 查询标识
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String findId;

    /**
     * 分页号
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private int no;

    /**
     * 读取标识
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String readId;

    /**
     * 大小
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private int size;

    /**
     * 实例化分页参数
     *
     * @param readId 读取标识
     * @param findId 查询标识
     * @param no     分页号
     * @param size   大小
     */
    public PaginationArgs(final String readId, final String findId, final int no, final int size) {
        this.readId = readId;
        this.findId = findId;
        this.no = no;
        this.size = size;
    }

    /**
     * 实例化分页参数
     *
     * @param readId           读取标识
     * @param findId           查询标识
     * @param paginationEntity 分页实体
     */
    public PaginationArgs(final String readId, final String findId, final PaginationEntityDto paginationEntity) {
        this.readId = readId;
        this.findId = findId;
        this.no = paginationEntity.getPaginationNo();
        this.size = paginationEntity.getPaginationSize();
    }

    /**
     * 实例化分页参数
     *
     * @param readId        读取标识
     * @param findId        查询标识
     * @param paginationMap 分页实体
     */
    public PaginationArgs(final String readId, final String findId, final Map<String, Object> paginationMap) {
        this.readId = readId;
        this.findId = findId;
        this.no = MapUtils.getIntValue(paginationMap, Contexts.paginationNo.name(), 1);
        this.size = MapUtils.getIntValue(paginationMap, Contexts.paginationSize.name(), 10);
    }
}
