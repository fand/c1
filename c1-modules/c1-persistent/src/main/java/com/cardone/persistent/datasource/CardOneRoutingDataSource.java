package com.cardone.persistent.datasource;

/**
 * Created by yaohaitao on 2014/10/28.
 */
@lombok.extern.slf4j.Slf4j
public class CardOneRoutingDataSource extends org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource {
    @Override
    protected Object determineCurrentLookupKey() {
        String determineCurrentLookupKey = com.cardone.context.ContextHolder.getDetermineCurrentLookupKey();

        if (log.isDebugEnabled()) {
            log.debug("determineCurrentLookupKey:" + determineCurrentLookupKey);
        }

        return determineCurrentLookupKey;
    }
}
