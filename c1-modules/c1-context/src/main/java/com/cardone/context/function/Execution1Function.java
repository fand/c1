package com.cardone.context.function;

/**
 * 方法
 *
 * @param <R> 输出泛型
 * @param <T> 输入泛型
 * @author yaohaitao
 */
@FunctionalInterface
public interface Execution1Function<R, T> {
    /**
     * 执行
     *
     * @param t 输入泛型参数
     * @return 输出泛型值
     */
    R execution(final T t);
}
