package com.cardone.context;

/**
 * 属性
 *
 * @author yaohaitao
 */
public enum Attributes {
    /**
     * 开始时间
     */
    beginDate,

    /**
     * 开始时间
     */
    beginDateStr,

    /**
     * 代码
     */
    code,

    /**
     * 创建人代码
     */
    createdByCode,

    /**
     * 创建人标识
     */
    createdById,

    /**
     * 创建人名称
     */
    createdByName,

    /**
     * 创建时间
     */
    createdDate,

    /**
     * 创建时间
     */
    createdDateStr,

    /**
     * 部门代码
     */
    departmentCode,

    /**
     * 部门标识
     */
    departmentId,

    /**
     * 结束时间
     */
    endDate,

    /**
     * 结束时间
     */
    endDateStr,

    /**
     * 标识
     */
    id,

    /**
     * 标识集合
     */
    idList,

    /**
     * 标识集合
     */
    ids,

    /**
     * 修改人代码
     */
    lastModifiedByCode,

    /**
     * 修改人标识
     */
    lastModifiedById,

    /**
     * 修改人名称
     */
    lastModifiedByName,

    /**
     * 修改时间
     */
    lastModifiedDate,

    /**
     * 修改时间
     */
    lastModifiedDateStr,

    /**
     * 登录用户代码
     */
    longinUserCode,

    /**
     * 登录用户标识(数据权限控制时使用)
     */
    longinUserId,

    /**
     * 登录用户名称
     */
    longinUserName,

    /**
     * 名称
     */
    name,

    /**
     * 排序代码
     */
    orderCode,

    /**
     * 排序数
     */
    orderNum,

    /**
     * 组织代码
     */
    orgCode,

    /**
     * 组织标识
     */
    orgId,

    /**
     * 分页号
     */
    paginationNo,

    /**
     * 分页大小
     */
    paginationSize,

    /**
     * 父级代码
     */
    parentCode,

    /**
     * 父级名称
     */
    parentId,

    /**
     * 父级名称
     */
    parentName,

    /**
     * 许可代码
     */
    permissionCode,

    /**
     * 许可类别代码
     */
    permissionTypeCode,

    /**
     * 项目代码
     */
    projectCode,

    /**
     * 项目标识
     */
    projectId,

    /**
     * 项目名称
     */
    projectName,

    /**
     * 说明
     */
    remark,

    /**
     * 角色代码
     */
    roleCode,

    /**
     * 角色标识
     */
    roleId,

    /**
     * 角色名称
     */
    roleName,

    /**
     * 服务名
     */
    serverName,

    /**
     * 站代码
     */
    siteCode,

    /**
     * 站标识
     */
    siteId,

    /**
     * 状态代码
     */
    stateCode,

    /**
     * 状态标识
     */
    stateId,

    /**
     * 状态名称
     */
    stateName,

    /**
     * 样式标识
     */
    styleId,

    /**
     * 系统时间
     */
    sysdate,

    /**
     * 文本
     */
    text,

    /**
     * 类别代码
     */
    typeCode,

    /**
     * 类别标识
     */
    typeId,

    /**
     * 类别名称
     */
    typeName,

    /**
     * 类别代码
     */
    typeTypeCode,

    /**
     * URL
     */
    url,

    /**
     * 用户代码
     */
    userCode,

    /**
     * 用户组代码
     */
    userGroupCode,

    /**
     * 用户组标识
     */
    userGroupId,

    /**
     * 用户组名称
     */
    userGroupName,

    /**
     * 用户标识
     */
    userId,

    /**
     * 用户名称
     */
    userName,

    /**
     * 验证类型代码
     */
    validatorTypeCodes,

    /**
     * 值
     */
    value,

    /**
     * 密码
     */
    password,

    /**
     * 版本
     */
    version;
}
