package com.cardone.context.action;

/**
 * 动作
 *
 * @param <T> 输入泛型1
 * @param <U> 输入泛型2
 * @author yaohaitao
 */
@FunctionalInterface
public interface Run2Action<T, U> {
    /**
     * 运行
     *
     * @param t 输入泛型参数1
     * @param u 输入泛型参数2
     */
    void run(final T t, final U u);
}
