package com.cardone.context;

import lombok.*;
import lombok.experimental.*;

import java.util.*;

/**
 * 字典异常：支持错误代码
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Accessors(chain = true)
public class DictionaryException extends RuntimeException {

    /**
     * 序列号
     */
    private static final long serialVersionUID = 8485572461812284705L;

    /**
     * 代码
     */
    private String code;

    /**
     * 数据
     */
    private Object data;

    /**
     * 类别代码
     */
    private String typeCode;

    private Map<String, String> errors;

    /**
     * 路径
     */
    private String url;

    public DictionaryException() {
        super();
    }

    public DictionaryException(final String message) {
        super(message);
    }

    public DictionaryException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public DictionaryException(final Throwable cause) {
        super(cause);
    }

    /**
     * 设置类别代码
     *
     * @param typeCodeClass 类别代码类型
     * @return 字典异常
     */
    public DictionaryException setTypeCode(final Class<?> typeCodeClass) {
        this.typeCode = typeCodeClass.getName();

        return this;
    }

    /**
     * 设置类别代码
     *
     * @param typeCode 类别代码
     * @return 字典异常
     */
    public DictionaryException setTypeCode(final String typeCode) {
        this.typeCode = typeCode;

        return this;
    }
}
