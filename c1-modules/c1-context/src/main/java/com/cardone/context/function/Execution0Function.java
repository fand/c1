package com.cardone.context.function;

/**
 * 方法
 *
 * @param <R> 输出泛型
 * @author yaohaitao
 */
@FunctionalInterface
public interface Execution0Function<R> {
    /**
     * 执行
     *
     * @return 输出泛型值
     */
    R execution();
}
