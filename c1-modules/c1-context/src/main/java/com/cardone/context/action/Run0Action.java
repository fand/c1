package com.cardone.context.action;

/**
 * 动作
 *
 * @author yaohaitao
 */
@FunctionalInterface
public interface Run0Action {
    /**
     * 运行
     */
    void run();
}
