package com.cardone.context.action;

/**
 * 动作
 *
 * @param <T> 输入泛型1
 * @param <U> 输入泛型2
 * @param <V> 输入泛型3
 * @param <W> 输入泛型4
 * @author yaohaitao
 */
@FunctionalInterface
public interface Run4Action<T, U, V, W> {
    /**
     * 运行
     *
     * @param t 输入泛型参数1
     * @param u 输入泛型参数2
     * @param v 输入泛型参数3
     * @param w 输入泛型参数4
     */
    void run(final T t, final U u, final V v, final W w);
}