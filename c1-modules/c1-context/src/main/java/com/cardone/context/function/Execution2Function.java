package com.cardone.context.function;

/**
 * 方法
 *
 * @param <R> 输出泛型
 * @param <T> 参数类型1
 * @param <U> 参数类型2
 * @author yaohaitao
 */
@FunctionalInterface
public interface Execution2Function<R, T, U> {
    /**
     * 执行
     *
     * @param t 输入泛型参数1
     * @param u 输入泛型参数2
     * @return 输出泛型值
     */
    R execution(final T t, final U u);
}
