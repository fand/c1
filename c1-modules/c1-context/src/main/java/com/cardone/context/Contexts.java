package com.cardone.context;

/**
 * 常量
 *
 * @author yaohaitao
 */
public enum Contexts {
    /**
     * 属性
     */
    attributes,

    /**
     * 区块
     */
    block,

    /**
     * 分页号
     */
    paginationNo,

    /**
     * 分页大小
     */
    paginationSize,

    /**
     * 视图名
     */
    cardoneViewName,

    /**
     * 跳过
     */
    skip,

    /**
     * 第一次
     */
    one,

    /**
     * 子集
     */
    children,

    /**
     * 时期格式
     */
    datePattern,

    /**
     * 关闭
     */
    closed,

    /**
     * 上下文路径
     */
    contextPath,

    servletPath("webServletPath"),

    /**
     * 站
     */
    contextSite,

    /**
     * 数据
     */
    data,

    /**
     * 数据集合
     */
    dataList,

    /**
     * 组标识
     */
    groupId("group-id"),

    /**
     * 登录
     */
    loginKey("login-key"),

    /**
     * 登录用户
     */
    loginUser,

    /**
     * 登录用户编号
     */
    loginUserCode,

    /**
     * 登录用户标识
     */
    loginUserId("login-user-id"),

    /**
     * 主
     */
    main,

    /**
     * 消息
     */
    message,

    /**
     * NamedParameterJdbcTemplate路由支持
     */
    namedParameterJdbcTemplateRouterSupportCacheName("com.cardone.persistent.support.NamedParameterJdbcTemplateRouterSupport"),

    /**
     * 对象
     */
    obj,

    /**
     * 打开
     */
    open,

    /**
     * 参数
     */
    parameters,

    /**
     * 行集合
     */
    rows,

    /**
     * 服务
     */
    service,

    /**
     * 站点标识
     */
    siteId("site-id"),

    remoteData("remote-data"),

    /**
     * 状态
     */
    state,

    /**
     * 统计
     */
    total,

    /**
     * 路径-黑名单
     */
    urlBlackList,

    /**
     * 超级角色标识
     */
    superRoleId("super-role-id"),

    /**
     * 路径-白名单
     */
    urlWhiteList;

    /**
     * string值
     */
    private String stringValue;

    /**
     * 构建
     */
    private Contexts() {
        this.stringValue = this.name();
    }

    /**
     * 构建
     *
     * @param stringValue string值
     */
    private Contexts(final String stringValue) {
        this.stringValue = stringValue;
    }

    /**
     * 获取string值
     *
     * @return string值
     */
    public String stringValue() {
        return this.stringValue;
    }
}
