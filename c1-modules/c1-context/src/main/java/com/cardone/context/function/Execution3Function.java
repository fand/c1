package com.cardone.context.function;

/**
 * 方法
 *
 * @param <R> 输出泛型
 * @param <T> 输入泛型参数1
 * @param <U> 输入泛型参数2
 * @param <V> 输入泛型参数3
 * @author yaohaitao
 */
@FunctionalInterface
public interface Execution3Function<R, T, U, V> {
    /**
     * 执行
     *
     * @param t 输入泛型参数1
     * @param u 输入泛型参数2
     * @param v 输入泛型参数3
     * @return 输出泛型值
     */
    R execution(final T t, final U u, final V v);
}
