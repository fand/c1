package com.cardone.context.action;

/**
 * 动作
 *
 * @param <T> 输入泛型1
 * @param <U> 输入泛型2
 * @param <V> 输入泛型3
 * @author yaohaitao
 */
@FunctionalInterface
public interface Run3Action<T, U, V> {
    /**
     * 运行
     *
     * @param t 输入泛型参数1
     * @param u 输入泛型参数2
     * @param v 输入泛型参数3
     */
    void run(final T t, final U u, final V v);
}
