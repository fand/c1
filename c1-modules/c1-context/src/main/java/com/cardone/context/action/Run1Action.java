package com.cardone.context.action;

/**
 * 动作
 *
 * @param <T> 输入泛型1
 * @author yaohaitao
 */
@FunctionalInterface
public interface Run1Action<T> {
    /**
     * 运行
     *
     * @param t 输入泛型参数1
     */
    void run(final T t);
}
