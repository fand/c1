package com.cardone.validator;

/**
 * Created by Administrator on 2014/10/30.
 */
public interface ValidatorRule {
    boolean validate(Object obj);

    String getMessage();

    String getTypeCode();

    String getCode();
}
