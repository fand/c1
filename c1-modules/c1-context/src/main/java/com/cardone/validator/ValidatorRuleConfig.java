package com.cardone.validator;

import java.util.List;

/**
 * Created by Administrator on 2014/10/30.
 */
public class ValidatorRuleConfig {
    @lombok.Setter
    @lombok.Getter
    private String name;

    @lombok.Setter
    @lombok.Getter
    private Object value;

    @lombok.Setter
    @lombok.Getter
    private Object overrideValue;

    @lombok.Setter
    @lombok.Getter
    private List<ValidatorRule> validatorRuleList;

    @lombok.Setter
    @lombok.Getter
    private List<ValidatorRuleConfig> children;
}
