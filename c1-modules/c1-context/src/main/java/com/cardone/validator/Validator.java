package com.cardone.validator;

import java.util.*;

/**
 * Created by Administrator on 2014/10/30.
 */
public interface Validator {
    /**
     * 验证对象
     *
     * @param obj 对象
     * @param id  验证规则标识
     * @return 验证结果
     */
    Map<String, String> validate(Object obj, String id);


    /**
     * 验证对象
     *
     * @param obj 对象
     * @param id  验证规则标识
     * @return 验证结果
     */
    Map<String, String> validate(Object obj, String id, boolean oneError);

    /**
     * 验证对象
     *
     * @param obj                 对象
     * @param validatorRuleConfig 验证规则标识
     * @return 验证结果
     */
    Map<String, String> validate(Object obj, ValidatorRuleConfig validatorRuleConfig);

    /**
     * 验证对象
     *
     * @param obj                 对象
     * @param validatorRuleConfig 验证规则标识
     * @return 验证结果
     */
    Map<String, String> validate(Object obj, ValidatorRuleConfig validatorRuleConfig, boolean oneError);


    /**
     * 验证对象
     *
     * @param obj 对象
     * @param id  验证规则标识
     * @return 验证结果
     */
    void validateThrowException(Object obj, String id);


    /**
     * 验证对象
     *
     * @param obj                 对象
     * @param validatorRuleConfig 验证规则标识
     */
    void validateThrowException(Object obj, ValidatorRuleConfig validatorRuleConfig);


    /**
     * 验证对象
     *
     * @param obj 对象
     * @param id  验证规则标识
     * @return 验证结果
     */
    void validateThrowException(Object obj, String id, boolean oneError);


    /**
     * 验证对象
     *
     * @param obj                 对象
     * @param validatorRuleConfig 验证规则标识
     */
    void validateThrowException(Object obj, ValidatorRuleConfig validatorRuleConfig, boolean oneError);
}
