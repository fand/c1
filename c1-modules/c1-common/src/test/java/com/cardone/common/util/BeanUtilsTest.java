package com.cardone.common.util;

import com.google.common.collect.*;
import org.junit.*;

import java.util.*;

public class BeanUtilsTest {

    @Test
    public void testCopyPropertiesObjectObjectTableOfStringStringObject() {
        final Table<String, String, Object> parameters = HashBasedTable.create();

        parameters.put("name", "age", "age_value");

        parameters.put("formatDate", "createDate", "");

        parameters.put("formatDate", "createDate", "yyyy-MM-dd");

        parameters.put("formatDate", "createDate", "yyyy-MM-dd");

        final Map<String, Object> targetConfigMap = Maps.newHashMap();
        targetConfigMap.put("target", "createDateStr");
        targetConfigMap.put("pattern", "yyyy-MM-dd");

        parameters.put("formatDate", "createDate", targetConfigMap);

        parameters.put("dictionary", "sexId", "sexName");

        final Map<String, Object> targetConfig1Map = Maps.newHashMap();
        targetConfig1Map.put("target", "sexCode");
        targetConfig1Map.put("type", "code");

        parameters.put("dictionary", "sexId", targetConfig1Map);

        final Map<String, Object> source = Maps.newHashMap();

        source.put("age", 122);
        source.put("createDate", new Date());

        final Map<String, Object> target = Maps.newHashMap();

//    BeanUtils.copyProperties(source, target, parameters);

        System.out.println(target);
    }

}
