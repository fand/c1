package com.cardone.common.beans.propertyeditors;

import com.cardone.common.util.*;
import lombok.extern.slf4j.*;
import org.apache.commons.lang3.StringUtils;

import java.text.*;

/**
 * 自定义时间绑定
 *
 * @author fand.cardone
 */
@Slf4j
public class CardOneDateEditor extends org.springframework.beans.propertyeditors.CustomDateEditor {
    public CardOneDateEditor(final DateFormat dateFormat, final boolean allowEmpty) {
        super(dateFormat, allowEmpty);
    }

    public CardOneDateEditor(final DateFormat dateFormat, final boolean allowEmpty, final int exactDateLength) {
        super(dateFormat, allowEmpty, exactDateLength);
    }

    @Override
    public void setAsText(final String text) throws IllegalArgumentException {
        if (StringUtils.isBlank(text)) {
            return;
        }

        this.setValue(DateUtils.parseDate(text));
    }
}
