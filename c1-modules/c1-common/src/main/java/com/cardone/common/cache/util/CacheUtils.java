package com.cardone.common.cache.util;

import com.cardone.common.cache.*;
import com.cardone.context.*;
import com.cardone.context.action.*;
import com.cardone.context.function.*;
import lombok.*;
import lombok.extern.slf4j.*;
import org.springframework.cache.*;
import org.springframework.cache.Cache.*;
import org.springframework.util.*;

import java.util.*;

/**
 * 缓存工具类
 *
 * @author yaohaitao
 */
@Slf4j
public class CacheUtils {
    /**
     * 动作集合
     */
    @Setter
    private static List<Run0Action> flushRunActionList;

    public static void flush() {
        if (CollectionUtils.isEmpty(CacheUtils.flushRunActionList)) {
            return;
        }

        for (final Run0Action flushRunAction : CacheUtils.flushRunActionList) {
            try {
                flushRunAction.run();
            } catch (final Exception e) {
                CacheUtils.log.error(e.getMessage(), e);
            }
        }
    }

    /**
     * 获取缓存
     *
     * @param name 缓存名称
     * @return 缓存
     */
    public static Cache get(final String name) {
        return get(Caches.defaultCacheManagerName.stringValue(), name);
    }

    /**
     * 获取缓存
     *
     * @param cacheManagerName 缓存管理名称
     * @param name             缓存名称
     * @return 缓存
     */
    public static Cache get(final String cacheManagerName, final String name) {
        CacheManager cacheManager = ContextHolder.getBean(CacheManager.class, cacheManagerName);

        if (cacheManager == null) {
            return null;
        }

        return cacheManager.getCache(name);
    }

    /**
     * 移除
     *
     * @param name 缓存名称
     * @param key  键
     */
    public static void evict(final String name, final Object key) {
        evict(Caches.defaultCacheManagerName.stringValue(), name, key);
    }

    /**
     * 移除
     *
     * @param cacheManagerName 缓存管理名称
     * @param name             缓存名称
     * @param key              键
     */
    public static void evict(final String cacheManagerName, final String name, final Object key) {
        Cache cache = get(cacheManagerName, name);

        if (cache == null) {
            return;
        }

        Object newKey = getNewKey(key);

        try {
            cache.evict(newKey);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private static Object getNewKey(Object key) {
        return (key instanceof String) ? key : com.cardone.common.util.MapperUtils.toJson(key);
    }

    /**
     * 获取缓存值
     *
     * @param requiredType 返回类型
     * @param name         缓存名称
     * @param key          缓存键名
     * @return 缓存值
     */
    public static <T> T getValue(final Class<T> requiredType, final String name, final Object key) {
        return CacheUtils.getValue(requiredType, Caches.defaultCacheManagerName.stringValue(), name, key);
    }

    /**
     * 获取缓存值
     *
     * @param requiredType     返回类型
     * @param cacheManagerName 缓存管理名称
     * @param name             缓存名称
     * @param key              缓存键名
     * @return 缓存值
     */
    @SuppressWarnings({"unchecked"})
    public static <T> T getValue(final Class<T> requiredType, final String cacheManagerName, final String name, final Object key) {
        Cache cache = get(cacheManagerName, name);

        if (cache == null) {
            return null;
        }

        Object newKey = getNewKey(key);

        return cache.get(newKey, requiredType);
    }

    /**
     * 获取缓存值
     *
     * @param name     缓存名称
     * @param key      缓存键名
     * @param function 方法
     * @return 缓存值
     */
    public static <T> T getValue(final String name, final Object key, final Execution0Function<T> function) {
        return CacheUtils.getValue(Caches.defaultCacheManagerName.stringValue(), name, key, function);
    }

    /**
     * 获取缓存值
     *
     * @param cacheManagerName 缓存管理名称
     * @param name             缓存名称
     * @param key              缓存键名
     * @param function         方法
     * @return 缓存值
     */
    @SuppressWarnings("unchecked")
    public static <T> T getValue(final String cacheManagerName, final String name, final Object key, final Execution0Function<T> function) {
        final Cache cache = CacheUtils.get(cacheManagerName, name);

        if (cache == null) {
            return null;
        }

        Object newKey = getNewKey(key);

        final ValueWrapper valueWrapper = cache.get(newKey);

        if (valueWrapper == null || valueWrapper.get() == null) {
            T value = function.execution();

            if (value != null) {
                cache.put(newKey, value);
            }

            return value;
        }

        return (T) valueWrapper.get();
    }

    /**
     * 获取缓存值
     *
     * @param name     缓存名称
     * @param key      缓存键名
     * @param function 方法
     * @return 缓存值
     */
    public static <T> T getValue(final String name, final Object key, final Execution1Function<T, Object> function) {
        return CacheUtils.getValue(Caches.defaultCacheManagerName.stringValue(), name, key, function);
    }

    /**
     * 获取缓存值
     *
     * @param cacheManagerName 缓存管理名称
     * @param name             缓存名称
     * @param key              缓存键名
     * @param function         方法
     * @return 缓存值
     */
    @SuppressWarnings("unchecked")
    public static <T> T getValue(final String cacheManagerName, final String name, final Object key, final Execution1Function<T, Object> function) {
        final Cache cache = CacheUtils.get(cacheManagerName, name);

        if (cache == null) {
            return null;
        }

        Object newKey = getNewKey(key);

        final ValueWrapper valueWrapper = cache.get(newKey);

        if (valueWrapper == null || valueWrapper.get() == null) {
            T value = function.execution(newKey);

            if (value != null) {
                cache.put(newKey, value);
            }

            return value;
        }

        return (T) valueWrapper.get();
    }
}
