package com.cardone.common.dto;

/**
 * 分页实体dto
 *
 * @author yaohaitao
 */
public interface PaginationEntityDto {
    /**
     * 获取：标识集合
     *
     * @return 标识集合
     */
    int getPaginationSize();

    /**
     * 获取：分页号
     *
     * @return 分页号
     */
    int getPaginationNo();
}