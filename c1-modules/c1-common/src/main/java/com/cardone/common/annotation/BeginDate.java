package com.cardone.common.annotation;

import java.lang.annotation.*;

import static java.lang.annotation.ElementType.*;

/**
 * 开始时间
 *
 * @author yaohaitao
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(value = {FIELD, METHOD, ANNOTATION_TYPE})
public @interface BeginDate {
}
