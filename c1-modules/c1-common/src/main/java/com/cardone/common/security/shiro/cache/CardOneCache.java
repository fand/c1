package com.cardone.common.security.shiro.cache;

import com.cardone.common.cache.util.CacheUtils;
import org.apache.shiro.cache.CacheException;

import java.util.Collection;
import java.util.Set;

/**
 * Created by Administrator on 2014/11/8.
 */
public class CardOneCache<K, V> implements org.apache.shiro.cache.Cache<K, V> {
    private String name;

    private Class<V> vType;

    public CardOneCache(String name, Class<V> vType) {
        this.name = name;

        this.vType = vType;
    }

    @Override
    public V get(K key) throws CacheException {
        return CacheUtils.getValue(vType, this.name, key);
    }

    @Override
    public V put(K key, V value) throws CacheException {
        CacheUtils.get(this.name).put(key, value);

        return value;
    }

    @Override
    public V remove(K key) throws CacheException {
        return null;
    }

    @Override
    public void clear() throws CacheException {
        CacheUtils.get(this.name).clear();
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public Set<K> keys() {
        return null;
    }

    @Override
    public Collection<V> values() {
//        CacheUtils.get(this.name).
        return null;
    }
}
