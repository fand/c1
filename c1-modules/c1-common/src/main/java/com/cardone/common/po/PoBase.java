package com.cardone.common.po;

import com.cardone.common.annotation.*;
import com.cardone.common.dto.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

import java.util.*;

/**
 * 用户
 *
 * @author yaohaitao
 */
@lombok.ToString(callSuper = true)
public class PoBase implements java.io.Serializable, PaginationEntityDto {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -1927823240614686667L;

    /**
     * 扩展属性
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private Map<String, Object> attrs;

    /**
     * 开始时间
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    @BeginDate
    private Date beginDate;

    /**
     * 代码
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String code;

    /**
     * 创建人代码
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String createdByCode;

    /**
     * 创建人标识
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String createdById;

    /**
     * 创建人名称
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String createdByName;

    /**
     * 创建时间
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private Date createdDate;

    /**
     * 结束时间
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    @EndDate
    private Date endDate;

    /**
     * 标识集合
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String ids;

    /**
     * 最后修改人代码
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String lastModifiedByCode;

    /**
     * 最后修改人标识
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String lastModifiedById;

    /**
     * 最后修改人名称
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String lastModifiedByName;

    /**
     * 最后修改时间
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private Date lastModifiedDate;

    /**
     * 登录用户代码(数据权限控制时使用)
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String longinUserCode;

    /**
     * 登录用户标识(数据权限控制时使用)
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String longinUserId;

    /**
     * 登录用户名称(数据权限控制时使用)
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String longinUserName;

    /**
     * 名称
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String name;

    /**
     * 排序数
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private Double orderNum;

    /**
     * 分页号
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    @Transient
    private int paginationNo;

    /**
     * 分页大小
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    @Transient
    private int paginationSize;

    /**
     * 状态代码
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String stateCode;

    /**
     * 状态标识
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String stateId;

    /**
     * 状态标识集合
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String stateIds;

    /**
     * 状态代码集合
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String stateCodes;

    /**
     * 状态名称
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String stateName;

    /**
     * 验证类型代码
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String validatorTypeCodes;

    /**
     * 版本
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private Integer versionInt;

    /**
     * 数据状态代码
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String dataStateCode;

    /**
     * 数据状态标识
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String dataStateId;

    /**
     * 数据状态标识集合
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String dataStateIds;

    /**
     * 数据状态代码集合
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String dataStateCodes;

    /**
     * 数据状态名称
     */
    @Getter
    @Setter
    @Accessors(chain = true)
    private String dataStateName;
}