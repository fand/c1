package com.cardone.common.util;

import com.cardone.context.*;
import lombok.extern.slf4j.*;
import org.apache.commons.collections.*;
import org.apache.commons.io.*;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;

/**
 * web工具类
 *
 * @author yaohaitao
 */
@Slf4j
public class WebUtils {
    private WebUtils() {
    }

    /**
     * 获取bean关于json
     *
     * @param mappedClass 映射类型
     * @return bean @
     */
    public static <T> T getBeanForJson(final Class<T> mappedClass) {
        final HttpServletRequest request = ContextHolder.getRequest();

        return WebUtils.getBeanForJson(mappedClass, request);
    }

    /**
     * 获取bean关于json
     *
     * @param mappedClass 映射类型
     * @param request
     * @return bean @
     */
    public static <T> T getBeanForJson(final Class<T> mappedClass, final HttpServletRequest request) {
        final String jsonString = WebUtils.getJsonString(request);

        return MapperUtils.getBeanForJson(mappedClass, jsonString);
    }

    public static boolean isReadJson(final ServletRequest request){
        return org.apache.commons.lang3.StringUtils.startsWith(request.getContentType(), com.google.common.net.MediaType.ANY_APPLICATION_TYPE.type());
    }

    /**
     * 获取json字符串
     *
     * @param request
     * @return json字符串
     */
    public static String getJsonString(final ServletRequest request) {
        boolean readJson = isReadJson(request);

        if (readJson) {
            String jsonString = null;

            try (java.io.InputStream is = request.getInputStream()) {
                jsonString = IOUtils.toString(is, request.getCharacterEncoding());
            } catch (final IOException e) {
                WebUtils.log.error(e.getMessage(), e);
            }

            if (StringUtils.isNotBlank(jsonString)) {
                return jsonString;
            }
        }

        final Map<String, Object> parameterMap = org.springframework.web.util.WebUtils.getParametersStartingWith(request, null);

        return MapperUtils.toJson(parameterMap);
    }

    public static Map<String, Object> getParameterMap(final ServletRequest request) {
        final Map<String, Object> parameterMap = org.springframework.web.util.WebUtils.getParametersStartingWith(request, null);

        boolean readJson = isReadJson(request);

        if (!readJson) {
            return parameterMap;
        }

        String jsonString = null;

        try (java.io.InputStream is = request.getInputStream()) {
            jsonString = IOUtils.toString(is, request.getCharacterEncoding());
        } catch (final IOException e) {
            WebUtils.log.error(e.getMessage(), e);
        }

        if (StringUtils.isBlank(jsonString)) {
            return parameterMap;
        }

        Map<String, Object> jsonMap = MapperUtils.getMapForJson(jsonString);

        if (MapUtils.isNotEmpty(jsonMap)) {
            parameterMap.putAll(jsonMap);
        }

        return parameterMap;
    }

    /**
     * 获取bean关于json
     *
     * @param mappedClass 映射类型
     * @param paramName   参数名称
     * @return bean @
     */
    public static <T> T getBeanForJson(final Class<T> mappedClass, final String paramName) {
        final HttpServletRequest request = ContextHolder.getRequest();

        return WebUtils.getBeanForJson(mappedClass, paramName, request);
    }

    /**
     * 获取bean关于json
     *
     * @param mappedClass 映射类型
     * @param paramName   参数名称
     * @param request
     * @return bean @
     */
    public static <T> T getBeanForJson(final Class<T> mappedClass, final String paramName, final HttpServletRequest request) {
        final String jsonString = WebUtils.getJsonString(request);

        return MapperUtils.getBeanForJson(mappedClass, paramName, jsonString);
    }

    /**
     * 获取json字符串
     *
     * @return json字符串
     */
    public static String getJsonString() {
        final HttpServletRequest request = ContextHolder.getRequest();

        return WebUtils.getJsonString(request);
    }

    /**
     * 获取键值对
     *
     * @return 键值对 @
     */
    public static Map<String, Object> getMapForJson() {
        final HttpServletRequest request = ContextHolder.getRequest();

        return WebUtils.getMapForJson(request);
    }

    /**
     * 获取键值对
     *
     * @param request
     * @return 键值对 @
     */
    public static Map<String, Object> getMapForJson(final HttpServletRequest request) {
        final String jsonString = WebUtils.getJsonString(request);

        return MapperUtils.getMapForJson(jsonString);
    }

    /**
     * 获取对象
     *
     * @param paramName 参数名称
     * @return 对象 @
     */
    public static Object getObjectForJson(final String paramName) {
        final HttpServletRequest request = ContextHolder.getRequest();

        return WebUtils.getObjectForJson(paramName, request);
    }

    /**
     * 获取对象
     *
     * @param paramName 参数名称
     * @param request
     * @return 对象 @
     */
    public static Object getObjectForJson(final String paramName, final HttpServletRequest request) {
        final String jsonString = WebUtils.getJsonString(request);

        return MapperUtils.getObjectForJson(paramName, jsonString);
    }
}
