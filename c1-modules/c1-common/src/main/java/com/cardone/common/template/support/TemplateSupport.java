package com.cardone.common.template.support;

/**
 * 模板支持
 *
 * @author yaohaitao
 */
public interface TemplateSupport {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.common.template.support.TemplateSupport";

    /**
     * 解析模板
     *
     * @param templateString 模板字符
     * @param model          数据
     * @return 解析后字符
     */
    String processString(final String templateString, final Object model);

    /**
     * 默认解析模板
     *
     * @param filePath 文件路径
     * @param isFormat 是否格式化文件路径
     * @param model    数据
     * @return 解析后字符
     */
    String process(final String filePath, final boolean isFormat, final Object model);

    /**
     * 解析模板
     *
     * @param filePath 文件路径
     * @param model    数据
     * @return 解析后字符
     */
    String process(final String filePath, final Object model);
}
