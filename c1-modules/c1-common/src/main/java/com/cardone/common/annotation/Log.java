package com.cardone.common.annotation;

import org.apache.commons.lang3.*;

import java.lang.annotation.*;

/**
 * 日志标注
 *
 * @author yaohaitao
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Log {
    /**
     * 方法参数上，取值位置
     *
     * @return 方法参数上，取值位置
     */
    int index() default 0;

    /**
     * 消息代码
     *
     * @return 消息代码
     */
    String messageCode() default StringUtils.EMPTY;

    /**
     * 消息名称
     *
     * @return 消息名称
     */
    String messageName() default StringUtils.EMPTY;

    /**
     * 消息类别代码
     *
     * @return 消息类别代码
     */
    String messageTypeCode() default StringUtils.EMPTY;

    /**
     * 消息类别名称
     *
     * @return 消息类别名称
     */
    String messageTypeName() default StringUtils.EMPTY;

    /**
     * 类别代码
     *
     * @return 类别代码
     */
    String[] typeCodes() default StringUtils.EMPTY;
}
