package com.cardone.common.util.support;

import com.cardone.common.util.*;
import com.google.gson.*;

import java.lang.reflect.*;
import java.util.*;

/**
 * 时间解析
 * Created by yaohaitao on 2014/8/26.
 */
public class DateSerializerSupport implements JsonDeserializer<Date> {
    @Override
    public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        return DateUtils.parseDate(json.getAsString());
    }
}
