package com.cardone.common;

/**
 * 常量
 *
 * @author yaohaitao
 */
public enum Characters {
    /**
     * 逗号
     */
    comma(","),

    /**
     * 分隔符
     */
    compart(":"),

    /**
     * x
     */
    x("x"),

    /**
     * 点
     */
    dot("."),

    /**
     * 错误
     */
    error,

    /**
     * 字符串值
     */
    nullStringValue("null"),

    /**
     * http分隔符
     */
    httpSeparator("/"),

    /**
     * 米, 公尺, 计, 表, 仪表
     */
    meter("*"),

    /**
     * 其它
     */
    other,

    /**
     * 百分号
     */
    percent("%"),

    /**
     * 下划线
     */
    underline("_"),

    /**
     * 验证
     */
    validator,

    /**
     * 时间格式
     */
    yyyyMMdd;

    /**
     * string值
     */
    private String stringValue;

    /**
     * 构建
     */
    private Characters() {
        this.stringValue = this.name();
    }

    /**
     * 构建
     *
     * @param stringValue string值
     */
    private Characters(final String stringValue) {
        this.stringValue = stringValue;
    }

    /**
     * 获取string值
     *
     * @return string值
     */
    public String stringValue() {
        return this.stringValue;
    }
}
