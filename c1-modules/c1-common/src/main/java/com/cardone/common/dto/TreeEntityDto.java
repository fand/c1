package com.cardone.common.dto;

/**
 * 树实体
 *
 * @author yaohaitao
 */
public interface TreeEntityDto {
    /**
     * 获取：标识
     *
     * @return 标识
     */
    String getId();

    /**
     * 获取： 父标识
     *
     * @return 父标识
     */
    String getParentId();

    /**
     * 获取：树类别代码
     *
     * @return 树类别代码
     */
    String getTreeTypeCode();

    /**
     * 获取：树代码
     *
     * @return 树代码
     */
    String getTreeCode();
}