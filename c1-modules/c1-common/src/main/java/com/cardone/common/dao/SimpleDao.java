package com.cardone.common.dao;

import com.cardone.common.dto.*;
import com.cardone.common.po.*;

import java.util.*;

/**
 * 简易 dao
 *
 * @author yaohaitao
 */
public interface SimpleDao<D extends PoBase> {
    /**
     * 保存:
     *
     * @param mappedClass 返回类型
     * @param save        对象
     * @return 对象
     */
    <P> P saveByIdOrCode(Class<P> mappedClass, D save, String... selectNames);

    /**
     * 保存:
     *
     * @param save 对象
     * @return 对象
     */
    Map<String, Object> saveByIdOrCode(D save, String... selectNames);

    /**
     * 保存:
     *
     * @param mappedClass 返回类型
     * @param saveMap     对象
     * @return 对象
     */
    <P> P saveByIdOrCode(Class<P> mappedClass, Map<String, Object> saveMap, String... selectNames);

    /**
     * 保存:
     *
     * @param saveMap 对象
     * @return 对象
     */
    Map<String, Object> saveByIdOrCode(Map<String, Object> saveMap, String... selectNames);

    /**
     * 插入
     *
     * @param insert 对象
     * @return 影响行数
     */
    int insertByCode(D insert);

    /**
     * 插入
     *
     * @param insertMap 对象
     * @return 影响行数
     */
    int insertByCode(Map<String, Object> insertMap);

    /**
     * 插入
     *
     * @param insertList 对象集合
     * @return 影响行数
     */
    int[] insertByCode(List<D> insertList);

    /**
     * 插入
     *
     * @param insert 对象
     * @return 影响行数
     */
    int insertByNotExistsCode(D insert);

    /**
     * 插入
     *
     * @param insertMap 对象
     * @return 影响行数
     */
    int insertByNotExistsCode(Map<String, Object> insertMap);

    /**
     * 插入
     *
     * @param insertMap     对象
     * @param whereAndEqMap 对象
     * @return 影响行数
     */
    int insertByNotExistsCode(Map<String, Object> insertMap, Map<String, Object> whereAndEqMap);

    /**
     * 插入
     *
     * @param insertList 对象集合
     * @return 影响行数
     */
    int[] insertByNotExistsCode(List<D> insertList);

    /**
     * 删除
     *
     * @param delete 对象
     * @return 影响行数
     */
    int deleteByIds(D delete);

    /**
     * 更新
     *
     * @param ids 标识集合
     * @return 影响行数
     */
    int updateByIds(String ids);

    /**
     * 更新
     *
     * @param delete 对象
     * @return 影响行数
     */
    int updateByIds(D delete);

    /**
     * 删除
     *
     * @param ids 标识集合
     * @return 影响行数
     */
    int deleteByIds(String ids);

    /**
     * 删除
     *
     * @param delete               对象
     * @param whereAndEqProperties 等于那些属性
     * @return 影响行数
     */
    int deleteByCode(D delete, String... whereAndEqProperties);

    /**
     * 删除
     *
     * @param deleteList           对象
     * @param whereAndEqProperties 等于那些属性
     * @return 影响行数
     */
    int[] deleteByCode(List<D> deleteList, String... whereAndEqProperties);

    /**
     * 删除
     *
     * @param whereAndEqMap 等于那些属性
     * @return 影响行数
     */
    int deleteByCode(Map<String, Object> whereAndEqMap);

    /**
     * 删除
     *
     * @param whereAndEqMapList 等于那些属性
     * @return 影响行数
     */
    int[] deleteByCode(List<Map<String, Object>> whereAndEqMapList);

    /**
     * 更新
     *
     * @param update      对象
     * @param updateNames 更新名称集合
     * @return 影响行数
     */
    int updateByCode(D update, String... updateNames);

    /**
     * 更新
     *
     * @param update 对象
     * @return 影响行数
     */
    int updateByCode(D update);

    /**
     * 更新
     *
     * @param updateMap   对象
     * @param updateNames 更新名称集合
     * @return 影响行数
     */
    int updateByCode(Map<String, Object> updateMap, String... updateNames);

    /**
     * 更新
     *
     * @param updateMap 对象
     * @return 影响行数
     */
    int updateByCode(Map<String, Object> updateMap);

    /**
     * 更新
     *
     * @param updateList  对象
     * @param updateNames 更新名称集合
     * @return 影响行数
     */
    int[] updateByCode(List<D> updateList, String... updateNames);

    /**
     * 更新
     *
     * @param updateList           对象
     * @param whereAndEqProperties 相等名称集合
     * @param updateNames          更新名称集合
     * @return 影响行数
     */
    int[] updateByCode(List<D> updateList, String[] whereAndEqProperties, String... updateNames);

    /**
     * 更新
     *
     * @param whereAndEqProperties 等于名称集合
     * @param updateNames          更新名称集合
     * @return 影响行数
     */
    int updateByCode(D update, String[] whereAndEqProperties, String... updateNames);

    /**
     * 更新
     *
     * @param updateMap   等于名称集合
     * @param updateNames 更新名称集合
     * @return 影响行数
     */
    int updateByCode(Map<String, Object> updateMap, String[] whereAndEqProperties, String... updateNames);

    /**
     * 更新
     *
     * @param whereAndEqMap 等于那些属性
     * @param updateMap     更新那些属性
     * @return 影响行数
     */
    int updateByCode(Map<String, Object> whereAndEqMap, Map<String, Object> updateMap);

    /**
     * 查询
     *
     * @param read 对象
     * @return 返回数据
     */
    Integer readByCodeNqIdForCount(D read);

    /**
     * 查询
     *
     * @return 返回数据
     */
    <R> R readByCode(final Class<R> requiredType, final String objectId, D read, String... whereAndEqProperties);

    /**
     * 查询
     *
     * @return 返回数据
     */
    <R> R readByCode(final Class<R> requiredType, final String objectId, Map<String, Object> whereAndEqMap);

    /**
     * 查询
     *
     * @param readMap 对象
     * @return 返回数据
     */
    Integer readByCodeNqIdForCount(Map<String, Object> readMap);

    /**
     * 查询
     *
     * @param read                 对象
     * @param whereAndEqProperties 对象
     * @return 返回数据
     */
    Integer readByLikeCode(D read, String... whereAndEqProperties);

    /**
     * 查询
     *
     * @param readMap 对象
     * @return 返回数据
     */
    Integer readByLikeCode(Map<String, Object> readMap);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param find        对象
     * @return 返回对象
     */
    <P> P findById(Class<P> mappedClass, D find, String... selectNames);

    /**
     * 查询
     *
     * @param find 对象
     * @return 返回对象
     */
    Map<String, Object> findById(D find, String... selectNames);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param findMap     model
     * @return 返回对象
     */
    <P> P findById(Class<P> mappedClass, Map<String, Object> findMap, String... selectNames);

    /**
     * 查询
     *
     * @param findMap model
     * @return 返回对象
     */
    Map<String, Object> findById(Map<String, Object> findMap, String... selectNames);

    /**
     * 查询
     *
     * @param mappedClass          返回类型
     * @param find                 对象
     * @param whereAndEqProperties 等于那些属性
     * @return 返回对象
     */
    <P> P findByCode(Class<P> mappedClass, D find, String[] whereAndEqProperties, String... selectNames);

    /**
     * 查询
     *
     * @param mappedClass   返回类型
     * @param whereAndEqMap 等于那些属性
     * @return 返回对象
     */
    <P> P findByCode(Class<P> mappedClass, Map<String, Object> whereAndEqMap, String... selectNames);

    /**
     * 查询
     *
     * @param whereAndEqMap 等于那些属性
     * @return 返回对象
     */
    Map<String, Object> findByCode(Map<String, Object> whereAndEqMap, String... selectNames);

    /**
     * 查询
     *
     * @param mappedClass          返回类型
     * @param find                 对象
     * @param whereAndEqProperties 等于那些属性
     * @return 返回对象
     */
    <P> P findByCode(Class<P> mappedClass, D find, String... whereAndEqProperties);

    /**
     * 查询
     *
     * @param find                 对象
     * @param whereAndEqProperties 等于那些属性
     * @return 返回对象
     */
    Map<String, Object> findByCode(D find, String[] whereAndEqProperties, String... selectNames);

    /**
     * 查询
     *
     * @param find                 对象
     * @param whereAndEqProperties 等于那些属性
     * @return 返回对象
     */
    Map<String, Object> findByCode(D find, String... whereAndEqProperties);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param find        对象
     * @return 返回对象
     */
    <P> P findByCodeOrId(Class<P> mappedClass, D find, String... selectNames);

    /**
     * 查询
     *
     * @param find 对象
     * @return 返回对象
     */
    Map<String, Object> findByCodeOrId(D find, String... selectNames);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param findMap     对象
     * @return 返回对象
     */
    <P> P findByCodeOrId(Class<P> mappedClass, Map<String, Object> findMap, String... selectNames);

    /**
     * 查询
     *
     * @param findMap 对象
     * @return 返回对象
     */
    Map<String, Object> findByCodeOrId(Map<String, Object> findMap, String... selectNames);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param findList    对象
     * @return 对象集合
     */
    <P> List<P> findListByLikeCode(Class<P> mappedClass, D findList, String[] whereAndEqProperties, String... selectNames);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param findList    对象
     * @return 对象集合
     */
    <P> List<P> findListByLikeCode(Class<P> mappedClass, D findList, String... whereAndEqProperties);

    /**
     * 查询
     *
     * @param findList 对象
     * @return 对象集合
     */
    List<Map<String, Object>> findListByLikeCode(D findList, String[] whereAndEqProperties, String... selectNames);

    /**
     * 查询
     *
     * @param findList 对象
     * @return 对象集合
     */
    List<Map<String, Object>> findListByLikeCode(D findList, String... whereAndEqProperties);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param findListMap 等于那些属性
     * @return 对象集合
     */
    <P> List<P> findListByLikeCode(Class<P> mappedClass, Map<String, Object> findListMap, String... selectNames);

    /**
     * 查询
     *
     * @param findListMap 等于那些属性
     * @return 对象集合
     */
    List<Map<String, Object>> findListByLikeCode(Map<String, Object> findListMap, String... selectNames);

    /**
     * 查询
     *
     * @param mappedClass          返回类型
     * @param findList             对象
     * @param whereAndEqProperties 等于那些属性
     * @return 对象集合
     */
    <P> List<P> findListByCode(Class<P> mappedClass, D findList, String[] whereAndEqProperties, String... selectNames);

    /**
     * 查询
     *
     * @param mappedClass          返回类型
     * @param findList             对象
     * @param whereAndEqProperties 等于那些属性
     * @return 对象集合
     */
    <P> List<P> findListByCode(Class<P> mappedClass, D findList, String... whereAndEqProperties);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param findListMap 等于那些属性
     * @return 对象集合
     */
    <P> List<P> findListByCode(Class<P> mappedClass, Map<String, Object> findListMap, String... selectNames);

    /**
     * 查询
     *
     * @param findList             对象
     * @param whereAndEqProperties 等于那些属性
     * @return 对象集合
     */
    List<Map<String, Object>> findListByCode(D findList, String[] whereAndEqProperties, String... selectNames);

    /**
     * 查询
     *
     * @param findList             对象
     * @param whereAndEqProperties 等于那些属性
     * @return 对象集合
     */
    List<Map<String, Object>> findListByCode(D findList, String... whereAndEqProperties);

    /**
     * 查询
     *
     * @param findListMap 等于那些属性
     * @return 对象集合
     */
    List<Map<String, Object>> findListByCode(Map<String, Object> findListMap, String... selectNames);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @return 对象集合
     */
    <P> List<P> findList(Class<P> mappedClass, String... selectNames);


    /**
     * 查询
     *
     * @return 对象集合
     */
    List<Map<String, Object>> findList(String... selectNames);

    /**
     * 分页
     *
     * @param mappedClass 返回类型
     * @param pagination  对象
     * @return 分页对象
     */
    <P> PaginationDto<P> paginationByLikeCode(Class<P> mappedClass, D pagination);

    /**
     * 分页
     *
     * @param pagination 对象
     * @return 分页对象
     */
    PaginationDto<Map<String, Object>> paginationByLikeCode(D pagination);

    /**
     * 分页
     *
     * @param mappedClass   返回类型
     * @param paginationMap 对象
     * @return 分页对象
     */
    <P> PaginationDto<P> paginationByLikeCode(Class<P> mappedClass, Map<String, Object> paginationMap);

    /**
     * 分页
     *
     * @param paginationMap 对象
     * @return 分页对象
     */
    PaginationDto<Map<String, Object>> paginationByLikeCode(Map<String, Object> paginationMap);
}