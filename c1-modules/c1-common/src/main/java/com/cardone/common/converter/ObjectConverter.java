package com.cardone.common.converter;

/**
 * 对象转换
 * Created by Administrator on 2014/9/26.
 */
public class ObjectConverter implements org.dozer.ConfigurableCustomConverter {
    private String parameter;

    @Override
    public Object convert(Object existingDestinationFieldValue, Object sourceFieldValue, Class<?> destinationClass, Class<?> sourceClass) {
        if (sourceFieldValue == null) {
            return sourceFieldValue;
        }

        if (org.apache.commons.lang3.StringUtils.isBlank(parameter)) {
            return sourceFieldValue;
        }

        com.cardone.context.function.Execution4Function execution4Function = com.cardone.context.ContextHolder.getBean(com.cardone.context.function.Execution4Function.class, parameter);

        if (execution4Function == null) {
            return sourceFieldValue;
        }

        return execution4Function.execution(existingDestinationFieldValue, sourceFieldValue, destinationClass, sourceClass);
    }

    @Override
    public void setParameter(String parameter) {
        this.parameter = parameter;
    }
}