package com.cardone.common.web.servlet.handler;

/**
 * Created by Administrator on 2014/10/27.
 */
public class AutoSimpleUrlHandlerMapping extends org.springframework.web.servlet.handler.SimpleUrlHandlerMapping {
    @Override
    public void initApplicationContext() throws org.springframework.beans.BeansException {
        super.initApplicationContext();

        java.util.Map<String, org.springframework.remoting.support.RemoteExporter> remoteExporterMap = com.cardone.context.ContextHolder.getApplicationContext().getBeansOfType(org.springframework.remoting.support.RemoteExporter.class);

        java.util.Map<String, Object> objectMap = com.google.common.collect.Maps.newHashMap();

        objectMap.putAll(remoteExporterMap);

        super.registerHandlers(objectMap);
    }
}
