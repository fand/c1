package com.cardone.common.security.shiro.cache;

import org.apache.shiro.cache.*;

/**
 * Created by Administrator on 2014/11/8.
 */
public class CardOneCacheManager extends AbstractCacheManager {
    @Override
    protected Cache createCache(String name) throws CacheException {
        return new CardOneCache<String, Object>(name, Object.class);
    }
}
