package com.cardone.common.cache;

import com.cardone.common.router.support.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.cache.*;

import java.util.*;
import java.util.concurrent.*;

/**
 * 路由缓存
 *
 * @author yaohaitao
 */
@Accessors(chain = true)
public class RouterCacheManager implements CacheManager {
    /**
     * 缓存集合
     */
    private final ConcurrentMap<String, Cache> caches = new ConcurrentHashMap<>();
    /**
     * 缓存名集合
     */
    private final Collection<String> names = Collections.unmodifiableSet(this.caches.keySet());
    /**
     * 缓存管理集合
     */
    @Setter
    private Map<String, CacheManager> cacheManagers;
    /**
     * 默认缓存管理
     */
    @Setter
    private CacheManager defaultCacheManager;
    /**
     * 路由键名
     */
    @Setter
    private RouterKeySupport routerKeySupport;

    @Override
    public Cache getCache(final String name) {
        Cache cache = this.caches.get(name);

        if (cache != null) {
            return cache;
        }

        final String cacheManagerKey = this.routerKeySupport.readKey(name);

        cache = org.apache.commons.lang3.ObjectUtils.defaultIfNull(this.cacheManagers.get(cacheManagerKey), this.defaultCacheManager).getCache(name);

        this.caches.put(name, cache);

        return cache;
    }

    @Override
    public Collection<String> getCacheNames() {
        return this.names;
    }
}