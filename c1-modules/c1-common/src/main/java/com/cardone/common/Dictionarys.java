package com.cardone.common;

/**
 * 字典数据
 *
 * @author yaohaitao
 */
public enum Dictionarys {
    /**
     * 验证:代码
     */
    codeNotUnique(Characters.validator.name(), "code.not.unique", "不能重复"),

    /**
     * 验证:代码
     */
    codeRequired(Characters.validator.name(), "code.required", "代码不能为空"),

    /**
     * 验证:标识
     */
    idNotUnique(Characters.validator.name(), "id.not.unique", "不能重复"),

    /**
     * 验证:名称
     */
    nameRequired(Characters.validator.name(), "name.required", "名称不能为空"),

    /**
     * 验证:类别标识
     */
    typeIdRequired(Characters.validator.name(), "typeId.required", "类别标识不能为空");

    /**
     * 间隔
     */
    private static final String SPACE = ".";

    /**
     * 代码
     */
    private String code;

    /**
     * 默认消息
     */
    private String defaultMessage;

    /**
     * 类别代码
     */
    private String typeCode;

    /**
     * 类别代码与代码
     */
    private String typeCodeAndCode;

    /**
     * 创建
     *
     * @param code           代码
     * @param defaultMessage 默认消息
     */
    private Dictionarys(final String typeCode, final String defaultMessage) {
        this.typeCode = typeCode;

        this.code = this.name();

        this.typeCodeAndCode = typeCode + Dictionarys.SPACE + this.name();

        this.defaultMessage = defaultMessage;
    }

    /**
     * 创建
     *
     * @param typeCode       类别代码
     * @param code           代码
     * @param defaultMessage 默认消息
     */
    private Dictionarys(final String typeCode, final String code, final String defaultMessage) {
        this.typeCode = typeCode;

        this.code = code;

        this.typeCodeAndCode = typeCode + Dictionarys.SPACE + this.name();

        this.defaultMessage = defaultMessage;
    }

    /**
     * 获取
     *
     * @return 代码
     */
    public String code() {
        return this.code;
    }

    /**
     * 获取
     *
     * @return 默认消息
     */
    public String defaultMessage() {
        return this.defaultMessage;
    }

    /**
     * 获取
     *
     * @return 类别代码
     */
    public String typeCode() {
        return this.typeCode;
    }

    /**
     * 获取
     *
     * @return 类别代码与代码
     */
    public String typeCodeAndCode() {
        return this.typeCodeAndCode;
    }
}