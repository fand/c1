package com.cardone.common.web.support;

import com.cardone.validator.*;
import java.util.*;

/**
 * Created by Administrator on 2014/10/29.
 */
public class InterfaceMethod {
    /**
     * 接口bean
     */
    @lombok.Setter
    @lombok.Getter
    private Object interfaceBean;

    /**
     * 方法名
     */
    @lombok.Setter
    @lombok.Getter
    private String methodName;

    /**
     * 键名
     */
    @lombok.Setter
    @lombok.Getter
    private String key;

    /**
     * 键值
     */
    @lombok.Setter
    @lombok.Getter
    private Object value;

    /**
     * 参数名集合
     */
    @lombok.Setter
    @lombok.Getter
    private List<ValidatorRuleConfig> parameterList;

    /**
     * 映射标识
     */
    @lombok.Setter
    @lombok.Getter
    private String mapId;

    /**
     * 验证token
     */
    @lombok.Setter
    @lombok.Getter
    private Boolean validatorToken = true;
}
