package com.cardone.common.util;

import com.cardone.common.*;
import com.google.common.collect.*;
import lombok.*;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.util.*;

import java.util.*;

/**
 * 字符串工具类
 *
 * @author yaohaitao
 */
public class StringUtils {
    @Setter
    private static java.util.Map<String, org.springframework.util.PathMatcher> pathMatcherMap = com.google.common.collect.Maps.newConcurrentMap();

    static {
        final AntPathMatcher defaultPathMatcher = new AntPathMatcher();

        defaultPathMatcher.setCachePatterns(true);

        pathMatcherMap.put(org.springframework.util.AntPathMatcher.DEFAULT_PATH_SEPARATOR, defaultPathMatcher);

        final AntPathMatcher classPathMatcher = new AntPathMatcher(".");

        classPathMatcher.setCachePatterns(true);

        pathMatcherMap.put(".", defaultPathMatcher);
    }

    private StringUtils() {
    }

    /**
     * 返回不为空的字符串
     *
     * @param strs 字符串集合
     * @return 不为空的字符串
     */
    public static String defaultIfBlank(final String... strs) {
        if (org.apache.commons.lang3.ArrayUtils.isEmpty(strs)) {
            return null;
        }

        for (final String str : strs) {
            if (org.apache.commons.lang3.StringUtils.isNotBlank(str)) {
                return str;
            }
        }

        return null;
    }

    /**
     * 比较
     *
     * @param paths 路径集合
     * @param url   路径
     * @return 比较结果
     */
    public static boolean matchs(final String paths, final String url) {
        final List<String> pathList = Lists.newArrayList(org.apache.commons.lang3.StringUtils.split(paths, Characters.comma.stringValue()));

        return StringUtils.matchList(pathList, url);
    }

    /**
     * 比较
     *
     * @param pathList 路径集合
     * @param url      路径
     * @return 比较结果
     */
    public static boolean matchList(final List<String> pathList, final String url) {
        String path = StringUtils.getPathForMatch(pathList, url);

        return org.apache.commons.lang3.StringUtils.isNotBlank(path);
    }

    /**
     * 获取路径
     *
     * @param pathList 路径集合
     * @param url      路径
     * @return 路径
     */
    public static String getPathForMatch(final List<String> pathList, final String url) {
        return getPathForMatch(pathList, url, null);
    }

    /**
     * 获取路径
     *
     * @param pathList 路径集合
     * @param url      路径
     * @return 路径
     */
    public static String getPathForMatch(final List<String> pathList, final String url, String pathSeparator) {
        if (CollectionUtils.isEmpty(pathList)) {
            return null;
        }

        if (org.apache.commons.lang3.StringUtils.isBlank(url)) {
            return url;
        }

        for (final String path : pathList) {
            if (org.apache.commons.lang3.StringUtils.equals(url, path)) {
                return path;
            }

            if (!StringUtils.getPathMatcher(pathSeparator).isPattern(path)) {
                continue;
            }

            if (StringUtils.getPathMatcher(pathSeparator).match(path, url)) {
                return path;
            }
        }

        return org.apache.commons.lang3.StringUtils.EMPTY;
    }

    /**
     * 获取路径比较
     *
     * @return 路径比较
     */
    public static PathMatcher getPathMatcher(String pathSeparator) {
        String newPathSeparator = org.apache.commons.lang3.StringUtils.defaultIfBlank(pathSeparator, org.springframework.util.AntPathMatcher.DEFAULT_PATH_SEPARATOR);

        if (StringUtils.pathMatcherMap.containsKey(newPathSeparator)) {
            return StringUtils.pathMatcherMap.get(newPathSeparator);
        }

        final AntPathMatcher antPathMatcher = new AntPathMatcher(newPathSeparator);

        antPathMatcher.setCachePatterns(true);

        pathMatcherMap.put(newPathSeparator, antPathMatcher);

        return antPathMatcher;
    }

    /**
     * 获取路径比较
     *
     * @return 路径比较
     */
    public static PathMatcher getPathMatcher() {
        return getPathMatcher(null);
    }

    /**
     * 获取路径
     *
     * @param pathCollection 路径集合
     * @param url            路径
     * @return 路径
     */
    public static String getPathForMatch(final java.util.Collection<String> pathCollection, final String url) {
        return getPathForMatch(pathCollection, url, null);
    }

    /**
     * 获取路径
     *
     * @param pathCollection 路径集合
     * @param url            路径
     * @return 路径
     */
    public static String getPathForMatch(final java.util.Collection<String> pathCollection, final String url, String pathSeparator) {
        if (CollectionUtils.isEmpty(pathCollection)) {
            return null;
        }

        return getPathForMatch(Lists.newArrayList(pathCollection), url, pathSeparator);
    }
}
