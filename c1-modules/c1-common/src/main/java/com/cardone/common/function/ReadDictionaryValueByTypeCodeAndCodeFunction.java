package com.cardone.common.function;

import com.cardone.context.function.*;

/**
 * 读取数据字典取
 *
 * @author yaohaitao
 */
public interface ReadDictionaryValueByTypeCodeAndCodeFunction extends Execution3Function<String, String, String, String> {
}