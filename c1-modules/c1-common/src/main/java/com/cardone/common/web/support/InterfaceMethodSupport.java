package com.cardone.common.web.support;

/**
 * Created by Administrator on 2014/10/29.
 */
public interface InterfaceMethodSupport {
    Object execution(final javax.servlet.http.HttpServletRequest request);
}
