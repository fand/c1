package com.cardone.common.cache;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Setter;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.interceptor.CacheOperationInvocationContext;
import org.springframework.cache.interceptor.CacheResolver;

import java.util.Collection;
import java.util.Map;

/**
 * Created by Administrator on 2014/11/8.
 */
public class CardOneCacheResolver implements CacheResolver {
    @Setter
    private Map<String, CacheManager> cacheManagerMap;

    @Setter
    private CacheManager defaultCacheManager;

    private Map<String, Collection<Cache>> cacheMap = Maps.newConcurrentMap();

    @Override
    public Collection<? extends Cache> resolveCaches(CacheOperationInvocationContext<?> context) {
        String key = StringUtils.join(context.getOperation().getCacheNames(), ",");

        Collection<Cache> caches = cacheMap.get(key);

        if (caches != null) {
            return caches;
        }

        caches = Lists.newArrayList();

        for (String cacheName : context.getOperation().getCacheNames()) {
            for (Map.Entry<String, CacheManager> cacheManagerEntry : cacheManagerMap.entrySet()) {
                if (StringUtils.endsWith(cacheName, cacheManagerEntry.getKey())) {
                    caches.add(cacheManagerEntry.getValue().getCache(cacheName));
                }
            }
        }

        if (CollectionUtils.isEmpty(caches)) {
            for (String cacheName : context.getOperation().getCacheNames()) {
                caches.add(defaultCacheManager.getCache(cacheName));
            }
        }

        cacheMap.put(key, caches);

        return caches;
    }
}
