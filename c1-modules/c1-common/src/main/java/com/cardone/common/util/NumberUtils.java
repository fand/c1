package com.cardone.common.util;

import org.apache.commons.lang.*;

/**
 * Created by Administrator on 2014/8/31.
 */
public class NumberUtils {
    private NumberUtils() {
    }

    public static int sum(int[] ints) {
        if (ArrayUtils.isEmpty(ints)) {
            return 0;
        }

        int sum = 0;

        for (int i = 0; i < ints.length; i++) {
            sum += ints[i];
        }

        return sum;
    }
}
