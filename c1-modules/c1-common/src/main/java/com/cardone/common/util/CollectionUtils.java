package com.cardone.common.util;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * collection 工具
 *
 * @author yaohaitao
 */
public class CollectionUtils {
    private static int padLength = Integer.MAX_VALUE - 1000;

    private CollectionUtils() {
    }

    /**
     * 排序关于长度
     *
     * @param stringlist 字符串集合
     */
    public static void sortForLength(final List<String> stringlist) {
        sortForLength(stringlist, null);
    }

    /**
     * 排序关于长度
     *
     * @param stringlist 字符串集合
     */
    public static void sortForLength(final List<String> stringlist, String pathSeparator) {
        if (org.apache.commons.collections.CollectionUtils.isEmpty(stringlist)) {
            return;
        }

        java.util.Collections.sort(stringlist, (s1, s2) -> {
                    int s1Length = StringUtils.length(s1);

                    if (com.cardone.common.util.StringUtils.getPathMatcher(pathSeparator).isPattern(s1)) {
                        s1Length = s1Length + CollectionUtils.padLength;
                    }

                    int s2Length = StringUtils.length(s2);

                    if (com.cardone.common.util.StringUtils.getPathMatcher(pathSeparator).isPattern(s2)) {
                        s2Length = s2Length + CollectionUtils.padLength;
                    }

                    return s1Length - s2Length;
                }
        );
    }
}
