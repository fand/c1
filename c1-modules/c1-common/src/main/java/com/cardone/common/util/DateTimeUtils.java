package com.cardone.common.util;

import org.joda.time.*;

public class DateTimeUtils {
    private DateTimeUtils() {
    }

    /**
     * 新时间
     *
     * @return 新时间
     */
    public static DateTime newDateTime() {
        return new DateTime();
    }
}
