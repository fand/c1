package com.cardone.common.template.util;

import com.cardone.common.template.support.*;

/**
 * 模板引擎工具类
 *
 * @author yaohaitao
 */
public class TemplateUtils {
    /**
     * 模板支持
     */
    private static TemplateSupport templateSupport;

    /**
     * 渲染模板字符串
     *
     * @param templateString 模板字符串
     * @param model          映射对象
     * @return 字符串
     */
    public static String processString(final String templateString, final Object model) {
        return TemplateUtils.templateSupport.processString(templateString, model);
    }

    /**
     * 设置：模板解析支持
     *
     * @param templateSupport 模板解析支持
     */
    public static TemplateSupport setTemplateSupport(final TemplateSupport templateSupport) {
        TemplateUtils.templateSupport = templateSupport;

        return templateSupport;
    }
}
