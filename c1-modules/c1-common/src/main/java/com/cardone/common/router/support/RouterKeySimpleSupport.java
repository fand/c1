package com.cardone.common.router.support;

import com.cardone.common.template.util.*;
import com.cardone.common.util.*;
import lombok.*;
import lombok.experimental.*;
import org.apache.commons.collections.*;

import java.util.*;

/**
 * 路由键名
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Accessors(chain = true)
@lombok.extern.slf4j.Slf4j
public class RouterKeySimpleSupport implements RouterKeySupport {
    /**
     * 默认组标识
     */
    private String defaultGroupId;
    /**
     * 默认键名
     */
    private String defaultKey;
    /**
     * 表达式路由键名配置
     */
    private Map<String, Map<String, String>> expressionRouterKeyTable;
    /**
     * 表达式路由键名配置
     */
    private Map<String, String> expressionRouterKeyMap;
    /**
     * 路由键名配置
     */
    private Map<String, Map<String, String>> routerKeyTable;

    /**
     * 路由键名配置
     */
    private Map<String, String> routerKeyMap;

    @Override
    public String readKeyByGroupId(final String groupId, final String value, final Object model) {
        final String newGroupId = StringUtils.defaultIfBlank(groupId, this.defaultGroupId);

        if (MapUtils.isNotEmpty(this.expressionRouterKeyTable) && (model != null)) {
            final Map<String, String> expressionRouterKeyMap = this.expressionRouterKeyTable.get(newGroupId);

            if (MapUtils.isNotEmpty(expressionRouterKeyMap)) {
                final String path = StringUtils.getPathForMatch(expressionRouterKeyMap.keySet(), value);

                if (org.apache.commons.lang3.StringUtils.isNotBlank(path)) {
                    final String expression = expressionRouterKeyMap.get(path);

                    final String key = TemplateUtils.processString(expression, model);

                    return StringUtils.defaultIfBlank(key, this.defaultKey);
                }
            }
        }

        return this.readKeyByGroupId(groupId, value);
    }

    @Override
    public String readKeyByGroupId(String groupId, String value) {
        if (MapUtils.isEmpty(this.routerKeyTable)) {
            return this.defaultKey;
        }

        final String newGroupId = StringUtils.defaultIfBlank(groupId, this.defaultGroupId);

        final Map<String, String> routerKeyMap = this.routerKeyTable.get(newGroupId);

        if (MapUtils.isNotEmpty(routerKeyMap)) {
            return this.defaultKey;
        }

        final String path = StringUtils.getPathForMatch(routerKeyMap.keySet(), value);

        if (org.apache.commons.lang3.StringUtils.isBlank(path)) {
            return this.defaultKey;
        }

        final String key = routerKeyMap.get(path);

        return StringUtils.defaultIfBlank(key, this.defaultKey);
    }

    @Override
    public String readKey(final String value, final Object model) {
        if (MapUtils.isEmpty(expressionRouterKeyMap)) {
            return this.defaultKey;
        }

        final String path = StringUtils.getPathForMatch(expressionRouterKeyMap.keySet(), value);

        if (org.apache.commons.lang3.StringUtils.isBlank(path)) {
            return this.defaultKey;
        }

        final String expression = expressionRouterKeyMap.get(path);

        final String key = TemplateUtils.processString(expression, model);

        return StringUtils.defaultIfBlank(key, this.defaultKey);
    }

    @Override
    public String readKey(final String value) {
        if (MapUtils.isEmpty(routerKeyMap)) {
            return this.defaultKey;
        }

        final String path = StringUtils.getPathForMatch(routerKeyMap.keySet(), value);

        if (org.apache.commons.lang3.StringUtils.isBlank(path)) {
            return this.defaultKey;
        }

        final String key = routerKeyMap.get(path);

        return StringUtils.defaultIfBlank(key, this.defaultKey);
    }
}