package com.cardone.common.dto;

import lombok.*;
import lombok.experimental.*;

import java.util.*;

/**
 * 分页
 *
 * @param <T> 泛型
 * @author yaohaitao
 */
@Getter
@Setter
@Accessors(chain = true)
public class PaginationDto<T> implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 2759596825937626448L;

    /**
     * 开始行数
     */
    private int beginRowNum;

    /**
     * 数据集合
     */
    private List<T> dataList;

    /**
     * 结束行数
     */
    private int endRowNum;

    /**
     * 是否有第一页
     */
    private boolean hasFirstNo;

    /**
     * 是否有最后一页
     */
    private boolean hasLastNo;

    /**
     * 是否有下一页
     */
    private boolean hasNextNo = false;

    /**
     * 是否有前一页
     */
    private boolean hasPreviousNo = false;

    /**
     * 最后一页
     */
    private int lastNo;

    /**
     * 下一页
     */
    private int nextNo = 2;

    /**
     * 分页号
     */
    private int no;

    /**
     * 分页号集合
     */
    private int[] nos;

    /**
     * 上一页
     */
    private int previousNo = 1;

    /**
     * 分页大小
     */
    private int size;

    /**
     * 统计分页号
     */
    private int totalNo;

    /**
     * 统计大小
     */
    private long totalSize;
}
