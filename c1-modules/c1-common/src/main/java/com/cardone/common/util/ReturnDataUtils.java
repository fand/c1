package com.cardone.common.util;

import com.cardone.context.Contexts;
import com.cardone.context.DictionaryException;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.collections.MapUtils;

import java.util.List;
import java.util.Map;

/**
 * 返回数据
 *
 * @author yaohaitao
 */
@Slf4j
public class ReturnDataUtils {
    private ReturnDataUtils() {
    }

    /**
     * 生成
     *
     * @param data 数据
     * @return 返回数据
     */
    public static Map<String, Object> newMap(final Object data) {
        return newMap(Contexts.data.stringValue(), data);
    }


    /**
     * 生成
     *
     * @param e 导常
     * @return 返回数据
     */
    public static Map<String, Object> newErrorsMap(final Throwable e) {
        final Map<String, Object> errors = Maps.newHashMap();

        List<Map<String, String>> errorMapList = Lists.newArrayList();

        if (e instanceof DictionaryException) {
            DictionaryException dictionaryException = (DictionaryException) e;

            if (MapUtils.isEmpty(dictionaryException.getErrors())) {
                Map<String, String> errorMap = Maps.newHashMap();

                List<String> codeList = Lists.newArrayList();

                if (org.apache.commons.lang3.StringUtils.isNotBlank(dictionaryException.getTypeCode())) {
                    codeList.add(dictionaryException.getTypeCode());
                }

                if (org.apache.commons.lang3.StringUtils.isNotBlank(dictionaryException.getCode())) {
                    codeList.add(dictionaryException.getCode());
                }

                if (org.apache.commons.collections.CollectionUtils.isEmpty(codeList)) {
                    errorMap.put("code", "system");
                } else {
                    errorMap.put("code", org.apache.commons.lang3.StringUtils.join(codeList, "."));
                }

                errorMap.put("message", dictionaryException.getMessage());

                errorMapList.add(errorMap);
            } else {
                for (Map.Entry<String, String> errorEntry : dictionaryException.getErrors().entrySet()) {
                    Map<String, String> errorMap = Maps.newHashMap();

                    errorMap.put("code", errorEntry.getKey());
                    errorMap.put("message", errorEntry.getValue());

                    errorMapList.add(errorMap);
                }
            }
        } else {
            Map<String, String> errorMap = Maps.newHashMap();

            errorMap.put("code", "system");
            errorMap.put("message", e.getMessage());

            errorMapList.add(errorMap);
        }

        errors.put("errors", errorMapList);

        return errors;
    }

    /**
     * 生成
     *
     * @param data 数据
     * @return 返回数据
     */
    public static Map<String, Object> newMap(String key, final Object data) {
        Map<String, Object> map = Maps.newHashMap();

        String newKey = StringUtils.defaultIfBlank(key, Contexts.data.stringValue());

        map.put(newKey, data);

        return map;
    }
}
