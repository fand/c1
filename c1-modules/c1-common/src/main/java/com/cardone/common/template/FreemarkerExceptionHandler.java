package com.cardone.common.template;

import freemarker.core.*;
import freemarker.template.*;
import lombok.extern.slf4j.*;

import java.io.*;

/**
 * 模板异常处理
 *
 * @author yaohaitao
 */
@Slf4j
public class FreemarkerExceptionHandler implements TemplateExceptionHandler {
    @Override
    public void handleTemplateException(final TemplateException te, final Environment env, final Writer out) throws TemplateException {
        FreemarkerExceptionHandler.log.error(te.getMessage(), te);
    }
}
