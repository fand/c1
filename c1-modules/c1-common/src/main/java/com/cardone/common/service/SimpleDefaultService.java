package com.cardone.common.service;

import com.cardone.common.dao.SimpleDao;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.po.PoBase;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;

/**
 * Created by yaohaitao on 2014/8/31.
 */
@Transactional(readOnly = true)
public abstract class SimpleDefaultService<D extends PoBase> implements SimpleService<D> {
    @Override
    @Transactional
    public <P> P saveByIdOrCode(Class<P> mappedClass, D save, String... selectNames) {
        return this.getDao().saveByIdOrCode(mappedClass, save, selectNames);
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void init() {
    }

    @Override
    @Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void init(String beanId) {
        if (com.cardone.context.ContextHolder.getApplicationContext().containsBean(beanId)) {
            java.util.List<D> dList = (java.util.List<D>) com.cardone.context.ContextHolder.getBean(beanId);

            this.init(dList);
        }
    }

    public void init(List<D> dList) {
    }

    /**
     * 数据持久
     *
     * @param <Dao> dao
     * @return dao
     */
    public abstract <Dao extends SimpleDao<D>> Dao getDao();

    @Override
    @Transactional
    public Map<String, Object> saveByIdOrCode(D save, String... selectNames) {
        return this.getDao().saveByIdOrCode(save, selectNames);
    }

    @Override
    @Transactional
    public <P> P saveByIdOrCode(Class<P> mappedClass, Map<String, Object> saveMap, String... selectNames) {
        return this.getDao().saveByIdOrCode(mappedClass, saveMap, selectNames);
    }

    @Override
    @Transactional
    public Map<String, Object> saveByIdOrCode(Map<String, Object> saveMap, String... selectNames) {
        return this.getDao().saveByIdOrCode(saveMap, selectNames);
    }

    @Override
    @Transactional
    public int insertByCode(D insert) {
        return this.getDao().insertByCode(insert);
    }

    @Override
    @Transactional
    public int insertByCode(Map<String, Object> insertMap) {
        return this.getDao().insertByCode(insertMap);
    }

    @Override
    @Transactional
    public int[] insertByCode(List<D> insertList) {
        return this.getDao().insertByCode(insertList);
    }

    @Override
    @Transactional
    public int insertByNotExistsCode(D insert) {
        return this.getDao().insertByNotExistsCode(insert);
    }

    @Override
    @Transactional
    public int insertByNotExistsCode(Map<String, Object> insertMap) {
        return this.getDao().insertByNotExistsCode(insertMap);
    }

    @Override
    @Transactional
    public int insertByNotExistsCode(Map<String, Object> insertMap, Map<String, Object> whereAndEqMap) {
        return this.getDao().insertByNotExistsCode(insertMap, whereAndEqMap);
    }

    @Override
    @Transactional
    public int[] insertByNotExistsCode(List<D> insertList) {
        return this.getDao().insertByNotExistsCode(insertList);
    }

    @Override
    @Transactional
    public int deleteByIds(D delete) {
        return this.getDao().deleteByIds(delete);
    }

    @Override
    @Transactional
    public int updateByIds(String ids) {
        return this.getDao().updateByIds(ids);
    }

    @Override
    @Transactional
    public int updateByIds(D delete) {
        return this.getDao().updateByIds(delete);
    }

    @Override
    @Transactional
    public int deleteByIds(String ids) {
        return this.getDao().deleteByIds(ids);
    }

    @Override
    @Transactional
    public int deleteByCode(D delete, String... whereAndEqProperties) {
        return this.getDao().deleteByCode(delete, whereAndEqProperties);
    }

    @Override
    @Transactional
    public int[] deleteByCode(List<D> deleteList, String... whereAndEqProperties) {
        return this.getDao().deleteByCode(deleteList, whereAndEqProperties);
    }

    @Override
    @Transactional
    public int deleteByCode(Map<String, Object> whereAndEqMap) {
        return this.getDao().deleteByCode(whereAndEqMap);
    }

    @Override
    @Transactional
    public int[] deleteByCode(List<Map<String, Object>> whereAndEqMapList) {
        return this.getDao().deleteByCode(whereAndEqMapList);
    }

    @Override
    @Transactional
    public int updateByCode(D update, String... updateNames) {
        return this.getDao().deleteByCode(update, updateNames);
    }

    @Override
    @Transactional
    public int updateByCode(D update) {
        return this.getDao().updateByCode(update);
    }

    @Override
    @Transactional
    public int updateByCode(Map<String, Object> updateMap, String... updateNames) {
        return this.getDao().updateByCode(updateMap, updateNames);
    }

    @Override
    @Transactional
    public int updateByCode(Map<String, Object> updateMap) {
        return this.getDao().updateByCode(updateMap);
    }

    @Override
    @Transactional
    public int[] updateByCode(List<D> updateList, String... updateNames) {
        return this.getDao().updateByCode(updateList, updateNames);
    }

    @Override
    @Transactional
    public int[] updateByCode(List<D> updateList, String[] whereAndEqProperties, String... updateNames) {
        return this.getDao().updateByCode(updateList, whereAndEqProperties, updateNames);
    }

    @Override
    @Transactional
    public int updateByCode(D update, String[] whereAndEqProperties, String... updateNames) {
        return this.getDao().updateByCode(update, whereAndEqProperties, updateNames);
    }

    @Override
    @Transactional
    public int updateByCode(Map<String, Object> updateMap, String[] whereAndEqProperties, String... updateNames) {
        return this.getDao().updateByCode(updateMap, whereAndEqProperties, updateNames);
    }

    @Override
    @Transactional
    public int updateByCode(Map<String, Object> whereAndEqMap, Map<String, Object> updateMap) {
        return this.getDao().updateByCode(whereAndEqMap, updateMap);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer readByCodeNqIdForCount(D read) {
        return this.getDao().readByCodeNqIdForCount(read);
    }

    @Override
    @Transactional(readOnly = true)
    public <R> R readByCode(Class<R> requiredType, String objectId, D read, String... whereAndEqProperties) {
        return this.getDao().readByCode(requiredType, objectId, read, whereAndEqProperties);
    }

    @Override
    @Transactional(readOnly = true)
    public <R> R readByCode(Class<R> requiredType, String objectId, Map<String, Object> whereAndEqMap) {
        return this.getDao().readByCode(requiredType, objectId, whereAndEqMap);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer readByCodeNqIdForCount(Map<String, Object> readMap) {
        return this.getDao().readByCodeNqIdForCount(readMap);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer readByLikeCode(D read, String... whereAndEqProperties) {
        return this.getDao().readByLikeCode(read, whereAndEqProperties);
    }

    @Override
    @Transactional(readOnly = true)
    public Integer readByLikeCode(Map<String, Object> readMap) {
        return this.getDao().readByLikeCode(readMap);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> P findById(Class<P> mappedClass, D find, String... selectNames) {
        return this.getDao().findById(mappedClass, find, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> findById(D find, String... selectNames) {
        return this.getDao().findById(find, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> P findById(Class<P> mappedClass, Map<String, Object> findMap, String... selectNames) {
        return this.getDao().findById(mappedClass, findMap, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> findById(Map<String, Object> findMap, String... selectNames) {
        return this.getDao().findById(findMap, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> P findByCode(Class<P> mappedClass, D find, String[] whereAndEqProperties, String... selectNames) {
        return this.getDao().findByCode(mappedClass, find, whereAndEqProperties, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> P findByCode(Class<P> mappedClass, Map<String, Object> whereAndEqMap, String... selectNames) {
        return this.getDao().findByCode(mappedClass, whereAndEqMap, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> findByCode(Map<String, Object> whereAndEqMap, String... selectNames) {
        return this.getDao().findByCode(whereAndEqMap, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> P findByCode(Class<P> mappedClass, D find, String... whereAndEqProperties) {
        return this.getDao().findByCode(mappedClass, find, whereAndEqProperties);
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> findByCode(D find, String[] whereAndEqProperties, String... selectNames) {
        return this.getDao().findByCode(find, whereAndEqProperties, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> findByCode(D find, String... whereAndEqProperties) {
        return this.getDao().findByCode(find, whereAndEqProperties);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> P findByCodeOrId(Class<P> mappedClass, D find, String... selectNames) {
        return this.getDao().findByCodeOrId(mappedClass, find, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> findByCodeOrId(D find, String... selectNames) {
        return this.getDao().findByCodeOrId(find, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> P findByCodeOrId(Class<P> mappedClass, Map<String, Object> findMap, String... selectNames) {
        return this.getDao().findByCodeOrId(mappedClass, findMap, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public Map<String, Object> findByCodeOrId(Map<String, Object> findMap, String... selectNames) {
        return this.getDao().findByCodeOrId(findMap, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findListByLikeCode(Class<P> mappedClass, D findList, String[] whereAndEqProperties, String... selectNames) {
        return this.getDao().findListByLikeCode(mappedClass, findList, whereAndEqProperties, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findListByLikeCode(Class<P> mappedClass, D findList, String... whereAndEqProperties) {
        return this.getDao().findListByLikeCode(mappedClass, findList, whereAndEqProperties);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Map<String, Object>> findListByLikeCode(D findList, String[] whereAndEqProperties, String... selectNames) {
        return this.getDao().findListByLikeCode(findList, whereAndEqProperties, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Map<String, Object>> findListByLikeCode(D findList, String... whereAndEqProperties) {
        return this.getDao().findListByLikeCode(findList, whereAndEqProperties);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findListByLikeCode(Class<P> mappedClass, Map<String, Object> findListMap, String... selectNames) {
        return this.getDao().findListByLikeCode(mappedClass, findListMap, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Map<String, Object>> findListByLikeCode(Map<String, Object> findListMap, String... selectNames) {
        return this.getDao().findListByLikeCode(findListMap, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findListByCode(Class<P> mappedClass, D findList, String[] whereAndEqProperties, String... selectNames) {
        return this.getDao().findListByCode(mappedClass, findList, whereAndEqProperties, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findListByCode(Class<P> mappedClass, D findList, String... whereAndEqProperties) {
        return this.getDao().findListByCode(mappedClass, findList, whereAndEqProperties);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findListByCode(Class<P> mappedClass, Map<String, Object> findListMap, String... selectNames) {
        return this.getDao().findListByCode(mappedClass, findListMap, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Map<String, Object>> findListByCode(D findList, String[] whereAndEqProperties, String... selectNames) {
        return this.getDao().findListByCode(findList, whereAndEqProperties, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Map<String, Object>> findListByCode(D findList, String... whereAndEqProperties) {
        return this.getDao().findListByCode(findList, whereAndEqProperties);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Map<String, Object>> findListByCode(Map<String, Object> findListMap, String... selectNames) {
        return this.getDao().findListByCode(findListMap, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findList(Class<P> mappedClass, String... selectNames) {
        return this.getDao().findList(mappedClass, selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public List<Map<String, Object>> findList(String... selectNames) {
        return this.getDao().findList(selectNames);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> PaginationDto<P> paginationByLikeCode(Class<P> mappedClass, D pagination) {
        return this.getDao().paginationByLikeCode(mappedClass, pagination);
    }

    @Override
    @Transactional(readOnly = true)
    public PaginationDto<Map<String, Object>> paginationByLikeCode(D pagination) {
        return this.getDao().paginationByLikeCode(pagination);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> PaginationDto<P> paginationByLikeCode(Class<P> mappedClass, Map<String, Object> paginationMap) {
        return this.getDao().paginationByLikeCode(mappedClass, paginationMap);
    }

    @Override
    @Transactional(readOnly = true)
    public PaginationDto<Map<String, Object>> paginationByLikeCode(Map<String, Object> paginationMap) {
        return this.getDao().paginationByLikeCode(paginationMap);
    }
}
