package com.cardone.common.util;

import lombok.*;
import lombok.extern.slf4j.*;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.text.*;
import java.util.*;

/**
 * 时间工具类
 *
 * @author yaohaitao
 */
@Slf4j
public class DateUtils {
    /**
     * 默认时间格式
     */
    public static final String DEFAULT_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";

    @Setter
    private static String[] parsePatterns = {"yyyy-MM-dd", "yyyy-MM-dd HH", "yyyy-MM-dd HH:mm", DEFAULT_DATE_PATTERN};

    private DateUtils() {
    }

    /**
     * 解析
     *
     * @param str           字符串
     * @param parsePatterns 时间格式化字符串
     * @return 時間
     * @throws ParseException
     */
    public static Date parseDate(final String str, final String... parsePatterns) {
        if (StringUtils.isBlank(str)) {
            return null;
        }

        String[] newParsePatterns = ArrayUtils.isNotEmpty(parsePatterns) ? parsePatterns : DateUtils.parsePatterns;

        try {
            return org.apache.commons.lang3.time.DateUtils.parseDate(str, newParsePatterns);
        } catch (ParseException e) {
            log.error(e.getMessage(), e);
        }

        return null;
    }


    /**
     * 新时间
     *
     * @return 新时间
     */
    public static Date newDate() {
        return new Date();
    }
}