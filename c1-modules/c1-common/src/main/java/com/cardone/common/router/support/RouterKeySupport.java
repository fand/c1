package com.cardone.common.router.support;

/**
 * 路由键名
 *
 * @author yaohaitao
 */
public interface RouterKeySupport {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.common.router.support.RouterKeySupport";

    /**
     * 获取：键名
     *
     * @param groupId 组标识
     * @param value   键值
     * @param model   model
     * @return 键名
     */
    String readKeyByGroupId(final String groupId, final String value, final Object model);

    /**
     * 获取：键名
     *
     * @param groupId 组标识
     * @param value   键值
     * @return 键名
     */
    String readKeyByGroupId(final String groupId, final String value);

    /**
     * 获取：键名
     *
     * @param value 键值
     * @param model model
     * @return 键名
     */
    String readKey(final String value, final Object model);

    /**
     * 获取：键名
     *
     * @param value 键值
     * @return 键名
     */
    String readKey(final String value);
}
