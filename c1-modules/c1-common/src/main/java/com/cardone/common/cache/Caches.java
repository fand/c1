package com.cardone.common.cache;

/**
 * 常量
 *
 * @author yaohaitao
 */
public enum Caches {

    /**
     * 默认缓存管理名称
     */
    defaultCacheManagerName(RouterCacheManager.class.getName()),

    /**
     * 登录键
     */
    loginKey;

    /**
     * 缓存键0
     */
    public static final String KEY_0 = "#root.targetClass.name+'.'+#root.methodName";

    /**
     * 缓存键1
     */
    public static final String KEY_1 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[0]+')'";

    /**
     * 缓存键2
     */
    public static final String KEY_2 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[0]+', '+#root.args[1]+')'";

    /**
     * 缓存键2to2
     */
    public static final String KEY_2_TO_2 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[1]+')'";

    /**
     * 缓存键2to3
     */
    public static final String KEY_2_TO_3 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[1]+', '+#root.args[2]+')'";

    /**
     * 缓存键2to4
     */
    public static final String KEY_2_TO_4 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[1]+', '+#root.args[2]+', '+#root.args[3]')'";

    /**
     * 缓存键2to5
     */
    public static final String KEY_2_TO_5 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[1]+', '+#root.args[2]+', '+#root.args[3]+', '+#root.args[4]+')'";

    /**
     * 缓存键3
     */
    public static final String KEY_3 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[0]+', '+#root.args[1]+', '+#root.args[2]+')'";

    /**
     * 缓存键3to3
     */
    public static final String KEY_3_TO_3 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[2]+')'";

    /**
     * 缓存键3to4
     */
    public static final String KEY_3_TO_4 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[2]+', '+#root.args[3]+')'";

    /**
     * 缓存键3to5
     */
    public static final String KEY_3_TO_5 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[2]+', '+#root.args[3]+', '+#root.args[4]')'";

    /**
     * 缓存键4
     */
    public static final String KEY_4 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[0]+', '+#root.args[1]+', '+#root.args[2]+', '+#root.args[3]+')'";

    /**
     * 缓存键4to4
     */
    public static final String KEY_4_TO_4 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[3]+')'";

    /**
     * 缓存键4to5
     */
    public static final String KEY_4_TO_5 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[3]+', '+#root.args[4]+')'";

    /**
     * 缓存键5
     */
    public static final String KEY_5 = "#root.targetClass.name+'.'+#root.methodName+'('+#root.args[0]+', '+#root.args[1]+', '+#root.args[2]+', '+#root.args[3]+', '+#root.args[4]+')'";

    /**
     * string值
     */
    private String stringValue;

    /**
     * 构建
     */
    private Caches() {
        this.stringValue = this.name();
    }

    /**
     * 构建
     *
     * @param stringValue string值
     */
    private Caches(final String stringValue) {
        this.stringValue = stringValue;
    }

    /**
     * 获取string值
     *
     * @return string值
     */
    public String stringValue() {
        return this.stringValue;
    }
}
