package com.cardone.common.cache.action;

import com.cardone.context.*;
import com.cardone.context.action.*;
import org.springframework.data.redis.core.*;

public class RedisFlushRunAction implements Run0Action {

    @SuppressWarnings("unchecked")
    @Override
    public void run() {
        ContextHolder.getBean(RedisTemplate.class).execute((org.springframework.data.redis.connection.RedisConnection connection) -> {
            connection.flushDb();

            return true;
        });
    }
}
