package com.cardone.common.service;

import com.cardone.common.dao.*;
import com.cardone.common.po.*;

/**
 * 简易 service
 *
 * @author yaohaitao
 */
public interface SimpleService<D extends PoBase> extends SimpleDao<D> {
    void init();

    void init(String beanId);
}