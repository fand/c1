/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2014/10/28 15:25:40                          */
/*==============================================================*/


DROP TABLE IF EXISTS C1_ENTITY_CODE;

DROP TABLE IF EXISTS C1_ARTICLE;

DROP TABLE IF EXISTS C1_DEPARTMENT;

DROP TABLE IF EXISTS C1_DICTIONARY;

DROP TABLE IF EXISTS C1_DICTIONARY_ITEM;

DROP TABLE IF EXISTS C1_DICTIONARY_TYPE;

DROP TABLE IF EXISTS C1_ENTITY_DEFAULT;

DROP TABLE IF EXISTS C1_ENTITY_EXTEND;

DROP TABLE IF EXISTS C1_ENTITY_FILE;

DROP TABLE IF EXISTS C1_ENTITY_LOG;

DROP TABLE IF EXISTS C1_NAVIGATION;

DROP TABLE IF EXISTS C1_NOTICE;

DROP TABLE IF EXISTS C1_OPERATE_LOG;

DROP TABLE IF EXISTS C1_ORG;

DROP TABLE IF EXISTS C1_PE_VA_RU_TRANSACT_MODE;

DROP TABLE IF EXISTS C1_PERMISSION;

DROP TABLE IF EXISTS C1_PERMISSION_ORG;

DROP TABLE IF EXISTS C1_ROLE;

DROP TABLE IF EXISTS C1_ROLE_PERMISSION;

DROP TABLE IF EXISTS C1_ROLE_USER;

DROP TABLE IF EXISTS C1_SITE;

DROP TABLE IF EXISTS C1_SITE_URL;

DROP TABLE IF EXISTS C1_US_DEPARTMENT_TRANSLATE;

DROP TABLE IF EXISTS C1_US_GR_BUSINESS_AREA;

DROP TABLE IF EXISTS C1_US_GROUP_ORG;

DROP TABLE IF EXISTS C1_US_GROUP_ROLE;

DROP TABLE IF EXISTS C1_US_GROUP_USER;

DROP TABLE IF EXISTS C1_US_ORG_TRANSLATE;

DROP TABLE IF EXISTS C1_USER;

DROP TABLE IF EXISTS C1_USER_ACTIVATION;

DROP TABLE IF EXISTS C1_USER_GROUP;

/*==============================================================*/
/* Table: C1_ENTITY_CODE                                        */
/*==============================================================*/
CREATE TABLE C1_ENTITY_CODE
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) COMMENT '代码',
   TYPE_CODE            VARCHAR(256) COMMENT '类别代码',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_ENTITY_CODE COMMENT '实体编号';

/*==============================================================*/
/* Table: C1_ARTICLE                                            */
/*==============================================================*/
CREATE TABLE C1_ARTICLE
(
   ORG_ID               VARCHAR(36) NOT NULL COMMENT '组织标识',
   ORG_CODE             VARCHAR(256) COMMENT '组织代码',
   ORG_NAME             VARCHAR(256) COMMENT '组织名称',
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   TITLE                VARCHAR(256) COMMENT '标题',
   RELEASE_DATE         DATE COMMENT '发布日期',
   CONTENT              TEXT COMMENT '正文',
   TYPE_ID              VARCHAR(36) COMMENT '类别标识',
   TYPE_CODE            VARCHAR(256) COMMENT '类别代码',
   TYPE_NAME            VARCHAR(256) COMMENT '类别名称',
   PRIMARY KEY (ID, ORG_ID)
);

ALTER TABLE C1_ARTICLE COMMENT '文章';

/*==============================================================*/
/* Table: C1_DEPARTMENT                                         */
/*==============================================================*/
CREATE TABLE C1_DEPARTMENT
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   ORG_ID               VARCHAR(36) COMMENT '组织标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   ORG_CODE             VARCHAR(256) COMMENT '组织代码',
   ORG_NAME             VARCHAR(256) COMMENT '组织名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_DEPARTMENT COMMENT '部门';

/*==============================================================*/
/* Table: C1_DICTIONARY                                         */
/*==============================================================*/
CREATE TABLE C1_DICTIONARY
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   TYPE_ID              VARCHAR(36) NOT NULL COMMENT '字典类型.类型标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   VALUE                VARCHAR(2048) COMMENT '值',
   REMARK               VARCHAR(512) COMMENT '说明',
   ORDER_NUM            BIGINT NOT NULL DEFAULT 0 COMMENT '排序数',
   EXPLAIN_TEXT         TEXT COMMENT '解释',
   PARENT_ID            VARCHAR(36) COMMENT '父级标识',
   PARENT_CODE          VARCHAR(256) COMMENT '父级代码',
   PARENT_NAME          VARCHAR(256) COMMENT '父级名称',
   TREE_ID              VARCHAR(1024) COMMENT '树标识',
   TREE_CODE            VARCHAR(1024) COMMENT '树代码',
   TREE_NAME            VARCHAR(1024) COMMENT '树名称',
   TYPE_CODE            VARCHAR(256) COMMENT '类别代码',
   TYPE_NAME            VARCHAR(256) COMMENT '类别名称',
   TYPE_TREE_ID         VARCHAR(1024) COMMENT '类别树标识',
   TYPE_TREE_CODE       VARCHAR(1024) COMMENT '类别树代码',
   TYPE_TREE_NAME       VARCHAR(1024) COMMENT '类别树名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_DICTIONARY COMMENT '字典';

/*==============================================================*/
/* Table: C1_DICTIONARY_ITEM                                    */
/*==============================================================*/
CREATE TABLE C1_DICTIONARY_ITEM
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   DICTIONARY_ID        VARCHAR(36) NOT NULL COMMENT '字典标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   VALUE                VARCHAR(2048) COMMENT '值',
   REMARK               VARCHAR(512) COMMENT '说明',
   ORDER_NUM            BIGINT NOT NULL DEFAULT 0 COMMENT '排序数',
   DICTIONARY_CODE      VARCHAR(256) COMMENT '字典代码',
   DICTIONARY_NAME      VARCHAR(256) COMMENT '字典名称',
   DICTIONARY_TYPE_ID   VARCHAR(36) COMMENT '字典类别标识',
   DICTIONARY_TYPE_CODE VARCHAR(256) COMMENT '字典类别代码',
   DICTIONARY_TYPE_NAME VARCHAR(256) COMMENT '字典类别名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_DICTIONARY_ITEM COMMENT '字典项';

/*==============================================================*/
/* Table: C1_DICTIONARY_TYPE                                    */
/*==============================================================*/
CREATE TABLE C1_DICTIONARY_TYPE
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   VALUE                VARCHAR(2048) COMMENT '值',
   ORDER_NUM            BIGINT NOT NULL DEFAULT 0 COMMENT '排序数',
   PARENT_ID            VARCHAR(36) COMMENT '父级标识',
   PARENT_CODE          VARCHAR(256) COMMENT '父级代码',
   PARENT_NAME          VARCHAR(256) COMMENT '父级名称',
   TREE_ID              VARCHAR(1024) COMMENT '树标识',
   TREE_CODE            VARCHAR(1024) COMMENT '树代码',
   TREE_NAME            VARCHAR(1024) COMMENT '树名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_DICTIONARY_TYPE COMMENT '字典类型';

/*==============================================================*/
/* Table: C1_ENTITY_DEFAULT                                     */
/*==============================================================*/
CREATE TABLE C1_ENTITY_DEFAULT
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   TYPE_ID              VARCHAR(36) NOT NULL COMMENT '字典.类别标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   ENTITY_ID            VARCHAR(36) NOT NULL COMMENT '实体标识',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_ENTITY_DEFAULT COMMENT '实体与默认';

/*==============================================================*/
/* Table: C1_ENTITY_EXTEND                                      */
/*==============================================================*/
CREATE TABLE C1_ENTITY_EXTEND
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   TYPE_ID              VARCHAR(36) NOT NULL COMMENT '字典.类别标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   ENTITY_ID            VARCHAR(36) NOT NULL COMMENT '实体标识',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   VALUE                VARCHAR(2048) COMMENT '值',
   REMARK               VARCHAR(512) COMMENT '说明',
   ORDER_NUM            BIGINT NOT NULL DEFAULT 0 COMMENT '排序数',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_ENTITY_EXTEND COMMENT '实体扩展';

/*==============================================================*/
/* Table: C1_ENTITY_FILE                                        */
/*==============================================================*/
CREATE TABLE C1_ENTITY_FILE
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   TYPE_ID              VARCHAR(36) COMMENT '字典.类别标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   NAME                 VARCHAR(256) COMMENT '名称',
   ORIGINAL_FILENAME    VARCHAR(256) COMMENT '原文件名',
   EXTENSION            VARCHAR(256) COMMENT '扩展',
   FILENAME             VARCHAR(256) COMMENT '文件名',
   ENTITY_ID            VARCHAR(36) COMMENT '实体标识',
   PARENT_ID            VARCHAR(36) COMMENT '父级标识',
   THUMBNAIL_VALUE      VARCHAR(256) COMMENT '小图尺寸',
   CONTENT_TYPE         VARCHAR(256) COMMENT '内容类型',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_ENTITY_FILE COMMENT '实体文件';

/*==============================================================*/
/* Table: C1_ENTITY_LOG                                         */
/*==============================================================*/
CREATE TABLE C1_ENTITY_LOG
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   TYPE_ID              VARCHAR(36) NOT NULL COMMENT '字典.类别标识',
   USER_ID              VARCHAR(36) NOT NULL COMMENT '用户标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   ENTITY_ID            VARCHAR(36) NOT NULL COMMENT '实体标识',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   MESSAGE              VARCHAR(1024) COMMENT '消息',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_ENTITY_LOG COMMENT '实体日志';

/*==============================================================*/
/* Table: C1_NAVIGATION                                         */
/*==============================================================*/
CREATE TABLE C1_NAVIGATION
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   ORDER_NUM            BIGINT NOT NULL DEFAULT 0 COMMENT '排序数',
   ICON_STYLE           VARCHAR(256) COMMENT '图标样式',
   URL                  VARCHAR(256) COMMENT 'URL',
   TARGET               VARCHAR(256) COMMENT '目标',
   DATA_OPTION          VARCHAR(512) COMMENT '数据选项',
   PARENT_ID            VARCHAR(36) COMMENT '父级标识',
   PARENT_CODE          VARCHAR(256) COMMENT '父级代码',
   PARENT_NAME          VARCHAR(256) COMMENT '父级名称',
   TREE_ID              VARCHAR(1024) COMMENT '树标识',
   TREE_CODE            VARCHAR(1024) COMMENT '树代码',
   TREE_NAME            VARCHAR(1024) COMMENT '树名称',
   TYPE_ID              VARCHAR(36) COMMENT '类别标识',
   TYPE_CODE            VARCHAR(256) COMMENT '类别代码',
   TYPE_NAME            VARCHAR(256) COMMENT '类别名称',
   SITE_ID              VARCHAR(36) COMMENT '站点标识',
   SITE_CODE            VARCHAR(256) COMMENT '站点代码',
   SITE_NAME            VARCHAR(256) COMMENT '站点名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_NAVIGATION COMMENT '导航';

/*==============================================================*/
/* Table: C1_NOTICE                                             */
/*==============================================================*/
CREATE TABLE C1_NOTICE
(
   ORG_ID               VARCHAR(36) NOT NULL COMMENT '组织标识',
   ORG_CODE             VARCHAR(256) COMMENT '组织代码',
   ORG_NAME             VARCHAR(256) COMMENT '组织名称',
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   TITLE                VARCHAR(256) COMMENT '标题',
   RELEASE_DATE         DATE COMMENT '发布日期',
   CONTENT              TEXT COMMENT '正文',
   TYPE_ID              VARCHAR(36) COMMENT '类别标识',
   TYPE_CODE            VARCHAR(256) COMMENT '类别代码',
   TYPE_NAME            VARCHAR(256) COMMENT '类别名称',
   PRIMARY KEY (ID, ORG_ID)
);

ALTER TABLE C1_NOTICE COMMENT '公告';

/*==============================================================*/
/* Table: C1_OPERATE_LOG                                        */
/*==============================================================*/
CREATE TABLE C1_OPERATE_LOG
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   TYPE_ID              VARCHAR(36) COMMENT '字典.类别标识',
   USER_ID              VARCHAR(36) NOT NULL COMMENT '用户标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   MESSAGE              VARCHAR(1024) COMMENT '消息',
   BUSINESS_CODE        VARCHAR(256) COMMENT '业务代码',
   VALUE                VARCHAR(2048) COMMENT '值',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_OPERATE_LOG COMMENT '操作日志';

/*==============================================================*/
/* Table: C1_ORG                                                */
/*==============================================================*/
CREATE TABLE C1_ORG
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   PARENT_ID            VARCHAR(36) COMMENT '父级标识',
   PARENT_CODE          VARCHAR(256) COMMENT '父级代码',
   PARENT_NAME          VARCHAR(256) COMMENT '父级名称',
   TREE_ID              VARCHAR(1024) COMMENT '树标识',
   TREE_CODE            VARCHAR(1024) COMMENT '树代码',
   TREE_NAME            VARCHAR(1024) COMMENT '树名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_ORG COMMENT '组织';

/*==============================================================*/
/* Table: C1_PE_VA_RU_TRANSACT_MODE                             */
/*==============================================================*/
CREATE TABLE C1_PE_VA_RU_TRANSACT_MODE
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   PERMISSION_ID        VARCHAR(36) COMMENT '许可标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   ORDER_NUM            BIGINT DEFAULT 0 COMMENT '排序数',
   VALIDATE_RULE_ID     VARCHAR(36) COMMENT '字典.验证规则标识',
   VALIDATE_RULE_CODE   VARCHAR(256) COMMENT '字典.验证规则代码',
   VALIDATE_RULE_NAME   VARCHAR(256) COMMENT '字典.验证规则名称',
   VALIDATE_SUCCESS_ID  VARCHAR(36) COMMENT '字典.验证成功标识',
   VALIDATE_SUCCESS_CODE VARCHAR(256) COMMENT '字典.验证成功代码',
   VALIDATE_SUCCESS_NAME VARCHAR(256) COMMENT '字典.验证成功名称',
   VALIDATE_ERROR_ID    VARCHAR(36) COMMENT '字典.验证失败标识',
   VALIDATE_ERROR_CODE  VARCHAR(256) COMMENT '字典.验证失败代码',
   VALIDATE_ERROR_NAME  VARCHAR(256) COMMENT '字典.验证失败名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_PE_VA_RU_TRANSACT_MODE COMMENT '许可、验证规则与处理模式';

/*==============================================================*/
/* Table: C1_PERMISSION                                         */
/*==============================================================*/
CREATE TABLE C1_PERMISSION
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   ORDER_NUM            BIGINT NOT NULL DEFAULT 0 COMMENT '排序数',
   TYPE_ID              VARCHAR(36) COMMENT '类别标识',
   TYPE_CODE            VARCHAR(256) COMMENT '类别代码',
   TYPE_NAME            VARCHAR(256) COMMENT '类别名称',
   SITE_ID              VARCHAR(36) COMMENT '站点标识',
   SITE_CODE            VARCHAR(256) COMMENT '站点代码',
   SITE_NAME            VARCHAR(256) COMMENT '站点名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_PERMISSION COMMENT '许可';

/*==============================================================*/
/* Table: C1_PERMISSION_ORG                                     */
/*==============================================================*/
CREATE TABLE C1_PERMISSION_ORG
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   ORG_ID               VARCHAR(36) COMMENT '组织标识',
   PERMISSION_ID        VARCHAR(36) COMMENT '许可标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_PERMISSION_ORG COMMENT '许可与组织';

/*==============================================================*/
/* Table: C1_ROLE                                               */
/*==============================================================*/
CREATE TABLE C1_ROLE
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   DEPARTMENT_ID        VARCHAR(36) COMMENT '部门标识',
   ORG_ID               VARCHAR(36) COMMENT '组织标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   ORDER_NUM            BIGINT NOT NULL DEFAULT 0 COMMENT '排序数',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_ROLE COMMENT '角色';

/*==============================================================*/
/* Table: C1_ROLE_PERMISSION                                    */
/*==============================================================*/
CREATE TABLE C1_ROLE_PERMISSION
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   ROLE_ID              VARCHAR(36) NOT NULL COMMENT '角色标识',
   PERMISSION_ID        VARCHAR(36) NOT NULL COMMENT '许可标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_ROLE_PERMISSION COMMENT '角色与许可';

/*==============================================================*/
/* Table: C1_ROLE_USER                                          */
/*==============================================================*/
CREATE TABLE C1_ROLE_USER
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   USER_ID              VARCHAR(36) COMMENT '用户标识',
   ROLE_ID              VARCHAR(36) COMMENT '角色标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_ROLE_USER COMMENT '角色与用户';

/*==============================================================*/
/* Table: C1_SITE                                               */
/*==============================================================*/
CREATE TABLE C1_SITE
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   STYLE_ID             VARCHAR(36) NOT NULL COMMENT '字典.样式标识',
   PROJECT_ID           VARCHAR(36) NOT NULL COMMENT '字典.工程标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   PROJECT_CODE         VARCHAR(256) COMMENT '工程编号',
   PROJECT_NAME         VARCHAR(256) COMMENT '工程名称',
   STYLE_CODE           VARCHAR(256) COMMENT '样式代码',
   STYLE_NAME           VARCHAR(256) COMMENT '样式名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_SITE COMMENT '站点';

/*==============================================================*/
/* Table: C1_SITE_URL                                           */
/*==============================================================*/
CREATE TABLE C1_SITE_URL
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   SITE_ID              VARCHAR(36) NOT NULL COMMENT '站点标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   ORDER_NUM            BIGINT NOT NULL DEFAULT 0 COMMENT '排序数',
   SITE_CODE            VARCHAR(256) COMMENT '站点代码',
   SITE_NAME            VARCHAR(256) COMMENT '站点名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_SITE_URL COMMENT '站点位置';

/*==============================================================*/
/* Table: C1_US_DEPARTMENT_TRANSLATE                            */
/*==============================================================*/
CREATE TABLE C1_US_DEPARTMENT_TRANSLATE
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   USER_ID              VARCHAR(36) COMMENT '用户标识',
   DEPARTMENT_ID        VARCHAR(36) COMMENT '部门标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_US_DEPARTMENT_TRANSLATE COMMENT '用户部门调动';

/*==============================================================*/
/* Table: C1_US_GR_BUSINESS_AREA                                */
/*==============================================================*/
CREATE TABLE C1_US_GR_BUSINESS_AREA
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   BUSINESS_AREA_ID     VARCHAR(36) NOT NULL COMMENT '字典.业务范围标识',
   USER_GROUP_ID        VARCHAR(36) NOT NULL COMMENT '用户组标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_US_GR_BUSINESS_AREA COMMENT '用户组与业务范围';

/*==============================================================*/
/* Table: C1_US_GROUP_ORG                                       */
/*==============================================================*/
CREATE TABLE C1_US_GROUP_ORG
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   USER_GROUP_ID        VARCHAR(36) NOT NULL COMMENT '用户组标识',
   ORG_ID               VARCHAR(36) NOT NULL COMMENT '组织标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_US_GROUP_ORG COMMENT '用户组与组织';

/*==============================================================*/
/* Table: C1_US_GROUP_ROLE                                      */
/*==============================================================*/
CREATE TABLE C1_US_GROUP_ROLE
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   ROLE_ID              VARCHAR(36) NOT NULL COMMENT '角色标识',
   USER_GROUP_ID        VARCHAR(36) NOT NULL COMMENT '用户组标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_US_GROUP_ROLE COMMENT '用户组与角色';

/*==============================================================*/
/* Table: C1_US_GROUP_USER                                      */
/*==============================================================*/
CREATE TABLE C1_US_GROUP_USER
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   USER_GROUP_ID        VARCHAR(36) NOT NULL COMMENT '用户组标识',
   USER_ID              VARCHAR(36) NOT NULL COMMENT '用户标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_US_GROUP_USER COMMENT '用户组与用户';

/*==============================================================*/
/* Table: C1_US_ORG_TRANSLATE                                   */
/*==============================================================*/
CREATE TABLE C1_US_ORG_TRANSLATE
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   ORG_ID               VARCHAR(36) NOT NULL COMMENT '组织标识',
   USER_ID              VARCHAR(36) NOT NULL COMMENT '用户标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_US_ORG_TRANSLATE COMMENT '用户组织调动';

/*==============================================================*/
/* Table: C1_USER                                               */
/*==============================================================*/
CREATE TABLE C1_USER
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   DEPARTMENT_ID        VARCHAR(36) COMMENT '部门标识',
   ORG_ID               VARCHAR(36) COMMENT '组织标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) COMMENT '代码',
   NAME                 VARCHAR(256) COMMENT '名称',
   NO                   VARCHAR(256) COMMENT '编号',
   PASSWORD             VARCHAR(256) COMMENT '密码',
   EMAIL                VARCHAR(256) COMMENT '邮箱',
   AGE                  INT COMMENT '年龄',
   INTRO                TEXT COMMENT '简介',
   MOBILE_PHONE         VARCHAR(256) COMMENT '手机',
   LOCUS                VARCHAR(256) COMMENT '现居住地',
   COMPANY_NAME         VARCHAR(256) COMMENT '工作单位',
   ADDRESS              VARCHAR(256) COMMENT '地址',
   TELEPHONE            VARCHAR(256) COMMENT '联系电话',
   BIRTHDAY             DATE COMMENT '出生日期',
   CALL_NAME            VARCHAR(256) COMMENT '昵称',
   PROFESSION_ID        VARCHAR(36) COMMENT '职业标识',
   PROFESSION_CODE      VARCHAR(256) COMMENT '职业代码',
   PROFESSION_NAME      VARCHAR(256) COMMENT '职业名称',
   DISTRICT_ID          VARCHAR(36) COMMENT '区标识',
   DISTRICT_CODE        VARCHAR(256) COMMENT '区代码',
   DISTRICT_NAME        VARCHAR(256) COMMENT '区名称',
   COUNTRY_ID           VARCHAR(36) COMMENT '国家标识',
   COUNTRY_CODE         VARCHAR(256) COMMENT '国家代码',
   COUNTRY_NAME         VARCHAR(256) COMMENT '国家名称',
   CITY_ID              VARCHAR(36) COMMENT '市标识',
   CITY_CODE            VARCHAR(256) COMMENT '市代码',
   CITY_NAME            VARCHAR(256) COMMENT '市名称',
   MARRY_STATE_ID       VARCHAR(36) COMMENT '婚姻状态标识',
   MARRY_STATE_CODE     VARCHAR(256) COMMENT '婚姻状态代码',
   MARRY_STATE_NAME     VARCHAR(256) COMMENT '婚姻状态名称',
   FOLK_ID              VARCHAR(36) COMMENT '民族标识',
   FOLK_CODE            VARCHAR(256) COMMENT '民族代码',
   FOLK_NAME            VARCHAR(256) COMMENT '民族名称',
   DIPLOMA_ID           VARCHAR(36) COMMENT '学历/文凭标识',
   DIPLOMA_CODE         VARCHAR(256) COMMENT '学历/文凭代码',
   DIPLOMA_NAME         VARCHAR(256) COMMENT '学历/文凭名称',
   PROVINCE_ID          VARCHAR(36) COMMENT '省标识',
   PROVINCE_CODE        VARCHAR(256) COMMENT '省代码',
   PROVINCE_NAME        VARCHAR(256) COMMENT '省名称',
   FLAG_ID              VARCHAR(36) COMMENT '标记标识',
   FLAG_CODE            VARCHAR(256) COMMENT '标记代码',
   FLAG_NAME            VARCHAR(256) COMMENT '标记名称',
   SEX_ID               VARCHAR(36) COMMENT '性别标识',
   SEX_CODE             VARCHAR(256) COMMENT '性别代码',
   SEX_NAME             VARCHAR(256) COMMENT '性别名称',
   ORG_CODE             VARCHAR(256) COMMENT '组织代码',
   ORG_NAME             VARCHAR(256) COMMENT '组织名称',
   DEPARTMENT_CODE      VARCHAR(256) COMMENT '部门代码',
   DEPARTMENT_NAME      VARCHAR(256) COMMENT '部门名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_USER COMMENT '用户';

/*==============================================================*/
/* Table: C1_USER_ACTIVATION                                    */
/*==============================================================*/
CREATE TABLE C1_USER_ACTIVATION
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   USER_ID              VARCHAR(36) NOT NULL COMMENT '用户标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   VERIFICATION_CODE    VARCHAR(256) COMMENT '验证码',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_USER_ACTIVATION COMMENT '用户激活';

/*==============================================================*/
/* Table: C1_USER_GROUP                                         */
/*==============================================================*/
CREATE TABLE C1_USER_GROUP
(
   ID                   VARCHAR(36) NOT NULL COMMENT '标识',
   TYPE_ID              VARCHAR(36) NOT NULL COMMENT '字典.类型标识',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATED_BY_ID        VARCHAR(36) COMMENT '创建人标识',
   CREATED_BY_CODE      VARCHAR(256) COMMENT '创建人代码',
   CREATED_BY_NAME      VARCHAR(256) COMMENT '创建人名称',
   CREATED_DATE         DATE COMMENT '创建时间',
   LAST_MODIFIED_BY_ID  VARCHAR(36) COMMENT '最后修改人标识',
   LAST_MODIFIED_BY_CODE VARCHAR(256) COMMENT '最后修改人代码',
   LAST_MODIFIED_BY_NAME VARCHAR(256) COMMENT '最后修改人名称',
   LAST_MODIFIED_DATE   DATE COMMENT '最后修改时间',
   STATE_ID             VARCHAR(36) COMMENT '状态标识',
   STATE_CODE           VARCHAR(256) COMMENT '状态代码',
   STATE_NAME           VARCHAR(256) COMMENT '状态名称',
   DATA_STATE_ID        VARCHAR(36) COMMENT '数据状态标识',
   DATA_STATE_CODE      VARCHAR(256) COMMENT '数据状态代码',
   DATA_STATE_NAME      VARCHAR(256) COMMENT '数据状态名称',
   VERSION_INT          INT DEFAULT 0 COMMENT '版本',
   CODE                 VARCHAR(256) NOT NULL COMMENT '代码',
   NAME                 VARCHAR(256) NOT NULL COMMENT '名称',
   ORDER_NUM            BIGINT NOT NULL DEFAULT 0 COMMENT '排序数',
   BUSINESS_CODE        VARCHAR(256) COMMENT '业务代码',
   PARENT_ID            VARCHAR(36) COMMENT '父级标识',
   PARENT_CODE          VARCHAR(256) COMMENT '父级代码',
   PARENT_NAME          VARCHAR(256) COMMENT '父级名称',
   TREE_ID              VARCHAR(1024) COMMENT '树标识',
   TREE_CODE            VARCHAR(1024) COMMENT '树代码',
   TREE_NAME            VARCHAR(1024) COMMENT '树名称',
   PRIMARY KEY (ID)
);

ALTER TABLE C1_USER_GROUP COMMENT '用户组';

ALTER TABLE C1_DEPARTMENT ADD CONSTRAINT FK_ORG_ID FOREIGN KEY (ORG_ID)
      REFERENCES C1_ORG (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_DICTIONARY ADD CONSTRAINT FK_TYPE_ID FOREIGN KEY (TYPE_ID)
      REFERENCES C1_DICTIONARY_TYPE (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_DICTIONARY_ITEM ADD CONSTRAINT FK_DICTIONARY_ID FOREIGN KEY (DICTIONARY_ID)
      REFERENCES C1_DICTIONARY (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_ENTITY_DEFAULT ADD CONSTRAINT FK_TYPE_ID FOREIGN KEY (TYPE_ID)
      REFERENCES C1_DICTIONARY (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_ENTITY_EXTEND ADD CONSTRAINT FK_TYPE_ID FOREIGN KEY (TYPE_ID)
      REFERENCES C1_DICTIONARY (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_ENTITY_FILE ADD CONSTRAINT FK_TYPE_ID FOREIGN KEY (TYPE_ID)
      REFERENCES C1_DICTIONARY (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_ENTITY_LOG ADD CONSTRAINT FK_TYPE_ID FOREIGN KEY (TYPE_ID)
      REFERENCES C1_DICTIONARY (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_ENTITY_LOG ADD CONSTRAINT FK_USER_ID FOREIGN KEY (USER_ID)
      REFERENCES C1_USER (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_OPERATE_LOG ADD CONSTRAINT FK_TYPE_ID FOREIGN KEY (TYPE_ID)
      REFERENCES C1_DICTIONARY (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_OPERATE_LOG ADD CONSTRAINT FK_USER_ID FOREIGN KEY (USER_ID)
      REFERENCES C1_USER (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_PE_VA_RU_TRANSACT_MODE ADD CONSTRAINT FK_PERMISSION_ID FOREIGN KEY (PERMISSION_ID)
      REFERENCES C1_PERMISSION (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_PERMISSION_ORG ADD CONSTRAINT FK_ORG_ID FOREIGN KEY (ORG_ID)
      REFERENCES C1_ORG (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_PERMISSION_ORG ADD CONSTRAINT FK_PERMISSION_ID FOREIGN KEY (PERMISSION_ID)
      REFERENCES C1_PERMISSION (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_ROLE ADD CONSTRAINT FK_DEPARTMENT_ID FOREIGN KEY (DEPARTMENT_ID)
      REFERENCES C1_DEPARTMENT (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_ROLE ADD CONSTRAINT FK_ORG_ID FOREIGN KEY (ORG_ID)
      REFERENCES C1_ORG (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_ROLE_PERMISSION ADD CONSTRAINT FK_PERMISSION_ID FOREIGN KEY (PERMISSION_ID)
      REFERENCES C1_PERMISSION (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_ROLE_PERMISSION ADD CONSTRAINT FK_ROLE_ID FOREIGN KEY (ROLE_ID)
      REFERENCES C1_ROLE (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_ROLE_USER ADD CONSTRAINT FK_ROLE_ID FOREIGN KEY (ROLE_ID)
      REFERENCES C1_ROLE (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_ROLE_USER ADD CONSTRAINT FK_USER_ID FOREIGN KEY (USER_ID)
      REFERENCES C1_USER (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_SITE ADD CONSTRAINT FK_PROJECT_ID FOREIGN KEY (PROJECT_ID)
      REFERENCES C1_DICTIONARY (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_SITE ADD CONSTRAINT FK_STYLE_ID FOREIGN KEY (STYLE_ID)
      REFERENCES C1_DICTIONARY (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_SITE_URL ADD CONSTRAINT FK_SITE_ID FOREIGN KEY (SITE_ID)
      REFERENCES C1_SITE (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_DEPARTMENT_TRANSLATE ADD CONSTRAINT FK_DEPARTMENT_ID FOREIGN KEY (DEPARTMENT_ID)
      REFERENCES C1_DEPARTMENT (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_DEPARTMENT_TRANSLATE ADD CONSTRAINT FK_USER_ID FOREIGN KEY (USER_ID)
      REFERENCES C1_USER (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_GR_BUSINESS_AREA ADD CONSTRAINT FK_BUSINESS_AREA_ID FOREIGN KEY (BUSINESS_AREA_ID)
      REFERENCES C1_DICTIONARY (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_GR_BUSINESS_AREA ADD CONSTRAINT FK_USER_GROUP_ID FOREIGN KEY (USER_GROUP_ID)
      REFERENCES C1_USER_GROUP (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_GROUP_ORG ADD CONSTRAINT FK_ORG_ID FOREIGN KEY (ORG_ID)
      REFERENCES C1_ORG (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_GROUP_ORG ADD CONSTRAINT FK_USER_GROUP_ID FOREIGN KEY (USER_GROUP_ID)
      REFERENCES C1_USER_GROUP (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_GROUP_ROLE ADD CONSTRAINT FK_ROLE_ID FOREIGN KEY (ROLE_ID)
      REFERENCES C1_ROLE (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_GROUP_ROLE ADD CONSTRAINT FK_USER_GROUP_ID FOREIGN KEY (USER_GROUP_ID)
      REFERENCES C1_USER_GROUP (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_GROUP_USER ADD CONSTRAINT FK_USER_GROUP_ID FOREIGN KEY (USER_GROUP_ID)
      REFERENCES C1_USER_GROUP (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_GROUP_USER ADD CONSTRAINT FK_USER_ID FOREIGN KEY (USER_ID)
      REFERENCES C1_USER (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_ORG_TRANSLATE ADD CONSTRAINT FK_ORG_ID FOREIGN KEY (ORG_ID)
      REFERENCES C1_ORG (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_US_ORG_TRANSLATE ADD CONSTRAINT FK_USER_ID FOREIGN KEY (USER_ID)
      REFERENCES C1_USER (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_USER ADD CONSTRAINT FK_DEPARTMENT_ID FOREIGN KEY (DEPARTMENT_ID)
      REFERENCES C1_DEPARTMENT (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_USER ADD CONSTRAINT FK_ORG_ID FOREIGN KEY (ORG_ID)
      REFERENCES C1_ORG (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_USER_ACTIVATION ADD CONSTRAINT FK_USER_ID FOREIGN KEY (USER_ID)
      REFERENCES C1_USER (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

ALTER TABLE C1_USER_GROUP ADD CONSTRAINT FK_TYPE_ID FOREIGN KEY (TYPE_ID)
      REFERENCES C1_DICTIONARY (ID) ON DELETE CASCADE ON UPDATE RESTRICT;

