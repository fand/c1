/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     2013/9/24 10:28:47                           */
/*==============================================================*/


ALTER TABLE "FD_BRAND"
   DROP CONSTRAINT FK_FD_BRAND_TYPE_ID_FD_DICTI;

DROP INDEX "FD_BRAND_TYPE_ID_FK_1";

DROP TABLE "FD_BRAND" CASCADE CONSTRAINTS;

/*==============================================================*/
/* Table: "FD_BRAND"                                            */
/*==============================================================*/
CREATE TABLE "FD_BRAND" 
(
   "ID"                 VARCHAR2(36)         NOT NULL,
   "TYPE_ID"            VARCHAR2(36),
   "CODE"               VARCHAR2(256)        NOT NULL,
   "NAME"               VARCHAR2(256)        NOT NULL,
   "WEIGHT"             INTEGER              NOT NULL,
   "ORDER_NUM"          INTEGER              DEFAULT 0 NOT NULL,
   "CONTENT"            VARCHAR2(2048),
   "BEGIN_DA_TI"        DATE                 DEFAULT SYSDATE NOT NULL,
   "END_DA_TI"          DATE                 DEFAULT 'TO_DATE(''99991231'',''yyyymmdd'')' NOT NULL,
   CONSTRAINT PK_FD_BRAND PRIMARY KEY ("ID")
);

COMMENT ON TABLE "FD_BRAND" IS
'广告';

COMMENT ON COLUMN "FD_BRAND"."ID" IS
'标识';

COMMENT ON COLUMN "FD_BRAND"."TYPE_ID" IS
'广告类型';

COMMENT ON COLUMN "FD_BRAND"."CODE" IS
'编号';

COMMENT ON COLUMN "FD_BRAND"."NAME" IS
'名称';

COMMENT ON COLUMN "FD_BRAND"."WEIGHT" IS
'权重';

COMMENT ON COLUMN "FD_BRAND"."ORDER_NUM" IS
'排序号';

COMMENT ON COLUMN "FD_BRAND"."CONTENT" IS
'内容';

COMMENT ON COLUMN "FD_BRAND"."BEGIN_DA_TI" IS
'开始时间';

COMMENT ON COLUMN "FD_BRAND"."END_DA_TI" IS
'结束时间';

/*==============================================================*/
/* Index: "FD_BRAND_TYPE_ID_FK_1"                               */
/*==============================================================*/
CREATE INDEX "FD_BRAND_TYPE_ID_FK_1" ON "FD_BRAND" (
   "TYPE_ID" ASC
);

