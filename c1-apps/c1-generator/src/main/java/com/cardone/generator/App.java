package com.cardone.generator;

import org.apache.commons.lang3.*;
import org.springframework.context.*;
import org.springframework.context.support.*;
import org.springframework.util.*;

import java.util.*;

/**
 * 主执行端
 *
 * @author yaohaitao
 */
public class App {
    /**
     * 主方法
     *
     * @param args 参数
     * @throws Exception 异常
     */
    @SuppressWarnings("resource")
    public static void main(final String[] args) throws Exception {
        ApplicationContext applicationContext;

        if (ArrayUtils.isNotEmpty(args)) {
            applicationContext = new FileSystemXmlApplicationContext(args);
        } else {
            applicationContext = new ClassPathXmlApplicationContext("classpath*:**/applicationContext.xml");
        }

        final Map<String, PoMapperFactory> poMapperFactoryMap = applicationContext.getBeansOfType(PoMapperFactory.class);

        if (CollectionUtils.isEmpty(poMapperFactoryMap)) {
            return;
        }

        for (final PoMapperFactory poMapperFactory : poMapperFactoryMap.values()) {
            poMapperFactory.run();
        }
    }
}