package com.cardone.generator.mapper;

import lombok.*;
import lombok.experimental.*;

import java.util.*;

/**
 * 模块映射
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Accessors(chain = true)
public class ModuleMapper implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -2617824744116728750L;

    /**
     * 业务映射
     */
    private List<BusinessMapper> businessMapperList;

    /**
     * 代码
     */
    private String code;

    /**
     * 名称
     */
    private String name;
}
