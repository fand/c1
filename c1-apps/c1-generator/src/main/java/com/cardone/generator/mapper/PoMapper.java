package com.cardone.generator.mapper;

import lombok.*;
import lombok.experimental.*;

import java.util.*;

/**
 * bean映射
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Accessors(chain = true)
public class PoMapper implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 1495796601538631576L;

    /**
     * 代码
     */
    private String code;

    /**
     * 字段映射
     */
    private Map<String, FieldMapper> fieldMapperMap;

    /**
     * 模块代码
     */
    private String moduleCode;

    /**
     * 名称
     */
    private String name;
    private String refGeneration;
    private String remarks;
    private String selfReferencingColName;
    private String tableCat;
    private String tableName;
    private String tableSchem;
    private String tableType;
    private String typeCat;
    private String typeName;
    private String typeSchem;
}
