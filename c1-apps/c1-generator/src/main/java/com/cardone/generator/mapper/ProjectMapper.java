package com.cardone.generator.mapper;

import lombok.*;
import lombok.experimental.*;

import java.util.*;

/**
 * 项目映射
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Accessors(chain = true)
public class ProjectMapper implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 2910623076775675790L;

    /**
     * 代码
     */
    private String code;

    /**
     * 模块映射
     */
    private List<ModuleMapper> moduleMapperList;

    /**
     * 名称
     */
    private String name;

    /**
     * 包代码
     */
    private String packageCode;
}
