package com.cardone.generator.mapper;

import lombok.*;
import lombok.experimental.*;

import java.util.*;

/**
 * Entity映射
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Accessors(chain = true)
public class EntityMapper extends PoMapper {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -6895672040505626361L;

    /**
     * 索引集合
     */
    private List<String> indexList;

    /**
     * 生成Controller层代码
     */
    private boolean markController = true;

    /**
     * 生成Service层代码
     */
    private boolean markService = true;

    /**
     * 表代码
     */
    private String tableCode;
}
