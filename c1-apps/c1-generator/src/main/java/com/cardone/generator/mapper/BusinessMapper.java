package com.cardone.generator.mapper;

import lombok.*;
import lombok.experimental.*;

/**
 * @author yaohaitao
 */
@Getter
@Setter
@Accessors(chain = true)
public class BusinessMapper implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 815257592663119234L;

    /**
     * 代码
     */
    private String code;

    /**
     * Entity映射
     */
    private EntityMapper entityMapper;

    /**
     * 名称
     */
    private String name;

    /**
     * 表名称
     */
    private String tableName;
}
