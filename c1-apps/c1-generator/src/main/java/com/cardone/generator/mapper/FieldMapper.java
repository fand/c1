package com.cardone.generator.mapper;

import lombok.*;
import lombok.experimental.*;

/**
 * 字段映射
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Accessors(chain = true)
public class FieldMapper implements java.io.Serializable {
    /**
     * 版本号
     */
    private static final long serialVersionUID = -3060242666159354649L;

    private Integer charOctetLength;

    /**
     * 代码
     */
    private String code;

    private String columnDef;

    private String columnName;
    private Integer columnSize;
    private Integer dataType;
    private Integer decimalDigits;
    private String isAutoincrement;
    private String isGeneratedcolumn;
    private String isNullable;
    private Short keySeq;
    /**
     * 名称
     */
    private String name;
    private String namePascalCase;
    private Integer nullable;
    private Integer numPrecRadix;
    private Integer ordinalPosition;
    private String pkName;
    private String remarks;
    private String scopeCatalog;
    private String scopeSchema;
    private String scopeTable;
    private String sourceDataType;
    private Integer sqlDataType;
    private Integer sqlDatetimeSub;
    private String tableCat;
    private String tableName;
    private String tableSchem;
    /**
     * 类型代码
     */
    private String typeCode;

    private String typeName;
}
