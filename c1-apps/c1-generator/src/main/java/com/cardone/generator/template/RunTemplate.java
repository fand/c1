package com.cardone.generator.template;

import com.cardone.generator.mapper.*;

import java.util.*;

/**
 * 运行模板
 *
 * @author yaohaitao
 */
public interface RunTemplate {
    /**
     * 执行
     *
     * @throws Exception
     */
    void run() throws Exception;

    /**
     * 获取实体映射
     *
     * @return
     */
    List<PoMapper> findListPoMapper();
}