<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.dao;

import java.util.List;
import java.util.Map;

import com.cardone.common.dto.PaginationDto;
<#if entityMapper??>
import ${packageCode}.${moduleMapperCode}.dto.${businessCode}Dto;
</#if>
import com.cardone.common.dao.SimpleDao;

/**
* ${businessName!}
*
* @author ${author!'yaohaitao'}
*
*/
<#if entityMapper??>
public interface ${businessCode}Dao extends SimpleDao
<${businessCode}Dto> {
<#else>
    public interface ${businessCode}Dao {
</#if>
    /**
    * sql标识
    *
    * @author yaohaitao
    *
    */
    public enum SqlIds {
    /**
    * 查询
    */
    findByCodes;

    /**
    * 根目录
    */
    public static final String ROOT = "/${projectCode!}/${moduleMapperCode}/${businessCode?uncap_first}/";

    /**
    * 标识
    */
    private final String id;

    /**
    * sql标识
    */
    private SqlIds() {
    this.id = SqlIds.ROOT + this.name();
    }

    /**
    * 获取
    *
    * @return sql标识
    */
    public String id() {
    return this.id;
    }
    }
    }