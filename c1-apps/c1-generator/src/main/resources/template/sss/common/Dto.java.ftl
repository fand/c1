<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import ${packageCode}.${moduleMapperCode}.po.${businessCode};

/**
* ${businessName!}
*
* @author ${author!'yaohaitao'}
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class ${businessCode}Dto extends ${businessCode} {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = ${serialVersionUID!'1'}L;
}