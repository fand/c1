<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.po;

<#list entityMapper.fieldMapperMap?values as fieldMapper>
    <#if fieldMapper.typeCode == 'Date'>
    import java.util.Date;

        <#break>
    </#if>
</#list>
<#list entityMapper.fieldMapperMap?values as fieldMapper>
    <#if fieldMapper.pkName! != ''>
    import org.springframework.data.annotation.Id;

        <#break>
    </#if>
</#list>

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* ${businessName!}
*
* @author ${author!'yaohaitao'}
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class ${businessCode} extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = ${serialVersionUID!'1'}L;
<#list entityMapper.fieldMapperMap?values as fieldMapper>
    <#if !StringUtils.contains(ignoreProperties, (','+fieldMapper.code+','))>

    /**
    * <@defaultIfBlank str="${fieldMapper.remarks!}" defaultSt="${fieldMapper.code!}"/>
    */
        <#if fieldMapper.pkName! != ''>
        @Id
        </#if>
    private ${fieldMapper.typeCode!'String'} ${fieldMapper.code};
    </#if>
</#list>
}