<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.vo;

<#if entityMapper??>
    <#list entityMapper.fieldMapperMap?values as fieldMapper>
        <#if fieldMapper.typeCode == 'Date'>
        import java.util.Date;

            <#break>
        </#if>
    </#list>
    <#list entityMapper.fieldMapperMap?values as fieldMapper>
        <#if fieldMapper.pkName! != ''>
        import org.springframework.data.annotation.Id;

            <#break>
        </#if>
    </#list>
</#if>

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

/**
* ${businessName!}
*
* @author ${author!'yaohaitao'}
*
*/
@Getter
@Setter
@Accessors(chain = true)
public class ${businessCode}Vo implements java.io.Serializable {
/**
* 版本号
*/
private static final long serialVersionUID = ${serialVersionUID!'1'}L;
<#if entityMapper??>
    <#list entityMapper.fieldMapperMap?values as fieldMapper>

    /**
    * <@defaultIfBlank str="${fieldMapper.remarks!}" defaultSt="${fieldMapper.code!}"/>
    */
    private ${fieldMapper.typeCode!'String'} ${fieldMapper.code};
    </#list>
</#if>
}