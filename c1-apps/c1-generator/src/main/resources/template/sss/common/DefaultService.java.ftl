<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;


import lombok.Getter;


import lombok.Setter;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import com.cardone.common.dao.SimpleDao;
import com.cardone.common.service.SimpleDefaultService;

import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
import com.cardone.context.Attributes;
import com.cardone.context.ContextHolder;
import com.cardone.common.util.EntityUtils;
<#if entityMapper??>
import ${packageCode}.${moduleMapperCode}.po.${businessCode};
import ${packageCode}.${moduleMapperCode}.dto.${businessCode}Dto;
import ${packageCode}.${moduleMapperCode}.dao.${businessCode}Dao;
</#if>

/**
* ${businessName}服务
*
* @author ${author!'yaohaitao'}
*
*/
@Getter
@Setter
@Transactional(readOnly = true)
<#if entityMapper??>
public class ${businessCode}DefaultService extends SimpleDefaultService
<${businessCode}Dto> implements ${businessCode}Service {
    @Override
    public ${businessCode}Dao getDao() {
    return ContextHolder.getBean(${businessCode}Dao.class);
    }

    @Override
    @CacheEvict(value = ${businessCode}Service.BEAN_ID, allEntries = true)
    public void init() {
        this.init(${businessCode}.class.getName() + ".initList");
    }
<#else>
    public class ${businessCode}DefaultService implements ${businessCode}Service {
</#if>
    }