<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
<#if entityMapper??>
import ${packageCode}.${moduleMapperCode}.dto.${businessCode}Dto;
</#if>
import ${packageCode}.${moduleMapperCode}.service.${businessCode}Service;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* ${businessName!}
*
* @author ${author!'yaohaitao'}
*/
@Setter
@Slf4j
@Controller
public class ${businessCode}Controller {
<#if entityMapper??>
/**
* ${businessName!}：删除
*
* @param delete${businessCode}
*            ${businessName!}对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/${moduleMapperCode}/${businessCode?uncap_first}/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final ${businessCode}Dto delete${businessCode}) {
    return () -> {
    ContextHolder.getBean(${businessCode}Service.class).deleteByIds(delete${businessCode});

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * ${businessName!}：编辑
    *
    * @param find${businessCode}
    *            ${businessName!}对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/${moduleMapperCode}/${businessCode?uncap_first}/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final ${businessCode}Dto find${businessCode}, final Model model) {
    return () -> {
    final ${businessCode}Dto ${businessCode?uncap_first} = ContextHolder.getBean(${businessCode}
    Service.class).findById(${businessCode}Dto.class, find${businessCode});

    return ReturnDataUtils.newMap("${businessCode?uncap_first}", ${businessCode?uncap_first});
    };
    }

    /**
    * ${businessName!}：分页查询
    *
    * @param pagination${businessCode}
    *            ${businessName!}对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/${moduleMapperCode}/${businessCode?uncap_first}/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final ${businessCode}Dto pagination${businessCode}) {
    return () -> {
    final PaginationDto
<${businessCode}Dto> ${businessCode?uncap_first}Pagination = ContextHolder.getBean(${businessCode}
    Service.class).paginationByLikeCode(${businessCode}Dto.class, pagination${businessCode});

    return ReturnDataUtils.newMap("${businessCode?uncap_first}Pagination", ${businessCode?uncap_first}Pagination);
    };
    }

    /**
    * ${businessName!}：分页查询
    *
    * @param pagination${businessCode}
    *            ${businessName!}对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/${moduleMapperCode}/${businessCode?uncap_first}/pagination.html" })
    public void paginationHtml(final ${businessCode}Dto pagination${businessCode}, final Model model) {
    final PaginationDto
<${businessCode}Dto> ${businessCode?uncap_first}Pagination = ContextHolder.getBean(${businessCode}
    Service.class).paginationByLikeCode(${businessCode}Dto.class, pagination${businessCode});

    model.addAttribute("${businessCode?uncap_first}Pagination", ${businessCode?uncap_first}Pagination);
    }

    /**
    * ${businessName!}：分页查询
    *
    * @param find${businessCode}
    *            ${businessName!}对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/${moduleMapperCode}/${businessCode?uncap_first}/edit.html" })
    public void editHtml(final ${businessCode}Dto find${businessCode}, final Model model) {
    final ${businessCode}Dto ${businessCode?uncap_first} = ContextHolder.getBean(${businessCode}
    Service.class).findById(${businessCode}Dto.class, find${businessCode});

    model.addAttribute("${businessCode?uncap_first}", ${businessCode?uncap_first});
    }

    /**
    * ${businessName!}：查询
    *
    * @param findList${businessCode}
    *            ${businessName!}对象
    * @return ${businessName!}对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/${moduleMapperCode}/${businessCode?uncap_first}/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final ${businessCode}Dto findList${businessCode}) {
    return () -> {
    final List<${businessCode}Dto> ${businessCode?uncap_first}List = ContextHolder.getBean(${businessCode}
    Service.class).findListByLikeCode(${businessCode}Dto.class, findList${businessCode});

    return ReturnDataUtils.newMap("${businessCode?uncap_first}", ${businessCode?uncap_first}List);
    };
    }

    /**
    * ${businessName!}：保存
    *
    * @param save${businessCode}
    *            ${businessName!}对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/${moduleMapperCode}/${businessCode?uncap_first}/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final ${businessCode}Dto save${businessCode}) {
    return () -> {
    ContextHolder.getBean(${businessCode}Service.class).saveByIdOrCode(${businessCode}Dto.class, save${businessCode});

    return ReturnDataUtils.newMap();
    };
    }
</#if>
    }
