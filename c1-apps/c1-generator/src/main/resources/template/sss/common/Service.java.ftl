<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.service;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import com.cardone.common.service.SimpleService;


import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
<#if entityMapper??>
import ${packageCode}.${moduleMapperCode}.dto.${businessCode}Dto;
</#if>

/**
* ${businessName}服务
*
* @author ${author!'yaohaitao'}
*/
<#if entityMapper??>
public interface ${businessCode}Service extends SimpleService
<${businessCode}Dto> {
<#else>
    public interface ${businessCode}Service {
</#if>
    /**
    * spring bean id
    */
    String BEAN_ID = "${packageCode}.${moduleMapperCode}.service.${businessCode}Service";
    }