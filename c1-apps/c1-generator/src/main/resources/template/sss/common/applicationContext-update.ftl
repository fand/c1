<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:p="http://www.springframework.org/schema/p"
       xmlns="http://www.springframework.org/schema/beans"
       xmlns:util="http://www.springframework.org/schema/util"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd
     	http://www.springframework.org/schema/util
     	http://www.springframework.org/schema/util/spring-util.xsd"
       default-lazy-init="true">
 <#if entityMapper??>
    <bean id="${packageCode}.${moduleMapperCode}.po.${businessCode}.updateStateCode" parent="redundancyUpdateAction">
        <property name="names">
            <list>
                <value>${packageCode}.${moduleMapperCode}.service.${businessCode}DefaultService.init</value>
            </list>
        </property>
        <property name="model">
            <map>
                <entry key="dataTableName" value="C1_DICTIONARY"/>
                <entry key="dataPrimaryKeyName" value="ID"/>
                <entry key="dataFieldName" value="CODE"/>
                <entry key="updateTableName" value="${(entityMapper.tableName)!}"/>
                <entry key="updateForeignKeyName" value="STATE_ID"/>
                <entry key="updateFieldName" value="STATE_CODE"/>
            </map>
        </property>
    </bean>

     <bean id="${packageCode}.${moduleMapperCode}.po.${businessCode}.updateStateName" parent="redundancyUpdateAction">
         <property name="names">
             <list>
                 <value>${packageCode}.${moduleMapperCode}.service.${businessCode}DefaultService.init</value>
             </list>
         </property>
         <property name="model">
             <map>
                 <entry key="dataTableName" value="C1_DICTIONARY"/>
                 <entry key="dataPrimaryKeyName" value="ID"/>
                 <entry key="dataFieldName" value="NAME"/>
                 <entry key="updateTableName" value="${(entityMapper.tableName)!}"/>
                 <entry key="updateForeignKeyName" value="STATE_ID"/>
                 <entry key="updateFieldName" value="STATE_NAME"/>
             </map>
         </property>
     </bean>

     <bean id="${packageCode}.${moduleMapperCode}.po.${businessCode}.updateDataStateCode" parent="redundancyUpdateAction">
         <property name="names">
             <list>
                 <value>${packageCode}.${moduleMapperCode}.service.${businessCode}DefaultService.init</value>
             </list>
         </property>
         <property name="model">
             <map>
                 <entry key="dataTableName" value="C1_DICTIONARY"/>
                 <entry key="dataPrimaryKeyName" value="ID"/>
                 <entry key="dataFieldName" value="CODE"/>
                 <entry key="updateTableName" value="${(entityMapper.tableName)!}"/>
                 <entry key="updateForeignKeyName" value="DATA_STATE_ID"/>
                 <entry key="updateFieldName" value="DATA_STATE_CODE"/>
             </map>
         </property>
     </bean>

     <bean id="${packageCode}.${moduleMapperCode}.po.${businessCode}.updateDataStateName" parent="redundancyUpdateAction">
         <property name="names">
             <list>
                 <value>${packageCode}.${moduleMapperCode}.service.${businessCode}DefaultService.init</value>
             </list>
         </property>
         <property name="model">
             <map>
                 <entry key="dataTableName" value="C1_DICTIONARY"/>
                 <entry key="dataPrimaryKeyName" value="ID"/>
                 <entry key="dataFieldName" value="NAME"/>
                 <entry key="updateTableName" value="${(entityMapper.tableName)!}"/>
                 <entry key="updateForeignKeyName" value="DATA_STATE_ID"/>
                 <entry key="updateFieldName" value="DATA_STATE_NAME"/>
             </map>
         </property>
     </bean>
 </#if>
</beans>