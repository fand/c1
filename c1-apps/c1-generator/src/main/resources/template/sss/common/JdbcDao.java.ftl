<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.ArrayUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;

import org.joda.time.DateTime;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.collections.MapUtils;
import com.google.common.collect.Maps;

import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.EntityUtils;
import com.cardone.context.Attributes;
import com.cardone.persistent.builder.ModelArgs;
import com.cardone.persistent.builder.Model;
import com.cardone.persistent.builder.ModelUtils;
import com.cardone.persistent.support.JdbcTemplateSupport;
import com.cardone.persistent.support.PaginationArgs;
import com.cardone.context.ContextHolder;
import com.cardone.context.DictionaryException;
import com.cardone.common.dao.SimpleDao;
import com.cardone.persistent.support.SimpleJdbcDao;

import java.lang.reflect.InvocationTargetException;
<#if entityMapper??>
import ${packageCode}.${moduleMapperCode}.dto.${businessCode}Dto;
</#if>

/**
* ${businessName!}
*
* @author ${author!'yaohaitao'}
*
*/
@Getter
@Setter
@Slf4j
<#if entityMapper??>
public class ${businessCode}JdbcDao extends SimpleJdbcDao
<${businessCode}Dto> implements ${businessCode}Dao {
    public ${businessCode}JdbcDao() {
    super(${businessCode}Dao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(${businessCode}Dto dto) {
    return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(${businessCode}Dto dto) {
    dto.setId(UUID.randomUUID().toString());
    }
<#else>
    public class ${businessCode}JdbcDao implements ${businessCode}Dao {
</#if>
    }