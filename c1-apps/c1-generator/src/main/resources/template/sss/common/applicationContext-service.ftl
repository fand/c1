<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns="http://www.springframework.org/schema/beans"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans.xsd"
       default-lazy-init="true">
    <!-- ${businessName} -->
    <bean id="${packageCode}.${moduleMapperCode}.dao.${businessCode}JdbcDao"
          class="${packageCode}.${moduleMapperCode}.dao.${businessCode}JdbcDao">
    <#if entityMapper??>
        <property name="idNames">
            <list>
                <value>id</value>
            </list>
        </property>
        <property name="codeNames">
            <list>
                <value>code</value>
            </list>
        </property>
        <property name="updateNames">
            <list>
                <value>code</value>
                <value>stateId</value>
                <value>version</value>
                <value>lastModifiedById</value>
                <value>lastModifiedByCode</value>
                <value>lastModifiedByName</value>
                <value>lastModifiedDate</value>
            </list>
        </property>
    </#if>
    </bean>

    <alias name="${packageCode}.${moduleMapperCode}.dao.${businessCode}JdbcDao"
           alias="${packageCode}.${moduleMapperCode}.dao.${businessCode}Dao"/>

    <!-- ${businessName} -->
    <bean id="${packageCode}.${moduleMapperCode}.service.${businessCode}DefaultService"
          class="${packageCode}.${moduleMapperCode}.service.${businessCode}DefaultService"/>

    <alias name="${packageCode}.${moduleMapperCode}.service.${businessCode}DefaultService"
           alias="${packageCode}.${moduleMapperCode}.service.${businessCode}Service"/>

    <bean id="${packageCode}.${moduleMapperCode}.service.${businessCode?uncap_first}.service" parent="serviceTemplate">
        <property name="service" ref="${packageCode}.${moduleMapperCode}.service.${businessCode}Service"/>
        <property name="serviceInterface" value="${packageCode}.${moduleMapperCode}.service.${businessCode}Service"/>
    </bean>

    <!--
	<bean name="${packageCode}.${moduleMapperCode}.service.${businessCode}Service" parent="clientTemplate">
		<property name="serviceUrl" value="${r"$"}{${projectCode}Host}${packageCode}.${moduleMapperCode}.service.${businessCode?uncap_first}.service" />
		<property name="serviceInterface" value="${packageCode}.${moduleMapperCode}.service.${businessCode}Service" />
	</bean>
-->
</beans>