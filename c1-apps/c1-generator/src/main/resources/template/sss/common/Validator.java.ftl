<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.validator;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ${packageCode}.${moduleMapperCode}.dto.${businessCode}Dto;

/**
* ${businessName!}
*
* @author ${author!'yaohaitao'}
*
*/
@Deprecated
public class ${businessCode}Validator implements Validator {
@Override
public boolean supports(Class<?> clazz) {
return ${businessCode!}Dto.class.equals(clazz);
}

@Override
public void validate(Object target, Errors errors) {
${businessCode}Dto ${businessCode?uncap_first} = (${businessCode}Dto) target;

if (StringUtils.isBlank(${businessCode?uncap_first}.getValidatorTypeCodes())) {
return;
}

// if (StringUtils.isBlank(${businessCode?uncap_first}.getCode())) {
//  errors.reject(Dictionarys.codeRequired.typeCodeAndCode(), Dictionarys.codeRequired.defaultMessage());
// }
}
}
