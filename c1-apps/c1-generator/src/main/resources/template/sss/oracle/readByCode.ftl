SELECT
<${r"#"}switch (object_id!)>
<#list entityMapper.fieldMapperMap?values as fieldMapper>
<${r"#"}case "${fieldMapper.code}">
T.${fieldMapper.columnName} AS ${fieldMapper.code}
<${r"#"}break>
</#list>
    <${r"#"}default>
        COUNT(1) AS COUNT_
        </${r"#"}switch>
        FROM ${entityMapper.tableName} T
        <${r"#"}include
        "whereByCode.ftl">