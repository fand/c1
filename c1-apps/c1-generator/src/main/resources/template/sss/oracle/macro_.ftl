<#assign StringUtils = statics["org.apache.commons.lang3.StringUtils"]>
<#assign ArrayUtils = statics["org.apache.commons.lang3.ArrayUtils"]>

<#macro defaultIfBlank str="" defaultSt="">${StringUtils.contains(str, '?')? string(defaultSt, str!defaultSt)}</#macro>

<#assign ignoreProperties = ',code,name,orderNum,orderCode,beginDate,createdByCode,createdById,createdByName,createdDate,endDate,lastModifiedByCode,lastModifiedById,lastModifiedByName,lastModifiedDate,paginationNo,paginationSize,stateCode,stateId,stateName,validatorTypeCodes,version,'>

<#assign ignoreEnums = ignoreProperties + ',departmentId,parentId,id,roleId,userGroupId,userId,typeId,siteId,styleId,value,remark,projectId,orgId,'>