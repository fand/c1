<#include "macro_.ftl">
INSERT
INTO
${entityMapper.tableName}
(
<${r"#"}assign prefixName=' '>
<#list entityMapper.fieldMapperMap?values as fieldMapper>
    <#if StringUtils.contains(fieldMapper.columnName, 'CREATED_DATE')>
    ${r"$"}{prefixName} ${fieldMapper.columnName}
    <${r"#"}assign prefixName=','>
    <#else>
        <${r"#"}if (insert_${fieldMapper.code}??) && (insert_${fieldMapper.code}_value??)>
    ${r"$"}{prefixName} ${fieldMapper.columnName}
    <${r"#"}assign prefixName=','>
        </${r"#"}if>
    </#if>
</#list>
    )
    VALUES
    (
    <${r"#"}assign prefixName=' '>
    <#list entityMapper.fieldMapperMap?values as fieldMapper>
        <#if StringUtils.contains(fieldMapper.columnName, 'CREATED_DATE')>
        ${r"$"}{prefixName} SYSDATE
        <${r"#"}assign prefixName=','>
        <#else>
            <${r"#"}if (insert_${fieldMapper.code}??) && (insert_${fieldMapper.code}_value??)>
        ${r"$"}{prefixName} :insert_${fieldMapper.code}_value
        <${r"#"}assign prefixName=','>
            </${r"#"}if>
        </#if>
    </#list>
        )