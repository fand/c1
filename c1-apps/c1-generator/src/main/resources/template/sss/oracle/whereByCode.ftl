<${r"#"}assign prefixName='WHERE'>
<#list entityMapper.fieldMapperMap?values as fieldMapper>
    <#if (fieldMapper_index != 0)>
        <${r"#"}if
        (prefixName!) != 'WHERE'>
    <${r"#"}assign prefixName='AND'>
        </${r"#"}if>
    </#if>
    <${r"#"}if
    (where_and_eq_${fieldMapper.code}??)>
    <${r"#"}if
    (where_and_eq_${fieldMapper.code}_value??)>
${r"${prefixName}"} T.${fieldMapper.columnName} = :where_and_eq_${fieldMapper.code}_value
    <${r"#"}else>
    ${r"${prefixName}"} T.${fieldMapper.columnName} IS NULL
        </${r"#"}if>
        <${r"#"}assign prefixName=''>
            </${r"#"}if>
            <${r"#"}if
            (where_and_nq_${fieldMapper.code}??)>
            <${r"#"}if
            (prefixName!) != 'WHERE'>
            <${r"#"}assign prefixName='AND'>
                </${r"#"}if>
                <${r"#"}if
                (where_and_nq_${fieldMapper.code}_value??)>
            ${r"${prefixName}"} T.${fieldMapper.columnName} <> :where_and_nq_${fieldMapper.code}_value
                <${r"#"}else>
                ${r"${prefixName}"} T.${fieldMapper.columnName} IS NOT NULL
                    </${r"#"}if>
                    <${r"#"}assign prefixName=''>
                        </${r"#"}if>
                        <${r"#"}if
                        (where_and_like_${fieldMapper.code}??)>
                        <${r"#"}if
                        (prefixName!) != 'WHERE'>
                        <${r"#"}assign prefixName='AND'>
                            </${r"#"}if>
                            <${r"#"}if
                            (where_and_like_${fieldMapper.code}_value??)>
                        ${r"${prefixName}"} T.${fieldMapper.columnName} <> :where_and_like_${fieldMapper.code}_value
                            </${r"#"}if>
                            <${r"#"}assign prefixName=''>
                                </${r"#"}if>
                                <${r"#"}if
                                (where_or_eq_${fieldMapper.code}??)>
                                <${r"#"}if
                                (prefixName!) != 'WHERE'>
                                <${r"#"}assign prefixName='OR'>
                                    </${r"#"}if>
                                    <${r"#"}if
                                    (where_or_eq_${fieldMapper.code}_value??)>
                                ${r"${prefixName}"} T.${fieldMapper.columnName} = :where_or_eq_${fieldMapper.code}_value
                                    <${r"#"}else>
                                    ${r"${prefixName}"} T.${fieldMapper.columnName} IS NULL
                                        </${r"#"}if>
                                        <${r"#"}assign prefixName=''>
                                            </${r"#"}if>
                                            <${r"#"}if
                                            (where_or_nq_${fieldMapper.code}??)>
                                            <${r"#"}if
                                            (prefixName!) != 'WHERE'>
                                            <${r"#"}assign prefixName='OR'>
                                                </${r"#"}if>
                                                <${r"#"}if
                                                (where_or_nq_${fieldMapper.code}_value??)>
                                            ${r"${prefixName}"} T.${fieldMapper.columnName} <>
                                                :where_or_nq_${fieldMapper.code}_value
                                                <${r"#"}else>
                                                ${r"${prefixName}"} T.${fieldMapper.columnName} IS NOT NULL
                                                    </${r"#"}if>
                                                    <${r"#"}assign prefixName=''>
                                                        </${r"#"}if>
                                                        <${r"#"}if
                                                        (where_or_like_${fieldMapper.code}??)>
                                                        <${r"#"}if
                                                        (prefixName!) != 'WHERE'>
                                                        <${r"#"}assign prefixName='OR'>
                                                            </${r"#"}if>
                                                            <${r"#"}if
                                                            (where_or_like_${fieldMapper.code}_value??)>
                                                        ${r"${prefixName}"} T.${fieldMapper.columnName} like
                                                            :where_or_like_${fieldMapper.code}_value
                                                            </${r"#"}if>
                                                            <${r"#"}assign prefixName=''>
                                                                </${r"#"}if>
                                                                <#if fieldMapper.code == 'endDate'>
                                                                    <${r"#"}if
                                                                    (where_and_between_sysdate??)>
                                                                    <${r"#"}if
                                                                    (prefixName!) != 'WHERE'>
                                                                <${r"#"}assign prefixName='AND'>
                                                                    </${r"#"}if>
                                                                ${r"${prefixName}"} SYSDATE BETWEEN NVL(T.BEGIN_DATE,
                                                                    SYSDATE) AND NVL(T.END_DATE, SYSDATE)
                                                                <${r"#"}assign prefixName=''>
                                                                    </${r"#"}if>
                                                                </#if>
</#list>