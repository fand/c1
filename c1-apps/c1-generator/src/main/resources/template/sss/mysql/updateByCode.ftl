<#include "macro_.ftl">
UPDATE ${entityMapper.tableName}
<${r"#"}assign prefixName='SET'>
<#list entityMapper.fieldMapperMap?values as fieldMapper>
    <${r"#"}if
    (update_${fieldMapper.code}??)>
    <${r"#"}if
    (update_${fieldMapper.code}_value??)>
${r"${prefixName}"} `${fieldMapper.columnName}` = :update_${fieldMapper.code}_value
<${r"#"}else>
    <#if StringUtils.contains(fieldMapper.columnName, 'LAST_MODIFIED_DATE')>
    ${r"${prefixName}"} `${fieldMapper.columnName}` = NOW()
    <#else>
    ${r"${prefixName}"} `${fieldMapper.columnName}` = NULL
    </#if>
    </${r"#"}if>
<${r"#"}assign prefixName=','>
    </${r"#"}if>
</#list>
    <${r"#"}include
    "whereByCode.ftl">