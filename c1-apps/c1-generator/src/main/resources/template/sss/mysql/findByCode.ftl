SELECT
<${r"#"}assign prefixName=' '>
<#list entityMapper.fieldMapperMap?values as fieldMapper>
    <${r"#"}if
    (select_${fieldMapper.code}??)>
${r"$"}{prefixName} `${fieldMapper.columnName}`
<${r"#"}assign prefixName=','>
    </${r"#"}if>
</#list>
    <${r"#"}if prefixName== ' '>
<#assign prefixName=' '>
<#list entityMapper.fieldMapperMap?values as fieldMapper>
${prefixName} `${fieldMapper.columnName}`
    <#assign prefixName=','>
</#list>
    </${r"#"}if>
    FROM ${entityMapper.tableName}
    <${r"#"}include "whereByCode.ftl">
<${r"#"}assign prefixName='ORDER BY'>
<#list entityMapper.fieldMapperMap?values as fieldMapper>
    <${r"#"}if
    (order_by_${fieldMapper.code}??)>
${r"$"}{prefixName} `${fieldMapper.columnName}` ${r"$"}{order_by_${fieldMapper.code}_value!}
    <${r"#"}assign prefixName=','>
        </${r"#"}if>
</#list>