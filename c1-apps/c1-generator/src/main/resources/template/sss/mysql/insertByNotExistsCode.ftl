<#include "macro_.ftl">
INSERT
INTO
${entityMapper.tableName}
(
<${r"#"}assign prefixName=' '>
<#list entityMapper.fieldMapperMap?values as fieldMapper>
    <#if StringUtils.contains(fieldMapper.columnName, 'CREATED_DATE')>
    ${r"$"}{prefixName} `${fieldMapper.columnName}`
    <${r"#"}assign prefixName=','>
    <#else>
        <${r"#"}if (insert_${fieldMapper.code}??) && (insert_${fieldMapper.code}_value??)>
    ${r"$"}{prefixName} `${fieldMapper.columnName}`
    <${r"#"}assign prefixName=','>
        </${r"#"}if>
    </#if>
</#list>
    )
    (
    SELECT
    <${r"#"}assign prefixName=' '>
    <#list entityMapper.fieldMapperMap?values as fieldMapper>
        <#if StringUtils.contains(fieldMapper.columnName, 'CREATED_DATE')>
        ${r"$"}{prefixName} NOW()
        <${r"#"}assign prefixName=','>
        <#else>
            <${r"#"}if (insert_${fieldMapper.code}??) && (insert_${fieldMapper.code}_value??)>
        ${r"$"}{prefixName} :insert_${fieldMapper.code}_value
        <${r"#"}assign prefixName=','>
            </${r"#"}if>
        </#if>
    </#list>
        FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM ${entityMapper.tableName} E
        <${r"#"}assign prefixName='WHERE'>
        <#list entityMapper.fieldMapperMap?values as fieldMapper>
            <#if (fieldMapper_index != 0)>
                <${r"#"}if
                (prefixName!) != 'WHERE'>
            <${r"#"}assign prefixName='AND'>
                </${r"#"}if>
            </#if>
            <${r"#"}if (where_and_eq_${fieldMapper.code}??)>
            <${r"#"}if (where_and_eq_${fieldMapper.code}_value??)>
        ${r"${prefixName}"} E.${fieldMapper.columnName} = :where_and_eq_${fieldMapper.code}_value
        <${r"#"}else>
        ${r"${prefixName}"} E.${fieldMapper.columnName} IS NULL
            </${r"#"}if>
        <${r"#"}assign prefixName=''>
            </${r"#"}if>
            <#if fieldMapper.code == 'endDate'>
                <${r"#"}if (where_and_between_sysdate??)>
                <${r"#"}if (prefixName!) != 'WHERE'>
            <${r"#"}assign prefixName='AND'>
                </${r"#"}if>
            ${r"${prefixName}"} NOW() BETWEEN IFNULL(E.BEGIN_DATE, NOW()) AND IFNULL(E.END_DATE, NOW())
            <${r"#"}assign prefixName=''>
                </${r"#"}if>
            </#if>
        </#list>
            )
            )