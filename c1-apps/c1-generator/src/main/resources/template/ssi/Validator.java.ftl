<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import ${packageCode}.${moduleMapperCode}.service.${moduleMapperCode?cap_first}Service;

import ${packageCode}.${moduleMapperCode}.dto.${poMapper.code}Dto;

/**
* <@defaultIfBlank str="${poMapper.remarks!}" defaultSt="${poMapper.code!}"/>
*
* @author ${author!'yaohaitao'}
*
*/
(${poMapper.code}Validator.BEAN_ID)
public class ${poMapper.code}Validator implements Validator {
public static final String BEAN_ID = "${packageCode}.${moduleMapperCode}.validator.${poMapper.code}Validator";

@Qualifier(${moduleMapperCode?cap_first}Service.BEAN_ID)
@Autowired(required = false)
private ${moduleMapperCode?cap_first}Service ${moduleMapperCode}Service;

@Override
public boolean supports(Class<?> clazz) {
return ${poMapper.code}Dto.class.equals(clazz);
}

@Override
public void validate(Object target, Errors errors) {
${poMapper.code}Dto ${poMapper.code?uncap_first} = (${poMapper.code}Dto) target;

//if (StringUtils.isBlank(${poMapper.code?uncap_first}.getCode())) {
//  errors.reject("${poMapper.code?uncap_first}.code.required", "代码不能为空");
//}
}
}
