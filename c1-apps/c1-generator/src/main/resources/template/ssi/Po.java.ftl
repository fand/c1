<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.po;

<#list poMapper.fieldMapperMap?values as fieldMapper>
    <#if fieldMapper.typeCode == 'Date'>
    import java.util.Date;

        <#break>
    </#if>
</#list>


import lombok.Getter; import lombok.Setter;


/**
* <@defaultIfBlank str="${poMapper.remarks!}" defaultSt="${poMapper.code!}"/>
*
* @author ${author!'yaohaitao'}
*
*/
@Getter @Setter


public class ${poMapper.code} implements java.io.Serializable {
/**
* 版本号
*/
private static final long serialVersionUID = ${serialVersionUID!'1'}L;
<#list poMapper.fieldMapperMap?values as fieldMapper>

/**
* <@defaultIfBlank str="${fieldMapper.remarks!}" defaultSt="${fieldMapper.code!}"/>
*/
private ${fieldMapper.typeCode!'String'} ${fieldMapper.code};
</#list>
}