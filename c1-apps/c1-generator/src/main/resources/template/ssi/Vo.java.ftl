<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.vo;

<#list poMapperList as poMapper>
    <#if poMapper.markService && poMapper.markController>
    import ${packageCode}.${moduleMapperCode}.dto.${poMapper.code}Dto;
    </#if>
</#list>

import lombok.Getter;

/**
* ${businessName}
*
* @author ${author!'yaohaitao'}
*/
public class ${businessCode!}Vo implements java.io.Serializable {
/**
* 版本号
*/
private static final long serialVersionUID = ${serialVersionUID!'1'}L;
<#list poMapperList as poMapper>
    <#if poMapper.markService && poMapper.markController>

    /**
    * <@defaultIfBlank str="${poMapper.remarks!}" defaultSt="${poMapper.code!}"/>
    */
    @Getter
    private final ${poMapper.code}Dto ${poMapper.code?uncap_first} = new ${poMapper.code}Dto();
    </#if>
</#list>
}