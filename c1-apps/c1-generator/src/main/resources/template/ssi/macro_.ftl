<#assign StringUtils = statics["org.apache.commons.lang3.StringUtils"]>

<#macro defaultIfBlank str="" defaultSt="">${StringUtils.contains(str, '?')? string(defaultSt, str!defaultSt)}</#macro>