<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.service;

import java.util.List;
import java.util.Map;

import cn.org.rapid_framework.page.Page;
import cn.org.rapid_framework.page.PageRequest;

<#list poMapperList as poMapper>
    <#if poMapper.markService>
    import ${packageCode}.${moduleMapperCode}.dto.${poMapper.code}Dto;
    </#if>
</#list>


/**
* ${businessName}服务
*
* @author ${author!'yaohaitao'}
*/
public interface ${businessCode}Service {
String BEAN_ID = "${packageCode}.${moduleMapperCode}.service.${businessCode}Service";
<#if businessCode == (moduleMapperCode?cap_first)>
    <#list moduleMapper.businessMap?values as localBusiness>
        <#if (localBusiness.code!) != (moduleMapperCode?cap_first)>

        /**
        * 获取：${localBusiness.name!localBusiness.code}服务
        *
        * @return ${localBusiness.name!localBusiness.code}服务
        */
        ${localBusiness.code!}Service get${localBusiness.code!}Service();
        </#if>
    </#list>
<#else>
</#if>
<#list poMapperList as poMapper>
    <#if poMapper.markService>
    /**
    * 删除:${poMapper.remarks!poMapper.code}
    *
    * @param delete${poMapper.code}
    *            ${poMapper.remarks!poMapper.code}对象
    *
    * @return 影响行数
    */
    int delete${poMapper.code}ByIds(${poMapper.code}Dto delete${poMapper.code});

    /**
    * 查询:${poMapper.remarks!poMapper.code}
    *
    * @param get${poMapper.code}
    *            ${poMapper.remarks!poMapper.code}对象
    *
    * @return ${poMapper.remarks!poMapper.code}对象
    */
    ${poMapper.code}Dto get${poMapper.code}ById(${poMapper.code}Dto get${poMapper.code});

    /**
    * 分页:${poMapper.remarks!poMapper.code}
    *
    * @param pageRequestMap
    *            ${poMapper.remarks!poMapper.code}对象
    *
    * @return ${poMapper.remarks!poMapper.code}分页对象
    */
    Page
    <${poMapper.code}Dto> pagination${poMapper.code}ByCode(PageRequest
    <Map> pageRequestMap);

        /**
        * 保存:${poMapper.remarks!poMapper.code}
        *
        * @param save${poMapper.code}
        *            ${poMapper.remarks!poMapper.code}对象
        *
        * @return ${poMapper.remarks!poMapper.code}对象
        */
    ${poMapper.code}Dto save${poMapper.code}ByIdOrCode(${poMapper.code}Dto save${poMapper.code});

    </#if>
</#list>
    }