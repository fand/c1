<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.action;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.val;

import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.ServletActionContext;

import cn.org.rapid_framework.page.Page;
import cn.org.rapid_framework.page.PageRequest;

<#list poMapperList as poMapper>
    <#if poMapper.markService && poMapper.markController>
    import ${packageCode}.${moduleMapperCode}.dto.${poMapper.code}Dto;
    </#if>
</#list>
import ${packageCode}.${moduleMapperCode}.service.${moduleMapperCode?cap_first}Service;
import ${packageCode}.${moduleMapperCode}.vo.${businessCode}Vo;
import com.au.system.javacommon.jquery.JqueryPageHelper;
import com.au.system.javacommon.jquery.ListRange;
import com.au.system.javacommon.util.Struts2JsonHelper;
import com.google.common.collect.Maps;
import com.opensymphony.xwork2.Action;

/**
* ${businessName}
*
* @author ${author!'yaohaitao'}
*/
public class ${businessCode}Action {
@Setter
private ${moduleMapperCode?cap_first}Service ${moduleMapperCode}Service;

@Getter
@Setter
private ${businessCode}Vo ${businessCode?uncap_first}Vo;
<#list poMapperList as poMapper>
    <#if poMapper.markService && poMapper.markController>

    public void ${poMapper.code?uncap_first}Deletes() {
    Map
    <String, Object> v_result = Maps.newHashMap();

    try {
    val v_request = ServletActionContext.getRequest();

    val v_ids = v_request.getParameter("ids");

    if (StringUtils.isNotBlank(v_ids)) {
    val v_delete${poMapper.code} = new ${poMapper.code}Dto();

    v_delete${poMapper.code}.setIds(v_ids);

    this.${moduleMapperCode}Service.get${businessCode}Service().delete${poMapper.code}ByIds(v_delete${poMapper.code});

    v_result.put("success", true);

    v_result.put("msg", "删除成功");

    this.${moduleMapperCode}Service.get${businessCode}Service().reload();
    } else {
    v_result.put("success", false);

    v_result.put("msg", "删除失败 数据错误!");
    }
    } catch (final Exception v_e) {
    v_result.put("success", false);

    v_result.put("msg", "删除失败!");

    v_e.printStackTrace();
    }

    Struts2JsonHelper.outJson(v_result);
    }

    public String ${poMapper.code?uncap_first}Edit() {
    val v_request = ServletActionContext.getRequest();

    val v_id = v_request.getParameter("id");

    val v_get${poMapper.code} = new ${poMapper.code}Dto();

    v_get${poMapper.code}.set${poMapper.code}Id(v_id);

    val v_${poMapper.code?uncap_first} = this.${moduleMapperCode}Service.get${businessCode}Service().get${poMapper.code}ById(v_get${poMapper.code});

    v_request.setAttribute("${poMapper.code?uncap_first}", v_${poMapper.code?uncap_first});

    return Action.SUCCESS;
    }

    public String ${poMapper.code?uncap_first}Index() {
    return Action.SUCCESS;
    }

    @SuppressWarnings({ "rawtypes", "unchecked" })
    public void ${poMapper.code?uncap_first}IndexPagination() {
    val v_pageRequestMap = JqueryPageHelper.createPageRequestForJquery(ServletActionContext.getRequest());

    final Page v_page = this.${moduleMapperCode}Service.get${businessCode}Service().pagination${poMapper.code}ByCode(v_pageRequestMap);

    val v_${poMapper.code?uncap_first}List = v_page.getResult();

    val v_resultList = new ListRange
    <Map>();

        v_resultList.setDataRows(v_${poMapper.code?uncap_first}List);

        v_resultList.setRecords(v_page.getTotalCount());

        v_resultList.setRows(v_page.getPageSize());

        if ((v_page.getTotalCount() % v_page.getPageSize()) > 0) {
        v_resultList.setTotal((v_page.getTotalCount() / v_page.getPageSize()) + 1);
        } else {
        v_resultList.setTotal(v_page.getTotalCount() / v_page.getPageSize());
        }

        v_resultList.setPage(v_pageRequestMap.getPageNumber());

        Struts2JsonHelper.outJson(v_resultList);
        }

        public String ${poMapper.code?uncap_first}New() {
        return Action.SUCCESS;
        }

        public void ${poMapper.code?uncap_first}Save() {
        final Map
        <String, Object> v_result = Maps.newHashMap();

        try {
        if (this.${businessCode?uncap_first}Vo != null) {
        this.${moduleMapperCode}Service.get${businessCode}Service().save${poMapper.code}
        ByIdOrCode(this.${businessCode?uncap_first}Vo.get${poMapper.code}());

        v_result.put("success", true);

        v_result.put("msg", "保存成功");

        this.${moduleMapperCode}Service.get${businessCode}Service().reload();
        } else {
        v_result.put("success", false);

        v_result.put("msg", "保存失败 数据错误!");
        }
        } catch (final Exception v_e) {
        v_result.put("success", false);

        v_result.put("msg", "保存失败!");

        v_e.printStackTrace();
        }

        Struts2JsonHelper.outJson(v_result);
        }

        public String ${poMapper.code?uncap_first}View() {
        return this.${poMapper.code?uncap_first}Edit();
        }
    </#if>
</#list>
    }
