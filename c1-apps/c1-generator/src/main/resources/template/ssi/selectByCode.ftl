SELECT
<${r"#"}assign prefixName=' '>
<#list poMapper.fieldMapperMap?values as fieldMapper>
    <${r"#"}if
    (select_${fieldMapper.code}??)>
${r"$"}{prefixName} T.${fieldMapper.columnName} AS ${fieldMapper.code}
<${r"#"}assign prefixName=','>
    </${r"#"}if>
</#list>
    <${r"#"}if prefixName== ' '>
<#assign prefixName=' '>
<#list poMapper.fieldMapperMap?values as fieldMapper>
${prefixName} T.${fieldMapper.columnName} AS ${fieldMapper.code}
    <#assign prefixName=','>
</#list>
    </${r"#"}if>
    FROM ${poMapper.tableName} T
    <${r"#"}include "where${poMapper.code}ByCode.ftl">
<${r"#"}assign prefixName='ORDER BY'>
<#list poMapper.fieldMapperMap?values as fieldMapper>
    <${r"#"}if
    (order_by_${fieldMapper.code}??)>
${r"$"}{prefixName} T.${fieldMapper.columnName} ${r"$"}{order_by_${fieldMapper.code}_value!}
    <${r"#"}assign prefixName=','>
        </${r"#"}if>
</#list>