UPDATE ${poMapper.tableName} T
<${r"#"}assign prefixName='SET'>
<#list poMapper.fieldMapperMap?values as fieldMapper>
    <${r"#"}if
    (update_${fieldMapper.code}??)>
    <${r"#"}if
    (update_${fieldMapper.code}_value??)>
${r"${prefixName}"} T.${fieldMapper.columnName} = :update_${fieldMapper.code}_value
<${r"#"}else>
${r"${prefixName}"} T.${fieldMapper.columnName} = NULL
    </${r"#"}if>
<${r"#"}assign prefixName=','>
    </${r"#"}if>
</#list>
    <${r"#"}include
    "where${poMapper.code}ByCode.ftl">