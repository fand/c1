<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;


import lombok.Getter;

import lombok.Setter;

import org.apache.commons.lang3.ArrayUtils;
import org.joda.time.DateTime;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import cn.org.rapid_framework.page.Page;
import cn.org.rapid_framework.page.PageRequest;

import ${packageCode}.${moduleMapperCode}.dao.${businessCode}Dao;
<#list poMapperList as poMapper>
    <#if poMapper.markService>
    import ${packageCode}.${moduleMapperCode}.dto.${poMapper.code}Dto;
    </#if>
</#list>

/**
* ${businessName}服务
*
* @author ${author!'yaohaitao'}
*
*/
@Getter
@Setter


public class ${businessCode}DefaultService implements ${businessCode}Service {
private ${businessCode}Dao ${businessCode?uncap_first}Dao;
<#if businessCode == (moduleMapperCode?cap_first)>
    <#list moduleMapper.businessMap?values as localBusiness>
        <#if (localBusiness.code!) != (moduleMapperCode?cap_first)>

        private ${localBusiness.code!}Service ${localBusiness.code?uncap_first}Service;
        </#if>
    </#list>
</#if>
<#list poMapperList as poMapper>
    <#if poMapper.markService>

    @Override
    @Transactional
    public int delete${poMapper.code}ByIds(${poMapper.code}Dto delete${poMapper.code}) {
    return this.${businessCode?uncap_first}Dao.delete${poMapper.code}ByIds(delete${poMapper.code});
    }

    @Override
    public ${poMapper.code}Dto get${poMapper.code}ById(${poMapper.code}Dto get${poMapper.code}) {
    return this.${businessCode?uncap_first}Dao.select${poMapper.code}ByIdOrCode(get${poMapper.code});
    }

    @SuppressWarnings("rawtypes")
    @Override
    public Page
    <${poMapper.code}Dto> pagination${poMapper.code}ByCode(PageRequest
    <Map> pageRequestMap) {
        return this.${businessCode?uncap_first}Dao.pagination${poMapper.code}ByCode(pageRequestMap);
        }

        @Override
        @Transactional
        public ${poMapper.code}Dto save${poMapper.code}ByIdOrCode(${poMapper.code}Dto save${poMapper.code}) {
        final ${poMapper.code}Dto v_old${poMapper.code} = this.${businessCode?uncap_first}Dao.select${poMapper.code}
        ByIdOrCode(save${poMapper.code});

        if (v_old${poMapper.code} == null) {
        save${poMapper.code}.set${poMapper.code}Id(UUID.randomUUID().toString());

        this.${businessCode?uncap_first}Dao.insert${poMapper.code}ByCode(save${poMapper.code});

        return save${poMapper.code};
        }

        final String[] v_updateProperties = com.cardone.common.util.BeanUtils.diffProperties(save${poMapper.code},
        v_old${poMapper.code}, "typeId",
        "${businessCode?uncap_first}Co", "${businessCode?uncap_first}Na", "${businessCode?uncap_first}Va", "orderNum");

        if (!ArrayUtils.isEmpty(v_updateProperties)) {
        this.${businessCode?uncap_first}Dao.update${poMapper.code}ByIdOrCode(save${poMapper.code});

        com.cardone.common.util.BeanUtils.copyProperties(save${poMapper.code}, v_old${poMapper.code},
        v_updateProperties);
        }

        return v_old${poMapper.code};
        }
    </#if>
</#list>
    }