INSERT
INTO
${poMapper.tableName} T
(
<${r"#"}assign prefixName=' '>
<#list poMapper.fieldMapperMap?values as fieldMapper>
    <${r"#"}if
    (insert_${fieldMapper.code}??) && (insert_${fieldMapper.code}_value??)>
${r"$"}{prefixName} ${fieldMapper.columnName}
<${r"#"}assign prefixName=','>
    </${r"#"}if>
</#list>
    )
    VALUES
    (
    <${r"#"}assign prefixName=' '>
    <#list poMapper.fieldMapperMap?values as fieldMapper>
        <${r"#"}if
        (insert_${fieldMapper.code}??) && (insert_${fieldMapper.code}_value??)>
    ${r"$"}{prefixName} :insert_${fieldMapper.code}_value
    <${r"#"}assign prefixName=','>
        </${r"#"}if>
    </#list>
        )