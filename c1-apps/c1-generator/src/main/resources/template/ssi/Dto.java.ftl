<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.dto;

import ${packageCode}.${moduleMapperCode}.po.${poMapper.code};


import lombok.Getter; import lombok.Setter;


/**
* <@defaultIfBlank str="${poMapper.remarks!}" defaultSt="${poMapper.code!}"/>
*
* @author ${author!'yaohaitao'}
*
*/
@Getter @Setter


public class ${poMapper.code}Dto extends ${poMapper.code} {
/**
* 版本号
*/
private static final long serialVersionUID = ${serialVersionUID!'1'}L;

/**
* 标识集合
*/
private String ids;
<#if poMapper.markService && poMapper.markController>

/**
* 分页号
*/
private int paginationNo;

/**
* 分页大小
*/
private int paginationSize;

/**
* 获取:标识
*
* @return 标识
*/
public String getId() {
return this.get${poMapper.code}Id();
}

/**
* 设置：标识
*
* @param id
*            标识
*/
public void setId(String id) {
this.set${poMapper.code}Id(id);
}
</#if>
}