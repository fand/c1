<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.val;

import cn.org.rapid_framework.page.Page;
import cn.org.rapid_framework.page.PageRequest;

import com.au.system.javacommon.support.DaoIbatisSupport;

<#list poMapperList as poMapper>
import ${packageCode}.${moduleMapperCode}.dto.${poMapper.code}Dto;
</#list>

/**
* ${businessName}
*
* @author ${author!'yaohaitao'}
*
*/
public class ${businessCode}IbatisDao extends DaoIbatisSupport implements ${businessCode}Dao {
<#list poMapperList as poMapper>
@Override
public int insert${poMapper.code}ByCode(${poMapper.code}Dto insert${poMapper.code}) {
return this.getSqlSessionTemplate().insert("${projectCode!}.${moduleMapperCode}.${poMapper.code?uncap_first}.insertByCode", insert${poMapper.code});
}

@Override
public int delete${poMapper.code}ByIds(${poMapper.code}Dto delete${poMapper.code}) {
return this.getSqlSessionTemplate().delete("${projectCode!}.${moduleMapperCode}.${poMapper.code?uncap_first}.deleteByIds", delete${poMapper.code});
}

@Override
public int update${poMapper.code}ByIdOrCode(${poMapper.code}Dto update${poMapper.code}) {
return this.getSqlSessionTemplate().insert("${projectCode!}.${moduleMapperCode}.${poMapper.code?uncap_first}.updateById", update${poMapper.code});
}

    <#if poMapper.markService>
    @Override
    public ${poMapper.code}Dto select${poMapper.code}ByIdOrCode(${poMapper.code}Dto select${poMapper.code}) {
    return (${poMapper.code}Dto) this.getSqlSessionTemplate().selectOne("${projectCode!}.${moduleMapperCode}.${poMapper.code?uncap_first}.selectByIdOrCode",
    select${poMapper.code});
    }

    @Override
    public Page
    <${poMapper.code}Dto> pagination${poMapper.code}ByCode(PageRequest
    <Map> pageRequestMap) {
        return this.pageQuery("${projectCode!}.${moduleMapperCode}.${poMapper.code?uncap_first}.paginationByCode",
        "${projectCode!}.${moduleMapperCode}.${poMapper.code?uncap_first}.countByCode",
        pageRequestMap);
        }

    </#if>
</#list>
    }