<#include "macro_.ftl">
package ${packageCode}.${moduleMapperCode}.dao;

import java.util.List;
import java.util.Map;

import cn.org.rapid_framework.page.Page;
import cn.org.rapid_framework.page.PageRequest;

<#list poMapperList as poMapper>
import ${packageCode}.${moduleMapperCode}.dto.${poMapper.code}Dto;
</#list>

/**
* ${businessName}
*
* @author ${author!'yaohaitao'}
*
*/
public interface ${businessCode}Dao {
String BEAN_ID = "${packageCode}.${moduleMapperCode}.dao.${businessCode}Dao";
<#list poMapperList as poMapper>
/**
* 插入:${poMapper.remarks!poMapper.code}
*
* @param insert${poMapper.code}
*            ${poMapper.remarks!poMapper.code}对象
*
* @return 影响行数
*/
int insert${poMapper.code}ByCode(${poMapper.code}Dto insert${poMapper.code});

/**
* 删除:${poMapper.remarks!poMapper.code}
*
* @param delete${poMapper.code}
*            ${poMapper.remarks!poMapper.code}对象
*
* @return 影响行数
*/
int delete${poMapper.code}ByIds(${poMapper.code}Dto delete${poMapper.code});

/**
* 更新:${poMapper.remarks!poMapper.code}
*
* @param update${poMapper.code}
*            ${poMapper.remarks!poMapper.code}对象
*
* @return 影响行数
*/
int update${poMapper.code}ByIdOrCode(${poMapper.code}Dto update${poMapper.code});

    <#if poMapper.markService>
    /**
    * 查询:${poMapper.remarks!poMapper.code}
    *
    * @param select${poMapper.code}
    *            ${poMapper.remarks!poMapper.code}对象
    *
    * @return 返回对象
    */
    ${poMapper.code}Dto select${poMapper.code}ByIdOrCode(${poMapper.code}Dto select${poMapper.code});

    /**
    * 分页:${poMapper.remarks!poMapper.code}
    *
    * @param pageRequestMap
    *            ${poMapper.remarks!poMapper.code}对象
    *
    * @return ${poMapper.remarks!poMapper.code}分页对象
    */
    Page
    <${poMapper.code}Dto> pagination${poMapper.code}ByCode(PageRequest
    <Map> pageRequestMap);

    </#if>
</#list>
    }