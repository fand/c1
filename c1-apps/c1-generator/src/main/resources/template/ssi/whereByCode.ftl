<${r"#"}assign prefixName='WHERE'>
<#list poMapper.fieldMapperMap?values as fieldMapper>
    <${r"#"}if
    (prefixName!) != 'WHERE'>
    <${r"#"}assign prefixName='AND'>
        </${r"#"}if>
        <${r"#"}if
        (where_and_eq_${fieldMapper.code}??)>
        <${r"#"}if
        (where_and_eq_${fieldMapper.code}_value??)>
    ${r"${prefixName}"} T.${fieldMapper.columnName} = :where_and_eq_${fieldMapper.code}_value
        <${r"#"}else>
        ${r"${prefixName}"} T.${fieldMapper.columnName} IS NULL
            </${r"#"}if>
            <${r"#"}assign prefixName=''>
                </${r"#"}if>
                <${r"#"}if
                (where_and_nq_${fieldMapper.code}??)>
                <${r"#"}if
                (prefixName!) != 'WHERE'>
                <${r"#"}assign prefixName='AND'>
                    </${r"#"}if>
                    <${r"#"}if
                    (where_and_nq_${fieldMapper.code}_value??)>
                ${r"${prefixName}"} T.${fieldMapper.columnName} <> :where_and_nq_${fieldMapper.code}_value
                    <${r"#"}else>
                    ${r"${prefixName}"} T.${fieldMapper.columnName} IS NOT NULL
                        </${r"#"}if>
                        <${r"#"}assign prefixName=''>
                            </${r"#"}if>
                            <${r"#"}if
                            (where_or_eq_${fieldMapper.code}??)>
                            <${r"#"}if
                            (prefixName!) != 'WHERE'>
                            <${r"#"}assign prefixName='OR'>
                                </${r"#"}if>
                                <${r"#"}if
                                (where_or_eq_${fieldMapper.code}_value??)>
                            ${r"${prefixName}"} T.${fieldMapper.columnName} = :where_or_eq_${fieldMapper.code}_value
                                <${r"#"}else>
                                ${r"${prefixName}"} T.${fieldMapper.columnName} IS NULL
                                    </${r"#"}if>
                                    <${r"#"}assign prefixName=''>
                                        </${r"#"}if>
                                        <${r"#"}if
                                        (where_or_nq_${fieldMapper.code}??)>
                                        <${r"#"}if
                                        (prefixName!) != 'WHERE'>
                                        <${r"#"}assign prefixName='OR'>
                                            </${r"#"}if>
                                            <${r"#"}if
                                            (where_or_nq_${fieldMapper.code}_value??)>
                                        ${r"${prefixName}"} T.${fieldMapper.columnName} <>
                                            :where_or_nq_${fieldMapper.code}_value
                                            <${r"#"}else>
                                            ${r"${prefixName}"} T.${fieldMapper.columnName} IS NOT NULL
                                                </${r"#"}if>
                                                <${r"#"}assign prefixName=''>
                                                    </${r"#"}if>
</#list>