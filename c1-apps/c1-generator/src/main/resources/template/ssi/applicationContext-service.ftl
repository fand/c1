<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xsi:schemaLocation="http://www.springframework.org/schema/beans http://www.springframework.org/schema/beans/spring-beans-3.0.xsd"
       default-lazy-init="false">
<#if businessCode == (moduleMapperCode?cap_first)>
    <#list moduleMapper.businessMap?values as localBusiness>
        <#if (localBusiness.code!) != (moduleMapperCode?cap_first)>
            <import resource="applicationContext-${localBusiness.code?cap_first}.xml"/>

        </#if>
    </#list>
</#if>
    <!-- ${businessName} -->
    <bean id="${packageCode}.${moduleMapperCode}.dao.${businessCode}IbatisDao"
          class="${packageCode}.${moduleMapperCode}.dao.${businessCode}IbatisDao"
          parent="daoTemplate">
    </bean>

    <alias name="${packageCode}.${moduleMapperCode}.dao.${businessCode}IbatisDao"
           alias="${packageCode}.${moduleMapperCode}.dao.${businessCode}Dao"/>

    <!-- ${businessName} -->
    <bean id="${packageCode}.${moduleMapperCode}.service.${businessCode}DefaultService"
          class="${packageCode}.${moduleMapperCode}.service.${businessCode}DefaultService">
        <property name="${businessCode?uncap_first}Dao"
                  ref="${packageCode}.${moduleMapperCode}.dao.${businessCode}Dao"/>
    <#if businessCode == (moduleMapperCode?cap_first)>
        <#list moduleMapper.businessMap?values as localBusiness>
            <#if (localBusiness.code!) != (moduleMapperCode?cap_first)>
                <property name="${localBusiness.code?uncap_first}Service"
                          ref="${packageCode}.${moduleMapperCode}.service.${localBusiness.code?cap_first}Service"/>
            </#if>
        </#list>
    </#if>
    </bean>

    <alias name="${packageCode}.${moduleMapperCode}.service.${businessCode}DefaultService"
           alias="${packageCode}.${moduleMapperCode}.service.${businessCode}Service"/>
<#if businessCode == (moduleMapperCode?cap_first)>

    <alias name="${packageCode}.${moduleMapperCode}.service.${businessCode}DefaultService"
           alias="${businessCode?uncap_first}Service"/>
</#if>
</beans>