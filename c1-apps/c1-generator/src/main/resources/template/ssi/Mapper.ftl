<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
        PUBLIC "-//ibatis.apache.org//DTD Mapper 3.0//EN"
        "http://ibatis.apache.org/dtd/ibatis-3-mapper.dtd">

<mapper namespace="${projectCode!}.${moduleMapperCode}.${poMapper.code?uncap_first}">
    <resultMap id="${poMapper.code?uncap_first}Result"
               type="${packageCode}.${moduleMapperCode}.dto.${poMapper.code}Dto">
    <#list poMapper.fieldMapperMap?values as fieldMapper>
        <result property="${fieldMapper.code}" column="${fieldMapper.columnName}"/>
    </#list>
    </resultMap>

    <sql id="${poMapper.code?uncap_first}Columns">
        <trim prefixOverrides=",">
            <![CDATA[
        <#list poMapper.fieldMapperMap?values as fieldMapper>
            , T.${fieldMapper.columnName} AS ${fieldMapper.code}
        </#list>
            ]]>
        </trim>
    </sql>

    <insert id="insertByCode">
        INSERT INTO
    ${poMapper.tableName} (
        <trim suffixOverrides=",">
        <#list poMapper.fieldMapperMap?values as fieldMapper>
            <if test="@Ognl@isNotEmpty(${fieldMapper.code})">
            ${fieldMapper.columnName},
            </if>
        </#list>
        </trim>
        ) VALUES (
        <trim suffixOverrides=",">
        <#list poMapper.fieldMapperMap?values as fieldMapper>
            <if test="@Ognl@isNotEmpty(${fieldMapper.code})">
            ${r"#"}{${fieldMapper.code}},
            </if>
        </#list>
        </trim>
        )
    </insert>

    <update id="updateById">
        UPDATE ${poMapper.tableName}
        <set>
        <#list poMapper.fieldMapperMap?values as fieldMapper>
            <if test="@Ognl@isNotEmpty(${fieldMapper.code})">
            ${fieldMapper.columnName} = ${r"#"}{${fieldMapper.code}} ,
            </if>
        </#list>
        </set>
        WHERE
        <choose>
            <when test="@Ognl@isNotEmpty(${poMapper.code?uncap_first}Id)">
            ${poMapper.tableName}_ID = ${r"#"}{${poMapper.code?uncap_first}Id}
            </when>
            <otherwise>
            ${poMapper.tableName}_CO = ${r"#"}{${poMapper.code?uncap_first}Co} AND END_DATE =
                TO_DATE('99991231','yyyymmdd')
            </otherwise>
        </choose>
    </update>

    <delete id="deleteByIds">
        UPDATE ${poMapper.tableName} T
        SET T.END_DATE = (SYSDATE -
        0.00001)
        WHERE INSTR(${r"#"}{ids}, T.${poMapper.tableName}_ID) > 0
    </delete>

    <select id="selectByIdOrCode" resultType="${packageCode}.${moduleMapperCode}.dto.${poMapper.code}Dto">
        SELECT
        <include refid="${poMapper.code?uncap_first}Columns"/>
        FROM ${poMapper.tableName} T
        WHERE
        <choose>
            <when test="@Ognl@isNotEmpty(${poMapper.code?uncap_first}Id)">
                T.${poMapper.tableName}_ID = ${r"#"}{${poMapper.code?uncap_first}Id}
            </when>
            <otherwise>
                T.${poMapper.tableName}_CO = ${r"#"}{${poMapper.code?uncap_first}Co} AND T.END_DATE =
                TO_DATE('99991231','yyyymmdd')
            </otherwise>
        </choose>
    </select>

    <select id="paginationByCode" resultType="${packageCode}.${moduleMapperCode}.dto.${poMapper.code}Dto">
        SELECT
        <include refid="${poMapper.code?uncap_first}Columns"/>
        FROM ${poMapper.tableName} T
        WHERE T.END_DATE = TO_DATE('99991231','yyyymmdd')
        <if test="@Ognl@isNotEmpty(${poMapper.code?uncap_first}Co)">
            AND (INSTR(T.${poMapper.tableName}_CO, ${r"#"}{${poMapper.code?uncap_first}Co}) >0 OR
            INSTR(T.${poMapper.tableName}_NA, ${r"#"}{${poMapper.code?uncap_first}Co})
            >0)
        </if>
        <choose>
            <when test="@Ognl@isNotEmpty(sortColumns)">
                ORDER BY ${r"$"}{sortColumns}
            </when>
            <otherwise>
            </otherwise>
        </choose>
    </select>

    <select id="countByCode" resultType="long">
        SELECT COUNT(1) FROM ${poMapper.tableName} T
        WHERE T.END_DATE = TO_DATE('99991231','yyyymmdd')
        <if test="@Ognl@isNotEmpty(${poMapper.code?uncap_first}Co)">
            AND (INSTR(T.${poMapper.tableName}_CO, ${r"#"}{${poMapper.code?uncap_first}Co}) >0 OR
            INSTR(T.${poMapper.tableName}_NA, ${r"#"}{${poMapper.code?uncap_first}Co})
            >0)
        </if>
    </select>
</mapper>
