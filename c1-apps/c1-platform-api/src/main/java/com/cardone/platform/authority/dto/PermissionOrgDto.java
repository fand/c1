package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 许可与组织
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class PermissionOrgDto extends PermissionOrg {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 634967786739370224L;

    /**
     * 许可与组织
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 许可标识
         */
        permissionId,
    }
}