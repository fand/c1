package com.cardone.platform.common.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 实体文件
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityFile extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 879401058097205655L;

    /**
     * 内容类型
     */
    private String contentType;

    /**
     * 实体标识
     */
    private String entityId;

    /**
     * 扩展
     */
    private String extension;

    /**
     * 文件名
     */
    private String filename;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 原文件名
     */
    private String originalFilename;

    /**
     * 父级标识
     */
    private String parentId;

    /**
     * 小图尺寸
     */
    private String thumbnailValue;

    /**
     * 字典.类别标识
     */
    private String typeId;
}