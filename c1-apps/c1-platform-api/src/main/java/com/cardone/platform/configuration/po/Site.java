package com.cardone.platform.configuration.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 站点
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class Site extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 428655907261654989L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 工程编号
     */
    private String projectCode;

    /**
     * 字典.工程标识
     */
    private String projectId;

    /**
     * 工程名称
     */
    private String projectName;

    /**
     * 样式代码
     */
    private String styleCode;

    /**
     * 字典.样式标识
     */
    private String styleId;

    /**
     * 样式名称
     */
    private String styleName;
}