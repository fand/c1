package com.cardone.platform.usercenter.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

import java.util.*;

/**
 * 用户
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class User extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 689137274431956874L;

    /**
     * 地址
     */
    private String address;

    /**
     * 年龄
     */
    private Integer age;

    /**
     * 出生日期
     */
    private Date birthday;

    /**
     * 昵称
     */
    private String callName;

    /**
     * 市代码
     */
    private String cityCode;

    /**
     * 市标识
     */
    private String cityId;

    /**
     * 市名称
     */
    private String cityName;

    /**
     * 工作单位
     */
    private String companyName;

    /**
     * 国家代码
     */
    private String countryCode;

    /**
     * 国家标识
     */
    private String countryId;

    /**
     * 国家名称
     */
    private String countryName;

    /**
     * 部门代码
     */
    private String departmentCode;

    /**
     * 部门标识
     */
    private String departmentId;

    /**
     * 部门名称
     */
    private String departmentName;

    /**
     * 学历/文凭代码
     */
    private String diplomaCode;

    /**
     * 学历/文凭标识
     */
    private String diplomaId;

    /**
     * 学历/文凭名称
     */
    private String diplomaName;

    /**
     * 区代码
     */
    private String districtCode;

    /**
     * 区标识
     */
    private String districtId;

    /**
     * 区名称
     */
    private String districtName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 标记代码
     */
    private String flagCode;

    /**
     * 标记标识
     */
    private String flagId;

    /**
     * 标记名称
     */
    private String flagName;

    /**
     * 民族代码
     */
    private String folkCode;

    /**
     * 民族标识
     */
    private String folkId;

    /**
     * 民族名称
     */
    private String folkName;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 简介
     */
    private String intro;

    /**
     * 现居住地
     */
    private String locus;

    /**
     * 婚姻状态代码
     */
    private String marryStateCode;

    /**
     * 婚姻状态标识
     */
    private String marryStateId;

    /**
     * 婚姻状态名称
     */
    private String marryStateName;

    /**
     * 手机
     */
    private String mobilePhone;

    /**
     * 编号
     */
    private String no;

    /**
     * 组织代码
     */
    private String orgCode;

    /**
     * 组织标识
     */
    private String orgId;

    /**
     * 组织名称
     */
    private String orgName;

    /**
     * 密码
     */
    private String password;

    /**
     * 职业代码
     */
    private String professionCode;

    /**
     * 职业标识
     */
    private String professionId;

    /**
     * 职业名称
     */
    private String professionName;

    /**
     * 省代码
     */
    private String provinceCode;

    /**
     * 省标识
     */
    private String provinceId;

    /**
     * 省名称
     */
    private String provinceName;

    /**
     * 性别代码
     */
    private String sexCode;

    /**
     * 性别标识
     */
    private String sexId;

    /**
     * 性别名称
     */
    private String sexName;

    /**
     * 联系电话
     */
    private String telephone;
}