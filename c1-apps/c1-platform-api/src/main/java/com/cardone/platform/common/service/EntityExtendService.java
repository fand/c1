package com.cardone.platform.common.service;

/**
 * 实体扩展服务
 *
 * @author yaohaitao
 */
public interface EntityExtendService extends com.cardone.common.service.SimpleService<com.cardone.platform.common.dto.EntityExtendDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.common.service.EntityExtendService";
}