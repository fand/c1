package com.cardone.platform.configuration.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 站点位置
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class SiteUrl extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 788180009661046722L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 站点代码
     */
    private String siteCode;

    /**
     * 站点标识
     */
    private String siteId;

    /**
     * 站点名称
     */
    private String siteName;
}