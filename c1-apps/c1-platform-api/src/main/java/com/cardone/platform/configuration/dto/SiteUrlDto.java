package com.cardone.platform.configuration.dto;

import com.cardone.platform.configuration.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 站位置
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class SiteUrlDto extends SiteUrl {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 857153989859210106L;
    /**
     * 项目代码
     */
    private String projectCode;
    /**
     * 项目名称
     */
    @Transient
    private String projectName;

    /**
     * 站位置
     *
     * @return 站位置
     */
    public static SiteUrlDto newSiteUrl() {
        return new SiteUrlDto();
    }

    /**
     * 站位置
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}