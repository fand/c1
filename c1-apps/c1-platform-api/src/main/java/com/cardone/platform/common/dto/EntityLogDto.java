package com.cardone.platform.common.dto;

import com.cardone.platform.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 实体日志
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityLogDto extends EntityLog {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 673722042637992786L;
    /**
     * 字典.类别代码
     */
    @Transient
    private String typeCode;
    /**
     * 字典.类别名称
     */
    @Transient
    private String typeName;
    /**
     * 字典.类别类别代码
     */
    @Transient
    private String typeTypeCode;

    /**
     * 实体日志
     *
     * @return 实体日志
     */
    public static EntityLogDto newEntityLog() {
        return new EntityLogDto();
    }

    /**
     * 实体日志
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 实体标识
         */
        entityId,

        /**
         * 消息
         */
        message,
    }
}