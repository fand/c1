package com.cardone.platform.cms.dto;

import com.cardone.platform.cms.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 公告
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class NoticeDto extends Notice {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 464875194964665661L;
}