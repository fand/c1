package com.cardone.platform.common.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 实体编号
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityCode extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 673058786555734112L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 类别代码
     */
    private String typeCode;
}