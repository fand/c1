package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 许可、验证规则与处理模式
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class PeVaRuTransactModeDto extends PeVaRuTransactMode {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 381794572672018180L;
    /**
     * 许可代码
     */
    @Transient
    private String permissionCode;
    /**
     * 许可代码
     */
    @Transient
    private String permissionName;
    /**
     * 许可类别代码
     */
    @Transient
    private String permissionTypeCode;
    /**
     * 许可类别代码
     */
    @Transient
    private String permissionTypeId;
    /**
     * 许可类别代码
     */
    @Transient
    private String permissionTypeName;
    /**
     * 字典.验证失败代码
     */
    @Transient
    private String validateErrorCode;
    /**
     * 字典.验证失败代码
     */
    @Transient
    private String validateErrorName;
    /**
     * 字典.验证规则代码
     */
    @Transient
    private String validateRuleCode;
    /**
     * 字典.验证规则代码
     */
    @Transient
    private String validateRuleName;
    /**
     * 字典.验证成功代码
     */
    @Transient
    private String validateSuccessCode;
    /**
     * 字典.验证成功代码
     */
    @Transient
    private String validateSuccessName;

    /**
     * 许可、验证规则与处理模式
     *
     * @return 许可、验证规则与处理模式
     */
    public static PeVaRuTransactModeDto newPeVaRuTransactMode() {
        return new PeVaRuTransactModeDto();
    }

    /**
     * 许可、验证规则与处理模式
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 许可标识
         */
        permissionId,

        /**
         * 字典.验证失败标识
         */
        validateErrorId,

        /**
         * 字典.验证规则标识
         */
        validateRuleId,

        /**
         * 字典.验证成功标识
         */
        validateSuccessId,
    }
}