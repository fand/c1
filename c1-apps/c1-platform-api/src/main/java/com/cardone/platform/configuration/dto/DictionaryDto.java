package com.cardone.platform.configuration.dto;

import com.cardone.platform.configuration.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 字典
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class DictionaryDto extends Dictionary {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 524722433133149213L;
    /**
     * 类别代码
     */
    private String typeCode;
    /**
     * 类别名称
     */
    private String typeName;
}