package com.cardone.platform.usercenter.service;

import java.util.*;

/**
 * 组织服务
 *
 * @author yaohaitao
 */
public interface OrgService extends com.cardone.common.service.SimpleService<com.cardone.platform.usercenter.dto.OrgDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.usercenter.service.OrgService";

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param id          组织标识
     * @param isSuperRole 是否超级角色
     * @return 组织对象集合
     */
    <P> List<P> findListByIdForTree(final Class<P> mappedClass, final String id, final boolean isSuperRole);
}