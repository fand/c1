package com.cardone.platform.common.dto;

import com.cardone.platform.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 实体文件
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityFileDto extends EntityFile {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 104073632000656744L;
    /**
     * 缩略图代码
     */
    @Transient
    private String thumbnailsCode;

    /**
     * 缩略图类别代码
     */
    @Transient
    private String thumbnailsTypeCode;
    /**
     * 类别代码
     */
    @Transient
    private String typeCode;
    /**
     * 类别名称
     */
    @Transient
    private String typeName;

    /**
     * 子集
     */
    @Transient
    private java.util.List<EntityFileDto> childs;

    /**
     * 实体文件
     *
     * @return 实体文件
     */
    public static EntityFileDto newEntityFile() {
        return new EntityFileDto();
    }

    /**
     * 实体文件
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 内容类型
         */
        contentType,

        /**
         * 实体标识
         */
        entityId,

        /**
         * 扩展
         */
        extension,

        /**
         * 文件名
         */
        filename,

        /**
         * 原文件名
         */
        originalFilename,

        /**
         * 小图尺寸
         */
        thumbnailValue,
    }
}