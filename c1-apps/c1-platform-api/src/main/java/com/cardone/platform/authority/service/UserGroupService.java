package com.cardone.platform.authority.service;

/**
 * 用户组服务
 *
 * @author yaohaitao
 */
public interface UserGroupService extends com.cardone.common.service.SimpleService<com.cardone.platform.authority.dto.UserGroupDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.UserGroupService";
}