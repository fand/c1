package com.cardone.platform.common.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 实体扩展
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityExtend extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 766769044770359335L;

    /**
     * 实体标识
     */
    private String entityId;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 说明
     */
    private String remark;

    /**
     * 字典.类别标识
     */
    private String typeId;

    /**
     * 值
     */
    private String value;
}