package com.cardone.platform.configuration.service;

import com.cardone.common.cache.*;
import com.cardone.common.service.*;
import com.cardone.platform.configuration.dto.*;
import org.springframework.cache.annotation.*;

import java.io.*;
import java.util.*;

/**
 * 字典服务
 *
 * @author yaohaitao
 */
public interface DictionaryService extends SimpleService<DictionaryDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.configuration.service.DictionaryService";


    /**
     * 查询:字典
     *
     * @param typeCodeList 类别代码集合
     * @return 字典集合Map
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_2)
    <V> Map<String, List<V>> findListMapByTypeCodeList(final Class<V> mappedClass, final List<String> typeCodeList);


    /**
     * 查询:字典
     *
     * @param typeCodes 类别代码集合
     * @return 字典集合Map
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_2)
    <V> Map<String, List<V>> findListMapByTypeCodeList(final Class<V> mappedClass, String typeCodes);

    /**
     * 查询:字典
     *
     * @param typeCodeList 类别代码集合
     * @return 字典集合Map
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_1)
    Map<String, List<Map<String, Object>>> findListMapByTypeCodeList(final List<String> typeCodeList);

    /**
     * 读取：字典标识
     *
     * @param defaultId 默认值
     * @param typeCode  类别代码
     * @param code      代码
     * @return 返回值
     */
    String readIdByTypeCodeAndCode(final String defaultId, final String typeCode, final String code);

    /**
     * 读取：字典名称
     *
     * @param defaultName 默认值
     * @param typeCode    类别代码
     * @param code        代码
     * @return 返回值
     */
    String readNameByTypeCodeAndCode(final String defaultName, final String typeCode, final String code);

    /**
     * 读取：字典值
     *
     * @param defaultValue 默认值
     * @param typeCode     类别代码
     * @param code         代码
     * @return 返回值
     */
    String readValueByTypeCodeAndCode(final String defaultValue, final String typeCode, final String code);

    /**
     * 读取：字典值
     *
     * @param typeCode 类别代码
     * @param code     代码
     * @return 返回值
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_2)
    String readValueByTypeCodeAndCode(final String typeCode, final String code);

    /**
     * 读取：字典名称
     *
     * @param typeCode 类别代码
     * @param code     代码
     * @return 返回值
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_2)
    String readNameByTypeCodeAndCode(final String typeCode, final String code);

    /**
     * 读取：字典标识
     *
     * @param typeCode 类别代码
     * @param code     代码
     * @return 返回值
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_2)
    String readIdByTypeCodeAndCode(final String typeCode, final String code);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param typeCode    类别代码
     * @param code        代码
     * @return 返回对象
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_3)
    <P> P findByTypeCodeAndCode(final Class<P> mappedClass, final String typeCode, final String code);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param typeCode    类型代码
     * @return 字典对象集合
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_2)
    <P> List<P> findListByTypeCode(final Class<P> mappedClass, final String typeCode);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param typeId      类型标识
     * @return 字典对象集合
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_2)
    <P> List<P> findListByTypeId(final Class<P> mappedClass, final String typeId);

    /**
     * 查询
     *
     * @param typeId 类型标识
     * @return 字典对象集合
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_1)
    List<Map<String, Object>> findListByTypeId(final String typeId);

    /**
     * 查询
     *
     * @param typeCode 类型代码
     * @return 字典对象集合
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_1)
    List<Map<String, Object>> findListByTypeCode(final String typeCode);

    /**
     * 查询
     *
     * @param typeCode 类别代码
     * @param code     代码
     * @return 返回对象
     */
    @Cacheable(value = DictionaryService.BEAN_ID, key = Caches.KEY_2)
    Map<String, Object> findByTypeCodeAndCode(final String typeCode, final String code);
}