package com.cardone.platform.authority.service;

/**
 * 用户组与组织服务
 *
 * @author yaohaitao
 */
public interface UsGroupOrgService extends com.cardone.common.service.SimpleService<com.cardone.platform.authority.dto.UsGroupOrgDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.UsGroupOrgService";
}