package com.cardone.platform.configuration.util;

import com.cardone.context.*;
import com.cardone.platform.configuration.dto.*;
import com.cardone.platform.configuration.service.*;

import java.util.*;

/**
 * 字典类型工具类
 *
 * @author yaohaitao
 */
public class DictionaryTypeUtils {
    private DictionaryTypeUtils() {
    }

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param parentCode  父级代码
     * @return 字典类型对象集合
     */
    public static <P> List<P> findListByParentCode(final Class<P> mappedClass, final String parentCode) {
        return ContextHolder.getBean(DictionaryTypeService.class).findListByParentCode(mappedClass, parentCode);
    }

    /**
     * 查询:字典类别
     *
     * @param parentCode 父级代码
     * @return 字典集合
     */
    public static List<DictionaryTypeDto> findListByParentCode(final String parentCode) {
        return ContextHolder.getBean(DictionaryTypeService.class).findListByParentCode(DictionaryTypeDto.class, parentCode);
    }

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param parentId    父级标识
     * @return 字典类型对象集合
     */
    public static <P> List<P> findListByParentId(final Class<P> mappedClass, final String parentId) {
        return ContextHolder.getBean(DictionaryTypeService.class).findListByParentId(mappedClass, parentId);
    }

    /**
     * 查询:字典类别
     *
     * @param parentId 父级标识
     * @return 字典集合
     */
    public static List<DictionaryTypeDto> findListByParentId(final String parentId) {
        return ContextHolder.getBean(DictionaryTypeService.class).findListByParentId(DictionaryTypeDto.class, parentId);
    }
}
