package com.cardone.platform.authority.service;

/**
 * 许可、验证规则与处理模式服务
 *
 * @author yaohaitao
 */
public interface PeVaRuTransactModeService extends com.cardone.common.service.SimpleService<com.cardone.platform.authority.dto.PeVaRuTransactModeDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.PeVaRuTransactModeService";
}