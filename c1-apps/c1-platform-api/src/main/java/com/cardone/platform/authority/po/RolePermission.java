package com.cardone.platform.authority.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 角色与许可
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class RolePermission extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 897423570579811148L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 许可标识
     */
    private String permissionId;

    /**
     * 角色标识
     */
    private String roleId;
}