package com.cardone.platform.common.service;

/**
 * 实体文件服务
 *
 * @author yaohaitao
 */
public interface EntityFileService extends com.cardone.common.service.SimpleService<com.cardone.platform.common.dto.EntityFileDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.common.service.EntityFileService";
}