package com.cardone.platform.common.service;

/**
 * 实体日志服务
 *
 * @author yaohaitao
 */
public interface EntityLogService extends com.cardone.common.service.SimpleService<com.cardone.platform.common.dto.EntityLogDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.common.service.EntityLogService";

    /**
     * 读取有效的实体标识
     *
     * @param id 标识
     * @return 有效的实体标识
     */
    @org.springframework.cache.annotation.Cacheable(value = EntityLogService.BEAN_ID, key = com.cardone.common.cache.Caches.KEY_1)
    String readEntityIdByIdForValid(final String id);
}