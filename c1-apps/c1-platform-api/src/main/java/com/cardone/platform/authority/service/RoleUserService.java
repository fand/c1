package com.cardone.platform.authority.service;

import org.springframework.cache.annotation.*;

import java.util.*;

/**
 * 角色与用户服务
 *
 * @author yaohaitao
 */
public interface RoleUserService extends com.cardone.common.service.SimpleService<com.cardone.platform.authority.dto.RoleUserDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.RoleUserService";

    /**
     * 保存:角色与用户
     *
     * @param userId  用户标识
     * @param roleIds 角色标识集合
     * @return 角色与用户对象
     */
    @CacheEvict(value = {RoleUserService.BEAN_ID, RoleService.BEAN_ID}, allEntries = true)
    Map<String, Object> saveUserIdAndRoleIds(final String userId, final String roleIds);

    /**
     * 插入
     *
     * @param roleCodeList 角色代码集合
     * @param userId       用户标识
     * @return 影响行数
     */
    @CacheEvict(value = {RoleUserService.BEAN_ID, RoleService.BEAN_ID}, allEntries = true)
    int[] insertByRoleCode(final List<String> roleCodeList, final String userId);
}