package com.cardone.platform.authority.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 许可与组织
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class PermissionOrg extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 638940233972798764L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 组织标识
     */
    private String orgId;

    /**
     * 许可标识
     */
    private String permissionId;
}