package com.cardone.platform.common.service;

/**
 * 实体与默认服务
 *
 * @author yaohaitao
 */
public interface EntityDefaultService extends com.cardone.common.service.SimpleService<com.cardone.platform.common.dto.EntityDefaultDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.common.service.EntityDefaultService";
}