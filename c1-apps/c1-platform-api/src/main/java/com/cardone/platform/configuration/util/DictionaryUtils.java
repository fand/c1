package com.cardone.platform.configuration.util;

import com.cardone.context.*;
import com.cardone.platform.configuration.dto.*;
import com.cardone.platform.configuration.service.*;

import java.util.*;

/**
 * 字典工具类
 *
 * @author yaohaitao
 */
public class DictionaryUtils {
    private DictionaryUtils() {
    }

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param typeCode    类别代码
     * @param code        代码
     * @return 返回对象
     */
    public static <P> P findByTypeCodeAndCode(final Class<P> mappedClass, final String typeCode, final String code) {
        return ContextHolder.getBean(DictionaryService.class).findByTypeCodeAndCode(mappedClass, typeCode, code);
    }

    /**
     * 查询:字典
     *
     * @param typeCode 类别代码
     * @param code     代码
     * @return 字典
     */
    public static DictionaryDto findByTypeCodeAndCode(final String typeCode, final String code) {
        return ContextHolder.getBean(DictionaryService.class).findByTypeCodeAndCode(DictionaryDto.class, typeCode, code);
    }

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param typeCode    类型代码
     * @return 字典对象集合
     */
    public static <P> List<P> findListByTypeCode(final Class<P> mappedClass, final String typeCode) {
        return ContextHolder.getBean(DictionaryService.class).findListByTypeCode(mappedClass, typeCode);
    }

    /**
     * 查询:字典
     *
     * @param typeCode 类别代码
     * @return 字典集合
     */
    public static List<DictionaryDto> findListByTypeCode(final String typeCode) {
        return ContextHolder.getBean(DictionaryService.class).findListByTypeCode(DictionaryDto.class, typeCode);
    }

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param typeId      类型标识
     * @return 字典对象集合
     */
    public static <P> List<P> findListByTypeId(final Class<P> mappedClass, final String typeId) {
        return ContextHolder.getBean(DictionaryService.class).findListByTypeId(mappedClass, typeId);
    }

    /**
     * 查询:字典
     *
     * @param typeId 类别标识
     * @return 字典集合
     */
    public static List<DictionaryDto> findListByTypeId(final String typeId) {
        return ContextHolder.getBean(DictionaryService.class).findListByTypeId(DictionaryDto.class, typeId);
    }

    /**
     * 读取：字典标识
     *
     * @param typeCode 类别代码
     * @param code     代码
     * @return 返回值
     */
    public static String readIdByTypeCodeAndCode(final String typeCode, final String code) {
        return ContextHolder.getBean(DictionaryService.class).readIdByTypeCodeAndCode(typeCode, code);
    }

    /**
     * 读取：字典标识
     *
     * @param defaultId 默认值
     * @param typeCode  类别代码
     * @param code      代码
     * @return 返回值
     */
    public static String readIdByTypeCodeAndCode(final String defaultId, final String typeCode, final String code) {
        return ContextHolder.getBean(DictionaryService.class).readIdByTypeCodeAndCode(defaultId, typeCode, code);
    }

    /**
     * 读取：字典名称
     *
     * @param typeCode 类别代码
     * @param code     代码
     * @return 返回值
     */
    public static String readNameByTypeCodeAndCode(final String typeCode, final String code) {
        return ContextHolder.getBean(DictionaryService.class).readNameByTypeCodeAndCode(typeCode, code);
    }

    /**
     * 读取：字典名称
     *
     * @param defaultName 默认值
     * @param typeCode    类别代码
     * @param code        代码
     * @return 返回值
     */
    public static String readNameByTypeCodeAndCode(final String defaultName, final String typeCode, final String code) {
        return ContextHolder.getBean(DictionaryService.class).readNameByTypeCodeAndCode(defaultName, typeCode, code);
    }

    /**
     * 读取：字典值
     *
     * @param typeCode 类别代码
     * @param code     代码
     * @return 返回值
     */
    public static String readValueByTypeCodeAndCode(final String typeCode, final String code) {
        return ContextHolder.getBean(DictionaryService.class).readValueByTypeCodeAndCode(typeCode, code);
    }

    /**
     * 读取：字典值
     *
     * @param defaultValue 默认值
     * @param typeCode     类别代码
     * @param code         代码
     * @return 返回值
     */
    public static String readValueByTypeCodeAndCode(final String defaultValue, final String typeCode, final String code) {
        return ContextHolder.getBean(DictionaryService.class).readValueByTypeCodeAndCode(defaultValue, typeCode, code);
    }
}
