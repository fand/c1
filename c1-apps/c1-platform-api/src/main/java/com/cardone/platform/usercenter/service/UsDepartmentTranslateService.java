package com.cardone.platform.usercenter.service;

/**
 * 用户部门调动服务
 *
 * @author yaohaitao
 */
public interface UsDepartmentTranslateService extends com.cardone.common.service.SimpleService<com.cardone.platform.usercenter.dto.UsDepartmentTranslateDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.usercenter.service.UsDepartmentTranslateService";
}