package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 角色与用户
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class RoleUserDto extends RoleUser {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 881724332533325248L;
    /**
     * 角色代码
     */
    private String roleCode;

    /**
     * 角色与用户
     *
     * @return 角色与用户
     */
    public static RoleUserDto newRoleUser() {
        return new RoleUserDto();
    }

    /**
     * 角色与用户
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}