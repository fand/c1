package com.cardone.platform.usercenter.service;

import com.cardone.common.cache.Caches;
import com.cardone.common.dto.PaginationDto;
import com.cardone.platform.usercenter.dto.UserDto;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

/**
 * 用户服务
 *
 * @author yaohaitao
 */
public interface UserService extends com.cardone.common.service.SimpleService<com.cardone.platform.usercenter.dto.UserDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.usercenter.service.UserService";

    /**
     * 保存:用户
     *
     * @param saveUser 用户对象
     * @return 用户对象
     */
    @CacheEvict(value = UserService.BEAN_ID, allEntries = true)
    UserDto saveByCode(final UserDto saveUser);

    /**
     * 登录
     *
     * @param loginUser 登录信息
     * @return 登录结果
     */
    UserDto login(final UserDto loginUser);

    /**
     * 登录
     *
     * @param loginUser 登录信息
     * @return 登录结果
     */
    UserDto login(final UserDto loginUser, String host);

    /**
     * 注册
     *
     * @param registerUser 注册信息
     * @return 注册结果
     */
    @CacheEvict(value = UserService.BEAN_ID, allEntries = true)
    UserDto register(final UserDto registerUser);

    /**
     * 更新
     *
     * @param id 标识
     * @return 影响行数
     */
    @CacheEvict(value = UserService.BEAN_ID, allEntries = true)
    int updateEndDateByIdForInvalid(final String id);

    /**
     * 查询
     *
     * @param findUser 用户对象
     * @return 返回对象
     */
    @Cacheable(value = UserService.BEAN_ID, key = Caches.KEY_1)
    UserDto findById(final UserDto findUser);

    /**
     * 查询
     *
     * @param id 标识
     * @return 返回对象
     */
    @Cacheable(value = UserService.BEAN_ID, key = Caches.KEY_1)
    UserDto findById(final String id);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param findUser    用户对象
     * @return 返回对象
     */
    @Cacheable(value = UserService.BEAN_ID, key = Caches.KEY_2)
    <P> P findByCodeForLogin(final Class<P> mappedClass, final UserDto findUser);

    /**
     * 分页
     *
     * @param mappedClass    返回类型
     * @param paginationUser 用户对象
     * @return 用户分页对象
     */
    <P> PaginationDto<P> paginationByOrgIdAndLikeCode(final Class<P> mappedClass, final UserDto paginationUser);

    /**
     * 更新
     *
     * @param id          用户标识
     * @param newPassword 密码
     * @return 影响行数
     */
    @CacheEvict(value = UserService.BEAN_ID, allEntries = true)
    int updatePasswordById(final String id, final String newPassword);

    /**
     * 更新
     *
     * @param id          用户标识
     * @param newPassword 密码
     * @return 影响行数
     */
    @CacheEvict(value = UserService.BEAN_ID, allEntries = true)
    int updatePasswordById(final String id, final String oldPassword, final String newPassword);

    /**
     * 更新
     *
     * @param id          用户标识
     * @param newPassword 密码
     * @param isDiges     是否加密
     * @return 影响行数
     */
    @CacheEvict(value = UserService.BEAN_ID, allEntries = true)
    int updatePasswordById(final String id, final String oldPassword, final String newPassword, final Boolean isDiges);

    /**
     * 更新
     *
     * @param updateUser        用户对象
     * @param updateMobilePhone 更新手机号码
     * @return 影响行数
     */
    @CacheEvict(value = UserService.BEAN_ID, allEntries = true)
    int updateForBaseInfo(final UserDto updateUser, final Boolean updateMobilePhone);

    /**
     * 更新
     *
     * @param updateUser        用户对象
     * @param updateMobilePhone 更新手机号码
     * @return 影响行数
     */
    @CacheEvict(value = UserService.BEAN_ID, allEntries = true)
    int updateForFullInfo(final UserDto updateUser, final Boolean updateMobilePhone);

    /**
     * 更新
     *
     * @param updateUser 用户对象
     * @return 影响行数
     */
    @CacheEvict(value = UserService.BEAN_ID, allEntries = true)
    int updateForDetail(final UserDto updateUser);

    /**
     * 更新手机号码
     *
     * @param id          标识
     * @param mobilePhone 手机号码
     * @return 影响行数
     */
    @CacheEvict(value = UserService.BEAN_ID, allEntries = true)
    int updateMobilePhone(final String id, final String mobilePhone);
}