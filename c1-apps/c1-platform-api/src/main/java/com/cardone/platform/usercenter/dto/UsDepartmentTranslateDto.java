package com.cardone.platform.usercenter.dto;

import com.cardone.platform.usercenter.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户部门调动
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsDepartmentTranslateDto extends UsDepartmentTranslate {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 896463196534117818L;

    /**
     * 用户部门调动
     *
     * @return 用户部门调动
     */
    public static UsDepartmentTranslateDto newUsDepartmentTranslate() {
        return new UsDepartmentTranslateDto();
    }

    /**
     * 用户部门调动
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}