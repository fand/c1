package com.cardone.platform.configuration.service;

import com.cardone.common.cache.*;
import com.cardone.common.service.*;
import com.cardone.platform.configuration.dto.*;
import org.springframework.cache.annotation.*;

import java.util.*;

/**
 * 站服务
 *
 * @author yaohaitao
 */
public interface SiteService extends SimpleService<SiteDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.configuration.service.SiteService";

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param serverName  服务名
     * @param projectCode 项目代码
     * @param code        站点代码
     * @return 返回对象
     */
    @Cacheable(value = SiteService.BEAN_ID, key = Caches.KEY_4)
    <P> P findByServerName(final Class<P> mappedClass, final String serverName, final String projectCode, final String code);

    /**
     * 查询
     *
     * @param id 标识
     * @return 返回对象
     */
    @Cacheable(value = SiteService.BEAN_ID, key = Caches.KEY_2)
    <P> P findById(final Class<P> mappedClass, String id);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param siteUrlCode 站路径代码
     * @return 返回对象
     */
    @Cacheable(value = SiteService.BEAN_ID, key = Caches.KEY_2)
    <P> P findBySiteUrlCode(final Class<P> mappedClass, final String siteUrlCode);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param projectCode 项目代码
     * @return 站对象集合
     */
    @Cacheable(value = SiteService.BEAN_ID, key = Caches.KEY_2)
    <P> List<P> findListByProjectCode(final Class<P> mappedClass, final String projectCode);
}