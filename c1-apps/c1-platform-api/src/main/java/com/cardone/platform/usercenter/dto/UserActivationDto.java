package com.cardone.platform.usercenter.dto;

import com.cardone.platform.usercenter.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户激活
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UserActivationDto extends UserActivation {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 403101687113202649L;

    /**
     * 用户激活
     *
     * @return 用户激活
     */
    public static UserActivationDto newUserActivation() {
        return new UserActivationDto();
    }

    /**
     * 用户激活
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 密码
         */
        password,
    }
}