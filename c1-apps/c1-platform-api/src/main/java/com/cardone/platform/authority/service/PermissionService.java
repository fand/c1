package com.cardone.platform.authority.service;

import com.cardone.common.cache.*;
import org.springframework.cache.annotation.*;

import java.util.*;

/**
 * 许可服务
 *
 * @author yaohaitao
 */
public interface PermissionService extends com.cardone.common.service.SimpleService<com.cardone.platform.authority.dto.PermissionDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.PermissionService";

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param siteId      站标识
     * @return 许可对象集合
     */
    @Cacheable(value = PermissionService.BEAN_ID, key = Caches.KEY_2)
    <P> List<P> findListBySiteId(final Class<P> mappedClass, final String siteId);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param siteId      站标识
     * @param roleId      角色标识
     * @return 许可对象集合
     */
    @Cacheable(value = PermissionService.BEAN_ID, key = Caches.KEY_3)
    <P> List<P> findListBySiteIdAndRoleId(final Class<P> mappedClass, final String siteId, final String roleId);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param siteId      站标识
     * @param typeId      类别标识
     * @return 许可对象集合
     */
    @Cacheable(value = PermissionService.BEAN_ID, key = Caches.KEY_3)
    <P> List<P> findListBySiteIdAndTypeId(final Class<P> mappedClass, final String siteId, final String typeId);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param siteId      站标识
     * @param typeCode    类别代码
     * @return 许可对象集合
     */
    @Cacheable(value = PermissionService.BEAN_ID, key = Caches.KEY_3)
    <P> List<P> findListBySiteIdAndTypeCode(final Class<P> mappedClass, final String siteId, final String typeCode);

    /**
     * 查询
     *
     * @param siteId   站标识
     * @param typeCode 类别代码
     * @return 许可对象集合
     */
    @Cacheable(value = PermissionService.BEAN_ID, key = Caches.KEY_2)
    List<String> readCodeListBySiteIdAndTypeCode(final String siteId, final String typeCode);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param siteId      站标识
     * @param roleIdList  角色标识集合
     * @return 许可对象集合
     */
    @Cacheable(value = PermissionService.BEAN_ID, key = Caches.KEY_3)
    <P> List<P> findListBySiteIdAndRoleIdList(final Class<P> mappedClass, final String siteId, final List<String> roleIdList);
}