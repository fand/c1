package com.cardone.platform.configuration.dto;

import com.cardone.platform.configuration.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 字典项
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class DictionaryItemDto extends DictionaryItem {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 546450120459043707L;
    /**
     * 字典代码
     */
    @Transient
    private String dictionaryCode;
    /**
     * 字典名称
     */
    @Transient
    private String dictionaryName;
    /**
     * 类别代码
     */
    @Transient
    private String typeCode;
    /**
     * 类别标识
     */
    @Transient
    private String typeId;
    /**
     * 类别名称
     */
    @Transient
    private String typeName;

    /**
     * 字典项
     *
     * @return 字典项
     */
    public static DictionaryItemDto newDictionaryItem() {
        return new DictionaryItemDto();
    }

    /**
     * 字典项
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 字典标识
         */
        dictionaryId,
    }
}