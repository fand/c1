package com.cardone.platform.authority.service;

/**
 * 用户组与用户服务
 *
 * @author yaohaitao
 */
public interface UsGroupUserService extends com.cardone.common.service.SimpleService<com.cardone.platform.authority.dto.UsGroupUserDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.UsGroupUserService";
}