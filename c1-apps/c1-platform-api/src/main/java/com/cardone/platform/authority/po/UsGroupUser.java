package com.cardone.platform.authority.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户组与用户
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGroupUser extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 899721314425023203L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 用户组标识
     */
    private String userGroupId;

    /**
     * 用户标识
     */
    private String userId;
}