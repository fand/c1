package com.cardone.platform.configuration.dto;

import com.cardone.platform.configuration.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

import java.util.*;

/**
 * 字典类型
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class DictionaryTypeDto extends DictionaryType {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 235988958805321618L;
    /**
     * 子集数量
     */
    @Transient
    private int childCount;
    /**
     * 子集
     */
    @Transient
    private List<DictionaryTypeDto> childs;
    /**
     * 子集总数
     */
    @Transient
    private int childTotal;
    /**
     * 字典集合
     */
    @Transient
    private List<DictionaryDto> dictionaryList;
    /**
     * 父级代码
     */
    private String parentCode;
    /**
     * 父级名称
     */
    private String parentName;

    /**
     * 字典类型
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}