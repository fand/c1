package com.cardone.platform.cms.service;

import com.cardone.common.service.*;
import com.cardone.platform.cms.dto.*;

/**
 * 文章服务
 *
 * @author yaohaitao
 */
public interface ArticleService extends SimpleService<ArticleDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.cms.service.ArticleService";
}