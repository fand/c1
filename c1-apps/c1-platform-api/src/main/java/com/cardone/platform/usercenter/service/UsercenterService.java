package com.cardone.platform.usercenter.service;

/**
 * 用户中心服务
 *
 * @author yaohaitao
 */
public interface UsercenterService {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.usercenter.service.UsercenterService";
}