package com.cardone.platform.authority.service;

import java.util.List;
import java.util.Map;

/**
 * 角色与许可服务
 *
 * @author yaohaitao
 */
public interface RolePermissionService extends com.cardone.common.service.SimpleService<com.cardone.platform.authority.dto.RolePermissionDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.RolePermissionService";

    /**
     * 查询角色及授权
     *
     * @param userId 用户标识
     * @return
     */
    Map<String, List<String>> findByUserId(String userId);
}