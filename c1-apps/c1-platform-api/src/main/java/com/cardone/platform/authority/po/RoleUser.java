package com.cardone.platform.authority.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 角色与用户
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class RoleUser extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 133510956005653323L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 角色标识
     */
    private String roleId;

    /**
     * 用户标识
     */
    private String userId;
}