package com.cardone.platform.common.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 操作日志
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class OperateLog extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 753646946853106915L;

    /**
     * 业务代码
     */
    private String businessCode;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 消息
     */
    private String message;

    /**
     * 字典.类别标识
     */
    private String typeId;

    /**
     * 用户标识
     */
    private String userId;

    /**
     * 值
     */
    private String value;
}