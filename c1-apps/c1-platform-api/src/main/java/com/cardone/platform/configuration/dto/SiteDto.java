package com.cardone.platform.configuration.dto;

import com.cardone.platform.configuration.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

import java.util.*;

/**
 * 站
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class SiteDto extends Site {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 308067566060574688L;

    /**
     * 字典.工程代码
     */
    private String projectCode;
    /**
     * 字典.工程名称
     */
    @Transient
    private String projectName;
    /**
     * 字典.工程类别代码
     */
    private String projectTypeCode;

    /**
     * 字典.样式代码
     */
    private String styleCode;
    /**
     * 字典.样式名称
     */
    private String styleName;
    /**
     * 字典.样式类别代码
     */
    private String styleTypeCode;

    /**
     * 站
     *
     * @author yaohaitao
     */
    public enum Attributes {
        /**
         * 站路径代码
         */
        siteUrlCode,
    }
}