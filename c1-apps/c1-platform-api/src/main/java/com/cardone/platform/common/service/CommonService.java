package com.cardone.platform.common.service;

/**
 * 公共服务
 *
 * @author yaohaitao
 */
public interface CommonService {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.common.service.CommonService";
}