package com.cardone.platform.common.dto;

import com.cardone.platform.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 实体编号
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityCodeDto extends EntityCode {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 107132526949959466L;

    /**
     * 实体编号
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 类别代码
         */
        typeCode,
    }
}