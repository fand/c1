package com.cardone.platform.cms.service;

import com.cardone.common.service.*;
import com.cardone.platform.cms.dto.*;

/**
 * 公告服务
 *
 * @author yaohaitao
 */
public interface NoticeService extends SimpleService<NoticeDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.cms.service.NoticeService";
}