package com.cardone.platform.common.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 实体与默认
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityDefault extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 662810830277869533L;

    /**
     * 实体标识
     */
    private String entityId;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 字典.类别标识
     */
    private String typeId;
}