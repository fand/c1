package com.cardone.platform.cms.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

import java.util.*;

/**
 * 公告
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class Notice extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 594782363507647476L;

    /**
     * 正文
     */
    private String content;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 组织代码
     */
    private String orgCode;

    /**
     * 组织标识
     */
    @Id
    private String orgId;

    /**
     * 组织名称
     */
    private String orgName;

    /**
     * 发布日期
     */
    private Date releaseDate;

    /**
     * 标题
     */
    private String title;

    /**
     * 类别代码
     */
    private String typeCode;

    /**
     * 类别标识
     */
    private String typeId;

    /**
     * 类别名称
     */
    private String typeName;
}