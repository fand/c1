package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户组与组织
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGroupOrgDto extends UsGroupOrg {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 888742041576048585L;
    /**
     * 用户组代码
     */
    @Transient
    private String userGroupCode;
    /**
     * 用户组名称
     */
    @Transient
    private String userGroupName;

    /**
     * 用户组与组织
     *
     * @return 用户组与组织
     */
    public static UsGroupOrgDto newUsGroupOrg() {
        return new UsGroupOrgDto();
    }

    /**
     * 用户组与组织
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}