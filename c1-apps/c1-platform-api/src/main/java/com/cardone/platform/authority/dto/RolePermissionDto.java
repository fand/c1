package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 角色与许可
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class RolePermissionDto extends RolePermission {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 866737737184453696L;

    /**
     * 角色与许可
     *
     * @return 角色与许可
     */
    public static RolePermissionDto newRolePermission() {
        return new RolePermissionDto();
    }

    /**
     * 角色与许可
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 许可标识
         */
        permissionId,
    }
}