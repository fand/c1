package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户组
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UserGroupDto extends UserGroup {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 809043785976177199L;

    /**
     * 用户组
     *
     * @return 用户组
     */
    public static UserGroupDto newUserGroup() {
        return new UserGroupDto();
    }

    /**
     * 用户组
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 业务代码
         */
        businessCode,
    }
}