package com.cardone.platform.usercenter.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户组织调动
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsOrgTranslate extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 755596440633404196L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 组织标识
     */
    private String orgId;

    /**
     * 用户标识
     */
    private String userId;
}