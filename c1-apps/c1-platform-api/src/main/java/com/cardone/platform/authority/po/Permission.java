package com.cardone.platform.authority.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 许可
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class Permission extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 785019734519208406L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 站点代码
     */
    private String siteCode;

    /**
     * 站点标识
     */
    private String siteId;

    /**
     * 站点名称
     */
    private String siteName;

    /**
     * 类别代码
     */
    private String typeCode;

    /**
     * 类别标识
     */
    private String typeId;

    /**
     * 类别名称
     */
    private String typeName;
}