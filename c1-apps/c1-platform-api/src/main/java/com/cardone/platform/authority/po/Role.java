package com.cardone.platform.authority.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 角色
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class Role extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 145041727643984722L;

    /**
     * 部门标识
     */
    private String departmentId;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 组织标识
     */
    private String orgId;
}