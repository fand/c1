package com.cardone.platform.authority.service;

/**
 * 用户组与业务范围服务
 *
 * @author yaohaitao
 */
public interface UsGrBusinessAreaService extends com.cardone.common.service.SimpleService<com.cardone.platform.authority.dto.UsGrBusinessAreaDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.UsGrBusinessAreaService";
}