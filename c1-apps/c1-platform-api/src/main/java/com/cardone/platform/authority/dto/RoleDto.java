package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 角色
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class RoleDto extends Role {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 665175639543480721L;
    /**
     * 不包含代码集合
     */
    private String notCodes;
    /**
     * 授权标识集合
     */
    private String permissionIds;

    /**
     * 角色
     *
     * @return 角色
     */
    public static RoleDto newRole() {
        return new RoleDto();
    }

    /**
     * 角色
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}