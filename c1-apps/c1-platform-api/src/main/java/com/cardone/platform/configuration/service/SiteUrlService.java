package com.cardone.platform.configuration.service;

/**
 * 站位置服务
 *
 * @author yaohaitao
 */
public interface SiteUrlService extends com.cardone.common.service.SimpleService<com.cardone.platform.configuration.dto.SiteUrlDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.configuration.service.SiteUrlService";
}