package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户组与业务范围
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGrBusinessAreaDto extends UsGrBusinessArea {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 606552615672877852L;
    /**
     * 业务范围类别代码
     */
    @Transient
    private String buAreaTypeCode;
    /**
     * 业务范围类别标识
     */
    @Transient
    private String buAreaTypeId;
    /**
     * 业务范围类别名称
     */
    @Transient
    private String buAreaTypeName;
    /**
     * 业务范围代码
     */
    @Transient
    private String businessAreaCode;
    /**
     * 业务范围名称
     */
    @Transient
    private String businessAreaName;
    /**
     * 用户组代码
     */
    @Transient
    private String userGroupCode;
    /**
     * 用户组名称
     */
    @Transient
    private String userGroupName;

    /**
     * 用户组与业务范围
     *
     * @return 用户组与业务范围
     */
    public static UsGrBusinessAreaDto newUsGrBusinessArea() {
        return new UsGrBusinessAreaDto();
    }

    /**
     * 用户组与业务范围
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 字典.业务范围标识
         */
        businessAreaId,
    }
}