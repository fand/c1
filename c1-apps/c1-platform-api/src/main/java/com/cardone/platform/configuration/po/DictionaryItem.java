package com.cardone.platform.configuration.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 字典项
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class DictionaryItem extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 551811536322016059L;

    /**
     * 字典代码
     */
    private String dictionaryCode;

    /**
     * 字典标识
     */
    private String dictionaryId;

    /**
     * 字典名称
     */
    private String dictionaryName;

    /**
     * 字典类别代码
     */
    private String dictionaryTypeCode;

    /**
     * 字典类别标识
     */
    private String dictionaryTypeId;

    /**
     * 字典类别名称
     */
    private String dictionaryTypeName;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 说明
     */
    private String remark;

    /**
     * 值
     */
    private String value;
}