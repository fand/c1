package com.cardone.platform.authority.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户组与角色
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGroupRole extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 561601719341832779L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 角色标识
     */
    private String roleId;

    /**
     * 用户组标识
     */
    private String userGroupId;
}