package com.cardone.platform.authority.service;

import com.cardone.common.cache.*;
import com.cardone.common.service.*;
import com.cardone.platform.authority.dto.*;
import org.springframework.cache.annotation.*;

import java.util.*;

/**
 * 导航服务
 *
 * @author yaohaitao
 */
public interface NavigationService extends SimpleService<NavigationDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.NavigationService";

    /**
     * 查询:导航
     *
     * @param siteCode 站代码
     * @param typeCode 类别代码
     * @param parentId 父级标识
     * @param level    层级
     * @return 导航对象集合
     */
    @Cacheable(value = NavigationService.BEAN_ID, key = Caches.KEY_4)
    List<NavigationDto> findListByParentIdForTree(final String siteCode, final String typeCode, final String parentId, final int level);

    /**
     * 查询:导航
     *
     * @param siteCode 站代码
     * @param typeCode 类别代码
     * @param parentId 父级标识
     * @return 导航对象集合
     */
    @Cacheable(value = NavigationService.BEAN_ID, key = Caches.KEY_3)
    List<NavigationDto> findListByParentId(final String siteCode, final String typeCode, final String parentId);

    /**
     * 查询:导航
     *
     * @param siteCode 站代码
     * @param typeCode 类别代码
     * @return 导航对象集合
     */
    @Cacheable(value = NavigationService.BEAN_ID, key = Caches.KEY_2)
    List<NavigationDto> findListBySiteCodeAndTypeCode(final String siteCode, final String typeCode);

    /**
     * 查询:导航
     *
     * @param siteCode   站代码
     * @param typeCode   类别代码
     * @param parentCode 父级代码
     * @return 导航对象集合
     */
    @Cacheable(value = NavigationService.BEAN_ID, key = Caches.KEY_3)
    List<NavigationDto> findListByParentCode(final String siteCode, final String typeCode, final String parentCode);

    /**
     * 查询:导航
     *
     * @param siteCode   站代码
     * @param typeCode   类别代码
     * @param parentCode 父级代码
     * @param level      层级
     * @return 导航对象集合
     */
    @Cacheable(value = NavigationService.BEAN_ID, key = Caches.KEY_4)
    List<NavigationDto> findListByParentCodeForTree(final String siteCode, final String typeCode, final String parentCode, final int level);
}