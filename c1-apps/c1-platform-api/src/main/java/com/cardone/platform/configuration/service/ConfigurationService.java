package com.cardone.platform.configuration.service;

/**
 * 配置服务
 *
 * @author yaohaitao
 */
public interface ConfigurationService {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.configuration.service.ConfigurationService";

    /**
     * 创建序列
     */
    void resetSeq();
}