package com.cardone.platform.configuration.service;

import com.cardone.common.cache.*;
import com.cardone.common.service.*;
import com.cardone.platform.configuration.dto.*;
import org.springframework.cache.annotation.*;

import java.util.*;

/**
 * 字典类型服务
 *
 * @author yaohaitao
 */
public interface DictionaryTypeService extends SimpleService<DictionaryTypeDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.configuration.service.DictionaryTypeService";

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param parentId    父级标识
     * @return 字典类型对象集合
     */
    @Cacheable(value = SiteUrlService.BEAN_ID, key = Caches.KEY_2)
    <P> List<P> findListByParentId(final Class<P> mappedClass, final String parentId);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param parentCode  父级代码
     * @return 字典类型对象集合
     */
    @Cacheable(value = SiteUrlService.BEAN_ID, key = Caches.KEY_2)
    <P> List<P> findListByParentCode(final Class<P> mappedClass, final String parentCode);

    /**
     * 导入excel
     *
     * @param filename 文件名
     * @throws java.io.IOException
     * @throws Exception
     */
    void importExcel(final String filename) throws Exception;
}