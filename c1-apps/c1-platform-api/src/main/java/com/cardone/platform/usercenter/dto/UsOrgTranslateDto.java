package com.cardone.platform.usercenter.dto;

import com.cardone.platform.usercenter.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户组织调动
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsOrgTranslateDto extends UsOrgTranslate {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 728484749499011415L;

    /**
     * 用户组织调动
     *
     * @return 用户组织调动
     */
    public static UsOrgTranslateDto newUsOrgTranslate() {
        return new UsOrgTranslateDto();
    }

    /**
     * 用户组织调动
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}