package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户组与角色
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGroupRoleDto extends UsGroupRole {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 150975846059418406L;
    /**
     * 角色代码
     */
    @Transient
    private String roleCode;
    /**
     * 角色名称
     */
    @Transient
    private String roleName;
    /**
     * 用户组代码
     */
    @Transient
    private String userGroupCode;
    /**
     * 用户组名称
     */
    @Transient
    private String userGroupName;

    /**
     * 用户组与角色
     *
     * @return 用户组与角色
     */
    public static UsGroupRoleDto newUsGroupRole() {
        return new UsGroupRoleDto();
    }

    /**
     * 用户组与角色
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}