package com.cardone.platform.usercenter.service;

/**
 * 用户组织调动服务
 *
 * @author yaohaitao
 */
public interface UsOrgTranslateService extends com.cardone.common.service.SimpleService<com.cardone.platform.usercenter.dto.UsOrgTranslateDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.usercenter.service.UsOrgTranslateService";
}