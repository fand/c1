package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户组与用户
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGroupUserDto extends UsGroupUser {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 690719310900642234L;
    /**
     * 用户代码
     */
    @Transient
    private String userCode;
    /**
     * 用户组代码
     */
    @Transient
    private String userGroupCode;
    /**
     * 用户组名称
     */
    @Transient
    private String userGroupName;
    /**
     * 用户名称
     */
    @Transient
    private String userName;

    /**
     * 用户组与用户
     *
     * @return 用户组与用户
     */
    public static UsGroupUserDto newUsGroupUser() {
        return new UsGroupUserDto();
    }

    /**
     * 用户组与用户
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}