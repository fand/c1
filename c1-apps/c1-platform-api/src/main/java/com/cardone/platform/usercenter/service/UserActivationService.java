package com.cardone.platform.usercenter.service;

/**
 * 用户激活服务
 *
 * @author yaohaitao
 */
public interface UserActivationService extends com.cardone.common.service.SimpleService<com.cardone.platform.usercenter.dto.UserActivationDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.usercenter.service.UserActivationService";
}