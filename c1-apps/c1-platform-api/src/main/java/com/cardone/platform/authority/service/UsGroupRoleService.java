package com.cardone.platform.authority.service;

/**
 * 用户组与角色服务
 *
 * @author yaohaitao
 */
public interface UsGroupRoleService extends com.cardone.common.service.SimpleService<com.cardone.platform.authority.dto.UsGroupRoleDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.UsGroupRoleService";
}