package com.cardone.platform.authority.service;

/**
 * 许可与组织服务
 *
 * @author yaohaitao
 */
public interface PermissionOrgService extends com.cardone.common.service.SimpleService<com.cardone.platform.authority.dto.PermissionOrgDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.PermissionOrgService";
}