package com.cardone.platform.authority.service;

import com.cardone.common.cache.*;
import com.cardone.platform.authority.dto.*;
import org.springframework.cache.annotation.*;

import java.util.*;

/**
 * 角色服务
 *
 * @author yaohaitao
 */
public interface RoleService extends com.cardone.common.service.SimpleService<com.cardone.platform.authority.dto.RoleDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.RoleService";

    /**
     * 保存:角色
     *
     * @param mappedClass 返回类型
     * @param saveRole    角色对象
     * @return 角色对象
     */
    @CacheEvict(value = {RoleService.BEAN_ID, PermissionService.BEAN_ID}, allEntries = true)
    <P> P saveForPermissionIds(final Class<P> mappedClass, final RoleDto saveRole);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param userId      用户标识
     * @return 角色对象集合
     */
    @Cacheable(value = RoleService.BEAN_ID, key = Caches.KEY_2)
    <P> List<P> findListByUserId(final Class<P> mappedClass, final String userId);


    /**
     * 读取
     *
     * @param readRole 角色对象
     * @return 标识
     */
    @Cacheable(value = RoleService.BEAN_ID, key = Caches.KEY_1)
    String readIdByCode(final RoleDto readRole);

    /**
     * 读取
     *
     * @param userId 用户标识
     * @return 角色标识集合
     */
    @Cacheable(value = RoleService.BEAN_ID, key = Caches.KEY_1)
    List<String> readIdListByUserId(final String userId);
}