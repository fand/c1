package com.cardone.platform.common.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 实体日志
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityLog extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 408703789279129173L;

    /**
     * 实体标识
     */
    private String entityId;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 消息
     */
    private String message;

    /**
     * 字典.类别标识
     */
    private String typeId;

    /**
     * 用户标识
     */
    private String userId;
}