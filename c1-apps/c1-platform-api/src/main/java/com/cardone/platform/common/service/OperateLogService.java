package com.cardone.platform.common.service;

import java.util.*;

/**
 * 操作日志服务
 *
 * @author yaohaitao
 */
public interface OperateLogService extends com.cardone.common.service.SimpleService<com.cardone.platform.common.dto.OperateLogDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.common.service.OperateLogService";

    /**
     * 读取数据库当前时间及最大创建时间
     *
     * @param typeTypeCode 类别类别代码
     * @param typeCode     类别代码
     * @param code         代码
     * @return 数据库当前时间及最大创建时间
     */
    List<Date> readListBeginDateForMax(final String typeTypeCode, final String typeCode, final String code);

    /**
     * 读取当天总数
     *
     * @param typeTypeCode 类别类别代码
     * @param typeCode     类别代码
     * @param code         代码
     * @return 当天总数
     */
    Integer readCountByCodeForToDay(final String typeTypeCode, final String typeCode, final String code);

    /**
     * 更新数据：失效
     *
     * @param typeCode 类别代码
     * @param code     代码
     * @return 失效数量
     */
    int updateEndDateForInvalid(final String typeCode, final String code);

    /**
     * 读取数据库当前时间及最大创建时间
     *
     * @param typeTypeCode 类别类别代码
     * @param typeCode     类别代码
     * @param code         代码
     * @param businessCode 业务代码
     * @return 数据库当前时间及最大创建时间
     */
    List<Date> readListBeginDateForMax(final String typeTypeCode, final String typeCode, final String code, final String businessCode);
}