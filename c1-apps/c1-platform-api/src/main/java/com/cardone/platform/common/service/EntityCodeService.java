package com.cardone.platform.common.service;

import com.cardone.common.service.*;
import com.cardone.context.function.*;
import com.cardone.platform.common.dto.*;

import java.util.*;

/**
 * 实体编号服务
 *
 * @author yaohaitao
 */
public interface EntityCodeService extends SimpleService<EntityCodeDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.common.service.EntityCodeService";

    /**
     * 生成代码
     *
     * @param id         标识
     * @param typeCode   类别代码
     * @param parameters 参数
     * @return 代码
     */
    String generateCode(final String id, final String typeCode, final Map<String, Object> parameters);

    /**
     * 生成代码
     *
     * @param id         标识
     * @param typeCode   类别代码
     * @param function   方法
     * @param parameters 参数
     * @return 代码
     */
    String generateCode(final String id, final String typeCode, final Execution1Function<String, String> function, final Map<String, Object> parameters);

    /**
     * 生成代码
     *
     * @param id       标识
     * @param typeCode 类别代码
     * @return 代码
     */
    String generateCode(final String id, final String typeCode);
}