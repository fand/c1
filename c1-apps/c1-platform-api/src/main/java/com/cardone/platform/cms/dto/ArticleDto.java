package com.cardone.platform.cms.dto;

import com.cardone.platform.cms.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 文章
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class ArticleDto extends Article {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 831355627786638856L;
}