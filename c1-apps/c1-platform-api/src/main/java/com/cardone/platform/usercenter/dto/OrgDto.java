package com.cardone.platform.usercenter.dto;

import com.cardone.platform.usercenter.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 组织
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class OrgDto extends Org {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 780617461418145191L;

    /**
     * 组织
     *
     * @return 组织
     */
    public static OrgDto newOrg() {
        return new OrgDto();
    }

    /**
     * 组织
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}