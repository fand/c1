package com.cardone.platform.usercenter.dto;

import com.cardone.common.util.StringUtils;
import com.cardone.platform.authority.dto.PermissionDto;
import com.cardone.platform.usercenter.po.User;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Transient;

import java.util.*;

/**
 * 用户
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UserDto extends User {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 439389283287862577L;
    /**
     * 部门代码
     */
    private String departmentCode;
    /**
     * 部门名称
     */
    private String departmentName;
    /**
     * 字典.学历/文凭代码
     */
    private String diplomaCode;
    /**
     * 字典.学历/文凭名称
     */
    private String diplomaName;
    /**
     * 字典.标记代码
     */
    private String flagCode;
    /**
     * 字典.标记名称
     */
    private String flagName;
    /**
     * 字典.民族代码
     */
    private String folkCode;
    /**
     * 字典.民族名称
     */
    private String folkName;

    /**
     * 登录键名
     */
    private String loginKey;
    /**
     * 字典.婚姻状态代码
     */
    private String marryStateCode;
    /**
     * 字典.婚姻状态名称
     */
    private String marryStateName;
    /**
     * 现在年龄
     */
    private Double nowAge;
    /**
     * 部门代码
     */
    private String orgCode;
    /**
     * 部门名称
     */
    private String orgName;

    private String validCode;

    /**
     * 字典.职业代码
     */
    private String professionCode;
    /**
     * 字典.职业名称
     */
    private String professionName;

    /**
     * 字典.性别代码
     */
    private String sexCode;

    /**
     * 字典.性别名称
     */
    private String sexName;

    /**
     * 新密码
     */
    private String newPassword;

    /**
     * 用户
     *
     * @author yaohaitao
     */
    public enum Attributes {
        /**
         * 账号名称
         */
        accountName,

        /**
         * 地址
         */
        address,

        /**
         * 年龄
         */
        age,

        /**
         * 出生日期
         */
        birthday,

        /**
         * 昵称
         */
        callName,

        /**
         * 字典.市标识
         */
        cityId,

        /**
         * 工作单位
         */
        companyName,

        /**
         * 字典.国家标识
         */
        countryId,

        /**
         * 字典.学历/文凭标识
         */
        diplomaId,

        /**
         * 字典.区标识
         */
        districtId,

        /**
         * 邮箱
         */
        email,

        /**
         * 字典.标记标识(专家/普通用户)
         */
        flagId,

        /**
         * 字典.民族标识
         */
        folkId,

        /**
         * 简介
         */
        intro,

        /**
         * 现居住地
         */
        locus,

        /**
         * 字典.婚姻状态标识
         */
        marryStateId,

        /**
         * 手机
         */
        mobilePhone,

        /**
         * 密码
         */
        password,

        /**
         * 字典.职业标识
         */
        professionId,

        /**
         * 字典.省标识
         */
        provinceId,

        /**
         * 字典.性别标识
         */
        sexId,

        /**
         * 联系电话
         */
        telephone,
    }

    public String toString() {
        return StringUtils.defaultIfBlank(this.getCallName(), this.getName(), this.getCode());
    }
}