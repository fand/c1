package com.cardone.platform.usercenter.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户激活
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UserActivation extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 445141963349184619L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 用户标识
     */
    private String userId;

    /**
     * 验证码
     */
    private String verificationCode;
}