package com.cardone.platform.authority.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户组与业务范围
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGrBusinessArea extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 482821495339900295L;

    /**
     * 字典.业务范围标识
     */
    private String businessAreaId;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 用户组标识
     */
    private String userGroupId;
}