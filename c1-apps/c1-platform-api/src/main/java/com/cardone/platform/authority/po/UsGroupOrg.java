package com.cardone.platform.authority.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户组与组织
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGroupOrg extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 826763562389932655L;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 组织标识
     */
    private String orgId;

    /**
     * 用户组标识
     */
    private String userGroupId;
}