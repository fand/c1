package com.cardone.platform.common.dto;

import com.cardone.platform.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 实体扩展
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityExtendDto extends EntityExtend {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 159171212548077781L;
    /**
     * 字典.类别代码
     */
    @Transient
    private String typeId;

    /**
     * 实体扩展
     *
     * @return 实体扩展
     */
    public static EntityExtendDto newEntityExtend() {
        return new EntityExtendDto();
    }

    /**
     * 实体扩展
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 实体标识
         */
        entityId,
    }
}