package com.cardone.platform.usercenter.po;

import com.cardone.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 用户部门调动
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsDepartmentTranslate extends PoBase {
    /**
     * 版本号
     */
    private static final long serialVersionUID = 760102066475067878L;

    /**
     * 部门标识
     */
    private String departmentId;

    /**
     * 标识
     */
    @Id
    private String id;

    /**
     * 用户标识
     */
    private String userId;
}