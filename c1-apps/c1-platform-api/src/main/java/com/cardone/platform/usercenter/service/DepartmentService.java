package com.cardone.platform.usercenter.service;

/**
 * 部门服务
 *
 * @author yaohaitao
 */
public interface DepartmentService extends com.cardone.common.service.SimpleService<com.cardone.platform.usercenter.dto.DepartmentDto> {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.usercenter.service.DepartmentService";
}