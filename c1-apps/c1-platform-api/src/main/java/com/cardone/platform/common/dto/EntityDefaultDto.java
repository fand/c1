package com.cardone.platform.common.dto;

import com.cardone.platform.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 实体与默认
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityDefaultDto extends EntityDefault {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 388809171119774833L;

    /**
     * 实体与默认
     *
     * @return 实体与默认
     */
    public static EntityDefaultDto newEntityDefault() {
        return new EntityDefaultDto();
    }

    /**
     * 实体与默认
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 实体标识
         */
        entityId,
    }
}