package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 许可
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class PermissionDto extends Permission {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 401877580616868435L;
    /**
     * 项目代码
     */
    @Transient
    private String projectCode;
    /**
     * 项目名称
     */
    @Transient
    private String projectName;
    /**
     * 角色标识集合
     */
    private String roleIds;
    /**
     * 站代码
     */
    @Transient
    private String siteCode;
    /**
     * 站名称
     */
    @Transient
    private String siteName;
    /**
     * 类别代码
     */
    @Transient
    private String typeCode;
    /**
     * 类别名称
     */
    @Transient
    private String typeName;

    /**
     * 许可
     *
     * @return 许可
     */
    public static PermissionDto newPermission() {
        return new PermissionDto();
    }

    /**
     * 许可
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}