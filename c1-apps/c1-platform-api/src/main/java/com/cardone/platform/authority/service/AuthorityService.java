package com.cardone.platform.authority.service;

/**
 * 权限服务
 *
 * @author yaohaitao
 */
public interface AuthorityService {
    /**
     * spring bean id
     */
    String BEAN_ID = "com.cardone.platform.authority.service.AuthorityService";
}