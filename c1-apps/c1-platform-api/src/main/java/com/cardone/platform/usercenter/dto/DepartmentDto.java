package com.cardone.platform.usercenter.dto;

import com.cardone.platform.usercenter.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 部门
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class DepartmentDto extends Department {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 839065869421643220L;

    /**
     * 部门
     *
     * @return 部门
     */
    public static DepartmentDto newDepartment() {
        return new DepartmentDto();
    }

    /**
     * 部门
     *
     * @author yaohaitao
     */
    public enum Attributes {
    }
}