package com.cardone.platform.authority.dto;

import com.cardone.platform.authority.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

import java.util.*;

/**
 * 导航
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class NavigationDto extends Navigation {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 393938505069744466L;
    /**
     * 主动
     */
    @Transient
    private boolean active;
    /**
     * 子集
     */
    @Transient
    private List<NavigationDto> childs;
    /**
     * 子集总数
     */
    @Transient
    private int childTotal;
    /**
     * 链接
     */
    @Transient
    private String href;
    /**
     * 是否许可
     */
    private boolean isPermission = true;
    /**
     * 父级代码
     */
    @Transient
    private String parentCode;
    /**
     * 父级名称
     */
    @Transient
    private String parentName;
    /**
     * 父级导航
     */
    @Transient
    private NavigationDto parentNavigation;
    /**
     * 站代码
     */
    @Transient
    private String siteCode;
    /**
     * 站名称
     */
    @Transient
    private String siteName;
    /**
     * 树代码
     */
    @Transient
    private String treeCode;
    /**
     * 树类别代码
     */
    @Transient
    private String treeTypeCode;
    /**
     * 类别代码
     */
    @Transient
    private String typeCode;
    /**
     * 类别名称
     */
    @Transient
    private String typeName;
    /**
     * 字典类别.类别代码
     */
    @Transient
    private String typeTypeCode;

    /**
     * 导航
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 数据选项
         */
        dataOption,

        /**
         * 图标样式
         */
        iconStyle,

        /**
         * 目标
         */
        target,

        /**
         * URL
         */
        url,
    }
}