package com.cardone.platform.configuration.service;

/**
 * 字典项服务
 *
 * @author yaohaitao
 */
public interface DictionaryItemService extends com.cardone.common.service.SimpleService<com.cardone.platform.configuration.dto.DictionaryItemDto> {
}