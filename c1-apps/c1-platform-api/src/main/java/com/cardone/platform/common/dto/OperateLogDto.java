package com.cardone.platform.common.dto;

import com.cardone.platform.common.po.*;
import lombok.*;
import lombok.experimental.*;
import org.springframework.data.annotation.*;

/**
 * 操作日志
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class OperateLogDto extends OperateLog {
    /**
     * 版本号
     */
    @Transient
    private static final long serialVersionUID = 734135213360721579L;
    /**
     * 字典.类别代码
     */
    @Transient
    private String typeCode;
    /**
     * 字典.类别名称
     */
    @Transient
    private String typeName;
    /**
     * 字典.类别类别代码
     */
    @Transient
    private String typeTypeCode;

    /**
     * 操作日志
     *
     * @return 操作日志
     */
    public static OperateLogDto newOperateLog() {
        return new OperateLogDto();
    }

    /**
     * 操作日志
     *
     * @author yaohaitao
     */
    public enum Attributes {

        /**
         * 业务代码
         */
        businessCode,

        /**
         * 消息
         */
        message,
    }
}