package com.cardone.platform.common.dao;

/**
 * 实体与默认
 *
 * @author yaohaitao
 */
public interface EntityDefaultDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.common.dto.EntityDefaultDto> {
    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 更新
         */
        @Deprecated
        updateByCode;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/common/entityDefault/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}