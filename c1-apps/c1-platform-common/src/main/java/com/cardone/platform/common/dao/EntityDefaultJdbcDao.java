package com.cardone.platform.common.dao;

import com.cardone.platform.common.dto.*;
import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 实体与默认
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.extern.slf4j.Slf4j
public class EntityDefaultJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.common.dto.EntityDefaultDto> implements EntityDefaultDao {
    public EntityDefaultJdbcDao() {
        super(EntityDefaultDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(EntityDefaultDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(EntityDefaultDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }
}