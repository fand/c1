package com.cardone.platform.common.service;

import com.cardone.common.*;
import com.cardone.context.*;
import com.cardone.platform.common.dao.*;
import com.cardone.platform.configuration.util.*;
import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 操作日志服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class OperateLogDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.common.dto.OperateLogDto> implements OperateLogService {
    @Override
    public OperateLogDao getDao() {
        return ContextHolder.getBean(OperateLogDao.class);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional
    public int insertByCode(final com.cardone.platform.common.dto.OperateLogDto insertOperateLog) {
        String typeId = DictionaryUtils.readIdByTypeCodeAndCode(insertOperateLog.getTypeTypeCode(), insertOperateLog.getTypeCode());

        if (StringUtils.isBlank(typeId)) {
            typeId = StringUtils.join(insertOperateLog.getTypeTypeCode(), Characters.comma.stringValue(), insertOperateLog.getTypeCode());
        }

        insertOperateLog.setTypeId(typeId);

        return ContextHolder.getBean(OperateLogDao.class).insertByCode(insertOperateLog);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public List<Date> readListBeginDateForMax(final String typeTypeCode, final String typeCode, final String code) {
        return this.readListBeginDateForMax(typeTypeCode, typeCode, code, null);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public Integer readCountByCodeForToDay(final String typeTypeCode, final String typeCode, final String code) {
        String typeId = DictionaryUtils.readIdByTypeCodeAndCode(typeTypeCode, typeCode);

        if (StringUtils.isBlank(typeId)) {
            typeId = StringUtils.join(typeTypeCode, Characters.comma.stringValue(), typeCode);
        }

        return ContextHolder.getBean(OperateLogDao.class).readCountByCodeForToDay(typeId, code);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional
    public int updateEndDateForInvalid(final String typeCode, final String code) {
        return ContextHolder.getBean(OperateLogDao.class).updateEndDateForInvalid(typeCode, code);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public List<Date> readListBeginDateForMax(final String typeTypeCode, final String typeCode, final String code, final String businessCode) {
        String typeId = DictionaryUtils.readIdByTypeCodeAndCode(typeTypeCode, typeCode);

        if (StringUtils.isBlank(typeId)) {
            typeId = StringUtils.join(typeTypeCode, Characters.comma.stringValue(), typeCode);
        }

        return ContextHolder.getBean(OperateLogDao.class).readListBeginDateForMax(typeId, code, businessCode);
    }
}