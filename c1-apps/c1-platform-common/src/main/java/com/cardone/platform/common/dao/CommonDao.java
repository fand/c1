package com.cardone.platform.common.dao;

/**
 * 公共
 *
 * @author yaohaitao
 */
public interface CommonDao {
    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 删除
         */
        @Deprecated
        deleteByIds,

        /**
         * 查询
         */
        @Deprecated
        findByCode,

        /**
         * 查询
         */
        @Deprecated
        findById,

        /**
         * 查询
         */
        @Deprecated
        findByLikeCode,

        /**
         * 插入
         */
        @Deprecated
        insertByCode,

        /**
         * 读取
         */
        @Deprecated
        readByCode,

        /**
         * 读取
         */
        @Deprecated
        readByLikeCode,

        /**
         * 更新
         */
        @Deprecated
        updateByCode;

        /**
         * 根目录
         */
        private static final String ROOT = "/platform/common/common/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}