package com.cardone.platform.common.dao;

import com.cardone.common.dao.*;
import com.cardone.context.function.*;
import com.cardone.platform.common.dto.*;

import java.util.*;

/**
 * 实体编号
 *
 * @author yaohaitao
 */
public interface EntityCodeDao extends SimpleDao<EntityCodeDto> {
    /**
     * 生成代码
     *
     * @param id         标识
     * @param typeCode   类别代码
     * @param function   方法
     * @param parameters 参数
     * @return 代码
     */
    String generateCode(final String id, final String typeCode, final Execution1Function<String, String> function, final Map<String, Object> parameters);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 插入
         */
        insertByNotExistsCodeFor,

        /**
         * 读取
         */
        readMaxCodeByCodeFor;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/common/entityCode/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}