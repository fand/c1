package com.cardone.platform.common.service;

import lombok.*;

/**
 * 公共服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class CommonDefaultService implements CommonService {
}