package com.cardone.platform.common.service;

import com.cardone.common.service.*;
import com.cardone.context.*;
import com.cardone.context.function.*;
import com.cardone.platform.common.dao.*;
import com.cardone.platform.common.dto.*;
import lombok.*;
import org.springframework.transaction.annotation.*;

import java.util.*;

/**
 * 实体编号服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Transactional(readOnly = true)
public class EntityCodeDefaultService extends SimpleDefaultService<EntityCodeDto> implements EntityCodeService {
    @Override
    public EntityCodeDao getDao() {
        return ContextHolder.getBean(EntityCodeDao.class);
    }

    @Override
    @Transactional
    public String generateCode(final String id, final String typeCode, final Map<String, Object> parameters) {
        return ContextHolder.getBean(EntityCodeDao.class).generateCode(id, typeCode, null, parameters);
    }

    @Override
    @Transactional
    public String generateCode(final String id, final String typeCode, final Execution1Function<String, String> function, final Map<String, Object> parameters) {
        return ContextHolder.getBean(EntityCodeDao.class).generateCode(id, typeCode, function, parameters);
    }

    @Override
    @Transactional
    public String generateCode(final String id, final String typeCode) {
        return ContextHolder.getBean(EntityCodeDao.class).generateCode(id, typeCode, null, null);
    }
}