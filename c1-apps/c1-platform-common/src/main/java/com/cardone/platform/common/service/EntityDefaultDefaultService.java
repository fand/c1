package com.cardone.platform.common.service;

import com.cardone.context.*;
import lombok.*;

/**
 * 实体与默认服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class EntityDefaultDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.common.dto.EntityDefaultDto> implements EntityDefaultService {
    @Override
    public com.cardone.platform.common.dao.EntityDefaultDao getDao() {
        return ContextHolder.getBean(com.cardone.platform.common.dao.EntityDefaultDao.class);
    }
}