package com.cardone.platform.common.dao;

import com.cardone.platform.common.dto.*;
import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 实体文件
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.extern.slf4j.Slf4j
public class EntityFileJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.common.dto.EntityFileDto> implements EntityFileDao {
    public EntityFileJdbcDao() {
        super(EntityFileDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(EntityFileDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(EntityFileDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }
}