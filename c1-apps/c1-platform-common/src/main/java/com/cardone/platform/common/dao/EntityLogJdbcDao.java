package com.cardone.platform.common.dao;

import com.cardone.context.*;
import com.cardone.persistent.builder.*;
import com.cardone.persistent.support.*;
import com.cardone.platform.common.dto.*;
import com.google.common.collect.*;
import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 实体日志
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.extern.slf4j.Slf4j
public class EntityLogJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.common.dto.EntityLogDto> implements EntityLogDao {
    public EntityLogJdbcDao() {
        super(EntityLogDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(EntityLogDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(EntityLogDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }

    @Override
    public int updateByEntityIdAndTypeIdForInvalid(final EntityLogDto updateEntityLog) {
        final Model model = ModelUtils.newModel(new ModelArgs(updateEntityLog).setIsSimple(true));

        return ContextHolder.getBean(JdbcTemplateSupport.class).update(SqlIds.updateByEntityIdAndTypeIdForInvalid.id(), model);
    }

    @Override
    public String readEntityIdByIdForValid(final String id) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.id.name(), id);

        return ContextHolder.getBean(JdbcTemplateSupport.class).read(String.class, SqlIds.readEntityIdByIdForValid.id(), model);
    }
}