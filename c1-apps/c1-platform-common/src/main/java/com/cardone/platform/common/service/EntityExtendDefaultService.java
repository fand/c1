package com.cardone.platform.common.service;

import com.cardone.context.*;
import com.cardone.platform.common.dao.*;
import lombok.*;

/**
 * 实体扩展服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class EntityExtendDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.common.dto.EntityExtendDto> implements EntityExtendService {
    @Override
    public EntityExtendDao getDao() {
        return ContextHolder.getBean(EntityExtendDao.class);
    }
}