package com.cardone.platform.common.dao;

import com.cardone.platform.common.dto.*;
import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 实体扩展
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.extern.slf4j.Slf4j
public class EntityExtendJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.common.dto.EntityExtendDto> implements EntityExtendDao {
    public EntityExtendJdbcDao() {
        super(EntityExtendDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(EntityExtendDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(EntityExtendDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }
}