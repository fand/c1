package com.cardone.platform.common.service;

import com.cardone.common.cache.util.*;
import com.cardone.context.*;
import com.cardone.platform.common.dao.*;
import lombok.*;
import org.springframework.transaction.annotation.*;

/**
 * 实体日志服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Transactional(readOnly = true)
public class EntityLogDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.common.dto.EntityLogDto> implements EntityLogService {
    @Override
    public EntityLogDao getDao() {
        return ContextHolder.getBean(EntityLogDao.class);
    }

    @Override
    @Transactional
    public int insertByCode(final com.cardone.platform.common.dto.EntityLogDto insertEntityLog) {
        final String typeId = ContextHolder.getBean(com.cardone.platform.configuration.service.DictionaryService.class).readIdByTypeCodeAndCode(insertEntityLog.getTypeTypeCode(), insertEntityLog.getTypeCode());

        if (org.apache.commons.lang.StringUtils.isBlank(typeId)) {
            throw new DictionaryException("不支持的日志类别");
        }

        insertEntityLog.setTypeId(typeId);

        return ContextHolder.getBean(EntityLogDao.class).insertByCode(insertEntityLog);
    }

    @Override
    @Transactional(readOnly = true)
    public String readEntityIdByIdForValid(final String id) {
        return CacheUtils.getValue(EntityLogService.class.getName(), "readEntityIdByIdForValid:" + id, () -> com.cardone.context.ContextHolder.getBean(com.cardone.platform.common.dao.EntityLogDao.class).readEntityIdByIdForValid(id));
    }
}