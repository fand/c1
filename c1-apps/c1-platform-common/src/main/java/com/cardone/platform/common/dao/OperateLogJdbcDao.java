package com.cardone.platform.common.dao;

import com.cardone.context.*;
import com.cardone.persistent.support.*;
import com.cardone.platform.common.dto.*;
import com.google.common.collect.*;
import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 操作日志
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.extern.slf4j.Slf4j
public class OperateLogJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.common.dto.OperateLogDto> implements OperateLogDao {
    public OperateLogJdbcDao() {
        super(OperateLogDao.SqlIds.ROOT);
    }

    @Override
    public int updateEndDateForInvalid(final String typeCode, final String code) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.typeCode.name(), typeCode);
        model.put(Attributes.code.name(), code);

        return ContextHolder.getBean(JdbcTemplateSupport.class).update(SqlIds.updateBeginDateForInvalid.id(), model);
    }

    @Override
    public Integer readCountByCodeForToDay(final String typeId, final String code) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.typeId.name(), typeId);
        model.put(Attributes.code.name(), code);

        return ContextHolder.getBean(JdbcTemplateSupport.class).read(Integer.class, SqlIds.readCountByCodeForToDay.id(), null, model);
    }

    @Override
    public List<Date> readListBeginDateForMax(final String typeId, final String code, final String businessCode) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.typeId.name(), typeId);
        model.put(Attributes.code.name(), code);
        model.put(OperateLogDto.Attributes.businessCode.name(), businessCode);

        return ContextHolder.getBean(JdbcTemplateSupport.class).readList(Date.class, SqlIds.readBeginDateForMax.id(), null, model);
    }

    @Override
    protected boolean isBlankById(OperateLogDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(OperateLogDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }
}