package com.cardone.platform.common.dao;

import lombok.*;

/**
 * 公共
 *
 * @author yaohaitao
 */
@Getter
@Setter
public class CommonJdbcDao implements CommonDao {
}