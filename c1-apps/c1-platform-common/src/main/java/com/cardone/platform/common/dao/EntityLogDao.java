package com.cardone.platform.common.dao;

import com.cardone.platform.common.dto.*;

/**
 * 实体日志
 *
 * @author yaohaitao
 */
public interface EntityLogDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.common.dto.EntityLogDto> {
    /**
     * 更新
     *
     * @param updateEntityLog 实体日志对象
     * @return 影响行数
     */
    int updateByEntityIdAndTypeIdForInvalid(final EntityLogDto updateEntityLog);

    /**
     * 读取有效的实体标识
     *
     * @param id 标识
     * @return 有效的实体标识
     */
    String readEntityIdByIdForValid(final String id);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 读取
         */
        readEntityIdByIdForValid,

        /**
         * 更新
         */
        updateByEntityIdAndTypeIdForInvalid;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/common/entityLog/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}