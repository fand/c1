package com.cardone.platform.common.aspect;

import com.cardone.context.Attributes;
import com.cardone.persistent.builder.Model;
import com.cardone.persistent.builder.ModelUtils;
import com.cardone.platform.usercenter.dto.UserDto;
import com.google.common.collect.Lists;
import lombok.Setter;
import lombok.extern.log4j.Log4j;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.aspectj.lang.JoinPoint;

import java.util.List;
import java.util.Map;

/**
 * 公共拦截
 *
 * @author yaoht
 */
@Log4j
@Setter
public class CommonAspect {

    private List<String> insertList = Lists.newArrayList("/**/insertByCode");
    private List<String> updateList = Lists.newArrayList("/**/updateByCode");

    /**
     * 设置属性
     *
     * @param joinPoint 切片点
     */
    public void updateCreatedOrLastModified(final JoinPoint joinPoint) {
        Object[] args = joinPoint.getArgs();

        if (ArrayUtils.isEmpty(args)) {
            return;
        }

        if (ArrayUtils.getLength(args) < 2) {
            return;
        }

        if (!(args[0] instanceof String)) {
            return;
        }

        if (args[1] == null) {
            return;
        }

        String sqlId = (String) args[0];

        if (log.isDebugEnabled()) {
            log.debug("注入属性：" + sqlId);
        }

        String keyPrefix = null;

        if (com.cardone.common.util.StringUtils.matchList(insertList, sqlId)) {
            keyPrefix = Model.Keys.insert.stringValue();
        } else if (com.cardone.common.util.StringUtils.matchList(updateList, sqlId)) {
            keyPrefix = Model.Keys.update.stringValue();
        }

        if (StringUtils.isBlank(keyPrefix)) {
            return;
        }

        UserDto user = (UserDto) SecurityUtils.getSubject().getPrincipal();

        if (user == null) {
            return;
        }

        final Model model = ModelUtils.newModel();

        if (Model.Keys.insert.stringValue().equals(keyPrefix)) {
            if (!(user == null) && StringUtils.isNotBlank(user.getId())) {
                model.putExtend(keyPrefix, Attributes.createdById.name(), null, user.getId());
                model.putExtend(keyPrefix, Attributes.createdByCode.name(), null, user.getCode());
                model.putExtend(keyPrefix, Attributes.createdByName.name(), null, user.toString());
            }

            model.putTrue(keyPrefix, Attributes.createdDate.name());
        } else if (Model.Keys.update.stringValue().equals(keyPrefix)) {
            if (!(user == null) && StringUtils.isNotBlank(user.getId())) {
                model.putExtend(keyPrefix, Attributes.lastModifiedById.name(), null, user.getId());
                model.putExtend(keyPrefix, Attributes.lastModifiedByCode.name(), null, user.getCode());
                model.putExtend(keyPrefix, Attributes.lastModifiedByName.name(), null, user.toString());
            }

            model.putTrue(keyPrefix, Attributes.lastModifiedDate.name());
        }

        if (args[1] instanceof Map) {
            Map<String, Object> modelMap = (Map<String, Object>) args[1];

            modelMap.putAll(model);
        } else if (args[1] instanceof List) {
            List<Map<String, Object>> modelMapList = (List<Map<String, Object>>) args[1];

            if (CollectionUtils.isEmpty(modelMapList)) {
                return;
            }

            for (Map<String, Object> modelMap : modelMapList) {
                if (modelMap == null) {
                    continue;
                }

                modelMap.putAll(model);
            }
        }
    }
}
