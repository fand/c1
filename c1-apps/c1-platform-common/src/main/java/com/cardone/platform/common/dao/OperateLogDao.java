package com.cardone.platform.common.dao;

import java.util.*;

/**
 * 操作日志
 *
 * @author yaohaitao
 */
public interface OperateLogDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.common.dto.OperateLogDto> {
    /**
     * 更新数据：失效
     *
     * @param typeCode 类别代码
     * @param code     代码
     * @return 失效数量
     */
    int updateEndDateForInvalid(final String typeCode, final String code);

    /**
     * 读取当天总数
     *
     * @param typeId 类别标识
     * @param code   代码
     * @return 当天总数
     */
    Integer readCountByCodeForToDay(final String typeId, final String code);

    /**
     * 读取最大创建时间
     *
     * @param typeId       类别标识
     * @param code         代码
     * @param businessCode 业务代码
     * @return 最大创建时间
     */
    List<Date> readListBeginDateForMax(final String typeId, final String code, final String businessCode);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 读取
         */
        readBeginDateForMax,

        /**
         * 读取
         */
        readCountByCodeForToDay,

        /**
         * 更新
         */
        updateBeginDateForInvalid;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/common/operateLog/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}