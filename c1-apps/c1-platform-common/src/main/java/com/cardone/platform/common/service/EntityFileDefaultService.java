package com.cardone.platform.common.service;

import com.cardone.context.*;
import com.cardone.platform.common.dao.*;
import lombok.*;

/**
 * 实体文件服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class EntityFileDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.common.dto.EntityFileDto> implements EntityFileService {
    @Override
    public EntityFileDao getDao() {
        return ContextHolder.getBean(EntityFileDao.class);
    }
}