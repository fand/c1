INSERT
INTO
c1_pe_va_ru_transact_mode
(
<#assign prefixName=' '>
        <#if (insert_beginDate??) && (insert_beginDate_value??)>
    ${prefixName} `BEGIN_DATE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_createdByCode??) && (insert_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_createdById??) && (insert_createdById_value??)>
    ${prefixName} `CREATED_BY_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_createdByName??) && (insert_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME`
    <#assign prefixName=','>
        </#if>
    ${prefixName} `CREATED_DATE`
    <#assign prefixName=','>
        <#if (insert_dataStateCode??) && (insert_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_dataStateId??) && (insert_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_dataStateName??) && (insert_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_endDate??) && (insert_endDate_value??)>
    ${prefixName} `END_DATE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_id??) && (insert_id_value??)>
    ${prefixName} `ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_orderNum??) && (insert_orderNum_value??)>
    ${prefixName} `ORDER_NUM`
    <#assign prefixName=','>
        </#if>
        <#if (insert_permissionId??) && (insert_permissionId_value??)>
    ${prefixName} `PERMISSION_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_stateCode??) && (insert_stateCode_value??)>
    ${prefixName} `STATE_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_stateId??) && (insert_stateId_value??)>
    ${prefixName} `STATE_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_stateName??) && (insert_stateName_value??)>
    ${prefixName} `STATE_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_validateErrorCode??) && (insert_validateErrorCode_value??)>
    ${prefixName} `VALIDATE_ERROR_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_validateErrorId??) && (insert_validateErrorId_value??)>
    ${prefixName} `VALIDATE_ERROR_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_validateErrorName??) && (insert_validateErrorName_value??)>
    ${prefixName} `VALIDATE_ERROR_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_validateRuleCode??) && (insert_validateRuleCode_value??)>
    ${prefixName} `VALIDATE_RULE_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_validateRuleId??) && (insert_validateRuleId_value??)>
    ${prefixName} `VALIDATE_RULE_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_validateRuleName??) && (insert_validateRuleName_value??)>
    ${prefixName} `VALIDATE_RULE_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_validateSuccessCode??) && (insert_validateSuccessCode_value??)>
    ${prefixName} `VALIDATE_SUCCESS_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_validateSuccessId??) && (insert_validateSuccessId_value??)>
    ${prefixName} `VALIDATE_SUCCESS_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_validateSuccessName??) && (insert_validateSuccessName_value??)>
    ${prefixName} `VALIDATE_SUCCESS_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_versionInt??) && (insert_versionInt_value??)>
    ${prefixName} `VERSION_INT`
    <#assign prefixName=','>
        </#if>
    )
    (
    SELECT
    <#assign prefixName=' '>
            <#if (insert_beginDate??) && (insert_beginDate_value??)>
        ${prefixName} :insert_beginDate_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_createdByCode??) && (insert_createdByCode_value??)>
        ${prefixName} :insert_createdByCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_createdById??) && (insert_createdById_value??)>
        ${prefixName} :insert_createdById_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_createdByName??) && (insert_createdByName_value??)>
        ${prefixName} :insert_createdByName_value
        <#assign prefixName=','>
            </#if>
        ${prefixName} NOW()
        <#assign prefixName=','>
            <#if (insert_dataStateCode??) && (insert_dataStateCode_value??)>
        ${prefixName} :insert_dataStateCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_dataStateId??) && (insert_dataStateId_value??)>
        ${prefixName} :insert_dataStateId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_dataStateName??) && (insert_dataStateName_value??)>
        ${prefixName} :insert_dataStateName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_endDate??) && (insert_endDate_value??)>
        ${prefixName} :insert_endDate_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_id??) && (insert_id_value??)>
        ${prefixName} :insert_id_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
        ${prefixName} :insert_lastModifiedByCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
        ${prefixName} :insert_lastModifiedById_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
        ${prefixName} :insert_lastModifiedByName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
        ${prefixName} :insert_lastModifiedDate_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_orderNum??) && (insert_orderNum_value??)>
        ${prefixName} :insert_orderNum_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_permissionId??) && (insert_permissionId_value??)>
        ${prefixName} :insert_permissionId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_stateCode??) && (insert_stateCode_value??)>
        ${prefixName} :insert_stateCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_stateId??) && (insert_stateId_value??)>
        ${prefixName} :insert_stateId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_stateName??) && (insert_stateName_value??)>
        ${prefixName} :insert_stateName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_validateErrorCode??) && (insert_validateErrorCode_value??)>
        ${prefixName} :insert_validateErrorCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_validateErrorId??) && (insert_validateErrorId_value??)>
        ${prefixName} :insert_validateErrorId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_validateErrorName??) && (insert_validateErrorName_value??)>
        ${prefixName} :insert_validateErrorName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_validateRuleCode??) && (insert_validateRuleCode_value??)>
        ${prefixName} :insert_validateRuleCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_validateRuleId??) && (insert_validateRuleId_value??)>
        ${prefixName} :insert_validateRuleId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_validateRuleName??) && (insert_validateRuleName_value??)>
        ${prefixName} :insert_validateRuleName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_validateSuccessCode??) && (insert_validateSuccessCode_value??)>
        ${prefixName} :insert_validateSuccessCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_validateSuccessId??) && (insert_validateSuccessId_value??)>
        ${prefixName} :insert_validateSuccessId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_validateSuccessName??) && (insert_validateSuccessName_value??)>
        ${prefixName} :insert_validateSuccessName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_versionInt??) && (insert_versionInt_value??)>
        ${prefixName} :insert_versionInt_value
        <#assign prefixName=','>
            </#if>
        FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM c1_pe_va_ru_transact_mode E
        <#assign prefixName='WHERE'>
            <#if (where_and_eq_beginDate??)>
            <#if (where_and_eq_beginDate_value??)>
        ${prefixName} E.BEGIN_DATE = :where_and_eq_beginDate_value
        <#else>
        ${prefixName} E.BEGIN_DATE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_createdByCode??)>
            <#if (where_and_eq_createdByCode_value??)>
        ${prefixName} E.CREATED_BY_CODE = :where_and_eq_createdByCode_value
        <#else>
        ${prefixName} E.CREATED_BY_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_createdById??)>
            <#if (where_and_eq_createdById_value??)>
        ${prefixName} E.CREATED_BY_ID = :where_and_eq_createdById_value
        <#else>
        ${prefixName} E.CREATED_BY_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_createdByName??)>
            <#if (where_and_eq_createdByName_value??)>
        ${prefixName} E.CREATED_BY_NAME = :where_and_eq_createdByName_value
        <#else>
        ${prefixName} E.CREATED_BY_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_createdDate??)>
            <#if (where_and_eq_createdDate_value??)>
        ${prefixName} E.CREATED_DATE = :where_and_eq_createdDate_value
        <#else>
        ${prefixName} E.CREATED_DATE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_dataStateCode??)>
            <#if (where_and_eq_dataStateCode_value??)>
        ${prefixName} E.DATA_STATE_CODE = :where_and_eq_dataStateCode_value
        <#else>
        ${prefixName} E.DATA_STATE_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_dataStateId??)>
            <#if (where_and_eq_dataStateId_value??)>
        ${prefixName} E.DATA_STATE_ID = :where_and_eq_dataStateId_value
        <#else>
        ${prefixName} E.DATA_STATE_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_dataStateName??)>
            <#if (where_and_eq_dataStateName_value??)>
        ${prefixName} E.DATA_STATE_NAME = :where_and_eq_dataStateName_value
        <#else>
        ${prefixName} E.DATA_STATE_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_endDate??)>
            <#if (where_and_eq_endDate_value??)>
        ${prefixName} E.END_DATE = :where_and_eq_endDate_value
        <#else>
        ${prefixName} E.END_DATE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if (where_and_between_sysdate??)>
                <#if (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            ${prefixName} NOW() BETWEEN IFNULL(E.BEGIN_DATE, NOW()) AND IFNULL(E.END_DATE, NOW())
            <#assign prefixName=''>
                </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_id??)>
            <#if (where_and_eq_id_value??)>
        ${prefixName} E.ID = :where_and_eq_id_value
        <#else>
        ${prefixName} E.ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_lastModifiedByCode??)>
            <#if (where_and_eq_lastModifiedByCode_value??)>
        ${prefixName} E.LAST_MODIFIED_BY_CODE = :where_and_eq_lastModifiedByCode_value
        <#else>
        ${prefixName} E.LAST_MODIFIED_BY_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_lastModifiedById??)>
            <#if (where_and_eq_lastModifiedById_value??)>
        ${prefixName} E.LAST_MODIFIED_BY_ID = :where_and_eq_lastModifiedById_value
        <#else>
        ${prefixName} E.LAST_MODIFIED_BY_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_lastModifiedByName??)>
            <#if (where_and_eq_lastModifiedByName_value??)>
        ${prefixName} E.LAST_MODIFIED_BY_NAME = :where_and_eq_lastModifiedByName_value
        <#else>
        ${prefixName} E.LAST_MODIFIED_BY_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_lastModifiedDate??)>
            <#if (where_and_eq_lastModifiedDate_value??)>
        ${prefixName} E.LAST_MODIFIED_DATE = :where_and_eq_lastModifiedDate_value
        <#else>
        ${prefixName} E.LAST_MODIFIED_DATE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_orderNum??)>
            <#if (where_and_eq_orderNum_value??)>
        ${prefixName} E.ORDER_NUM = :where_and_eq_orderNum_value
        <#else>
        ${prefixName} E.ORDER_NUM IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_permissionId??)>
            <#if (where_and_eq_permissionId_value??)>
        ${prefixName} E.PERMISSION_ID = :where_and_eq_permissionId_value
        <#else>
        ${prefixName} E.PERMISSION_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_stateCode??)>
            <#if (where_and_eq_stateCode_value??)>
        ${prefixName} E.STATE_CODE = :where_and_eq_stateCode_value
        <#else>
        ${prefixName} E.STATE_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_stateId??)>
            <#if (where_and_eq_stateId_value??)>
        ${prefixName} E.STATE_ID = :where_and_eq_stateId_value
        <#else>
        ${prefixName} E.STATE_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_stateName??)>
            <#if (where_and_eq_stateName_value??)>
        ${prefixName} E.STATE_NAME = :where_and_eq_stateName_value
        <#else>
        ${prefixName} E.STATE_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_validateErrorCode??)>
            <#if (where_and_eq_validateErrorCode_value??)>
        ${prefixName} E.VALIDATE_ERROR_CODE = :where_and_eq_validateErrorCode_value
        <#else>
        ${prefixName} E.VALIDATE_ERROR_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_validateErrorId??)>
            <#if (where_and_eq_validateErrorId_value??)>
        ${prefixName} E.VALIDATE_ERROR_ID = :where_and_eq_validateErrorId_value
        <#else>
        ${prefixName} E.VALIDATE_ERROR_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_validateErrorName??)>
            <#if (where_and_eq_validateErrorName_value??)>
        ${prefixName} E.VALIDATE_ERROR_NAME = :where_and_eq_validateErrorName_value
        <#else>
        ${prefixName} E.VALIDATE_ERROR_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_validateRuleCode??)>
            <#if (where_and_eq_validateRuleCode_value??)>
        ${prefixName} E.VALIDATE_RULE_CODE = :where_and_eq_validateRuleCode_value
        <#else>
        ${prefixName} E.VALIDATE_RULE_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_validateRuleId??)>
            <#if (where_and_eq_validateRuleId_value??)>
        ${prefixName} E.VALIDATE_RULE_ID = :where_and_eq_validateRuleId_value
        <#else>
        ${prefixName} E.VALIDATE_RULE_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_validateRuleName??)>
            <#if (where_and_eq_validateRuleName_value??)>
        ${prefixName} E.VALIDATE_RULE_NAME = :where_and_eq_validateRuleName_value
        <#else>
        ${prefixName} E.VALIDATE_RULE_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_validateSuccessCode??)>
            <#if (where_and_eq_validateSuccessCode_value??)>
        ${prefixName} E.VALIDATE_SUCCESS_CODE = :where_and_eq_validateSuccessCode_value
        <#else>
        ${prefixName} E.VALIDATE_SUCCESS_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_validateSuccessId??)>
            <#if (where_and_eq_validateSuccessId_value??)>
        ${prefixName} E.VALIDATE_SUCCESS_ID = :where_and_eq_validateSuccessId_value
        <#else>
        ${prefixName} E.VALIDATE_SUCCESS_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_validateSuccessName??)>
            <#if (where_and_eq_validateSuccessName_value??)>
        ${prefixName} E.VALIDATE_SUCCESS_NAME = :where_and_eq_validateSuccessName_value
        <#else>
        ${prefixName} E.VALIDATE_SUCCESS_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_versionInt??)>
            <#if (where_and_eq_versionInt_value??)>
        ${prefixName} E.VERSION_INT = :where_and_eq_versionInt_value
        <#else>
        ${prefixName} E.VERSION_INT IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
            )
            )