INSERT
INTO
c1_user
(
<#assign prefixName=' '>
        <#if (insert_address??) && (insert_address_value??)>
    ${prefixName} `ADDRESS`
    <#assign prefixName=','>
        </#if>
        <#if (insert_age??) && (insert_age_value??)>
    ${prefixName} `AGE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_beginDate??) && (insert_beginDate_value??)>
    ${prefixName} `BEGIN_DATE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_birthday??) && (insert_birthday_value??)>
    ${prefixName} `BIRTHDAY`
    <#assign prefixName=','>
        </#if>
        <#if (insert_callName??) && (insert_callName_value??)>
    ${prefixName} `CALL_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_cityCode??) && (insert_cityCode_value??)>
    ${prefixName} `CITY_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_cityId??) && (insert_cityId_value??)>
    ${prefixName} `CITY_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_cityName??) && (insert_cityName_value??)>
    ${prefixName} `CITY_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_code??) && (insert_code_value??)>
    ${prefixName} `CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_companyName??) && (insert_companyName_value??)>
    ${prefixName} `COMPANY_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_countryCode??) && (insert_countryCode_value??)>
    ${prefixName} `COUNTRY_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_countryId??) && (insert_countryId_value??)>
    ${prefixName} `COUNTRY_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_countryName??) && (insert_countryName_value??)>
    ${prefixName} `COUNTRY_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_createdByCode??) && (insert_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_createdById??) && (insert_createdById_value??)>
    ${prefixName} `CREATED_BY_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_createdByName??) && (insert_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME`
    <#assign prefixName=','>
        </#if>
    ${prefixName} `CREATED_DATE`
    <#assign prefixName=','>
        <#if (insert_dataStateCode??) && (insert_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_dataStateId??) && (insert_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_dataStateName??) && (insert_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_departmentCode??) && (insert_departmentCode_value??)>
    ${prefixName} `DEPARTMENT_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_departmentId??) && (insert_departmentId_value??)>
    ${prefixName} `DEPARTMENT_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_departmentName??) && (insert_departmentName_value??)>
    ${prefixName} `DEPARTMENT_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_diplomaCode??) && (insert_diplomaCode_value??)>
    ${prefixName} `DIPLOMA_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_diplomaId??) && (insert_diplomaId_value??)>
    ${prefixName} `DIPLOMA_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_diplomaName??) && (insert_diplomaName_value??)>
    ${prefixName} `DIPLOMA_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_districtCode??) && (insert_districtCode_value??)>
    ${prefixName} `DISTRICT_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_districtId??) && (insert_districtId_value??)>
    ${prefixName} `DISTRICT_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_districtName??) && (insert_districtName_value??)>
    ${prefixName} `DISTRICT_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_email??) && (insert_email_value??)>
    ${prefixName} `EMAIL`
    <#assign prefixName=','>
        </#if>
        <#if (insert_endDate??) && (insert_endDate_value??)>
    ${prefixName} `END_DATE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_flagCode??) && (insert_flagCode_value??)>
    ${prefixName} `FLAG_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_flagId??) && (insert_flagId_value??)>
    ${prefixName} `FLAG_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_flagName??) && (insert_flagName_value??)>
    ${prefixName} `FLAG_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_folkCode??) && (insert_folkCode_value??)>
    ${prefixName} `FOLK_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_folkId??) && (insert_folkId_value??)>
    ${prefixName} `FOLK_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_folkName??) && (insert_folkName_value??)>
    ${prefixName} `FOLK_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_id??) && (insert_id_value??)>
    ${prefixName} `ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_intro??) && (insert_intro_value??)>
    ${prefixName} `INTRO`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_locus??) && (insert_locus_value??)>
    ${prefixName} `LOCUS`
    <#assign prefixName=','>
        </#if>
        <#if (insert_marryStateCode??) && (insert_marryStateCode_value??)>
    ${prefixName} `MARRY_STATE_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_marryStateId??) && (insert_marryStateId_value??)>
    ${prefixName} `MARRY_STATE_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_marryStateName??) && (insert_marryStateName_value??)>
    ${prefixName} `MARRY_STATE_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_mobilePhone??) && (insert_mobilePhone_value??)>
    ${prefixName} `MOBILE_PHONE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_name??) && (insert_name_value??)>
    ${prefixName} `NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_no??) && (insert_no_value??)>
    ${prefixName} `NO`
    <#assign prefixName=','>
        </#if>
        <#if (insert_orgCode??) && (insert_orgCode_value??)>
    ${prefixName} `ORG_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_orgId??) && (insert_orgId_value??)>
    ${prefixName} `ORG_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_orgName??) && (insert_orgName_value??)>
    ${prefixName} `ORG_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_password??) && (insert_password_value??)>
    ${prefixName} `PASSWORD`
    <#assign prefixName=','>
        </#if>
        <#if (insert_professionCode??) && (insert_professionCode_value??)>
    ${prefixName} `PROFESSION_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_professionId??) && (insert_professionId_value??)>
    ${prefixName} `PROFESSION_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_professionName??) && (insert_professionName_value??)>
    ${prefixName} `PROFESSION_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_provinceCode??) && (insert_provinceCode_value??)>
    ${prefixName} `PROVINCE_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_provinceId??) && (insert_provinceId_value??)>
    ${prefixName} `PROVINCE_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_provinceName??) && (insert_provinceName_value??)>
    ${prefixName} `PROVINCE_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_sexCode??) && (insert_sexCode_value??)>
    ${prefixName} `SEX_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_sexId??) && (insert_sexId_value??)>
    ${prefixName} `SEX_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_sexName??) && (insert_sexName_value??)>
    ${prefixName} `SEX_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_stateCode??) && (insert_stateCode_value??)>
    ${prefixName} `STATE_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_stateId??) && (insert_stateId_value??)>
    ${prefixName} `STATE_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_stateName??) && (insert_stateName_value??)>
    ${prefixName} `STATE_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_telephone??) && (insert_telephone_value??)>
    ${prefixName} `TELEPHONE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_versionInt??) && (insert_versionInt_value??)>
    ${prefixName} `VERSION_INT`
    <#assign prefixName=','>
        </#if>
    )
    (
    SELECT
    <#assign prefixName=' '>
            <#if (insert_address??) && (insert_address_value??)>
        ${prefixName} :insert_address_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_age??) && (insert_age_value??)>
        ${prefixName} :insert_age_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_beginDate??) && (insert_beginDate_value??)>
        ${prefixName} :insert_beginDate_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_birthday??) && (insert_birthday_value??)>
        ${prefixName} :insert_birthday_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_callName??) && (insert_callName_value??)>
        ${prefixName} :insert_callName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_cityCode??) && (insert_cityCode_value??)>
        ${prefixName} :insert_cityCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_cityId??) && (insert_cityId_value??)>
        ${prefixName} :insert_cityId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_cityName??) && (insert_cityName_value??)>
        ${prefixName} :insert_cityName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_code??) && (insert_code_value??)>
        ${prefixName} :insert_code_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_companyName??) && (insert_companyName_value??)>
        ${prefixName} :insert_companyName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_countryCode??) && (insert_countryCode_value??)>
        ${prefixName} :insert_countryCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_countryId??) && (insert_countryId_value??)>
        ${prefixName} :insert_countryId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_countryName??) && (insert_countryName_value??)>
        ${prefixName} :insert_countryName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_createdByCode??) && (insert_createdByCode_value??)>
        ${prefixName} :insert_createdByCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_createdById??) && (insert_createdById_value??)>
        ${prefixName} :insert_createdById_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_createdByName??) && (insert_createdByName_value??)>
        ${prefixName} :insert_createdByName_value
        <#assign prefixName=','>
            </#if>
        ${prefixName} NOW()
        <#assign prefixName=','>
            <#if (insert_dataStateCode??) && (insert_dataStateCode_value??)>
        ${prefixName} :insert_dataStateCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_dataStateId??) && (insert_dataStateId_value??)>
        ${prefixName} :insert_dataStateId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_dataStateName??) && (insert_dataStateName_value??)>
        ${prefixName} :insert_dataStateName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_departmentCode??) && (insert_departmentCode_value??)>
        ${prefixName} :insert_departmentCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_departmentId??) && (insert_departmentId_value??)>
        ${prefixName} :insert_departmentId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_departmentName??) && (insert_departmentName_value??)>
        ${prefixName} :insert_departmentName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_diplomaCode??) && (insert_diplomaCode_value??)>
        ${prefixName} :insert_diplomaCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_diplomaId??) && (insert_diplomaId_value??)>
        ${prefixName} :insert_diplomaId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_diplomaName??) && (insert_diplomaName_value??)>
        ${prefixName} :insert_diplomaName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_districtCode??) && (insert_districtCode_value??)>
        ${prefixName} :insert_districtCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_districtId??) && (insert_districtId_value??)>
        ${prefixName} :insert_districtId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_districtName??) && (insert_districtName_value??)>
        ${prefixName} :insert_districtName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_email??) && (insert_email_value??)>
        ${prefixName} :insert_email_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_endDate??) && (insert_endDate_value??)>
        ${prefixName} :insert_endDate_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_flagCode??) && (insert_flagCode_value??)>
        ${prefixName} :insert_flagCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_flagId??) && (insert_flagId_value??)>
        ${prefixName} :insert_flagId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_flagName??) && (insert_flagName_value??)>
        ${prefixName} :insert_flagName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_folkCode??) && (insert_folkCode_value??)>
        ${prefixName} :insert_folkCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_folkId??) && (insert_folkId_value??)>
        ${prefixName} :insert_folkId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_folkName??) && (insert_folkName_value??)>
        ${prefixName} :insert_folkName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_id??) && (insert_id_value??)>
        ${prefixName} :insert_id_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_intro??) && (insert_intro_value??)>
        ${prefixName} :insert_intro_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
        ${prefixName} :insert_lastModifiedByCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
        ${prefixName} :insert_lastModifiedById_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
        ${prefixName} :insert_lastModifiedByName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
        ${prefixName} :insert_lastModifiedDate_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_locus??) && (insert_locus_value??)>
        ${prefixName} :insert_locus_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_marryStateCode??) && (insert_marryStateCode_value??)>
        ${prefixName} :insert_marryStateCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_marryStateId??) && (insert_marryStateId_value??)>
        ${prefixName} :insert_marryStateId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_marryStateName??) && (insert_marryStateName_value??)>
        ${prefixName} :insert_marryStateName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_mobilePhone??) && (insert_mobilePhone_value??)>
        ${prefixName} :insert_mobilePhone_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_name??) && (insert_name_value??)>
        ${prefixName} :insert_name_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_no??) && (insert_no_value??)>
        ${prefixName} :insert_no_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_orgCode??) && (insert_orgCode_value??)>
        ${prefixName} :insert_orgCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_orgId??) && (insert_orgId_value??)>
        ${prefixName} :insert_orgId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_orgName??) && (insert_orgName_value??)>
        ${prefixName} :insert_orgName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_password??) && (insert_password_value??)>
        ${prefixName} :insert_password_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_professionCode??) && (insert_professionCode_value??)>
        ${prefixName} :insert_professionCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_professionId??) && (insert_professionId_value??)>
        ${prefixName} :insert_professionId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_professionName??) && (insert_professionName_value??)>
        ${prefixName} :insert_professionName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_provinceCode??) && (insert_provinceCode_value??)>
        ${prefixName} :insert_provinceCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_provinceId??) && (insert_provinceId_value??)>
        ${prefixName} :insert_provinceId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_provinceName??) && (insert_provinceName_value??)>
        ${prefixName} :insert_provinceName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_sexCode??) && (insert_sexCode_value??)>
        ${prefixName} :insert_sexCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_sexId??) && (insert_sexId_value??)>
        ${prefixName} :insert_sexId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_sexName??) && (insert_sexName_value??)>
        ${prefixName} :insert_sexName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_stateCode??) && (insert_stateCode_value??)>
        ${prefixName} :insert_stateCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_stateId??) && (insert_stateId_value??)>
        ${prefixName} :insert_stateId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_stateName??) && (insert_stateName_value??)>
        ${prefixName} :insert_stateName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_telephone??) && (insert_telephone_value??)>
        ${prefixName} :insert_telephone_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_versionInt??) && (insert_versionInt_value??)>
        ${prefixName} :insert_versionInt_value
        <#assign prefixName=','>
            </#if>
        FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM c1_user E
        <#assign prefixName='WHERE'>
            <#if (where_and_eq_address??)>
            <#if (where_and_eq_address_value??)>
        ${prefixName} E.ADDRESS = :where_and_eq_address_value
        <#else>
        ${prefixName} E.ADDRESS IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_age??)>
            <#if (where_and_eq_age_value??)>
        ${prefixName} E.AGE = :where_and_eq_age_value
        <#else>
        ${prefixName} E.AGE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_beginDate??)>
            <#if (where_and_eq_beginDate_value??)>
        ${prefixName} E.BEGIN_DATE = :where_and_eq_beginDate_value
        <#else>
        ${prefixName} E.BEGIN_DATE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_birthday??)>
            <#if (where_and_eq_birthday_value??)>
        ${prefixName} E.BIRTHDAY = :where_and_eq_birthday_value
        <#else>
        ${prefixName} E.BIRTHDAY IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_callName??)>
            <#if (where_and_eq_callName_value??)>
        ${prefixName} E.CALL_NAME = :where_and_eq_callName_value
        <#else>
        ${prefixName} E.CALL_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_cityCode??)>
            <#if (where_and_eq_cityCode_value??)>
        ${prefixName} E.CITY_CODE = :where_and_eq_cityCode_value
        <#else>
        ${prefixName} E.CITY_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_cityId??)>
            <#if (where_and_eq_cityId_value??)>
        ${prefixName} E.CITY_ID = :where_and_eq_cityId_value
        <#else>
        ${prefixName} E.CITY_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_cityName??)>
            <#if (where_and_eq_cityName_value??)>
        ${prefixName} E.CITY_NAME = :where_and_eq_cityName_value
        <#else>
        ${prefixName} E.CITY_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_code??)>
            <#if (where_and_eq_code_value??)>
        ${prefixName} E.CODE = :where_and_eq_code_value
        <#else>
        ${prefixName} E.CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_companyName??)>
            <#if (where_and_eq_companyName_value??)>
        ${prefixName} E.COMPANY_NAME = :where_and_eq_companyName_value
        <#else>
        ${prefixName} E.COMPANY_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_countryCode??)>
            <#if (where_and_eq_countryCode_value??)>
        ${prefixName} E.COUNTRY_CODE = :where_and_eq_countryCode_value
        <#else>
        ${prefixName} E.COUNTRY_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_countryId??)>
            <#if (where_and_eq_countryId_value??)>
        ${prefixName} E.COUNTRY_ID = :where_and_eq_countryId_value
        <#else>
        ${prefixName} E.COUNTRY_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_countryName??)>
            <#if (where_and_eq_countryName_value??)>
        ${prefixName} E.COUNTRY_NAME = :where_and_eq_countryName_value
        <#else>
        ${prefixName} E.COUNTRY_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_createdByCode??)>
            <#if (where_and_eq_createdByCode_value??)>
        ${prefixName} E.CREATED_BY_CODE = :where_and_eq_createdByCode_value
        <#else>
        ${prefixName} E.CREATED_BY_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_createdById??)>
            <#if (where_and_eq_createdById_value??)>
        ${prefixName} E.CREATED_BY_ID = :where_and_eq_createdById_value
        <#else>
        ${prefixName} E.CREATED_BY_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_createdByName??)>
            <#if (where_and_eq_createdByName_value??)>
        ${prefixName} E.CREATED_BY_NAME = :where_and_eq_createdByName_value
        <#else>
        ${prefixName} E.CREATED_BY_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_createdDate??)>
            <#if (where_and_eq_createdDate_value??)>
        ${prefixName} E.CREATED_DATE = :where_and_eq_createdDate_value
        <#else>
        ${prefixName} E.CREATED_DATE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_dataStateCode??)>
            <#if (where_and_eq_dataStateCode_value??)>
        ${prefixName} E.DATA_STATE_CODE = :where_and_eq_dataStateCode_value
        <#else>
        ${prefixName} E.DATA_STATE_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_dataStateId??)>
            <#if (where_and_eq_dataStateId_value??)>
        ${prefixName} E.DATA_STATE_ID = :where_and_eq_dataStateId_value
        <#else>
        ${prefixName} E.DATA_STATE_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_dataStateName??)>
            <#if (where_and_eq_dataStateName_value??)>
        ${prefixName} E.DATA_STATE_NAME = :where_and_eq_dataStateName_value
        <#else>
        ${prefixName} E.DATA_STATE_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_departmentCode??)>
            <#if (where_and_eq_departmentCode_value??)>
        ${prefixName} E.DEPARTMENT_CODE = :where_and_eq_departmentCode_value
        <#else>
        ${prefixName} E.DEPARTMENT_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_departmentId??)>
            <#if (where_and_eq_departmentId_value??)>
        ${prefixName} E.DEPARTMENT_ID = :where_and_eq_departmentId_value
        <#else>
        ${prefixName} E.DEPARTMENT_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_departmentName??)>
            <#if (where_and_eq_departmentName_value??)>
        ${prefixName} E.DEPARTMENT_NAME = :where_and_eq_departmentName_value
        <#else>
        ${prefixName} E.DEPARTMENT_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_diplomaCode??)>
            <#if (where_and_eq_diplomaCode_value??)>
        ${prefixName} E.DIPLOMA_CODE = :where_and_eq_diplomaCode_value
        <#else>
        ${prefixName} E.DIPLOMA_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_diplomaId??)>
            <#if (where_and_eq_diplomaId_value??)>
        ${prefixName} E.DIPLOMA_ID = :where_and_eq_diplomaId_value
        <#else>
        ${prefixName} E.DIPLOMA_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_diplomaName??)>
            <#if (where_and_eq_diplomaName_value??)>
        ${prefixName} E.DIPLOMA_NAME = :where_and_eq_diplomaName_value
        <#else>
        ${prefixName} E.DIPLOMA_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_districtCode??)>
            <#if (where_and_eq_districtCode_value??)>
        ${prefixName} E.DISTRICT_CODE = :where_and_eq_districtCode_value
        <#else>
        ${prefixName} E.DISTRICT_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_districtId??)>
            <#if (where_and_eq_districtId_value??)>
        ${prefixName} E.DISTRICT_ID = :where_and_eq_districtId_value
        <#else>
        ${prefixName} E.DISTRICT_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_districtName??)>
            <#if (where_and_eq_districtName_value??)>
        ${prefixName} E.DISTRICT_NAME = :where_and_eq_districtName_value
        <#else>
        ${prefixName} E.DISTRICT_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_email??)>
            <#if (where_and_eq_email_value??)>
        ${prefixName} E.EMAIL = :where_and_eq_email_value
        <#else>
        ${prefixName} E.EMAIL IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_endDate??)>
            <#if (where_and_eq_endDate_value??)>
        ${prefixName} E.END_DATE = :where_and_eq_endDate_value
        <#else>
        ${prefixName} E.END_DATE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if (where_and_between_sysdate??)>
                <#if (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            ${prefixName} NOW() BETWEEN IFNULL(E.BEGIN_DATE, NOW()) AND IFNULL(E.END_DATE, NOW())
            <#assign prefixName=''>
                </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_flagCode??)>
            <#if (where_and_eq_flagCode_value??)>
        ${prefixName} E.FLAG_CODE = :where_and_eq_flagCode_value
        <#else>
        ${prefixName} E.FLAG_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_flagId??)>
            <#if (where_and_eq_flagId_value??)>
        ${prefixName} E.FLAG_ID = :where_and_eq_flagId_value
        <#else>
        ${prefixName} E.FLAG_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_flagName??)>
            <#if (where_and_eq_flagName_value??)>
        ${prefixName} E.FLAG_NAME = :where_and_eq_flagName_value
        <#else>
        ${prefixName} E.FLAG_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_folkCode??)>
            <#if (where_and_eq_folkCode_value??)>
        ${prefixName} E.FOLK_CODE = :where_and_eq_folkCode_value
        <#else>
        ${prefixName} E.FOLK_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_folkId??)>
            <#if (where_and_eq_folkId_value??)>
        ${prefixName} E.FOLK_ID = :where_and_eq_folkId_value
        <#else>
        ${prefixName} E.FOLK_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_folkName??)>
            <#if (where_and_eq_folkName_value??)>
        ${prefixName} E.FOLK_NAME = :where_and_eq_folkName_value
        <#else>
        ${prefixName} E.FOLK_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_id??)>
            <#if (where_and_eq_id_value??)>
        ${prefixName} E.ID = :where_and_eq_id_value
        <#else>
        ${prefixName} E.ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_intro??)>
            <#if (where_and_eq_intro_value??)>
        ${prefixName} E.INTRO = :where_and_eq_intro_value
        <#else>
        ${prefixName} E.INTRO IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_lastModifiedByCode??)>
            <#if (where_and_eq_lastModifiedByCode_value??)>
        ${prefixName} E.LAST_MODIFIED_BY_CODE = :where_and_eq_lastModifiedByCode_value
        <#else>
        ${prefixName} E.LAST_MODIFIED_BY_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_lastModifiedById??)>
            <#if (where_and_eq_lastModifiedById_value??)>
        ${prefixName} E.LAST_MODIFIED_BY_ID = :where_and_eq_lastModifiedById_value
        <#else>
        ${prefixName} E.LAST_MODIFIED_BY_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_lastModifiedByName??)>
            <#if (where_and_eq_lastModifiedByName_value??)>
        ${prefixName} E.LAST_MODIFIED_BY_NAME = :where_and_eq_lastModifiedByName_value
        <#else>
        ${prefixName} E.LAST_MODIFIED_BY_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_lastModifiedDate??)>
            <#if (where_and_eq_lastModifiedDate_value??)>
        ${prefixName} E.LAST_MODIFIED_DATE = :where_and_eq_lastModifiedDate_value
        <#else>
        ${prefixName} E.LAST_MODIFIED_DATE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_locus??)>
            <#if (where_and_eq_locus_value??)>
        ${prefixName} E.LOCUS = :where_and_eq_locus_value
        <#else>
        ${prefixName} E.LOCUS IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_marryStateCode??)>
            <#if (where_and_eq_marryStateCode_value??)>
        ${prefixName} E.MARRY_STATE_CODE = :where_and_eq_marryStateCode_value
        <#else>
        ${prefixName} E.MARRY_STATE_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_marryStateId??)>
            <#if (where_and_eq_marryStateId_value??)>
        ${prefixName} E.MARRY_STATE_ID = :where_and_eq_marryStateId_value
        <#else>
        ${prefixName} E.MARRY_STATE_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_marryStateName??)>
            <#if (where_and_eq_marryStateName_value??)>
        ${prefixName} E.MARRY_STATE_NAME = :where_and_eq_marryStateName_value
        <#else>
        ${prefixName} E.MARRY_STATE_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_mobilePhone??)>
            <#if (where_and_eq_mobilePhone_value??)>
        ${prefixName} E.MOBILE_PHONE = :where_and_eq_mobilePhone_value
        <#else>
        ${prefixName} E.MOBILE_PHONE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_name??)>
            <#if (where_and_eq_name_value??)>
        ${prefixName} E.NAME = :where_and_eq_name_value
        <#else>
        ${prefixName} E.NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_no??)>
            <#if (where_and_eq_no_value??)>
        ${prefixName} E.NO = :where_and_eq_no_value
        <#else>
        ${prefixName} E.NO IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_orgCode??)>
            <#if (where_and_eq_orgCode_value??)>
        ${prefixName} E.ORG_CODE = :where_and_eq_orgCode_value
        <#else>
        ${prefixName} E.ORG_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_orgId??)>
            <#if (where_and_eq_orgId_value??)>
        ${prefixName} E.ORG_ID = :where_and_eq_orgId_value
        <#else>
        ${prefixName} E.ORG_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_orgName??)>
            <#if (where_and_eq_orgName_value??)>
        ${prefixName} E.ORG_NAME = :where_and_eq_orgName_value
        <#else>
        ${prefixName} E.ORG_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_password??)>
            <#if (where_and_eq_password_value??)>
        ${prefixName} E.PASSWORD = :where_and_eq_password_value
        <#else>
        ${prefixName} E.PASSWORD IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_professionCode??)>
            <#if (where_and_eq_professionCode_value??)>
        ${prefixName} E.PROFESSION_CODE = :where_and_eq_professionCode_value
        <#else>
        ${prefixName} E.PROFESSION_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_professionId??)>
            <#if (where_and_eq_professionId_value??)>
        ${prefixName} E.PROFESSION_ID = :where_and_eq_professionId_value
        <#else>
        ${prefixName} E.PROFESSION_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_professionName??)>
            <#if (where_and_eq_professionName_value??)>
        ${prefixName} E.PROFESSION_NAME = :where_and_eq_professionName_value
        <#else>
        ${prefixName} E.PROFESSION_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_provinceCode??)>
            <#if (where_and_eq_provinceCode_value??)>
        ${prefixName} E.PROVINCE_CODE = :where_and_eq_provinceCode_value
        <#else>
        ${prefixName} E.PROVINCE_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_provinceId??)>
            <#if (where_and_eq_provinceId_value??)>
        ${prefixName} E.PROVINCE_ID = :where_and_eq_provinceId_value
        <#else>
        ${prefixName} E.PROVINCE_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_provinceName??)>
            <#if (where_and_eq_provinceName_value??)>
        ${prefixName} E.PROVINCE_NAME = :where_and_eq_provinceName_value
        <#else>
        ${prefixName} E.PROVINCE_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_sexCode??)>
            <#if (where_and_eq_sexCode_value??)>
        ${prefixName} E.SEX_CODE = :where_and_eq_sexCode_value
        <#else>
        ${prefixName} E.SEX_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_sexId??)>
            <#if (where_and_eq_sexId_value??)>
        ${prefixName} E.SEX_ID = :where_and_eq_sexId_value
        <#else>
        ${prefixName} E.SEX_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_sexName??)>
            <#if (where_and_eq_sexName_value??)>
        ${prefixName} E.SEX_NAME = :where_and_eq_sexName_value
        <#else>
        ${prefixName} E.SEX_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_stateCode??)>
            <#if (where_and_eq_stateCode_value??)>
        ${prefixName} E.STATE_CODE = :where_and_eq_stateCode_value
        <#else>
        ${prefixName} E.STATE_CODE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_stateId??)>
            <#if (where_and_eq_stateId_value??)>
        ${prefixName} E.STATE_ID = :where_and_eq_stateId_value
        <#else>
        ${prefixName} E.STATE_ID IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_stateName??)>
            <#if (where_and_eq_stateName_value??)>
        ${prefixName} E.STATE_NAME = :where_and_eq_stateName_value
        <#else>
        ${prefixName} E.STATE_NAME IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_telephone??)>
            <#if (where_and_eq_telephone_value??)>
        ${prefixName} E.TELEPHONE = :where_and_eq_telephone_value
        <#else>
        ${prefixName} E.TELEPHONE IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
                <#if
                (prefixName!) != 'WHERE'>
            <#assign prefixName='AND'>
                </#if>
            <#if (where_and_eq_versionInt??)>
            <#if (where_and_eq_versionInt_value??)>
        ${prefixName} E.VERSION_INT = :where_and_eq_versionInt_value
        <#else>
        ${prefixName} E.VERSION_INT IS NULL
            </#if>
        <#assign prefixName=''>
            </#if>
            )
            )