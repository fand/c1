SELECT
<#switch (object_id!)>
<#case "address">
ADDRESS AS address
<#break>
<#case "age">
AGE AS age
<#break>
<#case "beginDate">
BEGIN_DATE AS beginDate
<#break>
<#case "birthday">
BIRTHDAY AS birthday
<#break>
<#case "callName">
CALL_NAME AS callName
<#break>
<#case "cityCode">
CITY_CODE AS cityCode
<#break>
<#case "cityId">
CITY_ID AS cityId
<#break>
<#case "cityName">
CITY_NAME AS cityName
<#break>
<#case "code">
CODE AS code
<#break>
<#case "companyName">
COMPANY_NAME AS companyName
<#break>
<#case "countryCode">
COUNTRY_CODE AS countryCode
<#break>
<#case "countryId">
COUNTRY_ID AS countryId
<#break>
<#case "countryName">
COUNTRY_NAME AS countryName
<#break>
<#case "createdByCode">
CREATED_BY_CODE AS createdByCode
<#break>
<#case "createdById">
CREATED_BY_ID AS createdById
<#break>
<#case "createdByName">
CREATED_BY_NAME AS createdByName
<#break>
<#case "createdDate">
CREATED_DATE AS createdDate
<#break>
<#case "dataStateCode">
DATA_STATE_CODE AS dataStateCode
<#break>
<#case "dataStateId">
DATA_STATE_ID AS dataStateId
<#break>
<#case "dataStateName">
DATA_STATE_NAME AS dataStateName
<#break>
<#case "departmentCode">
DEPARTMENT_CODE AS departmentCode
<#break>
<#case "departmentId">
DEPARTMENT_ID AS departmentId
<#break>
<#case "departmentName">
DEPARTMENT_NAME AS departmentName
<#break>
<#case "diplomaCode">
DIPLOMA_CODE AS diplomaCode
<#break>
<#case "diplomaId">
DIPLOMA_ID AS diplomaId
<#break>
<#case "diplomaName">
DIPLOMA_NAME AS diplomaName
<#break>
<#case "districtCode">
DISTRICT_CODE AS districtCode
<#break>
<#case "districtId">
DISTRICT_ID AS districtId
<#break>
<#case "districtName">
DISTRICT_NAME AS districtName
<#break>
<#case "email">
EMAIL AS email
<#break>
<#case "endDate">
END_DATE AS endDate
<#break>
<#case "flagCode">
FLAG_CODE AS flagCode
<#break>
<#case "flagId">
FLAG_ID AS flagId
<#break>
<#case "flagName">
FLAG_NAME AS flagName
<#break>
<#case "folkCode">
FOLK_CODE AS folkCode
<#break>
<#case "folkId">
FOLK_ID AS folkId
<#break>
<#case "folkName">
FOLK_NAME AS folkName
<#break>
<#case "id">
ID AS id
<#break>
<#case "intro">
INTRO AS intro
<#break>
<#case "lastModifiedByCode">
LAST_MODIFIED_BY_CODE AS lastModifiedByCode
<#break>
<#case "lastModifiedById">
LAST_MODIFIED_BY_ID AS lastModifiedById
<#break>
<#case "lastModifiedByName">
LAST_MODIFIED_BY_NAME AS lastModifiedByName
<#break>
<#case "lastModifiedDate">
LAST_MODIFIED_DATE AS lastModifiedDate
<#break>
<#case "locus">
LOCUS AS locus
<#break>
<#case "marryStateCode">
MARRY_STATE_CODE AS marryStateCode
<#break>
<#case "marryStateId">
MARRY_STATE_ID AS marryStateId
<#break>
<#case "marryStateName">
MARRY_STATE_NAME AS marryStateName
<#break>
<#case "mobilePhone">
MOBILE_PHONE AS mobilePhone
<#break>
<#case "name">
NAME AS name
<#break>
<#case "no">
NO AS no
<#break>
<#case "orgCode">
ORG_CODE AS orgCode
<#break>
<#case "orgId">
ORG_ID AS orgId
<#break>
<#case "orgName">
ORG_NAME AS orgName
<#break>
<#case "password">
PASSWORD AS password
<#break>
<#case "professionCode">
PROFESSION_CODE AS professionCode
<#break>
<#case "professionId">
PROFESSION_ID AS professionId
<#break>
<#case "professionName">
PROFESSION_NAME AS professionName
<#break>
<#case "provinceCode">
PROVINCE_CODE AS provinceCode
<#break>
<#case "provinceId">
PROVINCE_ID AS provinceId
<#break>
<#case "provinceName">
PROVINCE_NAME AS provinceName
<#break>
<#case "sexCode">
SEX_CODE AS sexCode
<#break>
<#case "sexId">
SEX_ID AS sexId
<#break>
<#case "sexName">
SEX_NAME AS sexName
<#break>
<#case "stateCode">
STATE_CODE AS stateCode
<#break>
<#case "stateId">
STATE_ID AS stateId
<#break>
<#case "stateName">
STATE_NAME AS stateName
<#break>
<#case "telephone">
TELEPHONE AS telephone
<#break>
<#case "versionInt">
VERSION_INT AS versionInt
<#break>
    <#default>
        COUNT(1) AS COUNT_
        </#switch>
        FROM c1_user
        <#include
        "whereByCode.ftl">