INSERT
INTO
c1_user
(
<#assign prefixName=' '>
        <#if (insert_address??) && (insert_address_value??)>
    ${prefixName} `ADDRESS`
    <#assign prefixName=','>
        </#if>
        <#if (insert_age??) && (insert_age_value??)>
    ${prefixName} `AGE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_beginDate??) && (insert_beginDate_value??)>
    ${prefixName} `BEGIN_DATE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_birthday??) && (insert_birthday_value??)>
    ${prefixName} `BIRTHDAY`
    <#assign prefixName=','>
        </#if>
        <#if (insert_callName??) && (insert_callName_value??)>
    ${prefixName} `CALL_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_cityCode??) && (insert_cityCode_value??)>
    ${prefixName} `CITY_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_cityId??) && (insert_cityId_value??)>
    ${prefixName} `CITY_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_cityName??) && (insert_cityName_value??)>
    ${prefixName} `CITY_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_code??) && (insert_code_value??)>
    ${prefixName} `CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_companyName??) && (insert_companyName_value??)>
    ${prefixName} `COMPANY_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_countryCode??) && (insert_countryCode_value??)>
    ${prefixName} `COUNTRY_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_countryId??) && (insert_countryId_value??)>
    ${prefixName} `COUNTRY_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_countryName??) && (insert_countryName_value??)>
    ${prefixName} `COUNTRY_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_createdByCode??) && (insert_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_createdById??) && (insert_createdById_value??)>
    ${prefixName} `CREATED_BY_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_createdByName??) && (insert_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME`
    <#assign prefixName=','>
        </#if>
    ${prefixName} CREATED_DATE
    <#assign prefixName=','>
        <#if (insert_dataStateCode??) && (insert_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_dataStateId??) && (insert_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_dataStateName??) && (insert_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_departmentCode??) && (insert_departmentCode_value??)>
    ${prefixName} `DEPARTMENT_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_departmentId??) && (insert_departmentId_value??)>
    ${prefixName} `DEPARTMENT_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_departmentName??) && (insert_departmentName_value??)>
    ${prefixName} `DEPARTMENT_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_diplomaCode??) && (insert_diplomaCode_value??)>
    ${prefixName} `DIPLOMA_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_diplomaId??) && (insert_diplomaId_value??)>
    ${prefixName} `DIPLOMA_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_diplomaName??) && (insert_diplomaName_value??)>
    ${prefixName} `DIPLOMA_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_districtCode??) && (insert_districtCode_value??)>
    ${prefixName} `DISTRICT_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_districtId??) && (insert_districtId_value??)>
    ${prefixName} `DISTRICT_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_districtName??) && (insert_districtName_value??)>
    ${prefixName} `DISTRICT_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_email??) && (insert_email_value??)>
    ${prefixName} `EMAIL`
    <#assign prefixName=','>
        </#if>
        <#if (insert_endDate??) && (insert_endDate_value??)>
    ${prefixName} `END_DATE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_flagCode??) && (insert_flagCode_value??)>
    ${prefixName} `FLAG_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_flagId??) && (insert_flagId_value??)>
    ${prefixName} `FLAG_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_flagName??) && (insert_flagName_value??)>
    ${prefixName} `FLAG_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_folkCode??) && (insert_folkCode_value??)>
    ${prefixName} `FOLK_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_folkId??) && (insert_folkId_value??)>
    ${prefixName} `FOLK_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_folkName??) && (insert_folkName_value??)>
    ${prefixName} `FOLK_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_id??) && (insert_id_value??)>
    ${prefixName} `ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_intro??) && (insert_intro_value??)>
    ${prefixName} `INTRO`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_locus??) && (insert_locus_value??)>
    ${prefixName} `LOCUS`
    <#assign prefixName=','>
        </#if>
        <#if (insert_marryStateCode??) && (insert_marryStateCode_value??)>
    ${prefixName} `MARRY_STATE_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_marryStateId??) && (insert_marryStateId_value??)>
    ${prefixName} `MARRY_STATE_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_marryStateName??) && (insert_marryStateName_value??)>
    ${prefixName} `MARRY_STATE_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_mobilePhone??) && (insert_mobilePhone_value??)>
    ${prefixName} `MOBILE_PHONE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_name??) && (insert_name_value??)>
    ${prefixName} `NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_no??) && (insert_no_value??)>
    ${prefixName} `NO`
    <#assign prefixName=','>
        </#if>
        <#if (insert_orgCode??) && (insert_orgCode_value??)>
    ${prefixName} `ORG_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_orgId??) && (insert_orgId_value??)>
    ${prefixName} `ORG_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_orgName??) && (insert_orgName_value??)>
    ${prefixName} `ORG_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_password??) && (insert_password_value??)>
    ${prefixName} `PASSWORD`
    <#assign prefixName=','>
        </#if>
        <#if (insert_professionCode??) && (insert_professionCode_value??)>
    ${prefixName} `PROFESSION_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_professionId??) && (insert_professionId_value??)>
    ${prefixName} `PROFESSION_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_professionName??) && (insert_professionName_value??)>
    ${prefixName} `PROFESSION_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_provinceCode??) && (insert_provinceCode_value??)>
    ${prefixName} `PROVINCE_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_provinceId??) && (insert_provinceId_value??)>
    ${prefixName} `PROVINCE_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_provinceName??) && (insert_provinceName_value??)>
    ${prefixName} `PROVINCE_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_sexCode??) && (insert_sexCode_value??)>
    ${prefixName} `SEX_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_sexId??) && (insert_sexId_value??)>
    ${prefixName} `SEX_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_sexName??) && (insert_sexName_value??)>
    ${prefixName} `SEX_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_stateCode??) && (insert_stateCode_value??)>
    ${prefixName} `STATE_CODE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_stateId??) && (insert_stateId_value??)>
    ${prefixName} `STATE_ID`
    <#assign prefixName=','>
        </#if>
        <#if (insert_stateName??) && (insert_stateName_value??)>
    ${prefixName} `STATE_NAME`
    <#assign prefixName=','>
        </#if>
        <#if (insert_telephone??) && (insert_telephone_value??)>
    ${prefixName} `TELEPHONE`
    <#assign prefixName=','>
        </#if>
        <#if (insert_versionInt??) && (insert_versionInt_value??)>
    ${prefixName} `VERSION_INT`
    <#assign prefixName=','>
        </#if>
    )
    VALUES
    (
    <#assign prefixName=' '>
            <#if (insert_address??) && (insert_address_value??)>
        ${prefixName} :insert_address_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_age??) && (insert_age_value??)>
        ${prefixName} :insert_age_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_beginDate??) && (insert_beginDate_value??)>
        ${prefixName} :insert_beginDate_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_birthday??) && (insert_birthday_value??)>
        ${prefixName} :insert_birthday_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_callName??) && (insert_callName_value??)>
        ${prefixName} :insert_callName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_cityCode??) && (insert_cityCode_value??)>
        ${prefixName} :insert_cityCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_cityId??) && (insert_cityId_value??)>
        ${prefixName} :insert_cityId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_cityName??) && (insert_cityName_value??)>
        ${prefixName} :insert_cityName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_code??) && (insert_code_value??)>
        ${prefixName} :insert_code_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_companyName??) && (insert_companyName_value??)>
        ${prefixName} :insert_companyName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_countryCode??) && (insert_countryCode_value??)>
        ${prefixName} :insert_countryCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_countryId??) && (insert_countryId_value??)>
        ${prefixName} :insert_countryId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_countryName??) && (insert_countryName_value??)>
        ${prefixName} :insert_countryName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_createdByCode??) && (insert_createdByCode_value??)>
        ${prefixName} :insert_createdByCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_createdById??) && (insert_createdById_value??)>
        ${prefixName} :insert_createdById_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_createdByName??) && (insert_createdByName_value??)>
        ${prefixName} :insert_createdByName_value
        <#assign prefixName=','>
            </#if>
        ${prefixName} NOW()
        <#assign prefixName=','>
            <#if (insert_dataStateCode??) && (insert_dataStateCode_value??)>
        ${prefixName} :insert_dataStateCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_dataStateId??) && (insert_dataStateId_value??)>
        ${prefixName} :insert_dataStateId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_dataStateName??) && (insert_dataStateName_value??)>
        ${prefixName} :insert_dataStateName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_departmentCode??) && (insert_departmentCode_value??)>
        ${prefixName} :insert_departmentCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_departmentId??) && (insert_departmentId_value??)>
        ${prefixName} :insert_departmentId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_departmentName??) && (insert_departmentName_value??)>
        ${prefixName} :insert_departmentName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_diplomaCode??) && (insert_diplomaCode_value??)>
        ${prefixName} :insert_diplomaCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_diplomaId??) && (insert_diplomaId_value??)>
        ${prefixName} :insert_diplomaId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_diplomaName??) && (insert_diplomaName_value??)>
        ${prefixName} :insert_diplomaName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_districtCode??) && (insert_districtCode_value??)>
        ${prefixName} :insert_districtCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_districtId??) && (insert_districtId_value??)>
        ${prefixName} :insert_districtId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_districtName??) && (insert_districtName_value??)>
        ${prefixName} :insert_districtName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_email??) && (insert_email_value??)>
        ${prefixName} :insert_email_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_endDate??) && (insert_endDate_value??)>
        ${prefixName} :insert_endDate_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_flagCode??) && (insert_flagCode_value??)>
        ${prefixName} :insert_flagCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_flagId??) && (insert_flagId_value??)>
        ${prefixName} :insert_flagId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_flagName??) && (insert_flagName_value??)>
        ${prefixName} :insert_flagName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_folkCode??) && (insert_folkCode_value??)>
        ${prefixName} :insert_folkCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_folkId??) && (insert_folkId_value??)>
        ${prefixName} :insert_folkId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_folkName??) && (insert_folkName_value??)>
        ${prefixName} :insert_folkName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_id??) && (insert_id_value??)>
        ${prefixName} :insert_id_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_intro??) && (insert_intro_value??)>
        ${prefixName} :insert_intro_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
        ${prefixName} :insert_lastModifiedByCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
        ${prefixName} :insert_lastModifiedById_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
        ${prefixName} :insert_lastModifiedByName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
        ${prefixName} :insert_lastModifiedDate_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_locus??) && (insert_locus_value??)>
        ${prefixName} :insert_locus_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_marryStateCode??) && (insert_marryStateCode_value??)>
        ${prefixName} :insert_marryStateCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_marryStateId??) && (insert_marryStateId_value??)>
        ${prefixName} :insert_marryStateId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_marryStateName??) && (insert_marryStateName_value??)>
        ${prefixName} :insert_marryStateName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_mobilePhone??) && (insert_mobilePhone_value??)>
        ${prefixName} :insert_mobilePhone_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_name??) && (insert_name_value??)>
        ${prefixName} :insert_name_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_no??) && (insert_no_value??)>
        ${prefixName} :insert_no_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_orgCode??) && (insert_orgCode_value??)>
        ${prefixName} :insert_orgCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_orgId??) && (insert_orgId_value??)>
        ${prefixName} :insert_orgId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_orgName??) && (insert_orgName_value??)>
        ${prefixName} :insert_orgName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_password??) && (insert_password_value??)>
        ${prefixName} :insert_password_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_professionCode??) && (insert_professionCode_value??)>
        ${prefixName} :insert_professionCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_professionId??) && (insert_professionId_value??)>
        ${prefixName} :insert_professionId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_professionName??) && (insert_professionName_value??)>
        ${prefixName} :insert_professionName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_provinceCode??) && (insert_provinceCode_value??)>
        ${prefixName} :insert_provinceCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_provinceId??) && (insert_provinceId_value??)>
        ${prefixName} :insert_provinceId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_provinceName??) && (insert_provinceName_value??)>
        ${prefixName} :insert_provinceName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_sexCode??) && (insert_sexCode_value??)>
        ${prefixName} :insert_sexCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_sexId??) && (insert_sexId_value??)>
        ${prefixName} :insert_sexId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_sexName??) && (insert_sexName_value??)>
        ${prefixName} :insert_sexName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_stateCode??) && (insert_stateCode_value??)>
        ${prefixName} :insert_stateCode_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_stateId??) && (insert_stateId_value??)>
        ${prefixName} :insert_stateId_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_stateName??) && (insert_stateName_value??)>
        ${prefixName} :insert_stateName_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_telephone??) && (insert_telephone_value??)>
        ${prefixName} :insert_telephone_value
        <#assign prefixName=','>
            </#if>
            <#if (insert_versionInt??) && (insert_versionInt_value??)>
        ${prefixName} :insert_versionInt_value
        <#assign prefixName=','>
            </#if>
        )