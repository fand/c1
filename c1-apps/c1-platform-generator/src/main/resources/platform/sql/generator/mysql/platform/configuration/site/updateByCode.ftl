UPDATE c1_site
<#assign prefixName='SET'>
    <#if
    (update_beginDate??)>
    <#if
    (update_beginDate_value??)>
${prefixName} `BEGIN_DATE` = :update_beginDate_value
<#else>
    ${prefixName} `BEGIN_DATE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_code??)>
    <#if
    (update_code_value??)>
${prefixName} `CODE` = :update_code_value
<#else>
    ${prefixName} `CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdByCode??)>
    <#if
    (update_createdByCode_value??)>
${prefixName} `CREATED_BY_CODE` = :update_createdByCode_value
<#else>
    ${prefixName} `CREATED_BY_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdById??)>
    <#if
    (update_createdById_value??)>
${prefixName} `CREATED_BY_ID` = :update_createdById_value
<#else>
    ${prefixName} `CREATED_BY_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdByName??)>
    <#if
    (update_createdByName_value??)>
${prefixName} `CREATED_BY_NAME` = :update_createdByName_value
<#else>
    ${prefixName} `CREATED_BY_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdDate??)>
    <#if
    (update_createdDate_value??)>
${prefixName} `CREATED_DATE` = :update_createdDate_value
<#else>
    ${prefixName} `CREATED_DATE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_dataStateCode??)>
    <#if
    (update_dataStateCode_value??)>
${prefixName} `DATA_STATE_CODE` = :update_dataStateCode_value
<#else>
    ${prefixName} `DATA_STATE_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_dataStateId??)>
    <#if
    (update_dataStateId_value??)>
${prefixName} `DATA_STATE_ID` = :update_dataStateId_value
<#else>
    ${prefixName} `DATA_STATE_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_dataStateName??)>
    <#if
    (update_dataStateName_value??)>
${prefixName} `DATA_STATE_NAME` = :update_dataStateName_value
<#else>
    ${prefixName} `DATA_STATE_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_endDate??)>
    <#if
    (update_endDate_value??)>
${prefixName} `END_DATE` = :update_endDate_value
<#else>
    ${prefixName} `END_DATE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_id??)>
    <#if
    (update_id_value??)>
${prefixName} `ID` = :update_id_value
<#else>
    ${prefixName} `ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedByCode??)>
    <#if
    (update_lastModifiedByCode_value??)>
${prefixName} `LAST_MODIFIED_BY_CODE` = :update_lastModifiedByCode_value
<#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedById??)>
    <#if
    (update_lastModifiedById_value??)>
${prefixName} `LAST_MODIFIED_BY_ID` = :update_lastModifiedById_value
<#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedByName??)>
    <#if
    (update_lastModifiedByName_value??)>
${prefixName} `LAST_MODIFIED_BY_NAME` = :update_lastModifiedByName_value
<#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedDate??)>
    <#if
    (update_lastModifiedDate_value??)>
${prefixName} `LAST_MODIFIED_DATE` = :update_lastModifiedDate_value
<#else>
    ${prefixName} `LAST_MODIFIED_DATE` = NOW()
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_name??)>
    <#if
    (update_name_value??)>
${prefixName} `NAME` = :update_name_value
<#else>
    ${prefixName} `NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_projectCode??)>
    <#if
    (update_projectCode_value??)>
${prefixName} `PROJECT_CODE` = :update_projectCode_value
<#else>
    ${prefixName} `PROJECT_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_projectId??)>
    <#if
    (update_projectId_value??)>
${prefixName} `PROJECT_ID` = :update_projectId_value
<#else>
    ${prefixName} `PROJECT_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_projectName??)>
    <#if
    (update_projectName_value??)>
${prefixName} `PROJECT_NAME` = :update_projectName_value
<#else>
    ${prefixName} `PROJECT_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_stateCode??)>
    <#if
    (update_stateCode_value??)>
${prefixName} `STATE_CODE` = :update_stateCode_value
<#else>
    ${prefixName} `STATE_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_stateId??)>
    <#if
    (update_stateId_value??)>
${prefixName} `STATE_ID` = :update_stateId_value
<#else>
    ${prefixName} `STATE_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_stateName??)>
    <#if
    (update_stateName_value??)>
${prefixName} `STATE_NAME` = :update_stateName_value
<#else>
    ${prefixName} `STATE_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_styleCode??)>
    <#if
    (update_styleCode_value??)>
${prefixName} `STYLE_CODE` = :update_styleCode_value
<#else>
    ${prefixName} `STYLE_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_styleId??)>
    <#if
    (update_styleId_value??)>
${prefixName} `STYLE_ID` = :update_styleId_value
<#else>
    ${prefixName} `STYLE_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_styleName??)>
    <#if
    (update_styleName_value??)>
${prefixName} `STYLE_NAME` = :update_styleName_value
<#else>
    ${prefixName} `STYLE_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_versionInt??)>
    <#if
    (update_versionInt_value??)>
${prefixName} `VERSION_INT` = :update_versionInt_value
<#else>
    ${prefixName} `VERSION_INT` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#include
    "whereByCode.ftl">