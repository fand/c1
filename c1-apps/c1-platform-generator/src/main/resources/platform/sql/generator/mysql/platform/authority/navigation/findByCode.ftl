SELECT
<#assign prefixName=' '>
    <#if
    (select_beginDate??)>
${prefixName} `BEGIN_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_code??)>
${prefixName} `CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdByCode??)>
${prefixName} `CREATED_BY_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdById??)>
${prefixName} `CREATED_BY_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdByName??)>
${prefixName} `CREATED_BY_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdDate??)>
${prefixName} `CREATED_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_dataOption??)>
${prefixName} `DATA_OPTION`
<#assign prefixName=','>
    </#if>
    <#if
    (select_dataStateCode??)>
${prefixName} `DATA_STATE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_dataStateId??)>
${prefixName} `DATA_STATE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_dataStateName??)>
${prefixName} `DATA_STATE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_endDate??)>
${prefixName} `END_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_iconStyle??)>
${prefixName} `ICON_STYLE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_id??)>
${prefixName} `ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_name??)>
${prefixName} `NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_orderNum??)>
${prefixName} `ORDER_NUM`
<#assign prefixName=','>
    </#if>
    <#if
    (select_parentCode??)>
${prefixName} `PARENT_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_parentId??)>
${prefixName} `PARENT_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_parentName??)>
${prefixName} `PARENT_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_siteCode??)>
${prefixName} `SITE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_siteId??)>
${prefixName} `SITE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_siteName??)>
${prefixName} `SITE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_stateCode??)>
${prefixName} `STATE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_stateId??)>
${prefixName} `STATE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_stateName??)>
${prefixName} `STATE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_target??)>
${prefixName} `TARGET`
<#assign prefixName=','>
    </#if>
    <#if
    (select_treeCode??)>
${prefixName} `TREE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_treeId??)>
${prefixName} `TREE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_treeName??)>
${prefixName} `TREE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_typeCode??)>
${prefixName} `TYPE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_typeId??)>
${prefixName} `TYPE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_typeName??)>
${prefixName} `TYPE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_url??)>
${prefixName} `URL`
<#assign prefixName=','>
    </#if>
    <#if
    (select_versionInt??)>
${prefixName} `VERSION_INT`
<#assign prefixName=','>
    </#if>
    <#if prefixName== ' '>
  `BEGIN_DATE`
, `CODE`
, `CREATED_BY_CODE`
, `CREATED_BY_ID`
, `CREATED_BY_NAME`
, `CREATED_DATE`
, `DATA_OPTION`
, `DATA_STATE_CODE`
, `DATA_STATE_ID`
, `DATA_STATE_NAME`
, `END_DATE`
, `ICON_STYLE`
, `ID`
, `LAST_MODIFIED_BY_CODE`
, `LAST_MODIFIED_BY_ID`
, `LAST_MODIFIED_BY_NAME`
, `LAST_MODIFIED_DATE`
, `NAME`
, `ORDER_NUM`
, `PARENT_CODE`
, `PARENT_ID`
, `PARENT_NAME`
, `SITE_CODE`
, `SITE_ID`
, `SITE_NAME`
, `STATE_CODE`
, `STATE_ID`
, `STATE_NAME`
, `TARGET`
, `TREE_CODE`
, `TREE_ID`
, `TREE_NAME`
, `TYPE_CODE`
, `TYPE_ID`
, `TYPE_NAME`
, `URL`
, `VERSION_INT`
    </#if>
    FROM c1_navigation
    <#include "whereByCode.ftl">
<#assign prefixName='ORDER BY'>
    <#if
    (order_by_beginDate??)>
${prefixName} `BEGIN_DATE` ${order_by_beginDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_code??)>
${prefixName} `CODE` ${order_by_code_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdByCode??)>
${prefixName} `CREATED_BY_CODE` ${order_by_createdByCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdById??)>
${prefixName} `CREATED_BY_ID` ${order_by_createdById_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdByName??)>
${prefixName} `CREATED_BY_NAME` ${order_by_createdByName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdDate??)>
${prefixName} `CREATED_DATE` ${order_by_createdDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_dataOption??)>
${prefixName} `DATA_OPTION` ${order_by_dataOption_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_dataStateCode??)>
${prefixName} `DATA_STATE_CODE` ${order_by_dataStateCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_dataStateId??)>
${prefixName} `DATA_STATE_ID` ${order_by_dataStateId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_dataStateName??)>
${prefixName} `DATA_STATE_NAME` ${order_by_dataStateName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_endDate??)>
${prefixName} `END_DATE` ${order_by_endDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_iconStyle??)>
${prefixName} `ICON_STYLE` ${order_by_iconStyle_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_id??)>
${prefixName} `ID` ${order_by_id_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE` ${order_by_lastModifiedByCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID` ${order_by_lastModifiedById_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME` ${order_by_lastModifiedByName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE` ${order_by_lastModifiedDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_name??)>
${prefixName} `NAME` ${order_by_name_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_orderNum??)>
${prefixName} `ORDER_NUM` ${order_by_orderNum_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_parentCode??)>
${prefixName} `PARENT_CODE` ${order_by_parentCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_parentId??)>
${prefixName} `PARENT_ID` ${order_by_parentId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_parentName??)>
${prefixName} `PARENT_NAME` ${order_by_parentName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_siteCode??)>
${prefixName} `SITE_CODE` ${order_by_siteCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_siteId??)>
${prefixName} `SITE_ID` ${order_by_siteId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_siteName??)>
${prefixName} `SITE_NAME` ${order_by_siteName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_stateCode??)>
${prefixName} `STATE_CODE` ${order_by_stateCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_stateId??)>
${prefixName} `STATE_ID` ${order_by_stateId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_stateName??)>
${prefixName} `STATE_NAME` ${order_by_stateName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_target??)>
${prefixName} `TARGET` ${order_by_target_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_treeCode??)>
${prefixName} `TREE_CODE` ${order_by_treeCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_treeId??)>
${prefixName} `TREE_ID` ${order_by_treeId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_treeName??)>
${prefixName} `TREE_NAME` ${order_by_treeName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_typeCode??)>
${prefixName} `TYPE_CODE` ${order_by_typeCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_typeId??)>
${prefixName} `TYPE_ID` ${order_by_typeId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_typeName??)>
${prefixName} `TYPE_NAME` ${order_by_typeName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_url??)>
${prefixName} `URL` ${order_by_url_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_versionInt??)>
${prefixName} `VERSION_INT` ${order_by_versionInt_value!}
    <#assign prefixName=','>
        </#if>
