SELECT
<#assign prefixName=' '>
    <#if
    (select_beginDate??)>
${prefixName} `BEGIN_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdByCode??)>
${prefixName} `CREATED_BY_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdById??)>
${prefixName} `CREATED_BY_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdByName??)>
${prefixName} `CREATED_BY_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdDate??)>
${prefixName} `CREATED_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_dataStateCode??)>
${prefixName} `DATA_STATE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_dataStateId??)>
${prefixName} `DATA_STATE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_dataStateName??)>
${prefixName} `DATA_STATE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_endDate??)>
${prefixName} `END_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_id??)>
${prefixName} `ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_orderNum??)>
${prefixName} `ORDER_NUM`
<#assign prefixName=','>
    </#if>
    <#if
    (select_permissionId??)>
${prefixName} `PERMISSION_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_stateCode??)>
${prefixName} `STATE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_stateId??)>
${prefixName} `STATE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_stateName??)>
${prefixName} `STATE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_validateErrorCode??)>
${prefixName} `VALIDATE_ERROR_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_validateErrorId??)>
${prefixName} `VALIDATE_ERROR_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_validateErrorName??)>
${prefixName} `VALIDATE_ERROR_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_validateRuleCode??)>
${prefixName} `VALIDATE_RULE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_validateRuleId??)>
${prefixName} `VALIDATE_RULE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_validateRuleName??)>
${prefixName} `VALIDATE_RULE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_validateSuccessCode??)>
${prefixName} `VALIDATE_SUCCESS_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_validateSuccessId??)>
${prefixName} `VALIDATE_SUCCESS_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_validateSuccessName??)>
${prefixName} `VALIDATE_SUCCESS_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_versionInt??)>
${prefixName} `VERSION_INT`
<#assign prefixName=','>
    </#if>
    <#if prefixName== ' '>
  `BEGIN_DATE`
, `CREATED_BY_CODE`
, `CREATED_BY_ID`
, `CREATED_BY_NAME`
, `CREATED_DATE`
, `DATA_STATE_CODE`
, `DATA_STATE_ID`
, `DATA_STATE_NAME`
, `END_DATE`
, `ID`
, `LAST_MODIFIED_BY_CODE`
, `LAST_MODIFIED_BY_ID`
, `LAST_MODIFIED_BY_NAME`
, `LAST_MODIFIED_DATE`
, `ORDER_NUM`
, `PERMISSION_ID`
, `STATE_CODE`
, `STATE_ID`
, `STATE_NAME`
, `VALIDATE_ERROR_CODE`
, `VALIDATE_ERROR_ID`
, `VALIDATE_ERROR_NAME`
, `VALIDATE_RULE_CODE`
, `VALIDATE_RULE_ID`
, `VALIDATE_RULE_NAME`
, `VALIDATE_SUCCESS_CODE`
, `VALIDATE_SUCCESS_ID`
, `VALIDATE_SUCCESS_NAME`
, `VERSION_INT`
    </#if>
    FROM c1_pe_va_ru_transact_mode
    <#include "whereByCode.ftl">
<#assign prefixName='ORDER BY'>
    <#if
    (order_by_beginDate??)>
${prefixName} `BEGIN_DATE` ${order_by_beginDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdByCode??)>
${prefixName} `CREATED_BY_CODE` ${order_by_createdByCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdById??)>
${prefixName} `CREATED_BY_ID` ${order_by_createdById_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdByName??)>
${prefixName} `CREATED_BY_NAME` ${order_by_createdByName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdDate??)>
${prefixName} `CREATED_DATE` ${order_by_createdDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_dataStateCode??)>
${prefixName} `DATA_STATE_CODE` ${order_by_dataStateCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_dataStateId??)>
${prefixName} `DATA_STATE_ID` ${order_by_dataStateId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_dataStateName??)>
${prefixName} `DATA_STATE_NAME` ${order_by_dataStateName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_endDate??)>
${prefixName} `END_DATE` ${order_by_endDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_id??)>
${prefixName} `ID` ${order_by_id_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE` ${order_by_lastModifiedByCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID` ${order_by_lastModifiedById_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME` ${order_by_lastModifiedByName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE` ${order_by_lastModifiedDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_orderNum??)>
${prefixName} `ORDER_NUM` ${order_by_orderNum_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_permissionId??)>
${prefixName} `PERMISSION_ID` ${order_by_permissionId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_stateCode??)>
${prefixName} `STATE_CODE` ${order_by_stateCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_stateId??)>
${prefixName} `STATE_ID` ${order_by_stateId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_stateName??)>
${prefixName} `STATE_NAME` ${order_by_stateName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_validateErrorCode??)>
${prefixName} `VALIDATE_ERROR_CODE` ${order_by_validateErrorCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_validateErrorId??)>
${prefixName} `VALIDATE_ERROR_ID` ${order_by_validateErrorId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_validateErrorName??)>
${prefixName} `VALIDATE_ERROR_NAME` ${order_by_validateErrorName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_validateRuleCode??)>
${prefixName} `VALIDATE_RULE_CODE` ${order_by_validateRuleCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_validateRuleId??)>
${prefixName} `VALIDATE_RULE_ID` ${order_by_validateRuleId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_validateRuleName??)>
${prefixName} `VALIDATE_RULE_NAME` ${order_by_validateRuleName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_validateSuccessCode??)>
${prefixName} `VALIDATE_SUCCESS_CODE` ${order_by_validateSuccessCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_validateSuccessId??)>
${prefixName} `VALIDATE_SUCCESS_ID` ${order_by_validateSuccessId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_validateSuccessName??)>
${prefixName} `VALIDATE_SUCCESS_NAME` ${order_by_validateSuccessName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_versionInt??)>
${prefixName} `VERSION_INT` ${order_by_versionInt_value!}
    <#assign prefixName=','>
        </#if>
