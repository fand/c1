UPDATE c1_user
<#assign prefixName='SET'>
    <#if
    (update_address??)>
    <#if
    (update_address_value??)>
${prefixName} `ADDRESS` = :update_address_value
<#else>
    ${prefixName} `ADDRESS` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_age??)>
    <#if
    (update_age_value??)>
${prefixName} `AGE` = :update_age_value
<#else>
    ${prefixName} `AGE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_beginDate??)>
    <#if
    (update_beginDate_value??)>
${prefixName} `BEGIN_DATE` = :update_beginDate_value
<#else>
    ${prefixName} `BEGIN_DATE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_birthday??)>
    <#if
    (update_birthday_value??)>
${prefixName} `BIRTHDAY` = :update_birthday_value
<#else>
    ${prefixName} `BIRTHDAY` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_callName??)>
    <#if
    (update_callName_value??)>
${prefixName} `CALL_NAME` = :update_callName_value
<#else>
    ${prefixName} `CALL_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_cityCode??)>
    <#if
    (update_cityCode_value??)>
${prefixName} `CITY_CODE` = :update_cityCode_value
<#else>
    ${prefixName} `CITY_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_cityId??)>
    <#if
    (update_cityId_value??)>
${prefixName} `CITY_ID` = :update_cityId_value
<#else>
    ${prefixName} `CITY_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_cityName??)>
    <#if
    (update_cityName_value??)>
${prefixName} `CITY_NAME` = :update_cityName_value
<#else>
    ${prefixName} `CITY_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_code??)>
    <#if
    (update_code_value??)>
${prefixName} `CODE` = :update_code_value
<#else>
    ${prefixName} `CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_companyName??)>
    <#if
    (update_companyName_value??)>
${prefixName} `COMPANY_NAME` = :update_companyName_value
<#else>
    ${prefixName} `COMPANY_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_countryCode??)>
    <#if
    (update_countryCode_value??)>
${prefixName} `COUNTRY_CODE` = :update_countryCode_value
<#else>
    ${prefixName} `COUNTRY_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_countryId??)>
    <#if
    (update_countryId_value??)>
${prefixName} `COUNTRY_ID` = :update_countryId_value
<#else>
    ${prefixName} `COUNTRY_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_countryName??)>
    <#if
    (update_countryName_value??)>
${prefixName} `COUNTRY_NAME` = :update_countryName_value
<#else>
    ${prefixName} `COUNTRY_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdByCode??)>
    <#if
    (update_createdByCode_value??)>
${prefixName} `CREATED_BY_CODE` = :update_createdByCode_value
<#else>
    ${prefixName} `CREATED_BY_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdById??)>
    <#if
    (update_createdById_value??)>
${prefixName} `CREATED_BY_ID` = :update_createdById_value
<#else>
    ${prefixName} `CREATED_BY_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdByName??)>
    <#if
    (update_createdByName_value??)>
${prefixName} `CREATED_BY_NAME` = :update_createdByName_value
<#else>
    ${prefixName} `CREATED_BY_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdDate??)>
    <#if
    (update_createdDate_value??)>
${prefixName} `CREATED_DATE` = :update_createdDate_value
<#else>
    ${prefixName} `CREATED_DATE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_dataStateCode??)>
    <#if
    (update_dataStateCode_value??)>
${prefixName} `DATA_STATE_CODE` = :update_dataStateCode_value
<#else>
    ${prefixName} `DATA_STATE_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_dataStateId??)>
    <#if
    (update_dataStateId_value??)>
${prefixName} `DATA_STATE_ID` = :update_dataStateId_value
<#else>
    ${prefixName} `DATA_STATE_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_dataStateName??)>
    <#if
    (update_dataStateName_value??)>
${prefixName} `DATA_STATE_NAME` = :update_dataStateName_value
<#else>
    ${prefixName} `DATA_STATE_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_departmentCode??)>
    <#if
    (update_departmentCode_value??)>
${prefixName} `DEPARTMENT_CODE` = :update_departmentCode_value
<#else>
    ${prefixName} `DEPARTMENT_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_departmentId??)>
    <#if
    (update_departmentId_value??)>
${prefixName} `DEPARTMENT_ID` = :update_departmentId_value
<#else>
    ${prefixName} `DEPARTMENT_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_departmentName??)>
    <#if
    (update_departmentName_value??)>
${prefixName} `DEPARTMENT_NAME` = :update_departmentName_value
<#else>
    ${prefixName} `DEPARTMENT_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_diplomaCode??)>
    <#if
    (update_diplomaCode_value??)>
${prefixName} `DIPLOMA_CODE` = :update_diplomaCode_value
<#else>
    ${prefixName} `DIPLOMA_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_diplomaId??)>
    <#if
    (update_diplomaId_value??)>
${prefixName} `DIPLOMA_ID` = :update_diplomaId_value
<#else>
    ${prefixName} `DIPLOMA_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_diplomaName??)>
    <#if
    (update_diplomaName_value??)>
${prefixName} `DIPLOMA_NAME` = :update_diplomaName_value
<#else>
    ${prefixName} `DIPLOMA_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_districtCode??)>
    <#if
    (update_districtCode_value??)>
${prefixName} `DISTRICT_CODE` = :update_districtCode_value
<#else>
    ${prefixName} `DISTRICT_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_districtId??)>
    <#if
    (update_districtId_value??)>
${prefixName} `DISTRICT_ID` = :update_districtId_value
<#else>
    ${prefixName} `DISTRICT_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_districtName??)>
    <#if
    (update_districtName_value??)>
${prefixName} `DISTRICT_NAME` = :update_districtName_value
<#else>
    ${prefixName} `DISTRICT_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_email??)>
    <#if
    (update_email_value??)>
${prefixName} `EMAIL` = :update_email_value
<#else>
    ${prefixName} `EMAIL` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_endDate??)>
    <#if
    (update_endDate_value??)>
${prefixName} `END_DATE` = :update_endDate_value
<#else>
    ${prefixName} `END_DATE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_flagCode??)>
    <#if
    (update_flagCode_value??)>
${prefixName} `FLAG_CODE` = :update_flagCode_value
<#else>
    ${prefixName} `FLAG_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_flagId??)>
    <#if
    (update_flagId_value??)>
${prefixName} `FLAG_ID` = :update_flagId_value
<#else>
    ${prefixName} `FLAG_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_flagName??)>
    <#if
    (update_flagName_value??)>
${prefixName} `FLAG_NAME` = :update_flagName_value
<#else>
    ${prefixName} `FLAG_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_folkCode??)>
    <#if
    (update_folkCode_value??)>
${prefixName} `FOLK_CODE` = :update_folkCode_value
<#else>
    ${prefixName} `FOLK_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_folkId??)>
    <#if
    (update_folkId_value??)>
${prefixName} `FOLK_ID` = :update_folkId_value
<#else>
    ${prefixName} `FOLK_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_folkName??)>
    <#if
    (update_folkName_value??)>
${prefixName} `FOLK_NAME` = :update_folkName_value
<#else>
    ${prefixName} `FOLK_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_id??)>
    <#if
    (update_id_value??)>
${prefixName} `ID` = :update_id_value
<#else>
    ${prefixName} `ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_intro??)>
    <#if
    (update_intro_value??)>
${prefixName} `INTRO` = :update_intro_value
<#else>
    ${prefixName} `INTRO` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedByCode??)>
    <#if
    (update_lastModifiedByCode_value??)>
${prefixName} `LAST_MODIFIED_BY_CODE` = :update_lastModifiedByCode_value
<#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedById??)>
    <#if
    (update_lastModifiedById_value??)>
${prefixName} `LAST_MODIFIED_BY_ID` = :update_lastModifiedById_value
<#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedByName??)>
    <#if
    (update_lastModifiedByName_value??)>
${prefixName} `LAST_MODIFIED_BY_NAME` = :update_lastModifiedByName_value
<#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedDate??)>
    <#if
    (update_lastModifiedDate_value??)>
${prefixName} `LAST_MODIFIED_DATE` = :update_lastModifiedDate_value
<#else>
    ${prefixName} `LAST_MODIFIED_DATE` = NOW()
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_locus??)>
    <#if
    (update_locus_value??)>
${prefixName} `LOCUS` = :update_locus_value
<#else>
    ${prefixName} `LOCUS` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_marryStateCode??)>
    <#if
    (update_marryStateCode_value??)>
${prefixName} `MARRY_STATE_CODE` = :update_marryStateCode_value
<#else>
    ${prefixName} `MARRY_STATE_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_marryStateId??)>
    <#if
    (update_marryStateId_value??)>
${prefixName} `MARRY_STATE_ID` = :update_marryStateId_value
<#else>
    ${prefixName} `MARRY_STATE_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_marryStateName??)>
    <#if
    (update_marryStateName_value??)>
${prefixName} `MARRY_STATE_NAME` = :update_marryStateName_value
<#else>
    ${prefixName} `MARRY_STATE_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_mobilePhone??)>
    <#if
    (update_mobilePhone_value??)>
${prefixName} `MOBILE_PHONE` = :update_mobilePhone_value
<#else>
    ${prefixName} `MOBILE_PHONE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_name??)>
    <#if
    (update_name_value??)>
${prefixName} `NAME` = :update_name_value
<#else>
    ${prefixName} `NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_no??)>
    <#if
    (update_no_value??)>
${prefixName} `NO` = :update_no_value
<#else>
    ${prefixName} `NO` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_orgCode??)>
    <#if
    (update_orgCode_value??)>
${prefixName} `ORG_CODE` = :update_orgCode_value
<#else>
    ${prefixName} `ORG_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_orgId??)>
    <#if
    (update_orgId_value??)>
${prefixName} `ORG_ID` = :update_orgId_value
<#else>
    ${prefixName} `ORG_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_orgName??)>
    <#if
    (update_orgName_value??)>
${prefixName} `ORG_NAME` = :update_orgName_value
<#else>
    ${prefixName} `ORG_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_password??)>
    <#if
    (update_password_value??)>
${prefixName} `PASSWORD` = :update_password_value
<#else>
    ${prefixName} `PASSWORD` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_professionCode??)>
    <#if
    (update_professionCode_value??)>
${prefixName} `PROFESSION_CODE` = :update_professionCode_value
<#else>
    ${prefixName} `PROFESSION_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_professionId??)>
    <#if
    (update_professionId_value??)>
${prefixName} `PROFESSION_ID` = :update_professionId_value
<#else>
    ${prefixName} `PROFESSION_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_professionName??)>
    <#if
    (update_professionName_value??)>
${prefixName} `PROFESSION_NAME` = :update_professionName_value
<#else>
    ${prefixName} `PROFESSION_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_provinceCode??)>
    <#if
    (update_provinceCode_value??)>
${prefixName} `PROVINCE_CODE` = :update_provinceCode_value
<#else>
    ${prefixName} `PROVINCE_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_provinceId??)>
    <#if
    (update_provinceId_value??)>
${prefixName} `PROVINCE_ID` = :update_provinceId_value
<#else>
    ${prefixName} `PROVINCE_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_provinceName??)>
    <#if
    (update_provinceName_value??)>
${prefixName} `PROVINCE_NAME` = :update_provinceName_value
<#else>
    ${prefixName} `PROVINCE_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_sexCode??)>
    <#if
    (update_sexCode_value??)>
${prefixName} `SEX_CODE` = :update_sexCode_value
<#else>
    ${prefixName} `SEX_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_sexId??)>
    <#if
    (update_sexId_value??)>
${prefixName} `SEX_ID` = :update_sexId_value
<#else>
    ${prefixName} `SEX_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_sexName??)>
    <#if
    (update_sexName_value??)>
${prefixName} `SEX_NAME` = :update_sexName_value
<#else>
    ${prefixName} `SEX_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_stateCode??)>
    <#if
    (update_stateCode_value??)>
${prefixName} `STATE_CODE` = :update_stateCode_value
<#else>
    ${prefixName} `STATE_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_stateId??)>
    <#if
    (update_stateId_value??)>
${prefixName} `STATE_ID` = :update_stateId_value
<#else>
    ${prefixName} `STATE_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_stateName??)>
    <#if
    (update_stateName_value??)>
${prefixName} `STATE_NAME` = :update_stateName_value
<#else>
    ${prefixName} `STATE_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_telephone??)>
    <#if
    (update_telephone_value??)>
${prefixName} `TELEPHONE` = :update_telephone_value
<#else>
    ${prefixName} `TELEPHONE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_versionInt??)>
    <#if
    (update_versionInt_value??)>
${prefixName} `VERSION_INT` = :update_versionInt_value
<#else>
    ${prefixName} `VERSION_INT` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#include
    "whereByCode.ftl">