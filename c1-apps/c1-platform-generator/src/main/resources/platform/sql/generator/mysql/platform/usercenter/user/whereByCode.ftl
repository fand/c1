<#assign prefixName='WHERE'>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_address??)>
        <#if
        (where_and_eq_address_value??)>
    ${prefixName} `ADDRESS` = :where_and_eq_address_value
        <#else>
        ${prefixName} `ADDRESS` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_address??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_address_value??)>
                ${prefixName} `ADDRESS` <> :where_and_nq_address_value
                    <#else>
                    ${prefixName} `ADDRESS` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_address??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_address_value??)>
                            ${prefixName} `ADDRESS` <> :where_and_like_address_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_address??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_address_value??)>
                                    ${prefixName} `ADDRESS` = :where_or_eq_address
                                        _value
                                        <#else>
                                        ${prefixName} `ADDRESS` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_address??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_address_value??)>
                                                ${prefixName} `ADDRESS` <>
                                                    :where_or_nq_address_value
                                                    <#else>
                                                    ${prefixName} `ADDRESS` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_address??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_address_value??)>
                                                            ${prefixName} `ADDRESS` like
                                                                :where_or_like_address_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_age??)>
        <#if
        (where_and_eq_age_value??)>
    ${prefixName} `AGE` = :where_and_eq_age_value
        <#else>
        ${prefixName} `AGE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_age??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_age_value??)>
                ${prefixName} `AGE` <> :where_and_nq_age_value
                    <#else>
                    ${prefixName} `AGE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_age??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_age_value??)>
                            ${prefixName} `AGE` <> :where_and_like_age_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_age??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_age_value??)>
                                    ${prefixName} `AGE` = :where_or_eq_age
                                        _value
                                        <#else>
                                        ${prefixName} `AGE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_age??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_age_value??)>
                                                ${prefixName} `AGE` <>
                                                    :where_or_nq_age_value
                                                    <#else>
                                                    ${prefixName} `AGE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_age??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_age_value??)>
                                                            ${prefixName} `AGE` like
                                                                :where_or_like_age_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_beginDate??)>
        <#if
        (where_and_eq_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` = :where_and_eq_beginDate_value
        <#else>
        ${prefixName} `BEGIN_DATE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_beginDate??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_beginDate_value??)>
                ${prefixName} `BEGIN_DATE` <> :where_and_nq_beginDate_value
                    <#else>
                    ${prefixName} `BEGIN_DATE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_beginDate??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_beginDate_value??)>
                            ${prefixName} `BEGIN_DATE` <> :where_and_like_beginDate_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_beginDate??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_beginDate_value??)>
                                    ${prefixName} `BEGIN_DATE` = :where_or_eq_beginDate
                                        _value
                                        <#else>
                                        ${prefixName} `BEGIN_DATE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_beginDate??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_beginDate_value??)>
                                                ${prefixName} `BEGIN_DATE` <>
                                                    :where_or_nq_beginDate_value
                                                    <#else>
                                                    ${prefixName} `BEGIN_DATE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_beginDate??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_beginDate_value??)>
                                                            ${prefixName} `BEGIN_DATE` like
                                                                :where_or_like_beginDate_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_birthday??)>
        <#if
        (where_and_eq_birthday_value??)>
    ${prefixName} `BIRTHDAY` = :where_and_eq_birthday_value
        <#else>
        ${prefixName} `BIRTHDAY` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_birthday??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_birthday_value??)>
                ${prefixName} `BIRTHDAY` <> :where_and_nq_birthday_value
                    <#else>
                    ${prefixName} `BIRTHDAY` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_birthday??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_birthday_value??)>
                            ${prefixName} `BIRTHDAY` <> :where_and_like_birthday_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_birthday??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_birthday_value??)>
                                    ${prefixName} `BIRTHDAY` = :where_or_eq_birthday
                                        _value
                                        <#else>
                                        ${prefixName} `BIRTHDAY` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_birthday??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_birthday_value??)>
                                                ${prefixName} `BIRTHDAY` <>
                                                    :where_or_nq_birthday_value
                                                    <#else>
                                                    ${prefixName} `BIRTHDAY` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_birthday??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_birthday_value??)>
                                                            ${prefixName} `BIRTHDAY` like
                                                                :where_or_like_birthday_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_callName??)>
        <#if
        (where_and_eq_callName_value??)>
    ${prefixName} `CALL_NAME` = :where_and_eq_callName_value
        <#else>
        ${prefixName} `CALL_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_callName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_callName_value??)>
                ${prefixName} `CALL_NAME` <> :where_and_nq_callName_value
                    <#else>
                    ${prefixName} `CALL_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_callName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_callName_value??)>
                            ${prefixName} `CALL_NAME` <> :where_and_like_callName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_callName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_callName_value??)>
                                    ${prefixName} `CALL_NAME` = :where_or_eq_callName
                                        _value
                                        <#else>
                                        ${prefixName} `CALL_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_callName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_callName_value??)>
                                                ${prefixName} `CALL_NAME` <>
                                                    :where_or_nq_callName_value
                                                    <#else>
                                                    ${prefixName} `CALL_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_callName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_callName_value??)>
                                                            ${prefixName} `CALL_NAME` like
                                                                :where_or_like_callName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_cityCode??)>
        <#if
        (where_and_eq_cityCode_value??)>
    ${prefixName} `CITY_CODE` = :where_and_eq_cityCode_value
        <#else>
        ${prefixName} `CITY_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_cityCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_cityCode_value??)>
                ${prefixName} `CITY_CODE` <> :where_and_nq_cityCode_value
                    <#else>
                    ${prefixName} `CITY_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_cityCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_cityCode_value??)>
                            ${prefixName} `CITY_CODE` <> :where_and_like_cityCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_cityCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_cityCode_value??)>
                                    ${prefixName} `CITY_CODE` = :where_or_eq_cityCode
                                        _value
                                        <#else>
                                        ${prefixName} `CITY_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_cityCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_cityCode_value??)>
                                                ${prefixName} `CITY_CODE` <>
                                                    :where_or_nq_cityCode_value
                                                    <#else>
                                                    ${prefixName} `CITY_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_cityCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_cityCode_value??)>
                                                            ${prefixName} `CITY_CODE` like
                                                                :where_or_like_cityCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_cityId??)>
        <#if
        (where_and_eq_cityId_value??)>
    ${prefixName} `CITY_ID` = :where_and_eq_cityId_value
        <#else>
        ${prefixName} `CITY_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_cityId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_cityId_value??)>
                ${prefixName} `CITY_ID` <> :where_and_nq_cityId_value
                    <#else>
                    ${prefixName} `CITY_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_cityId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_cityId_value??)>
                            ${prefixName} `CITY_ID` <> :where_and_like_cityId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_cityId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_cityId_value??)>
                                    ${prefixName} `CITY_ID` = :where_or_eq_cityId
                                        _value
                                        <#else>
                                        ${prefixName} `CITY_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_cityId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_cityId_value??)>
                                                ${prefixName} `CITY_ID` <>
                                                    :where_or_nq_cityId_value
                                                    <#else>
                                                    ${prefixName} `CITY_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_cityId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_cityId_value??)>
                                                            ${prefixName} `CITY_ID` like
                                                                :where_or_like_cityId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_cityName??)>
        <#if
        (where_and_eq_cityName_value??)>
    ${prefixName} `CITY_NAME` = :where_and_eq_cityName_value
        <#else>
        ${prefixName} `CITY_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_cityName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_cityName_value??)>
                ${prefixName} `CITY_NAME` <> :where_and_nq_cityName_value
                    <#else>
                    ${prefixName} `CITY_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_cityName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_cityName_value??)>
                            ${prefixName} `CITY_NAME` <> :where_and_like_cityName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_cityName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_cityName_value??)>
                                    ${prefixName} `CITY_NAME` = :where_or_eq_cityName
                                        _value
                                        <#else>
                                        ${prefixName} `CITY_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_cityName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_cityName_value??)>
                                                ${prefixName} `CITY_NAME` <>
                                                    :where_or_nq_cityName_value
                                                    <#else>
                                                    ${prefixName} `CITY_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_cityName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_cityName_value??)>
                                                            ${prefixName} `CITY_NAME` like
                                                                :where_or_like_cityName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_code??)>
        <#if
        (where_and_eq_code_value??)>
    ${prefixName} `CODE` = :where_and_eq_code_value
        <#else>
        ${prefixName} `CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_code??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_code_value??)>
                ${prefixName} `CODE` <> :where_and_nq_code_value
                    <#else>
                    ${prefixName} `CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_code??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_code_value??)>
                            ${prefixName} `CODE` <> :where_and_like_code_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_code??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_code_value??)>
                                    ${prefixName} `CODE` = :where_or_eq_code
                                        _value
                                        <#else>
                                        ${prefixName} `CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_code??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_code_value??)>
                                                ${prefixName} `CODE` <>
                                                    :where_or_nq_code_value
                                                    <#else>
                                                    ${prefixName} `CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_code??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_code_value??)>
                                                            ${prefixName} `CODE` like
                                                                :where_or_like_code_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_companyName??)>
        <#if
        (where_and_eq_companyName_value??)>
    ${prefixName} `COMPANY_NAME` = :where_and_eq_companyName_value
        <#else>
        ${prefixName} `COMPANY_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_companyName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_companyName_value??)>
                ${prefixName} `COMPANY_NAME` <> :where_and_nq_companyName_value
                    <#else>
                    ${prefixName} `COMPANY_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_companyName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_companyName_value??)>
                            ${prefixName} `COMPANY_NAME` <> :where_and_like_companyName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_companyName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_companyName_value??)>
                                    ${prefixName} `COMPANY_NAME` = :where_or_eq_companyName
                                        _value
                                        <#else>
                                        ${prefixName} `COMPANY_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_companyName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_companyName_value??)>
                                                ${prefixName} `COMPANY_NAME` <>
                                                    :where_or_nq_companyName_value
                                                    <#else>
                                                    ${prefixName} `COMPANY_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_companyName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_companyName_value??)>
                                                            ${prefixName} `COMPANY_NAME` like
                                                                :where_or_like_companyName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_countryCode??)>
        <#if
        (where_and_eq_countryCode_value??)>
    ${prefixName} `COUNTRY_CODE` = :where_and_eq_countryCode_value
        <#else>
        ${prefixName} `COUNTRY_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_countryCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_countryCode_value??)>
                ${prefixName} `COUNTRY_CODE` <> :where_and_nq_countryCode_value
                    <#else>
                    ${prefixName} `COUNTRY_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_countryCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_countryCode_value??)>
                            ${prefixName} `COUNTRY_CODE` <> :where_and_like_countryCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_countryCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_countryCode_value??)>
                                    ${prefixName} `COUNTRY_CODE` = :where_or_eq_countryCode
                                        _value
                                        <#else>
                                        ${prefixName} `COUNTRY_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_countryCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_countryCode_value??)>
                                                ${prefixName} `COUNTRY_CODE` <>
                                                    :where_or_nq_countryCode_value
                                                    <#else>
                                                    ${prefixName} `COUNTRY_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_countryCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_countryCode_value??)>
                                                            ${prefixName} `COUNTRY_CODE` like
                                                                :where_or_like_countryCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_countryId??)>
        <#if
        (where_and_eq_countryId_value??)>
    ${prefixName} `COUNTRY_ID` = :where_and_eq_countryId_value
        <#else>
        ${prefixName} `COUNTRY_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_countryId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_countryId_value??)>
                ${prefixName} `COUNTRY_ID` <> :where_and_nq_countryId_value
                    <#else>
                    ${prefixName} `COUNTRY_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_countryId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_countryId_value??)>
                            ${prefixName} `COUNTRY_ID` <> :where_and_like_countryId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_countryId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_countryId_value??)>
                                    ${prefixName} `COUNTRY_ID` = :where_or_eq_countryId
                                        _value
                                        <#else>
                                        ${prefixName} `COUNTRY_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_countryId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_countryId_value??)>
                                                ${prefixName} `COUNTRY_ID` <>
                                                    :where_or_nq_countryId_value
                                                    <#else>
                                                    ${prefixName} `COUNTRY_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_countryId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_countryId_value??)>
                                                            ${prefixName} `COUNTRY_ID` like
                                                                :where_or_like_countryId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_countryName??)>
        <#if
        (where_and_eq_countryName_value??)>
    ${prefixName} `COUNTRY_NAME` = :where_and_eq_countryName_value
        <#else>
        ${prefixName} `COUNTRY_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_countryName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_countryName_value??)>
                ${prefixName} `COUNTRY_NAME` <> :where_and_nq_countryName_value
                    <#else>
                    ${prefixName} `COUNTRY_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_countryName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_countryName_value??)>
                            ${prefixName} `COUNTRY_NAME` <> :where_and_like_countryName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_countryName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_countryName_value??)>
                                    ${prefixName} `COUNTRY_NAME` = :where_or_eq_countryName
                                        _value
                                        <#else>
                                        ${prefixName} `COUNTRY_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_countryName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_countryName_value??)>
                                                ${prefixName} `COUNTRY_NAME` <>
                                                    :where_or_nq_countryName_value
                                                    <#else>
                                                    ${prefixName} `COUNTRY_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_countryName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_countryName_value??)>
                                                            ${prefixName} `COUNTRY_NAME` like
                                                                :where_or_like_countryName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_createdByCode??)>
        <#if
        (where_and_eq_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` = :where_and_eq_createdByCode_value
        <#else>
        ${prefixName} `CREATED_BY_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_createdByCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_createdByCode_value??)>
                ${prefixName} `CREATED_BY_CODE` <> :where_and_nq_createdByCode_value
                    <#else>
                    ${prefixName} `CREATED_BY_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_createdByCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_createdByCode_value??)>
                            ${prefixName} `CREATED_BY_CODE` <> :where_and_like_createdByCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_createdByCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_createdByCode_value??)>
                                    ${prefixName} `CREATED_BY_CODE` = :where_or_eq_createdByCode
                                        _value
                                        <#else>
                                        ${prefixName} `CREATED_BY_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_createdByCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_createdByCode_value??)>
                                                ${prefixName} `CREATED_BY_CODE` <>
                                                    :where_or_nq_createdByCode_value
                                                    <#else>
                                                    ${prefixName} `CREATED_BY_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_createdByCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_createdByCode_value??)>
                                                            ${prefixName} `CREATED_BY_CODE` like
                                                                :where_or_like_createdByCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_createdById??)>
        <#if
        (where_and_eq_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` = :where_and_eq_createdById_value
        <#else>
        ${prefixName} `CREATED_BY_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_createdById??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_createdById_value??)>
                ${prefixName} `CREATED_BY_ID` <> :where_and_nq_createdById_value
                    <#else>
                    ${prefixName} `CREATED_BY_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_createdById??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_createdById_value??)>
                            ${prefixName} `CREATED_BY_ID` <> :where_and_like_createdById_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_createdById??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_createdById_value??)>
                                    ${prefixName} `CREATED_BY_ID` = :where_or_eq_createdById
                                        _value
                                        <#else>
                                        ${prefixName} `CREATED_BY_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_createdById??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_createdById_value??)>
                                                ${prefixName} `CREATED_BY_ID` <>
                                                    :where_or_nq_createdById_value
                                                    <#else>
                                                    ${prefixName} `CREATED_BY_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_createdById??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_createdById_value??)>
                                                            ${prefixName} `CREATED_BY_ID` like
                                                                :where_or_like_createdById_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_createdByName??)>
        <#if
        (where_and_eq_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` = :where_and_eq_createdByName_value
        <#else>
        ${prefixName} `CREATED_BY_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_createdByName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_createdByName_value??)>
                ${prefixName} `CREATED_BY_NAME` <> :where_and_nq_createdByName_value
                    <#else>
                    ${prefixName} `CREATED_BY_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_createdByName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_createdByName_value??)>
                            ${prefixName} `CREATED_BY_NAME` <> :where_and_like_createdByName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_createdByName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_createdByName_value??)>
                                    ${prefixName} `CREATED_BY_NAME` = :where_or_eq_createdByName
                                        _value
                                        <#else>
                                        ${prefixName} `CREATED_BY_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_createdByName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_createdByName_value??)>
                                                ${prefixName} `CREATED_BY_NAME` <>
                                                    :where_or_nq_createdByName_value
                                                    <#else>
                                                    ${prefixName} `CREATED_BY_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_createdByName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_createdByName_value??)>
                                                            ${prefixName} `CREATED_BY_NAME` like
                                                                :where_or_like_createdByName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_createdDate??)>
        <#if
        (where_and_eq_createdDate_value??)>
    ${prefixName} `CREATED_DATE` = :where_and_eq_createdDate_value
        <#else>
        ${prefixName} `CREATED_DATE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_createdDate??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_createdDate_value??)>
                ${prefixName} `CREATED_DATE` <> :where_and_nq_createdDate_value
                    <#else>
                    ${prefixName} `CREATED_DATE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_createdDate??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_createdDate_value??)>
                            ${prefixName} `CREATED_DATE` <> :where_and_like_createdDate_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_createdDate??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_createdDate_value??)>
                                    ${prefixName} `CREATED_DATE` = :where_or_eq_createdDate
                                        _value
                                        <#else>
                                        ${prefixName} `CREATED_DATE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_createdDate??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_createdDate_value??)>
                                                ${prefixName} `CREATED_DATE` <>
                                                    :where_or_nq_createdDate_value
                                                    <#else>
                                                    ${prefixName} `CREATED_DATE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_createdDate??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_createdDate_value??)>
                                                            ${prefixName} `CREATED_DATE` like
                                                                :where_or_like_createdDate_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_dataStateCode??)>
        <#if
        (where_and_eq_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` = :where_and_eq_dataStateCode_value
        <#else>
        ${prefixName} `DATA_STATE_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_dataStateCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_dataStateCode_value??)>
                ${prefixName} `DATA_STATE_CODE` <> :where_and_nq_dataStateCode_value
                    <#else>
                    ${prefixName} `DATA_STATE_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_dataStateCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_dataStateCode_value??)>
                            ${prefixName} `DATA_STATE_CODE` <> :where_and_like_dataStateCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_dataStateCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_dataStateCode_value??)>
                                    ${prefixName} `DATA_STATE_CODE` = :where_or_eq_dataStateCode
                                        _value
                                        <#else>
                                        ${prefixName} `DATA_STATE_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_dataStateCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_dataStateCode_value??)>
                                                ${prefixName} `DATA_STATE_CODE` <>
                                                    :where_or_nq_dataStateCode_value
                                                    <#else>
                                                    ${prefixName} `DATA_STATE_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_dataStateCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_dataStateCode_value??)>
                                                            ${prefixName} `DATA_STATE_CODE` like
                                                                :where_or_like_dataStateCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_dataStateId??)>
        <#if
        (where_and_eq_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` = :where_and_eq_dataStateId_value
        <#else>
        ${prefixName} `DATA_STATE_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_dataStateId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_dataStateId_value??)>
                ${prefixName} `DATA_STATE_ID` <> :where_and_nq_dataStateId_value
                    <#else>
                    ${prefixName} `DATA_STATE_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_dataStateId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_dataStateId_value??)>
                            ${prefixName} `DATA_STATE_ID` <> :where_and_like_dataStateId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_dataStateId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_dataStateId_value??)>
                                    ${prefixName} `DATA_STATE_ID` = :where_or_eq_dataStateId
                                        _value
                                        <#else>
                                        ${prefixName} `DATA_STATE_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_dataStateId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_dataStateId_value??)>
                                                ${prefixName} `DATA_STATE_ID` <>
                                                    :where_or_nq_dataStateId_value
                                                    <#else>
                                                    ${prefixName} `DATA_STATE_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_dataStateId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_dataStateId_value??)>
                                                            ${prefixName} `DATA_STATE_ID` like
                                                                :where_or_like_dataStateId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_dataStateName??)>
        <#if
        (where_and_eq_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` = :where_and_eq_dataStateName_value
        <#else>
        ${prefixName} `DATA_STATE_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_dataStateName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_dataStateName_value??)>
                ${prefixName} `DATA_STATE_NAME` <> :where_and_nq_dataStateName_value
                    <#else>
                    ${prefixName} `DATA_STATE_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_dataStateName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_dataStateName_value??)>
                            ${prefixName} `DATA_STATE_NAME` <> :where_and_like_dataStateName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_dataStateName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_dataStateName_value??)>
                                    ${prefixName} `DATA_STATE_NAME` = :where_or_eq_dataStateName
                                        _value
                                        <#else>
                                        ${prefixName} `DATA_STATE_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_dataStateName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_dataStateName_value??)>
                                                ${prefixName} `DATA_STATE_NAME` <>
                                                    :where_or_nq_dataStateName_value
                                                    <#else>
                                                    ${prefixName} `DATA_STATE_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_dataStateName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_dataStateName_value??)>
                                                            ${prefixName} `DATA_STATE_NAME` like
                                                                :where_or_like_dataStateName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_departmentCode??)>
        <#if
        (where_and_eq_departmentCode_value??)>
    ${prefixName} `DEPARTMENT_CODE` = :where_and_eq_departmentCode_value
        <#else>
        ${prefixName} `DEPARTMENT_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_departmentCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_departmentCode_value??)>
                ${prefixName} `DEPARTMENT_CODE` <> :where_and_nq_departmentCode_value
                    <#else>
                    ${prefixName} `DEPARTMENT_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_departmentCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_departmentCode_value??)>
                            ${prefixName} `DEPARTMENT_CODE` <> :where_and_like_departmentCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_departmentCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_departmentCode_value??)>
                                    ${prefixName} `DEPARTMENT_CODE` = :where_or_eq_departmentCode
                                        _value
                                        <#else>
                                        ${prefixName} `DEPARTMENT_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_departmentCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_departmentCode_value??)>
                                                ${prefixName} `DEPARTMENT_CODE` <>
                                                    :where_or_nq_departmentCode_value
                                                    <#else>
                                                    ${prefixName} `DEPARTMENT_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_departmentCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_departmentCode_value??)>
                                                            ${prefixName} `DEPARTMENT_CODE` like
                                                                :where_or_like_departmentCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_departmentId??)>
        <#if
        (where_and_eq_departmentId_value??)>
    ${prefixName} `DEPARTMENT_ID` = :where_and_eq_departmentId_value
        <#else>
        ${prefixName} `DEPARTMENT_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_departmentId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_departmentId_value??)>
                ${prefixName} `DEPARTMENT_ID` <> :where_and_nq_departmentId_value
                    <#else>
                    ${prefixName} `DEPARTMENT_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_departmentId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_departmentId_value??)>
                            ${prefixName} `DEPARTMENT_ID` <> :where_and_like_departmentId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_departmentId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_departmentId_value??)>
                                    ${prefixName} `DEPARTMENT_ID` = :where_or_eq_departmentId
                                        _value
                                        <#else>
                                        ${prefixName} `DEPARTMENT_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_departmentId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_departmentId_value??)>
                                                ${prefixName} `DEPARTMENT_ID` <>
                                                    :where_or_nq_departmentId_value
                                                    <#else>
                                                    ${prefixName} `DEPARTMENT_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_departmentId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_departmentId_value??)>
                                                            ${prefixName} `DEPARTMENT_ID` like
                                                                :where_or_like_departmentId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_departmentName??)>
        <#if
        (where_and_eq_departmentName_value??)>
    ${prefixName} `DEPARTMENT_NAME` = :where_and_eq_departmentName_value
        <#else>
        ${prefixName} `DEPARTMENT_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_departmentName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_departmentName_value??)>
                ${prefixName} `DEPARTMENT_NAME` <> :where_and_nq_departmentName_value
                    <#else>
                    ${prefixName} `DEPARTMENT_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_departmentName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_departmentName_value??)>
                            ${prefixName} `DEPARTMENT_NAME` <> :where_and_like_departmentName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_departmentName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_departmentName_value??)>
                                    ${prefixName} `DEPARTMENT_NAME` = :where_or_eq_departmentName
                                        _value
                                        <#else>
                                        ${prefixName} `DEPARTMENT_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_departmentName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_departmentName_value??)>
                                                ${prefixName} `DEPARTMENT_NAME` <>
                                                    :where_or_nq_departmentName_value
                                                    <#else>
                                                    ${prefixName} `DEPARTMENT_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_departmentName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_departmentName_value??)>
                                                            ${prefixName} `DEPARTMENT_NAME` like
                                                                :where_or_like_departmentName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_diplomaCode??)>
        <#if
        (where_and_eq_diplomaCode_value??)>
    ${prefixName} `DIPLOMA_CODE` = :where_and_eq_diplomaCode_value
        <#else>
        ${prefixName} `DIPLOMA_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_diplomaCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_diplomaCode_value??)>
                ${prefixName} `DIPLOMA_CODE` <> :where_and_nq_diplomaCode_value
                    <#else>
                    ${prefixName} `DIPLOMA_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_diplomaCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_diplomaCode_value??)>
                            ${prefixName} `DIPLOMA_CODE` <> :where_and_like_diplomaCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_diplomaCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_diplomaCode_value??)>
                                    ${prefixName} `DIPLOMA_CODE` = :where_or_eq_diplomaCode
                                        _value
                                        <#else>
                                        ${prefixName} `DIPLOMA_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_diplomaCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_diplomaCode_value??)>
                                                ${prefixName} `DIPLOMA_CODE` <>
                                                    :where_or_nq_diplomaCode_value
                                                    <#else>
                                                    ${prefixName} `DIPLOMA_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_diplomaCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_diplomaCode_value??)>
                                                            ${prefixName} `DIPLOMA_CODE` like
                                                                :where_or_like_diplomaCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_diplomaId??)>
        <#if
        (where_and_eq_diplomaId_value??)>
    ${prefixName} `DIPLOMA_ID` = :where_and_eq_diplomaId_value
        <#else>
        ${prefixName} `DIPLOMA_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_diplomaId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_diplomaId_value??)>
                ${prefixName} `DIPLOMA_ID` <> :where_and_nq_diplomaId_value
                    <#else>
                    ${prefixName} `DIPLOMA_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_diplomaId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_diplomaId_value??)>
                            ${prefixName} `DIPLOMA_ID` <> :where_and_like_diplomaId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_diplomaId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_diplomaId_value??)>
                                    ${prefixName} `DIPLOMA_ID` = :where_or_eq_diplomaId
                                        _value
                                        <#else>
                                        ${prefixName} `DIPLOMA_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_diplomaId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_diplomaId_value??)>
                                                ${prefixName} `DIPLOMA_ID` <>
                                                    :where_or_nq_diplomaId_value
                                                    <#else>
                                                    ${prefixName} `DIPLOMA_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_diplomaId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_diplomaId_value??)>
                                                            ${prefixName} `DIPLOMA_ID` like
                                                                :where_or_like_diplomaId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_diplomaName??)>
        <#if
        (where_and_eq_diplomaName_value??)>
    ${prefixName} `DIPLOMA_NAME` = :where_and_eq_diplomaName_value
        <#else>
        ${prefixName} `DIPLOMA_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_diplomaName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_diplomaName_value??)>
                ${prefixName} `DIPLOMA_NAME` <> :where_and_nq_diplomaName_value
                    <#else>
                    ${prefixName} `DIPLOMA_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_diplomaName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_diplomaName_value??)>
                            ${prefixName} `DIPLOMA_NAME` <> :where_and_like_diplomaName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_diplomaName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_diplomaName_value??)>
                                    ${prefixName} `DIPLOMA_NAME` = :where_or_eq_diplomaName
                                        _value
                                        <#else>
                                        ${prefixName} `DIPLOMA_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_diplomaName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_diplomaName_value??)>
                                                ${prefixName} `DIPLOMA_NAME` <>
                                                    :where_or_nq_diplomaName_value
                                                    <#else>
                                                    ${prefixName} `DIPLOMA_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_diplomaName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_diplomaName_value??)>
                                                            ${prefixName} `DIPLOMA_NAME` like
                                                                :where_or_like_diplomaName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_districtCode??)>
        <#if
        (where_and_eq_districtCode_value??)>
    ${prefixName} `DISTRICT_CODE` = :where_and_eq_districtCode_value
        <#else>
        ${prefixName} `DISTRICT_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_districtCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_districtCode_value??)>
                ${prefixName} `DISTRICT_CODE` <> :where_and_nq_districtCode_value
                    <#else>
                    ${prefixName} `DISTRICT_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_districtCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_districtCode_value??)>
                            ${prefixName} `DISTRICT_CODE` <> :where_and_like_districtCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_districtCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_districtCode_value??)>
                                    ${prefixName} `DISTRICT_CODE` = :where_or_eq_districtCode
                                        _value
                                        <#else>
                                        ${prefixName} `DISTRICT_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_districtCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_districtCode_value??)>
                                                ${prefixName} `DISTRICT_CODE` <>
                                                    :where_or_nq_districtCode_value
                                                    <#else>
                                                    ${prefixName} `DISTRICT_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_districtCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_districtCode_value??)>
                                                            ${prefixName} `DISTRICT_CODE` like
                                                                :where_or_like_districtCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_districtId??)>
        <#if
        (where_and_eq_districtId_value??)>
    ${prefixName} `DISTRICT_ID` = :where_and_eq_districtId_value
        <#else>
        ${prefixName} `DISTRICT_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_districtId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_districtId_value??)>
                ${prefixName} `DISTRICT_ID` <> :where_and_nq_districtId_value
                    <#else>
                    ${prefixName} `DISTRICT_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_districtId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_districtId_value??)>
                            ${prefixName} `DISTRICT_ID` <> :where_and_like_districtId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_districtId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_districtId_value??)>
                                    ${prefixName} `DISTRICT_ID` = :where_or_eq_districtId
                                        _value
                                        <#else>
                                        ${prefixName} `DISTRICT_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_districtId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_districtId_value??)>
                                                ${prefixName} `DISTRICT_ID` <>
                                                    :where_or_nq_districtId_value
                                                    <#else>
                                                    ${prefixName} `DISTRICT_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_districtId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_districtId_value??)>
                                                            ${prefixName} `DISTRICT_ID` like
                                                                :where_or_like_districtId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_districtName??)>
        <#if
        (where_and_eq_districtName_value??)>
    ${prefixName} `DISTRICT_NAME` = :where_and_eq_districtName_value
        <#else>
        ${prefixName} `DISTRICT_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_districtName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_districtName_value??)>
                ${prefixName} `DISTRICT_NAME` <> :where_and_nq_districtName_value
                    <#else>
                    ${prefixName} `DISTRICT_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_districtName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_districtName_value??)>
                            ${prefixName} `DISTRICT_NAME` <> :where_and_like_districtName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_districtName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_districtName_value??)>
                                    ${prefixName} `DISTRICT_NAME` = :where_or_eq_districtName
                                        _value
                                        <#else>
                                        ${prefixName} `DISTRICT_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_districtName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_districtName_value??)>
                                                ${prefixName} `DISTRICT_NAME` <>
                                                    :where_or_nq_districtName_value
                                                    <#else>
                                                    ${prefixName} `DISTRICT_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_districtName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_districtName_value??)>
                                                            ${prefixName} `DISTRICT_NAME` like
                                                                :where_or_like_districtName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_email??)>
        <#if
        (where_and_eq_email_value??)>
    ${prefixName} `EMAIL` = :where_and_eq_email_value
        <#else>
        ${prefixName} `EMAIL` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_email??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_email_value??)>
                ${prefixName} `EMAIL` <> :where_and_nq_email_value
                    <#else>
                    ${prefixName} `EMAIL` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_email??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_email_value??)>
                            ${prefixName} `EMAIL` <> :where_and_like_email_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_email??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_email_value??)>
                                    ${prefixName} `EMAIL` = :where_or_eq_email
                                        _value
                                        <#else>
                                        ${prefixName} `EMAIL` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_email??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_email_value??)>
                                                ${prefixName} `EMAIL` <>
                                                    :where_or_nq_email_value
                                                    <#else>
                                                    ${prefixName} `EMAIL` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_email??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_email_value??)>
                                                            ${prefixName} `EMAIL` like
                                                                :where_or_like_email_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_endDate??)>
        <#if
        (where_and_eq_endDate_value??)>
    ${prefixName} `END_DATE` = :where_and_eq_endDate_value
        <#else>
        ${prefixName} `END_DATE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_endDate??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_endDate_value??)>
                ${prefixName} `END_DATE` <> :where_and_nq_endDate_value
                    <#else>
                    ${prefixName} `END_DATE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_endDate??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_endDate_value??)>
                            ${prefixName} `END_DATE` <> :where_and_like_endDate_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_endDate??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_endDate_value??)>
                                    ${prefixName} `END_DATE` = :where_or_eq_endDate
                                        _value
                                        <#else>
                                        ${prefixName} `END_DATE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_endDate??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_endDate_value??)>
                                                ${prefixName} `END_DATE` <>
                                                    :where_or_nq_endDate_value
                                                    <#else>
                                                    ${prefixName} `END_DATE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_endDate??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_endDate_value??)>
                                                            ${prefixName} `END_DATE` like
                                                                :where_or_like_endDate_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
                                                                        <#if
                                                                        (where_and_between_sysdate??)>
                                                                        <#if
                                                                        (prefixName!) != 'WHERE'>
                                                                    <#assign prefixName='AND'>
                                                                        </#if>
                                                                    ${prefixName} (NOW() BETWEEN
                                                                        IFNULL(BEGIN_DATE, NOW()) AND IFNULL(END_DATE,
                                                                        NOW()))
                                                                    <#assign prefixName=''>
                                                                        </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_flagCode??)>
        <#if
        (where_and_eq_flagCode_value??)>
    ${prefixName} `FLAG_CODE` = :where_and_eq_flagCode_value
        <#else>
        ${prefixName} `FLAG_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_flagCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_flagCode_value??)>
                ${prefixName} `FLAG_CODE` <> :where_and_nq_flagCode_value
                    <#else>
                    ${prefixName} `FLAG_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_flagCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_flagCode_value??)>
                            ${prefixName} `FLAG_CODE` <> :where_and_like_flagCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_flagCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_flagCode_value??)>
                                    ${prefixName} `FLAG_CODE` = :where_or_eq_flagCode
                                        _value
                                        <#else>
                                        ${prefixName} `FLAG_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_flagCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_flagCode_value??)>
                                                ${prefixName} `FLAG_CODE` <>
                                                    :where_or_nq_flagCode_value
                                                    <#else>
                                                    ${prefixName} `FLAG_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_flagCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_flagCode_value??)>
                                                            ${prefixName} `FLAG_CODE` like
                                                                :where_or_like_flagCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_flagId??)>
        <#if
        (where_and_eq_flagId_value??)>
    ${prefixName} `FLAG_ID` = :where_and_eq_flagId_value
        <#else>
        ${prefixName} `FLAG_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_flagId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_flagId_value??)>
                ${prefixName} `FLAG_ID` <> :where_and_nq_flagId_value
                    <#else>
                    ${prefixName} `FLAG_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_flagId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_flagId_value??)>
                            ${prefixName} `FLAG_ID` <> :where_and_like_flagId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_flagId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_flagId_value??)>
                                    ${prefixName} `FLAG_ID` = :where_or_eq_flagId
                                        _value
                                        <#else>
                                        ${prefixName} `FLAG_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_flagId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_flagId_value??)>
                                                ${prefixName} `FLAG_ID` <>
                                                    :where_or_nq_flagId_value
                                                    <#else>
                                                    ${prefixName} `FLAG_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_flagId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_flagId_value??)>
                                                            ${prefixName} `FLAG_ID` like
                                                                :where_or_like_flagId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_flagName??)>
        <#if
        (where_and_eq_flagName_value??)>
    ${prefixName} `FLAG_NAME` = :where_and_eq_flagName_value
        <#else>
        ${prefixName} `FLAG_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_flagName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_flagName_value??)>
                ${prefixName} `FLAG_NAME` <> :where_and_nq_flagName_value
                    <#else>
                    ${prefixName} `FLAG_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_flagName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_flagName_value??)>
                            ${prefixName} `FLAG_NAME` <> :where_and_like_flagName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_flagName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_flagName_value??)>
                                    ${prefixName} `FLAG_NAME` = :where_or_eq_flagName
                                        _value
                                        <#else>
                                        ${prefixName} `FLAG_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_flagName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_flagName_value??)>
                                                ${prefixName} `FLAG_NAME` <>
                                                    :where_or_nq_flagName_value
                                                    <#else>
                                                    ${prefixName} `FLAG_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_flagName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_flagName_value??)>
                                                            ${prefixName} `FLAG_NAME` like
                                                                :where_or_like_flagName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_folkCode??)>
        <#if
        (where_and_eq_folkCode_value??)>
    ${prefixName} `FOLK_CODE` = :where_and_eq_folkCode_value
        <#else>
        ${prefixName} `FOLK_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_folkCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_folkCode_value??)>
                ${prefixName} `FOLK_CODE` <> :where_and_nq_folkCode_value
                    <#else>
                    ${prefixName} `FOLK_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_folkCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_folkCode_value??)>
                            ${prefixName} `FOLK_CODE` <> :where_and_like_folkCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_folkCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_folkCode_value??)>
                                    ${prefixName} `FOLK_CODE` = :where_or_eq_folkCode
                                        _value
                                        <#else>
                                        ${prefixName} `FOLK_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_folkCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_folkCode_value??)>
                                                ${prefixName} `FOLK_CODE` <>
                                                    :where_or_nq_folkCode_value
                                                    <#else>
                                                    ${prefixName} `FOLK_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_folkCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_folkCode_value??)>
                                                            ${prefixName} `FOLK_CODE` like
                                                                :where_or_like_folkCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_folkId??)>
        <#if
        (where_and_eq_folkId_value??)>
    ${prefixName} `FOLK_ID` = :where_and_eq_folkId_value
        <#else>
        ${prefixName} `FOLK_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_folkId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_folkId_value??)>
                ${prefixName} `FOLK_ID` <> :where_and_nq_folkId_value
                    <#else>
                    ${prefixName} `FOLK_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_folkId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_folkId_value??)>
                            ${prefixName} `FOLK_ID` <> :where_and_like_folkId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_folkId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_folkId_value??)>
                                    ${prefixName} `FOLK_ID` = :where_or_eq_folkId
                                        _value
                                        <#else>
                                        ${prefixName} `FOLK_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_folkId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_folkId_value??)>
                                                ${prefixName} `FOLK_ID` <>
                                                    :where_or_nq_folkId_value
                                                    <#else>
                                                    ${prefixName} `FOLK_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_folkId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_folkId_value??)>
                                                            ${prefixName} `FOLK_ID` like
                                                                :where_or_like_folkId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_folkName??)>
        <#if
        (where_and_eq_folkName_value??)>
    ${prefixName} `FOLK_NAME` = :where_and_eq_folkName_value
        <#else>
        ${prefixName} `FOLK_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_folkName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_folkName_value??)>
                ${prefixName} `FOLK_NAME` <> :where_and_nq_folkName_value
                    <#else>
                    ${prefixName} `FOLK_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_folkName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_folkName_value??)>
                            ${prefixName} `FOLK_NAME` <> :where_and_like_folkName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_folkName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_folkName_value??)>
                                    ${prefixName} `FOLK_NAME` = :where_or_eq_folkName
                                        _value
                                        <#else>
                                        ${prefixName} `FOLK_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_folkName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_folkName_value??)>
                                                ${prefixName} `FOLK_NAME` <>
                                                    :where_or_nq_folkName_value
                                                    <#else>
                                                    ${prefixName} `FOLK_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_folkName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_folkName_value??)>
                                                            ${prefixName} `FOLK_NAME` like
                                                                :where_or_like_folkName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_id??)>
        <#if
        (where_and_eq_id_value??)>
    ${prefixName} `ID` = :where_and_eq_id_value
        <#else>
        ${prefixName} `ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_id??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_id_value??)>
                ${prefixName} `ID` <> :where_and_nq_id_value
                    <#else>
                    ${prefixName} `ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_id??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_id_value??)>
                            ${prefixName} `ID` <> :where_and_like_id_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_id??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_id_value??)>
                                    ${prefixName} `ID` = :where_or_eq_id
                                        _value
                                        <#else>
                                        ${prefixName} `ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_id??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_id_value??)>
                                                ${prefixName} `ID` <>
                                                    :where_or_nq_id_value
                                                    <#else>
                                                    ${prefixName} `ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_id??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_id_value??)>
                                                            ${prefixName} `ID` like
                                                                :where_or_like_id_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_intro??)>
        <#if
        (where_and_eq_intro_value??)>
    ${prefixName} `INTRO` = :where_and_eq_intro_value
        <#else>
        ${prefixName} `INTRO` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_intro??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_intro_value??)>
                ${prefixName} `INTRO` <> :where_and_nq_intro_value
                    <#else>
                    ${prefixName} `INTRO` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_intro??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_intro_value??)>
                            ${prefixName} `INTRO` <> :where_and_like_intro_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_intro??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_intro_value??)>
                                    ${prefixName} `INTRO` = :where_or_eq_intro
                                        _value
                                        <#else>
                                        ${prefixName} `INTRO` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_intro??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_intro_value??)>
                                                ${prefixName} `INTRO` <>
                                                    :where_or_nq_intro_value
                                                    <#else>
                                                    ${prefixName} `INTRO` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_intro??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_intro_value??)>
                                                            ${prefixName} `INTRO` like
                                                                :where_or_like_intro_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_lastModifiedByCode??)>
        <#if
        (where_and_eq_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = :where_and_eq_lastModifiedByCode_value
        <#else>
        ${prefixName} `LAST_MODIFIED_BY_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_lastModifiedByCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_lastModifiedByCode_value??)>
                ${prefixName} `LAST_MODIFIED_BY_CODE` <> :where_and_nq_lastModifiedByCode_value
                    <#else>
                    ${prefixName} `LAST_MODIFIED_BY_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_lastModifiedByCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_lastModifiedByCode_value??)>
                            ${prefixName} `LAST_MODIFIED_BY_CODE` <> :where_and_like_lastModifiedByCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_lastModifiedByCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_lastModifiedByCode_value??)>
                                    ${prefixName} `LAST_MODIFIED_BY_CODE` = :where_or_eq_lastModifiedByCode
                                        _value
                                        <#else>
                                        ${prefixName} `LAST_MODIFIED_BY_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_lastModifiedByCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_lastModifiedByCode_value??)>
                                                ${prefixName} `LAST_MODIFIED_BY_CODE` <>
                                                    :where_or_nq_lastModifiedByCode_value
                                                    <#else>
                                                    ${prefixName} `LAST_MODIFIED_BY_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_lastModifiedByCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_lastModifiedByCode_value??)>
                                                            ${prefixName} `LAST_MODIFIED_BY_CODE` like
                                                                :where_or_like_lastModifiedByCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_lastModifiedById??)>
        <#if
        (where_and_eq_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` = :where_and_eq_lastModifiedById_value
        <#else>
        ${prefixName} `LAST_MODIFIED_BY_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_lastModifiedById??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_lastModifiedById_value??)>
                ${prefixName} `LAST_MODIFIED_BY_ID` <> :where_and_nq_lastModifiedById_value
                    <#else>
                    ${prefixName} `LAST_MODIFIED_BY_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_lastModifiedById??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_lastModifiedById_value??)>
                            ${prefixName} `LAST_MODIFIED_BY_ID` <> :where_and_like_lastModifiedById_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_lastModifiedById??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_lastModifiedById_value??)>
                                    ${prefixName} `LAST_MODIFIED_BY_ID` = :where_or_eq_lastModifiedById
                                        _value
                                        <#else>
                                        ${prefixName} `LAST_MODIFIED_BY_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_lastModifiedById??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_lastModifiedById_value??)>
                                                ${prefixName} `LAST_MODIFIED_BY_ID` <>
                                                    :where_or_nq_lastModifiedById_value
                                                    <#else>
                                                    ${prefixName} `LAST_MODIFIED_BY_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_lastModifiedById??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_lastModifiedById_value??)>
                                                            ${prefixName} `LAST_MODIFIED_BY_ID` like
                                                                :where_or_like_lastModifiedById_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_lastModifiedByName??)>
        <#if
        (where_and_eq_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = :where_and_eq_lastModifiedByName_value
        <#else>
        ${prefixName} `LAST_MODIFIED_BY_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_lastModifiedByName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_lastModifiedByName_value??)>
                ${prefixName} `LAST_MODIFIED_BY_NAME` <> :where_and_nq_lastModifiedByName_value
                    <#else>
                    ${prefixName} `LAST_MODIFIED_BY_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_lastModifiedByName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_lastModifiedByName_value??)>
                            ${prefixName} `LAST_MODIFIED_BY_NAME` <> :where_and_like_lastModifiedByName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_lastModifiedByName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_lastModifiedByName_value??)>
                                    ${prefixName} `LAST_MODIFIED_BY_NAME` = :where_or_eq_lastModifiedByName
                                        _value
                                        <#else>
                                        ${prefixName} `LAST_MODIFIED_BY_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_lastModifiedByName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_lastModifiedByName_value??)>
                                                ${prefixName} `LAST_MODIFIED_BY_NAME` <>
                                                    :where_or_nq_lastModifiedByName_value
                                                    <#else>
                                                    ${prefixName} `LAST_MODIFIED_BY_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_lastModifiedByName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_lastModifiedByName_value??)>
                                                            ${prefixName} `LAST_MODIFIED_BY_NAME` like
                                                                :where_or_like_lastModifiedByName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_lastModifiedDate??)>
        <#if
        (where_and_eq_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` = :where_and_eq_lastModifiedDate_value
        <#else>
        ${prefixName} `LAST_MODIFIED_DATE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_lastModifiedDate??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_lastModifiedDate_value??)>
                ${prefixName} `LAST_MODIFIED_DATE` <> :where_and_nq_lastModifiedDate_value
                    <#else>
                    ${prefixName} `LAST_MODIFIED_DATE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_lastModifiedDate??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_lastModifiedDate_value??)>
                            ${prefixName} `LAST_MODIFIED_DATE` <> :where_and_like_lastModifiedDate_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_lastModifiedDate??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_lastModifiedDate_value??)>
                                    ${prefixName} `LAST_MODIFIED_DATE` = :where_or_eq_lastModifiedDate
                                        _value
                                        <#else>
                                        ${prefixName} `LAST_MODIFIED_DATE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_lastModifiedDate??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_lastModifiedDate_value??)>
                                                ${prefixName} `LAST_MODIFIED_DATE` <>
                                                    :where_or_nq_lastModifiedDate_value
                                                    <#else>
                                                    ${prefixName} `LAST_MODIFIED_DATE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_lastModifiedDate??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_lastModifiedDate_value??)>
                                                            ${prefixName} `LAST_MODIFIED_DATE` like
                                                                :where_or_like_lastModifiedDate_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_locus??)>
        <#if
        (where_and_eq_locus_value??)>
    ${prefixName} `LOCUS` = :where_and_eq_locus_value
        <#else>
        ${prefixName} `LOCUS` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_locus??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_locus_value??)>
                ${prefixName} `LOCUS` <> :where_and_nq_locus_value
                    <#else>
                    ${prefixName} `LOCUS` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_locus??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_locus_value??)>
                            ${prefixName} `LOCUS` <> :where_and_like_locus_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_locus??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_locus_value??)>
                                    ${prefixName} `LOCUS` = :where_or_eq_locus
                                        _value
                                        <#else>
                                        ${prefixName} `LOCUS` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_locus??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_locus_value??)>
                                                ${prefixName} `LOCUS` <>
                                                    :where_or_nq_locus_value
                                                    <#else>
                                                    ${prefixName} `LOCUS` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_locus??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_locus_value??)>
                                                            ${prefixName} `LOCUS` like
                                                                :where_or_like_locus_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_marryStateCode??)>
        <#if
        (where_and_eq_marryStateCode_value??)>
    ${prefixName} `MARRY_STATE_CODE` = :where_and_eq_marryStateCode_value
        <#else>
        ${prefixName} `MARRY_STATE_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_marryStateCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_marryStateCode_value??)>
                ${prefixName} `MARRY_STATE_CODE` <> :where_and_nq_marryStateCode_value
                    <#else>
                    ${prefixName} `MARRY_STATE_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_marryStateCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_marryStateCode_value??)>
                            ${prefixName} `MARRY_STATE_CODE` <> :where_and_like_marryStateCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_marryStateCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_marryStateCode_value??)>
                                    ${prefixName} `MARRY_STATE_CODE` = :where_or_eq_marryStateCode
                                        _value
                                        <#else>
                                        ${prefixName} `MARRY_STATE_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_marryStateCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_marryStateCode_value??)>
                                                ${prefixName} `MARRY_STATE_CODE` <>
                                                    :where_or_nq_marryStateCode_value
                                                    <#else>
                                                    ${prefixName} `MARRY_STATE_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_marryStateCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_marryStateCode_value??)>
                                                            ${prefixName} `MARRY_STATE_CODE` like
                                                                :where_or_like_marryStateCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_marryStateId??)>
        <#if
        (where_and_eq_marryStateId_value??)>
    ${prefixName} `MARRY_STATE_ID` = :where_and_eq_marryStateId_value
        <#else>
        ${prefixName} `MARRY_STATE_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_marryStateId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_marryStateId_value??)>
                ${prefixName} `MARRY_STATE_ID` <> :where_and_nq_marryStateId_value
                    <#else>
                    ${prefixName} `MARRY_STATE_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_marryStateId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_marryStateId_value??)>
                            ${prefixName} `MARRY_STATE_ID` <> :where_and_like_marryStateId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_marryStateId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_marryStateId_value??)>
                                    ${prefixName} `MARRY_STATE_ID` = :where_or_eq_marryStateId
                                        _value
                                        <#else>
                                        ${prefixName} `MARRY_STATE_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_marryStateId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_marryStateId_value??)>
                                                ${prefixName} `MARRY_STATE_ID` <>
                                                    :where_or_nq_marryStateId_value
                                                    <#else>
                                                    ${prefixName} `MARRY_STATE_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_marryStateId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_marryStateId_value??)>
                                                            ${prefixName} `MARRY_STATE_ID` like
                                                                :where_or_like_marryStateId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_marryStateName??)>
        <#if
        (where_and_eq_marryStateName_value??)>
    ${prefixName} `MARRY_STATE_NAME` = :where_and_eq_marryStateName_value
        <#else>
        ${prefixName} `MARRY_STATE_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_marryStateName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_marryStateName_value??)>
                ${prefixName} `MARRY_STATE_NAME` <> :where_and_nq_marryStateName_value
                    <#else>
                    ${prefixName} `MARRY_STATE_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_marryStateName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_marryStateName_value??)>
                            ${prefixName} `MARRY_STATE_NAME` <> :where_and_like_marryStateName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_marryStateName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_marryStateName_value??)>
                                    ${prefixName} `MARRY_STATE_NAME` = :where_or_eq_marryStateName
                                        _value
                                        <#else>
                                        ${prefixName} `MARRY_STATE_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_marryStateName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_marryStateName_value??)>
                                                ${prefixName} `MARRY_STATE_NAME` <>
                                                    :where_or_nq_marryStateName_value
                                                    <#else>
                                                    ${prefixName} `MARRY_STATE_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_marryStateName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_marryStateName_value??)>
                                                            ${prefixName} `MARRY_STATE_NAME` like
                                                                :where_or_like_marryStateName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_mobilePhone??)>
        <#if
        (where_and_eq_mobilePhone_value??)>
    ${prefixName} `MOBILE_PHONE` = :where_and_eq_mobilePhone_value
        <#else>
        ${prefixName} `MOBILE_PHONE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_mobilePhone??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_mobilePhone_value??)>
                ${prefixName} `MOBILE_PHONE` <> :where_and_nq_mobilePhone_value
                    <#else>
                    ${prefixName} `MOBILE_PHONE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_mobilePhone??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_mobilePhone_value??)>
                            ${prefixName} `MOBILE_PHONE` <> :where_and_like_mobilePhone_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_mobilePhone??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_mobilePhone_value??)>
                                    ${prefixName} `MOBILE_PHONE` = :where_or_eq_mobilePhone
                                        _value
                                        <#else>
                                        ${prefixName} `MOBILE_PHONE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_mobilePhone??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_mobilePhone_value??)>
                                                ${prefixName} `MOBILE_PHONE` <>
                                                    :where_or_nq_mobilePhone_value
                                                    <#else>
                                                    ${prefixName} `MOBILE_PHONE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_mobilePhone??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_mobilePhone_value??)>
                                                            ${prefixName} `MOBILE_PHONE` like
                                                                :where_or_like_mobilePhone_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_name??)>
        <#if
        (where_and_eq_name_value??)>
    ${prefixName} `NAME` = :where_and_eq_name_value
        <#else>
        ${prefixName} `NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_name??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_name_value??)>
                ${prefixName} `NAME` <> :where_and_nq_name_value
                    <#else>
                    ${prefixName} `NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_name??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_name_value??)>
                            ${prefixName} `NAME` <> :where_and_like_name_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_name??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_name_value??)>
                                    ${prefixName} `NAME` = :where_or_eq_name
                                        _value
                                        <#else>
                                        ${prefixName} `NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_name??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_name_value??)>
                                                ${prefixName} `NAME` <>
                                                    :where_or_nq_name_value
                                                    <#else>
                                                    ${prefixName} `NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_name??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_name_value??)>
                                                            ${prefixName} `NAME` like
                                                                :where_or_like_name_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_no??)>
        <#if
        (where_and_eq_no_value??)>
    ${prefixName} `NO` = :where_and_eq_no_value
        <#else>
        ${prefixName} `NO` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_no??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_no_value??)>
                ${prefixName} `NO` <> :where_and_nq_no_value
                    <#else>
                    ${prefixName} `NO` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_no??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_no_value??)>
                            ${prefixName} `NO` <> :where_and_like_no_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_no??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_no_value??)>
                                    ${prefixName} `NO` = :where_or_eq_no
                                        _value
                                        <#else>
                                        ${prefixName} `NO` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_no??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_no_value??)>
                                                ${prefixName} `NO` <>
                                                    :where_or_nq_no_value
                                                    <#else>
                                                    ${prefixName} `NO` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_no??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_no_value??)>
                                                            ${prefixName} `NO` like
                                                                :where_or_like_no_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_orgCode??)>
        <#if
        (where_and_eq_orgCode_value??)>
    ${prefixName} `ORG_CODE` = :where_and_eq_orgCode_value
        <#else>
        ${prefixName} `ORG_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_orgCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_orgCode_value??)>
                ${prefixName} `ORG_CODE` <> :where_and_nq_orgCode_value
                    <#else>
                    ${prefixName} `ORG_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_orgCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_orgCode_value??)>
                            ${prefixName} `ORG_CODE` <> :where_and_like_orgCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_orgCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_orgCode_value??)>
                                    ${prefixName} `ORG_CODE` = :where_or_eq_orgCode
                                        _value
                                        <#else>
                                        ${prefixName} `ORG_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_orgCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_orgCode_value??)>
                                                ${prefixName} `ORG_CODE` <>
                                                    :where_or_nq_orgCode_value
                                                    <#else>
                                                    ${prefixName} `ORG_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_orgCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_orgCode_value??)>
                                                            ${prefixName} `ORG_CODE` like
                                                                :where_or_like_orgCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_orgId??)>
        <#if
        (where_and_eq_orgId_value??)>
    ${prefixName} `ORG_ID` = :where_and_eq_orgId_value
        <#else>
        ${prefixName} `ORG_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_orgId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_orgId_value??)>
                ${prefixName} `ORG_ID` <> :where_and_nq_orgId_value
                    <#else>
                    ${prefixName} `ORG_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_orgId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_orgId_value??)>
                            ${prefixName} `ORG_ID` <> :where_and_like_orgId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_orgId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_orgId_value??)>
                                    ${prefixName} `ORG_ID` = :where_or_eq_orgId
                                        _value
                                        <#else>
                                        ${prefixName} `ORG_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_orgId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_orgId_value??)>
                                                ${prefixName} `ORG_ID` <>
                                                    :where_or_nq_orgId_value
                                                    <#else>
                                                    ${prefixName} `ORG_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_orgId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_orgId_value??)>
                                                            ${prefixName} `ORG_ID` like
                                                                :where_or_like_orgId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_orgName??)>
        <#if
        (where_and_eq_orgName_value??)>
    ${prefixName} `ORG_NAME` = :where_and_eq_orgName_value
        <#else>
        ${prefixName} `ORG_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_orgName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_orgName_value??)>
                ${prefixName} `ORG_NAME` <> :where_and_nq_orgName_value
                    <#else>
                    ${prefixName} `ORG_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_orgName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_orgName_value??)>
                            ${prefixName} `ORG_NAME` <> :where_and_like_orgName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_orgName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_orgName_value??)>
                                    ${prefixName} `ORG_NAME` = :where_or_eq_orgName
                                        _value
                                        <#else>
                                        ${prefixName} `ORG_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_orgName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_orgName_value??)>
                                                ${prefixName} `ORG_NAME` <>
                                                    :where_or_nq_orgName_value
                                                    <#else>
                                                    ${prefixName} `ORG_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_orgName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_orgName_value??)>
                                                            ${prefixName} `ORG_NAME` like
                                                                :where_or_like_orgName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_password??)>
        <#if
        (where_and_eq_password_value??)>
    ${prefixName} `PASSWORD` = :where_and_eq_password_value
        <#else>
        ${prefixName} `PASSWORD` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_password??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_password_value??)>
                ${prefixName} `PASSWORD` <> :where_and_nq_password_value
                    <#else>
                    ${prefixName} `PASSWORD` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_password??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_password_value??)>
                            ${prefixName} `PASSWORD` <> :where_and_like_password_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_password??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_password_value??)>
                                    ${prefixName} `PASSWORD` = :where_or_eq_password
                                        _value
                                        <#else>
                                        ${prefixName} `PASSWORD` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_password??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_password_value??)>
                                                ${prefixName} `PASSWORD` <>
                                                    :where_or_nq_password_value
                                                    <#else>
                                                    ${prefixName} `PASSWORD` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_password??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_password_value??)>
                                                            ${prefixName} `PASSWORD` like
                                                                :where_or_like_password_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_professionCode??)>
        <#if
        (where_and_eq_professionCode_value??)>
    ${prefixName} `PROFESSION_CODE` = :where_and_eq_professionCode_value
        <#else>
        ${prefixName} `PROFESSION_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_professionCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_professionCode_value??)>
                ${prefixName} `PROFESSION_CODE` <> :where_and_nq_professionCode_value
                    <#else>
                    ${prefixName} `PROFESSION_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_professionCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_professionCode_value??)>
                            ${prefixName} `PROFESSION_CODE` <> :where_and_like_professionCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_professionCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_professionCode_value??)>
                                    ${prefixName} `PROFESSION_CODE` = :where_or_eq_professionCode
                                        _value
                                        <#else>
                                        ${prefixName} `PROFESSION_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_professionCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_professionCode_value??)>
                                                ${prefixName} `PROFESSION_CODE` <>
                                                    :where_or_nq_professionCode_value
                                                    <#else>
                                                    ${prefixName} `PROFESSION_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_professionCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_professionCode_value??)>
                                                            ${prefixName} `PROFESSION_CODE` like
                                                                :where_or_like_professionCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_professionId??)>
        <#if
        (where_and_eq_professionId_value??)>
    ${prefixName} `PROFESSION_ID` = :where_and_eq_professionId_value
        <#else>
        ${prefixName} `PROFESSION_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_professionId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_professionId_value??)>
                ${prefixName} `PROFESSION_ID` <> :where_and_nq_professionId_value
                    <#else>
                    ${prefixName} `PROFESSION_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_professionId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_professionId_value??)>
                            ${prefixName} `PROFESSION_ID` <> :where_and_like_professionId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_professionId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_professionId_value??)>
                                    ${prefixName} `PROFESSION_ID` = :where_or_eq_professionId
                                        _value
                                        <#else>
                                        ${prefixName} `PROFESSION_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_professionId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_professionId_value??)>
                                                ${prefixName} `PROFESSION_ID` <>
                                                    :where_or_nq_professionId_value
                                                    <#else>
                                                    ${prefixName} `PROFESSION_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_professionId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_professionId_value??)>
                                                            ${prefixName} `PROFESSION_ID` like
                                                                :where_or_like_professionId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_professionName??)>
        <#if
        (where_and_eq_professionName_value??)>
    ${prefixName} `PROFESSION_NAME` = :where_and_eq_professionName_value
        <#else>
        ${prefixName} `PROFESSION_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_professionName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_professionName_value??)>
                ${prefixName} `PROFESSION_NAME` <> :where_and_nq_professionName_value
                    <#else>
                    ${prefixName} `PROFESSION_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_professionName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_professionName_value??)>
                            ${prefixName} `PROFESSION_NAME` <> :where_and_like_professionName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_professionName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_professionName_value??)>
                                    ${prefixName} `PROFESSION_NAME` = :where_or_eq_professionName
                                        _value
                                        <#else>
                                        ${prefixName} `PROFESSION_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_professionName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_professionName_value??)>
                                                ${prefixName} `PROFESSION_NAME` <>
                                                    :where_or_nq_professionName_value
                                                    <#else>
                                                    ${prefixName} `PROFESSION_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_professionName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_professionName_value??)>
                                                            ${prefixName} `PROFESSION_NAME` like
                                                                :where_or_like_professionName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_provinceCode??)>
        <#if
        (where_and_eq_provinceCode_value??)>
    ${prefixName} `PROVINCE_CODE` = :where_and_eq_provinceCode_value
        <#else>
        ${prefixName} `PROVINCE_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_provinceCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_provinceCode_value??)>
                ${prefixName} `PROVINCE_CODE` <> :where_and_nq_provinceCode_value
                    <#else>
                    ${prefixName} `PROVINCE_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_provinceCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_provinceCode_value??)>
                            ${prefixName} `PROVINCE_CODE` <> :where_and_like_provinceCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_provinceCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_provinceCode_value??)>
                                    ${prefixName} `PROVINCE_CODE` = :where_or_eq_provinceCode
                                        _value
                                        <#else>
                                        ${prefixName} `PROVINCE_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_provinceCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_provinceCode_value??)>
                                                ${prefixName} `PROVINCE_CODE` <>
                                                    :where_or_nq_provinceCode_value
                                                    <#else>
                                                    ${prefixName} `PROVINCE_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_provinceCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_provinceCode_value??)>
                                                            ${prefixName} `PROVINCE_CODE` like
                                                                :where_or_like_provinceCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_provinceId??)>
        <#if
        (where_and_eq_provinceId_value??)>
    ${prefixName} `PROVINCE_ID` = :where_and_eq_provinceId_value
        <#else>
        ${prefixName} `PROVINCE_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_provinceId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_provinceId_value??)>
                ${prefixName} `PROVINCE_ID` <> :where_and_nq_provinceId_value
                    <#else>
                    ${prefixName} `PROVINCE_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_provinceId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_provinceId_value??)>
                            ${prefixName} `PROVINCE_ID` <> :where_and_like_provinceId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_provinceId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_provinceId_value??)>
                                    ${prefixName} `PROVINCE_ID` = :where_or_eq_provinceId
                                        _value
                                        <#else>
                                        ${prefixName} `PROVINCE_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_provinceId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_provinceId_value??)>
                                                ${prefixName} `PROVINCE_ID` <>
                                                    :where_or_nq_provinceId_value
                                                    <#else>
                                                    ${prefixName} `PROVINCE_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_provinceId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_provinceId_value??)>
                                                            ${prefixName} `PROVINCE_ID` like
                                                                :where_or_like_provinceId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_provinceName??)>
        <#if
        (where_and_eq_provinceName_value??)>
    ${prefixName} `PROVINCE_NAME` = :where_and_eq_provinceName_value
        <#else>
        ${prefixName} `PROVINCE_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_provinceName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_provinceName_value??)>
                ${prefixName} `PROVINCE_NAME` <> :where_and_nq_provinceName_value
                    <#else>
                    ${prefixName} `PROVINCE_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_provinceName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_provinceName_value??)>
                            ${prefixName} `PROVINCE_NAME` <> :where_and_like_provinceName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_provinceName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_provinceName_value??)>
                                    ${prefixName} `PROVINCE_NAME` = :where_or_eq_provinceName
                                        _value
                                        <#else>
                                        ${prefixName} `PROVINCE_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_provinceName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_provinceName_value??)>
                                                ${prefixName} `PROVINCE_NAME` <>
                                                    :where_or_nq_provinceName_value
                                                    <#else>
                                                    ${prefixName} `PROVINCE_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_provinceName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_provinceName_value??)>
                                                            ${prefixName} `PROVINCE_NAME` like
                                                                :where_or_like_provinceName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_sexCode??)>
        <#if
        (where_and_eq_sexCode_value??)>
    ${prefixName} `SEX_CODE` = :where_and_eq_sexCode_value
        <#else>
        ${prefixName} `SEX_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_sexCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_sexCode_value??)>
                ${prefixName} `SEX_CODE` <> :where_and_nq_sexCode_value
                    <#else>
                    ${prefixName} `SEX_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_sexCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_sexCode_value??)>
                            ${prefixName} `SEX_CODE` <> :where_and_like_sexCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_sexCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_sexCode_value??)>
                                    ${prefixName} `SEX_CODE` = :where_or_eq_sexCode
                                        _value
                                        <#else>
                                        ${prefixName} `SEX_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_sexCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_sexCode_value??)>
                                                ${prefixName} `SEX_CODE` <>
                                                    :where_or_nq_sexCode_value
                                                    <#else>
                                                    ${prefixName} `SEX_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_sexCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_sexCode_value??)>
                                                            ${prefixName} `SEX_CODE` like
                                                                :where_or_like_sexCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_sexId??)>
        <#if
        (where_and_eq_sexId_value??)>
    ${prefixName} `SEX_ID` = :where_and_eq_sexId_value
        <#else>
        ${prefixName} `SEX_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_sexId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_sexId_value??)>
                ${prefixName} `SEX_ID` <> :where_and_nq_sexId_value
                    <#else>
                    ${prefixName} `SEX_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_sexId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_sexId_value??)>
                            ${prefixName} `SEX_ID` <> :where_and_like_sexId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_sexId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_sexId_value??)>
                                    ${prefixName} `SEX_ID` = :where_or_eq_sexId
                                        _value
                                        <#else>
                                        ${prefixName} `SEX_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_sexId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_sexId_value??)>
                                                ${prefixName} `SEX_ID` <>
                                                    :where_or_nq_sexId_value
                                                    <#else>
                                                    ${prefixName} `SEX_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_sexId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_sexId_value??)>
                                                            ${prefixName} `SEX_ID` like
                                                                :where_or_like_sexId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_sexName??)>
        <#if
        (where_and_eq_sexName_value??)>
    ${prefixName} `SEX_NAME` = :where_and_eq_sexName_value
        <#else>
        ${prefixName} `SEX_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_sexName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_sexName_value??)>
                ${prefixName} `SEX_NAME` <> :where_and_nq_sexName_value
                    <#else>
                    ${prefixName} `SEX_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_sexName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_sexName_value??)>
                            ${prefixName} `SEX_NAME` <> :where_and_like_sexName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_sexName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_sexName_value??)>
                                    ${prefixName} `SEX_NAME` = :where_or_eq_sexName
                                        _value
                                        <#else>
                                        ${prefixName} `SEX_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_sexName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_sexName_value??)>
                                                ${prefixName} `SEX_NAME` <>
                                                    :where_or_nq_sexName_value
                                                    <#else>
                                                    ${prefixName} `SEX_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_sexName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_sexName_value??)>
                                                            ${prefixName} `SEX_NAME` like
                                                                :where_or_like_sexName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_stateCode??)>
        <#if
        (where_and_eq_stateCode_value??)>
    ${prefixName} `STATE_CODE` = :where_and_eq_stateCode_value
        <#else>
        ${prefixName} `STATE_CODE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_stateCode??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_stateCode_value??)>
                ${prefixName} `STATE_CODE` <> :where_and_nq_stateCode_value
                    <#else>
                    ${prefixName} `STATE_CODE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_stateCode??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_stateCode_value??)>
                            ${prefixName} `STATE_CODE` <> :where_and_like_stateCode_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_stateCode??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_stateCode_value??)>
                                    ${prefixName} `STATE_CODE` = :where_or_eq_stateCode
                                        _value
                                        <#else>
                                        ${prefixName} `STATE_CODE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_stateCode??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_stateCode_value??)>
                                                ${prefixName} `STATE_CODE` <>
                                                    :where_or_nq_stateCode_value
                                                    <#else>
                                                    ${prefixName} `STATE_CODE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_stateCode??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_stateCode_value??)>
                                                            ${prefixName} `STATE_CODE` like
                                                                :where_or_like_stateCode_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_stateId??)>
        <#if
        (where_and_eq_stateId_value??)>
    ${prefixName} `STATE_ID` = :where_and_eq_stateId_value
        <#else>
        ${prefixName} `STATE_ID` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_stateId??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_stateId_value??)>
                ${prefixName} `STATE_ID` <> :where_and_nq_stateId_value
                    <#else>
                    ${prefixName} `STATE_ID` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_stateId??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_stateId_value??)>
                            ${prefixName} `STATE_ID` <> :where_and_like_stateId_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_stateId??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_stateId_value??)>
                                    ${prefixName} `STATE_ID` = :where_or_eq_stateId
                                        _value
                                        <#else>
                                        ${prefixName} `STATE_ID` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_stateId??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_stateId_value??)>
                                                ${prefixName} `STATE_ID` <>
                                                    :where_or_nq_stateId_value
                                                    <#else>
                                                    ${prefixName} `STATE_ID` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_stateId??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_stateId_value??)>
                                                            ${prefixName} `STATE_ID` like
                                                                :where_or_like_stateId_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_stateName??)>
        <#if
        (where_and_eq_stateName_value??)>
    ${prefixName} `STATE_NAME` = :where_and_eq_stateName_value
        <#else>
        ${prefixName} `STATE_NAME` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_stateName??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_stateName_value??)>
                ${prefixName} `STATE_NAME` <> :where_and_nq_stateName_value
                    <#else>
                    ${prefixName} `STATE_NAME` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_stateName??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_stateName_value??)>
                            ${prefixName} `STATE_NAME` <> :where_and_like_stateName_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_stateName??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_stateName_value??)>
                                    ${prefixName} `STATE_NAME` = :where_or_eq_stateName
                                        _value
                                        <#else>
                                        ${prefixName} `STATE_NAME` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_stateName??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_stateName_value??)>
                                                ${prefixName} `STATE_NAME` <>
                                                    :where_or_nq_stateName_value
                                                    <#else>
                                                    ${prefixName} `STATE_NAME` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_stateName??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_stateName_value??)>
                                                            ${prefixName} `STATE_NAME` like
                                                                :where_or_like_stateName_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_telephone??)>
        <#if
        (where_and_eq_telephone_value??)>
    ${prefixName} `TELEPHONE` = :where_and_eq_telephone_value
        <#else>
        ${prefixName} `TELEPHONE` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_telephone??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_telephone_value??)>
                ${prefixName} `TELEPHONE` <> :where_and_nq_telephone_value
                    <#else>
                    ${prefixName} `TELEPHONE` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_telephone??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_telephone_value??)>
                            ${prefixName} `TELEPHONE` <> :where_and_like_telephone_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_telephone??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_telephone_value??)>
                                    ${prefixName} `TELEPHONE` = :where_or_eq_telephone
                                        _value
                                        <#else>
                                        ${prefixName} `TELEPHONE` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_telephone??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_telephone_value??)>
                                                ${prefixName} `TELEPHONE` <>
                                                    :where_or_nq_telephone_value
                                                    <#else>
                                                    ${prefixName} `TELEPHONE` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_telephone??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_telephone_value??)>
                                                            ${prefixName} `TELEPHONE` like
                                                                :where_or_like_telephone_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
    <#if
    (prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
        </#if>
        <#if
        (where_and_eq_versionInt??)>
        <#if
        (where_and_eq_versionInt_value??)>
    ${prefixName} `VERSION_INT` = :where_and_eq_versionInt_value
        <#else>
        ${prefixName} `VERSION_INT` IS NULL
            </#if>
            <#assign prefixName=''>
                </#if>
                <#if
                (where_and_nq_versionInt??)>
                <#if
                (prefixName!) != 'WHERE'>
                <#assign prefixName='AND'>
                    </#if>
                    <#if
                    (where_and_nq_versionInt_value??)>
                ${prefixName} `VERSION_INT` <> :where_and_nq_versionInt_value
                    <#else>
                    ${prefixName} `VERSION_INT` IS NOT NULL
                        </#if>
                        <#assign prefixName=''>
                            </#if>
                            <#if
                            (where_and_like_versionInt??)>
                            <#if
                            (prefixName!) != 'WHERE'>
                            <#assign prefixName='AND'>
                                </#if>
                                <#if
                                (where_and_like_versionInt_value??)>
                            ${prefixName} `VERSION_INT` <> :where_and_like_versionInt_value
                                </#if>
                                <#assign prefixName=''>
                                    </#if>
                                    <#if
                                    (where_or_eq_versionInt??)>
                                    <#if
                                    (prefixName!) != 'WHERE'>
                                    <#assign prefixName='OR'>
                                        </#if>
                                        <#if
                                        (where_or_eq_versionInt_value??)>
                                    ${prefixName} `VERSION_INT` = :where_or_eq_versionInt
                                        _value
                                        <#else>
                                        ${prefixName} `VERSION_INT` IS NULL
                                            </#if>
                                            <#assign prefixName=''>
                                                </#if>
                                                <#if
                                                (where_or_nq_versionInt??)>
                                                <#if
                                                (prefixName!) != 'WHERE'>
                                                <#assign prefixName='OR'>
                                                    </#if>
                                                    <#if
                                                    (where_or_nq_versionInt_value??)>
                                                ${prefixName} `VERSION_INT` <>
                                                    :where_or_nq_versionInt_value
                                                    <#else>
                                                    ${prefixName} `VERSION_INT` IS NOT NULL
                                                        </#if>
                                                        <#assign prefixName=''>
                                                            </#if>
                                                            <#if
                                                            (where_or_like_versionInt??)>
                                                            <#if
                                                            (prefixName!) != 'WHERE'>
                                                            <#assign prefixName='OR'>
                                                                </#if>
                                                                <#if
                                                                (where_or_like_versionInt_value??)>
                                                            ${prefixName} `VERSION_INT` like
                                                                :where_or_like_versionInt_value
                                                                </#if>
                                                                <#assign prefixName=''>
                                                                    </#if>
