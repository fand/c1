SELECT
<#assign prefixName=' '>
    <#if
    (select_address??)>
${prefixName} `ADDRESS`
<#assign prefixName=','>
    </#if>
    <#if
    (select_age??)>
${prefixName} `AGE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_beginDate??)>
${prefixName} `BEGIN_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_birthday??)>
${prefixName} `BIRTHDAY`
<#assign prefixName=','>
    </#if>
    <#if
    (select_callName??)>
${prefixName} `CALL_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_cityCode??)>
${prefixName} `CITY_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_cityId??)>
${prefixName} `CITY_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_cityName??)>
${prefixName} `CITY_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_code??)>
${prefixName} `CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_companyName??)>
${prefixName} `COMPANY_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_countryCode??)>
${prefixName} `COUNTRY_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_countryId??)>
${prefixName} `COUNTRY_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_countryName??)>
${prefixName} `COUNTRY_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdByCode??)>
${prefixName} `CREATED_BY_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdById??)>
${prefixName} `CREATED_BY_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdByName??)>
${prefixName} `CREATED_BY_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_createdDate??)>
${prefixName} `CREATED_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_dataStateCode??)>
${prefixName} `DATA_STATE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_dataStateId??)>
${prefixName} `DATA_STATE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_dataStateName??)>
${prefixName} `DATA_STATE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_departmentCode??)>
${prefixName} `DEPARTMENT_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_departmentId??)>
${prefixName} `DEPARTMENT_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_departmentName??)>
${prefixName} `DEPARTMENT_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_diplomaCode??)>
${prefixName} `DIPLOMA_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_diplomaId??)>
${prefixName} `DIPLOMA_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_diplomaName??)>
${prefixName} `DIPLOMA_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_districtCode??)>
${prefixName} `DISTRICT_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_districtId??)>
${prefixName} `DISTRICT_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_districtName??)>
${prefixName} `DISTRICT_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_email??)>
${prefixName} `EMAIL`
<#assign prefixName=','>
    </#if>
    <#if
    (select_endDate??)>
${prefixName} `END_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_flagCode??)>
${prefixName} `FLAG_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_flagId??)>
${prefixName} `FLAG_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_flagName??)>
${prefixName} `FLAG_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_folkCode??)>
${prefixName} `FOLK_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_folkId??)>
${prefixName} `FOLK_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_folkName??)>
${prefixName} `FOLK_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_id??)>
${prefixName} `ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_intro??)>
${prefixName} `INTRO`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_locus??)>
${prefixName} `LOCUS`
<#assign prefixName=','>
    </#if>
    <#if
    (select_marryStateCode??)>
${prefixName} `MARRY_STATE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_marryStateId??)>
${prefixName} `MARRY_STATE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_marryStateName??)>
${prefixName} `MARRY_STATE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_mobilePhone??)>
${prefixName} `MOBILE_PHONE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_name??)>
${prefixName} `NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_no??)>
${prefixName} `NO`
<#assign prefixName=','>
    </#if>
    <#if
    (select_orgCode??)>
${prefixName} `ORG_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_orgId??)>
${prefixName} `ORG_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_orgName??)>
${prefixName} `ORG_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_password??)>
${prefixName} `PASSWORD`
<#assign prefixName=','>
    </#if>
    <#if
    (select_professionCode??)>
${prefixName} `PROFESSION_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_professionId??)>
${prefixName} `PROFESSION_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_professionName??)>
${prefixName} `PROFESSION_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_provinceCode??)>
${prefixName} `PROVINCE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_provinceId??)>
${prefixName} `PROVINCE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_provinceName??)>
${prefixName} `PROVINCE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_sexCode??)>
${prefixName} `SEX_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_sexId??)>
${prefixName} `SEX_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_sexName??)>
${prefixName} `SEX_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_stateCode??)>
${prefixName} `STATE_CODE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_stateId??)>
${prefixName} `STATE_ID`
<#assign prefixName=','>
    </#if>
    <#if
    (select_stateName??)>
${prefixName} `STATE_NAME`
<#assign prefixName=','>
    </#if>
    <#if
    (select_telephone??)>
${prefixName} `TELEPHONE`
<#assign prefixName=','>
    </#if>
    <#if
    (select_versionInt??)>
${prefixName} `VERSION_INT`
<#assign prefixName=','>
    </#if>
    <#if prefixName== ' '>
  `ADDRESS`
, `AGE`
, `BEGIN_DATE`
, `BIRTHDAY`
, `CALL_NAME`
, `CITY_CODE`
, `CITY_ID`
, `CITY_NAME`
, `CODE`
, `COMPANY_NAME`
, `COUNTRY_CODE`
, `COUNTRY_ID`
, `COUNTRY_NAME`
, `CREATED_BY_CODE`
, `CREATED_BY_ID`
, `CREATED_BY_NAME`
, `CREATED_DATE`
, `DATA_STATE_CODE`
, `DATA_STATE_ID`
, `DATA_STATE_NAME`
, `DEPARTMENT_CODE`
, `DEPARTMENT_ID`
, `DEPARTMENT_NAME`
, `DIPLOMA_CODE`
, `DIPLOMA_ID`
, `DIPLOMA_NAME`
, `DISTRICT_CODE`
, `DISTRICT_ID`
, `DISTRICT_NAME`
, `EMAIL`
, `END_DATE`
, `FLAG_CODE`
, `FLAG_ID`
, `FLAG_NAME`
, `FOLK_CODE`
, `FOLK_ID`
, `FOLK_NAME`
, `ID`
, `INTRO`
, `LAST_MODIFIED_BY_CODE`
, `LAST_MODIFIED_BY_ID`
, `LAST_MODIFIED_BY_NAME`
, `LAST_MODIFIED_DATE`
, `LOCUS`
, `MARRY_STATE_CODE`
, `MARRY_STATE_ID`
, `MARRY_STATE_NAME`
, `MOBILE_PHONE`
, `NAME`
, `NO`
, `ORG_CODE`
, `ORG_ID`
, `ORG_NAME`
, `PASSWORD`
, `PROFESSION_CODE`
, `PROFESSION_ID`
, `PROFESSION_NAME`
, `PROVINCE_CODE`
, `PROVINCE_ID`
, `PROVINCE_NAME`
, `SEX_CODE`
, `SEX_ID`
, `SEX_NAME`
, `STATE_CODE`
, `STATE_ID`
, `STATE_NAME`
, `TELEPHONE`
, `VERSION_INT`
    </#if>
    FROM c1_user
    <#include "whereByCode.ftl">
<#assign prefixName='ORDER BY'>
    <#if
    (order_by_address??)>
${prefixName} `ADDRESS` ${order_by_address_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_age??)>
${prefixName} `AGE` ${order_by_age_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_beginDate??)>
${prefixName} `BEGIN_DATE` ${order_by_beginDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_birthday??)>
${prefixName} `BIRTHDAY` ${order_by_birthday_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_callName??)>
${prefixName} `CALL_NAME` ${order_by_callName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_cityCode??)>
${prefixName} `CITY_CODE` ${order_by_cityCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_cityId??)>
${prefixName} `CITY_ID` ${order_by_cityId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_cityName??)>
${prefixName} `CITY_NAME` ${order_by_cityName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_code??)>
${prefixName} `CODE` ${order_by_code_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_companyName??)>
${prefixName} `COMPANY_NAME` ${order_by_companyName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_countryCode??)>
${prefixName} `COUNTRY_CODE` ${order_by_countryCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_countryId??)>
${prefixName} `COUNTRY_ID` ${order_by_countryId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_countryName??)>
${prefixName} `COUNTRY_NAME` ${order_by_countryName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdByCode??)>
${prefixName} `CREATED_BY_CODE` ${order_by_createdByCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdById??)>
${prefixName} `CREATED_BY_ID` ${order_by_createdById_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdByName??)>
${prefixName} `CREATED_BY_NAME` ${order_by_createdByName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_createdDate??)>
${prefixName} `CREATED_DATE` ${order_by_createdDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_dataStateCode??)>
${prefixName} `DATA_STATE_CODE` ${order_by_dataStateCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_dataStateId??)>
${prefixName} `DATA_STATE_ID` ${order_by_dataStateId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_dataStateName??)>
${prefixName} `DATA_STATE_NAME` ${order_by_dataStateName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_departmentCode??)>
${prefixName} `DEPARTMENT_CODE` ${order_by_departmentCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_departmentId??)>
${prefixName} `DEPARTMENT_ID` ${order_by_departmentId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_departmentName??)>
${prefixName} `DEPARTMENT_NAME` ${order_by_departmentName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_diplomaCode??)>
${prefixName} `DIPLOMA_CODE` ${order_by_diplomaCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_diplomaId??)>
${prefixName} `DIPLOMA_ID` ${order_by_diplomaId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_diplomaName??)>
${prefixName} `DIPLOMA_NAME` ${order_by_diplomaName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_districtCode??)>
${prefixName} `DISTRICT_CODE` ${order_by_districtCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_districtId??)>
${prefixName} `DISTRICT_ID` ${order_by_districtId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_districtName??)>
${prefixName} `DISTRICT_NAME` ${order_by_districtName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_email??)>
${prefixName} `EMAIL` ${order_by_email_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_endDate??)>
${prefixName} `END_DATE` ${order_by_endDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_flagCode??)>
${prefixName} `FLAG_CODE` ${order_by_flagCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_flagId??)>
${prefixName} `FLAG_ID` ${order_by_flagId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_flagName??)>
${prefixName} `FLAG_NAME` ${order_by_flagName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_folkCode??)>
${prefixName} `FOLK_CODE` ${order_by_folkCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_folkId??)>
${prefixName} `FOLK_ID` ${order_by_folkId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_folkName??)>
${prefixName} `FOLK_NAME` ${order_by_folkName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_id??)>
${prefixName} `ID` ${order_by_id_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_intro??)>
${prefixName} `INTRO` ${order_by_intro_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE` ${order_by_lastModifiedByCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID` ${order_by_lastModifiedById_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME` ${order_by_lastModifiedByName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE` ${order_by_lastModifiedDate_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_locus??)>
${prefixName} `LOCUS` ${order_by_locus_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_marryStateCode??)>
${prefixName} `MARRY_STATE_CODE` ${order_by_marryStateCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_marryStateId??)>
${prefixName} `MARRY_STATE_ID` ${order_by_marryStateId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_marryStateName??)>
${prefixName} `MARRY_STATE_NAME` ${order_by_marryStateName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_mobilePhone??)>
${prefixName} `MOBILE_PHONE` ${order_by_mobilePhone_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_name??)>
${prefixName} `NAME` ${order_by_name_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_no??)>
${prefixName} `NO` ${order_by_no_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_orgCode??)>
${prefixName} `ORG_CODE` ${order_by_orgCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_orgId??)>
${prefixName} `ORG_ID` ${order_by_orgId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_orgName??)>
${prefixName} `ORG_NAME` ${order_by_orgName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_password??)>
${prefixName} `PASSWORD` ${order_by_password_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_professionCode??)>
${prefixName} `PROFESSION_CODE` ${order_by_professionCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_professionId??)>
${prefixName} `PROFESSION_ID` ${order_by_professionId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_professionName??)>
${prefixName} `PROFESSION_NAME` ${order_by_professionName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_provinceCode??)>
${prefixName} `PROVINCE_CODE` ${order_by_provinceCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_provinceId??)>
${prefixName} `PROVINCE_ID` ${order_by_provinceId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_provinceName??)>
${prefixName} `PROVINCE_NAME` ${order_by_provinceName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_sexCode??)>
${prefixName} `SEX_CODE` ${order_by_sexCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_sexId??)>
${prefixName} `SEX_ID` ${order_by_sexId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_sexName??)>
${prefixName} `SEX_NAME` ${order_by_sexName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_stateCode??)>
${prefixName} `STATE_CODE` ${order_by_stateCode_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_stateId??)>
${prefixName} `STATE_ID` ${order_by_stateId_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_stateName??)>
${prefixName} `STATE_NAME` ${order_by_stateName_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_telephone??)>
${prefixName} `TELEPHONE` ${order_by_telephone_value!}
    <#assign prefixName=','>
        </#if>
    <#if
    (order_by_versionInt??)>
${prefixName} `VERSION_INT` ${order_by_versionInt_value!}
    <#assign prefixName=','>
        </#if>
