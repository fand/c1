UPDATE c1_article
<#assign prefixName='SET'>
    <#if
    (update_beginDate??)>
    <#if
    (update_beginDate_value??)>
${prefixName} `BEGIN_DATE` = :update_beginDate_value
<#else>
    ${prefixName} `BEGIN_DATE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_content??)>
    <#if
    (update_content_value??)>
${prefixName} `CONTENT` = :update_content_value
<#else>
    ${prefixName} `CONTENT` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdByCode??)>
    <#if
    (update_createdByCode_value??)>
${prefixName} `CREATED_BY_CODE` = :update_createdByCode_value
<#else>
    ${prefixName} `CREATED_BY_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdById??)>
    <#if
    (update_createdById_value??)>
${prefixName} `CREATED_BY_ID` = :update_createdById_value
<#else>
    ${prefixName} `CREATED_BY_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdByName??)>
    <#if
    (update_createdByName_value??)>
${prefixName} `CREATED_BY_NAME` = :update_createdByName_value
<#else>
    ${prefixName} `CREATED_BY_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_createdDate??)>
    <#if
    (update_createdDate_value??)>
${prefixName} `CREATED_DATE` = :update_createdDate_value
<#else>
    ${prefixName} `CREATED_DATE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_dataStateCode??)>
    <#if
    (update_dataStateCode_value??)>
${prefixName} `DATA_STATE_CODE` = :update_dataStateCode_value
<#else>
    ${prefixName} `DATA_STATE_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_dataStateId??)>
    <#if
    (update_dataStateId_value??)>
${prefixName} `DATA_STATE_ID` = :update_dataStateId_value
<#else>
    ${prefixName} `DATA_STATE_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_dataStateName??)>
    <#if
    (update_dataStateName_value??)>
${prefixName} `DATA_STATE_NAME` = :update_dataStateName_value
<#else>
    ${prefixName} `DATA_STATE_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_endDate??)>
    <#if
    (update_endDate_value??)>
${prefixName} `END_DATE` = :update_endDate_value
<#else>
    ${prefixName} `END_DATE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_id??)>
    <#if
    (update_id_value??)>
${prefixName} `ID` = :update_id_value
<#else>
    ${prefixName} `ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedByCode??)>
    <#if
    (update_lastModifiedByCode_value??)>
${prefixName} `LAST_MODIFIED_BY_CODE` = :update_lastModifiedByCode_value
<#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedById??)>
    <#if
    (update_lastModifiedById_value??)>
${prefixName} `LAST_MODIFIED_BY_ID` = :update_lastModifiedById_value
<#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedByName??)>
    <#if
    (update_lastModifiedByName_value??)>
${prefixName} `LAST_MODIFIED_BY_NAME` = :update_lastModifiedByName_value
<#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_lastModifiedDate??)>
    <#if
    (update_lastModifiedDate_value??)>
${prefixName} `LAST_MODIFIED_DATE` = :update_lastModifiedDate_value
<#else>
    ${prefixName} `LAST_MODIFIED_DATE` = NOW()
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_orgCode??)>
    <#if
    (update_orgCode_value??)>
${prefixName} `ORG_CODE` = :update_orgCode_value
<#else>
    ${prefixName} `ORG_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_orgId??)>
    <#if
    (update_orgId_value??)>
${prefixName} `ORG_ID` = :update_orgId_value
<#else>
    ${prefixName} `ORG_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_orgName??)>
    <#if
    (update_orgName_value??)>
${prefixName} `ORG_NAME` = :update_orgName_value
<#else>
    ${prefixName} `ORG_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_releaseDate??)>
    <#if
    (update_releaseDate_value??)>
${prefixName} `RELEASE_DATE` = :update_releaseDate_value
<#else>
    ${prefixName} `RELEASE_DATE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_stateCode??)>
    <#if
    (update_stateCode_value??)>
${prefixName} `STATE_CODE` = :update_stateCode_value
<#else>
    ${prefixName} `STATE_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_stateId??)>
    <#if
    (update_stateId_value??)>
${prefixName} `STATE_ID` = :update_stateId_value
<#else>
    ${prefixName} `STATE_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_stateName??)>
    <#if
    (update_stateName_value??)>
${prefixName} `STATE_NAME` = :update_stateName_value
<#else>
    ${prefixName} `STATE_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_title??)>
    <#if
    (update_title_value??)>
${prefixName} `TITLE` = :update_title_value
<#else>
    ${prefixName} `TITLE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_typeCode??)>
    <#if
    (update_typeCode_value??)>
${prefixName} `TYPE_CODE` = :update_typeCode_value
<#else>
    ${prefixName} `TYPE_CODE` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_typeId??)>
    <#if
    (update_typeId_value??)>
${prefixName} `TYPE_ID` = :update_typeId_value
<#else>
    ${prefixName} `TYPE_ID` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_typeName??)>
    <#if
    (update_typeName_value??)>
${prefixName} `TYPE_NAME` = :update_typeName_value
<#else>
    ${prefixName} `TYPE_NAME` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#if
    (update_versionInt??)>
    <#if
    (update_versionInt_value??)>
${prefixName} `VERSION_INT` = :update_versionInt_value
<#else>
    ${prefixName} `VERSION_INT` = NULL
    </#if>
<#assign prefixName=','>
    </#if>
    <#include
    "whereByCode.ftl">