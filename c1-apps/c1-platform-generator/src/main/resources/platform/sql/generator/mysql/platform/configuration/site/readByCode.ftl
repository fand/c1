SELECT
<#switch (object_id!)>
<#case "beginDate">
BEGIN_DATE AS beginDate
<#break>
<#case "code">
CODE AS code
<#break>
<#case "createdByCode">
CREATED_BY_CODE AS createdByCode
<#break>
<#case "createdById">
CREATED_BY_ID AS createdById
<#break>
<#case "createdByName">
CREATED_BY_NAME AS createdByName
<#break>
<#case "createdDate">
CREATED_DATE AS createdDate
<#break>
<#case "dataStateCode">
DATA_STATE_CODE AS dataStateCode
<#break>
<#case "dataStateId">
DATA_STATE_ID AS dataStateId
<#break>
<#case "dataStateName">
DATA_STATE_NAME AS dataStateName
<#break>
<#case "endDate">
END_DATE AS endDate
<#break>
<#case "id">
ID AS id
<#break>
<#case "lastModifiedByCode">
LAST_MODIFIED_BY_CODE AS lastModifiedByCode
<#break>
<#case "lastModifiedById">
LAST_MODIFIED_BY_ID AS lastModifiedById
<#break>
<#case "lastModifiedByName">
LAST_MODIFIED_BY_NAME AS lastModifiedByName
<#break>
<#case "lastModifiedDate">
LAST_MODIFIED_DATE AS lastModifiedDate
<#break>
<#case "name">
NAME AS name
<#break>
<#case "projectCode">
PROJECT_CODE AS projectCode
<#break>
<#case "projectId">
PROJECT_ID AS projectId
<#break>
<#case "projectName">
PROJECT_NAME AS projectName
<#break>
<#case "stateCode">
STATE_CODE AS stateCode
<#break>
<#case "stateId">
STATE_ID AS stateId
<#break>
<#case "stateName">
STATE_NAME AS stateName
<#break>
<#case "styleCode">
STYLE_CODE AS styleCode
<#break>
<#case "styleId">
STYLE_ID AS styleId
<#break>
<#case "styleName">
STYLE_NAME AS styleName
<#break>
<#case "versionInt">
VERSION_INT AS versionInt
<#break>
    <#default>
        COUNT(1) AS COUNT_
        </#switch>
        FROM c1_site
        <#include
        "whereByCode.ftl">