package com.cardone.platform.usercenter.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.usercenter.po.UsDepartmentTranslate;

/**
* 用户部门调动
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsDepartmentTranslateDto extends UsDepartmentTranslate {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 714160157380253425L;
}