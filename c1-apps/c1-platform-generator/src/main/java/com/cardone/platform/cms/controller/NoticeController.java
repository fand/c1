package com.cardone.platform.cms.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.cms.dto.NoticeDto;
import com.cardone.platform.cms.service.NoticeService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 公告
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class NoticeController {
/**
* 公告：删除
*
* @param deleteNotice
*            公告对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/cms/notice/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final NoticeDto deleteNotice) {
    return () -> {
    ContextHolder.getBean(NoticeService.class).deleteByIds(deleteNotice);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 公告：编辑
    *
    * @param findNotice
    *            公告对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/cms/notice/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final NoticeDto findNotice, final Model model) {
    return () -> {
    final NoticeDto notice = ContextHolder.getBean(Notice
    Service.class).findById(NoticeDto.class, findNotice);

    return ReturnDataUtils.newMap("notice", notice);
    };
    }

    /**
    * 公告：分页查询
    *
    * @param paginationNotice
    *            公告对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/cms/notice/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final NoticeDto paginationNotice) {
    return () -> {
    final PaginationDto
<NoticeDto> noticePagination = ContextHolder.getBean(Notice
    Service.class).paginationByLikeCode(NoticeDto.class, paginationNotice);

    return ReturnDataUtils.newMap("noticePagination", noticePagination);
    };
    }

    /**
    * 公告：分页查询
    *
    * @param paginationNotice
    *            公告对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/cms/notice/pagination.html" })
    public void paginationHtml(final NoticeDto paginationNotice, final Model model) {
    final PaginationDto
<NoticeDto> noticePagination = ContextHolder.getBean(Notice
    Service.class).paginationByLikeCode(NoticeDto.class, paginationNotice);

    model.addAttribute("noticePagination", noticePagination);
    }

    /**
    * 公告：分页查询
    *
    * @param findNotice
    *            公告对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/cms/notice/edit.html" })
    public void editHtml(final NoticeDto findNotice, final Model model) {
    final NoticeDto notice = ContextHolder.getBean(Notice
    Service.class).findById(NoticeDto.class, findNotice);

    model.addAttribute("notice", notice);
    }

    /**
    * 公告：查询
    *
    * @param findListNotice
    *            公告对象
    * @return 公告对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/cms/notice/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final NoticeDto findListNotice) {
    return () -> {
    final List<NoticeDto> noticeList = ContextHolder.getBean(Notice
    Service.class).findListByLikeCode(NoticeDto.class, findListNotice);

    return ReturnDataUtils.newMap("notice", noticeList);
    };
    }

    /**
    * 公告：保存
    *
    * @param saveNotice
    *            公告对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/cms/notice/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final NoticeDto saveNotice) {
    return () -> {
    ContextHolder.getBean(NoticeService.class).saveByIdOrCode(NoticeDto.class, saveNotice);

    return ReturnDataUtils.newMap();
    };
    }
    }
