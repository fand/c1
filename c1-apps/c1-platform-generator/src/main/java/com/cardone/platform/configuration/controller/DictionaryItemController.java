package com.cardone.platform.configuration.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.configuration.dto.DictionaryItemDto;
import com.cardone.platform.configuration.service.DictionaryItemService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 字典项
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class DictionaryItemController {
/**
* 字典项：删除
*
* @param deleteDictionaryItem
*            字典项对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/configuration/dictionaryItem/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final DictionaryItemDto deleteDictionaryItem) {
    return () -> {
    ContextHolder.getBean(DictionaryItemService.class).deleteByIds(deleteDictionaryItem);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 字典项：编辑
    *
    * @param findDictionaryItem
    *            字典项对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryItem/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final DictionaryItemDto findDictionaryItem, final Model model) {
    return () -> {
    final DictionaryItemDto dictionaryItem = ContextHolder.getBean(DictionaryItem
    Service.class).findById(DictionaryItemDto.class, findDictionaryItem);

    return ReturnDataUtils.newMap("dictionaryItem", dictionaryItem);
    };
    }

    /**
    * 字典项：分页查询
    *
    * @param paginationDictionaryItem
    *            字典项对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryItem/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final DictionaryItemDto paginationDictionaryItem) {
    return () -> {
    final PaginationDto
<DictionaryItemDto> dictionaryItemPagination = ContextHolder.getBean(DictionaryItem
    Service.class).paginationByLikeCode(DictionaryItemDto.class, paginationDictionaryItem);

    return ReturnDataUtils.newMap("dictionaryItemPagination", dictionaryItemPagination);
    };
    }

    /**
    * 字典项：分页查询
    *
    * @param paginationDictionaryItem
    *            字典项对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryItem/pagination.html" })
    public void paginationHtml(final DictionaryItemDto paginationDictionaryItem, final Model model) {
    final PaginationDto
<DictionaryItemDto> dictionaryItemPagination = ContextHolder.getBean(DictionaryItem
    Service.class).paginationByLikeCode(DictionaryItemDto.class, paginationDictionaryItem);

    model.addAttribute("dictionaryItemPagination", dictionaryItemPagination);
    }

    /**
    * 字典项：分页查询
    *
    * @param findDictionaryItem
    *            字典项对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryItem/edit.html" })
    public void editHtml(final DictionaryItemDto findDictionaryItem, final Model model) {
    final DictionaryItemDto dictionaryItem = ContextHolder.getBean(DictionaryItem
    Service.class).findById(DictionaryItemDto.class, findDictionaryItem);

    model.addAttribute("dictionaryItem", dictionaryItem);
    }

    /**
    * 字典项：查询
    *
    * @param findListDictionaryItem
    *            字典项对象
    * @return 字典项对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryItem/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final DictionaryItemDto findListDictionaryItem) {
    return () -> {
    final List<DictionaryItemDto> dictionaryItemList = ContextHolder.getBean(DictionaryItem
    Service.class).findListByLikeCode(DictionaryItemDto.class, findListDictionaryItem);

    return ReturnDataUtils.newMap("dictionaryItem", dictionaryItemList);
    };
    }

    /**
    * 字典项：保存
    *
    * @param saveDictionaryItem
    *            字典项对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryItem/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final DictionaryItemDto saveDictionaryItem) {
    return () -> {
    ContextHolder.getBean(DictionaryItemService.class).saveByIdOrCode(DictionaryItemDto.class, saveDictionaryItem);

    return ReturnDataUtils.newMap();
    };
    }
    }
