package com.cardone.platform.configuration.service;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import com.cardone.common.service.SimpleService;


import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
import com.cardone.platform.configuration.dto.SiteUrlDto;

/**
* 站点位置服务
*
* @author yaohaitao
*/
public interface SiteUrlService extends SimpleService
<SiteUrlDto> {
    /**
    * spring bean id
    */
    String BEAN_ID = "com.cardone.platform.configuration.service.SiteUrlService";
    }