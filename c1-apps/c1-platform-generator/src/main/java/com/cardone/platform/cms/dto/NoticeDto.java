package com.cardone.platform.cms.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.cms.po.Notice;

/**
* 公告
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class NoticeDto extends Notice {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 537723143668169405L;
}