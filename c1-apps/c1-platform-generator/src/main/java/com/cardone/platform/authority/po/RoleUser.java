package com.cardone.platform.authority.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 角色与用户
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class RoleUser extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 180849403534752728L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 角色标识
    */
    private String roleId;

    /**
    * 用户标识
    */
    private String userId;
}