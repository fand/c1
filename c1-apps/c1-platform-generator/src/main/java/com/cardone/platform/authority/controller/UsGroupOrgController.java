package com.cardone.platform.authority.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.authority.dto.UsGroupOrgDto;
import com.cardone.platform.authority.service.UsGroupOrgService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 用户组与组织
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class UsGroupOrgController {
/**
* 用户组与组织：删除
*
* @param deleteUsGroupOrg
*            用户组与组织对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/authority/usGroupOrg/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final UsGroupOrgDto deleteUsGroupOrg) {
    return () -> {
    ContextHolder.getBean(UsGroupOrgService.class).deleteByIds(deleteUsGroupOrg);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 用户组与组织：编辑
    *
    * @param findUsGroupOrg
    *            用户组与组织对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupOrg/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final UsGroupOrgDto findUsGroupOrg, final Model model) {
    return () -> {
    final UsGroupOrgDto usGroupOrg = ContextHolder.getBean(UsGroupOrg
    Service.class).findById(UsGroupOrgDto.class, findUsGroupOrg);

    return ReturnDataUtils.newMap("usGroupOrg", usGroupOrg);
    };
    }

    /**
    * 用户组与组织：分页查询
    *
    * @param paginationUsGroupOrg
    *            用户组与组织对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupOrg/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final UsGroupOrgDto paginationUsGroupOrg) {
    return () -> {
    final PaginationDto
<UsGroupOrgDto> usGroupOrgPagination = ContextHolder.getBean(UsGroupOrg
    Service.class).paginationByLikeCode(UsGroupOrgDto.class, paginationUsGroupOrg);

    return ReturnDataUtils.newMap("usGroupOrgPagination", usGroupOrgPagination);
    };
    }

    /**
    * 用户组与组织：分页查询
    *
    * @param paginationUsGroupOrg
    *            用户组与组织对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupOrg/pagination.html" })
    public void paginationHtml(final UsGroupOrgDto paginationUsGroupOrg, final Model model) {
    final PaginationDto
<UsGroupOrgDto> usGroupOrgPagination = ContextHolder.getBean(UsGroupOrg
    Service.class).paginationByLikeCode(UsGroupOrgDto.class, paginationUsGroupOrg);

    model.addAttribute("usGroupOrgPagination", usGroupOrgPagination);
    }

    /**
    * 用户组与组织：分页查询
    *
    * @param findUsGroupOrg
    *            用户组与组织对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupOrg/edit.html" })
    public void editHtml(final UsGroupOrgDto findUsGroupOrg, final Model model) {
    final UsGroupOrgDto usGroupOrg = ContextHolder.getBean(UsGroupOrg
    Service.class).findById(UsGroupOrgDto.class, findUsGroupOrg);

    model.addAttribute("usGroupOrg", usGroupOrg);
    }

    /**
    * 用户组与组织：查询
    *
    * @param findListUsGroupOrg
    *            用户组与组织对象
    * @return 用户组与组织对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupOrg/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final UsGroupOrgDto findListUsGroupOrg) {
    return () -> {
    final List<UsGroupOrgDto> usGroupOrgList = ContextHolder.getBean(UsGroupOrg
    Service.class).findListByLikeCode(UsGroupOrgDto.class, findListUsGroupOrg);

    return ReturnDataUtils.newMap("usGroupOrg", usGroupOrgList);
    };
    }

    /**
    * 用户组与组织：保存
    *
    * @param saveUsGroupOrg
    *            用户组与组织对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupOrg/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final UsGroupOrgDto saveUsGroupOrg) {
    return () -> {
    ContextHolder.getBean(UsGroupOrgService.class).saveByIdOrCode(UsGroupOrgDto.class, saveUsGroupOrg);

    return ReturnDataUtils.newMap();
    };
    }
    }
