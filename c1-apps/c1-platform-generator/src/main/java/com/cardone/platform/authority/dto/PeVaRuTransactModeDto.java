package com.cardone.platform.authority.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.authority.po.PeVaRuTransactMode;

/**
* 许可、验证规则与处理模式
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class PeVaRuTransactModeDto extends PeVaRuTransactMode {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 778764845343018482L;
}