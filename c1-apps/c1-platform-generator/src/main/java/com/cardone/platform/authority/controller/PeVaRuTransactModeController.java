package com.cardone.platform.authority.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.authority.dto.PeVaRuTransactModeDto;
import com.cardone.platform.authority.service.PeVaRuTransactModeService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 许可、验证规则与处理模式
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class PeVaRuTransactModeController {
/**
* 许可、验证规则与处理模式：删除
*
* @param deletePeVaRuTransactMode
*            许可、验证规则与处理模式对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/authority/peVaRuTransactMode/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final PeVaRuTransactModeDto deletePeVaRuTransactMode) {
    return () -> {
    ContextHolder.getBean(PeVaRuTransactModeService.class).deleteByIds(deletePeVaRuTransactMode);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 许可、验证规则与处理模式：编辑
    *
    * @param findPeVaRuTransactMode
    *            许可、验证规则与处理模式对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/peVaRuTransactMode/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final PeVaRuTransactModeDto findPeVaRuTransactMode, final Model model) {
    return () -> {
    final PeVaRuTransactModeDto peVaRuTransactMode = ContextHolder.getBean(PeVaRuTransactMode
    Service.class).findById(PeVaRuTransactModeDto.class, findPeVaRuTransactMode);

    return ReturnDataUtils.newMap("peVaRuTransactMode", peVaRuTransactMode);
    };
    }

    /**
    * 许可、验证规则与处理模式：分页查询
    *
    * @param paginationPeVaRuTransactMode
    *            许可、验证规则与处理模式对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/peVaRuTransactMode/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final PeVaRuTransactModeDto paginationPeVaRuTransactMode) {
    return () -> {
    final PaginationDto
<PeVaRuTransactModeDto> peVaRuTransactModePagination = ContextHolder.getBean(PeVaRuTransactMode
    Service.class).paginationByLikeCode(PeVaRuTransactModeDto.class, paginationPeVaRuTransactMode);

    return ReturnDataUtils.newMap("peVaRuTransactModePagination", peVaRuTransactModePagination);
    };
    }

    /**
    * 许可、验证规则与处理模式：分页查询
    *
    * @param paginationPeVaRuTransactMode
    *            许可、验证规则与处理模式对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/peVaRuTransactMode/pagination.html" })
    public void paginationHtml(final PeVaRuTransactModeDto paginationPeVaRuTransactMode, final Model model) {
    final PaginationDto
<PeVaRuTransactModeDto> peVaRuTransactModePagination = ContextHolder.getBean(PeVaRuTransactMode
    Service.class).paginationByLikeCode(PeVaRuTransactModeDto.class, paginationPeVaRuTransactMode);

    model.addAttribute("peVaRuTransactModePagination", peVaRuTransactModePagination);
    }

    /**
    * 许可、验证规则与处理模式：分页查询
    *
    * @param findPeVaRuTransactMode
    *            许可、验证规则与处理模式对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/peVaRuTransactMode/edit.html" })
    public void editHtml(final PeVaRuTransactModeDto findPeVaRuTransactMode, final Model model) {
    final PeVaRuTransactModeDto peVaRuTransactMode = ContextHolder.getBean(PeVaRuTransactMode
    Service.class).findById(PeVaRuTransactModeDto.class, findPeVaRuTransactMode);

    model.addAttribute("peVaRuTransactMode", peVaRuTransactMode);
    }

    /**
    * 许可、验证规则与处理模式：查询
    *
    * @param findListPeVaRuTransactMode
    *            许可、验证规则与处理模式对象
    * @return 许可、验证规则与处理模式对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/peVaRuTransactMode/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final PeVaRuTransactModeDto findListPeVaRuTransactMode) {
    return () -> {
    final List<PeVaRuTransactModeDto> peVaRuTransactModeList = ContextHolder.getBean(PeVaRuTransactMode
    Service.class).findListByLikeCode(PeVaRuTransactModeDto.class, findListPeVaRuTransactMode);

    return ReturnDataUtils.newMap("peVaRuTransactMode", peVaRuTransactModeList);
    };
    }

    /**
    * 许可、验证规则与处理模式：保存
    *
    * @param savePeVaRuTransactMode
    *            许可、验证规则与处理模式对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/peVaRuTransactMode/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final PeVaRuTransactModeDto savePeVaRuTransactMode) {
    return () -> {
    ContextHolder.getBean(PeVaRuTransactModeService.class).saveByIdOrCode(PeVaRuTransactModeDto.class, savePeVaRuTransactMode);

    return ReturnDataUtils.newMap();
    };
    }
    }
