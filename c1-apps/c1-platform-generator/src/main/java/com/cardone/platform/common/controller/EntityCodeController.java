package com.cardone.platform.common.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.common.dto.EntityCodeDto;
import com.cardone.platform.common.service.EntityCodeService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 实体编号
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class EntityCodeController {
/**
* 实体编号：删除
*
* @param deleteEntityCode
*            实体编号对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/common/entityCode/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final EntityCodeDto deleteEntityCode) {
    return () -> {
    ContextHolder.getBean(EntityCodeService.class).deleteByIds(deleteEntityCode);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 实体编号：编辑
    *
    * @param findEntityCode
    *            实体编号对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityCode/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final EntityCodeDto findEntityCode, final Model model) {
    return () -> {
    final EntityCodeDto entityCode = ContextHolder.getBean(EntityCode
    Service.class).findById(EntityCodeDto.class, findEntityCode);

    return ReturnDataUtils.newMap("entityCode", entityCode);
    };
    }

    /**
    * 实体编号：分页查询
    *
    * @param paginationEntityCode
    *            实体编号对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityCode/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final EntityCodeDto paginationEntityCode) {
    return () -> {
    final PaginationDto
<EntityCodeDto> entityCodePagination = ContextHolder.getBean(EntityCode
    Service.class).paginationByLikeCode(EntityCodeDto.class, paginationEntityCode);

    return ReturnDataUtils.newMap("entityCodePagination", entityCodePagination);
    };
    }

    /**
    * 实体编号：分页查询
    *
    * @param paginationEntityCode
    *            实体编号对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityCode/pagination.html" })
    public void paginationHtml(final EntityCodeDto paginationEntityCode, final Model model) {
    final PaginationDto
<EntityCodeDto> entityCodePagination = ContextHolder.getBean(EntityCode
    Service.class).paginationByLikeCode(EntityCodeDto.class, paginationEntityCode);

    model.addAttribute("entityCodePagination", entityCodePagination);
    }

    /**
    * 实体编号：分页查询
    *
    * @param findEntityCode
    *            实体编号对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityCode/edit.html" })
    public void editHtml(final EntityCodeDto findEntityCode, final Model model) {
    final EntityCodeDto entityCode = ContextHolder.getBean(EntityCode
    Service.class).findById(EntityCodeDto.class, findEntityCode);

    model.addAttribute("entityCode", entityCode);
    }

    /**
    * 实体编号：查询
    *
    * @param findListEntityCode
    *            实体编号对象
    * @return 实体编号对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityCode/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final EntityCodeDto findListEntityCode) {
    return () -> {
    final List<EntityCodeDto> entityCodeList = ContextHolder.getBean(EntityCode
    Service.class).findListByLikeCode(EntityCodeDto.class, findListEntityCode);

    return ReturnDataUtils.newMap("entityCode", entityCodeList);
    };
    }

    /**
    * 实体编号：保存
    *
    * @param saveEntityCode
    *            实体编号对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityCode/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final EntityCodeDto saveEntityCode) {
    return () -> {
    ContextHolder.getBean(EntityCodeService.class).saveByIdOrCode(EntityCodeDto.class, saveEntityCode);

    return ReturnDataUtils.newMap();
    };
    }
    }
