package com.cardone.platform.authority.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.authority.dto.NavigationDto;
import com.cardone.platform.authority.service.NavigationService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 导航
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class NavigationController {
/**
* 导航：删除
*
* @param deleteNavigation
*            导航对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/authority/navigation/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final NavigationDto deleteNavigation) {
    return () -> {
    ContextHolder.getBean(NavigationService.class).deleteByIds(deleteNavigation);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 导航：编辑
    *
    * @param findNavigation
    *            导航对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/navigation/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final NavigationDto findNavigation, final Model model) {
    return () -> {
    final NavigationDto navigation = ContextHolder.getBean(Navigation
    Service.class).findById(NavigationDto.class, findNavigation);

    return ReturnDataUtils.newMap("navigation", navigation);
    };
    }

    /**
    * 导航：分页查询
    *
    * @param paginationNavigation
    *            导航对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/navigation/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final NavigationDto paginationNavigation) {
    return () -> {
    final PaginationDto
<NavigationDto> navigationPagination = ContextHolder.getBean(Navigation
    Service.class).paginationByLikeCode(NavigationDto.class, paginationNavigation);

    return ReturnDataUtils.newMap("navigationPagination", navigationPagination);
    };
    }

    /**
    * 导航：分页查询
    *
    * @param paginationNavigation
    *            导航对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/navigation/pagination.html" })
    public void paginationHtml(final NavigationDto paginationNavigation, final Model model) {
    final PaginationDto
<NavigationDto> navigationPagination = ContextHolder.getBean(Navigation
    Service.class).paginationByLikeCode(NavigationDto.class, paginationNavigation);

    model.addAttribute("navigationPagination", navigationPagination);
    }

    /**
    * 导航：分页查询
    *
    * @param findNavigation
    *            导航对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/navigation/edit.html" })
    public void editHtml(final NavigationDto findNavigation, final Model model) {
    final NavigationDto navigation = ContextHolder.getBean(Navigation
    Service.class).findById(NavigationDto.class, findNavigation);

    model.addAttribute("navigation", navigation);
    }

    /**
    * 导航：查询
    *
    * @param findListNavigation
    *            导航对象
    * @return 导航对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/navigation/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final NavigationDto findListNavigation) {
    return () -> {
    final List<NavigationDto> navigationList = ContextHolder.getBean(Navigation
    Service.class).findListByLikeCode(NavigationDto.class, findListNavigation);

    return ReturnDataUtils.newMap("navigation", navigationList);
    };
    }

    /**
    * 导航：保存
    *
    * @param saveNavigation
    *            导航对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/navigation/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final NavigationDto saveNavigation) {
    return () -> {
    ContextHolder.getBean(NavigationService.class).saveByIdOrCode(NavigationDto.class, saveNavigation);

    return ReturnDataUtils.newMap();
    };
    }
    }
