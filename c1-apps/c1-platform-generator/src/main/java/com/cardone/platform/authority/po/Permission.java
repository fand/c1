package com.cardone.platform.authority.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 许可
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class Permission extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 629074486092673893L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 站点代码
    */
    private String siteCode;

    /**
    * 站点标识
    */
    private String siteId;

    /**
    * 站点名称
    */
    private String siteName;

    /**
    * 类别代码
    */
    private String typeCode;

    /**
    * 类别标识
    */
    private String typeId;

    /**
    * 类别名称
    */
    private String typeName;
}