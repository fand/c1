package com.cardone.platform.common.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.common.po.EntityExtend;

/**
* 实体扩展
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityExtendDto extends EntityExtend {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 596000259772142623L;
}