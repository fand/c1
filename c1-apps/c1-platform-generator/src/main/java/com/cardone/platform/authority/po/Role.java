package com.cardone.platform.authority.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 角色
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class Role extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 621174343840090419L;

    /**
    * 部门标识
    */
    private String departmentId;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 组织标识
    */
    private String orgId;
}