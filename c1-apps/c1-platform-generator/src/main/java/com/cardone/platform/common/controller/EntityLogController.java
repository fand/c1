package com.cardone.platform.common.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.common.dto.EntityLogDto;
import com.cardone.platform.common.service.EntityLogService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 实体日志
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class EntityLogController {
/**
* 实体日志：删除
*
* @param deleteEntityLog
*            实体日志对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/common/entityLog/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final EntityLogDto deleteEntityLog) {
    return () -> {
    ContextHolder.getBean(EntityLogService.class).deleteByIds(deleteEntityLog);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 实体日志：编辑
    *
    * @param findEntityLog
    *            实体日志对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityLog/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final EntityLogDto findEntityLog, final Model model) {
    return () -> {
    final EntityLogDto entityLog = ContextHolder.getBean(EntityLog
    Service.class).findById(EntityLogDto.class, findEntityLog);

    return ReturnDataUtils.newMap("entityLog", entityLog);
    };
    }

    /**
    * 实体日志：分页查询
    *
    * @param paginationEntityLog
    *            实体日志对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityLog/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final EntityLogDto paginationEntityLog) {
    return () -> {
    final PaginationDto
<EntityLogDto> entityLogPagination = ContextHolder.getBean(EntityLog
    Service.class).paginationByLikeCode(EntityLogDto.class, paginationEntityLog);

    return ReturnDataUtils.newMap("entityLogPagination", entityLogPagination);
    };
    }

    /**
    * 实体日志：分页查询
    *
    * @param paginationEntityLog
    *            实体日志对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityLog/pagination.html" })
    public void paginationHtml(final EntityLogDto paginationEntityLog, final Model model) {
    final PaginationDto
<EntityLogDto> entityLogPagination = ContextHolder.getBean(EntityLog
    Service.class).paginationByLikeCode(EntityLogDto.class, paginationEntityLog);

    model.addAttribute("entityLogPagination", entityLogPagination);
    }

    /**
    * 实体日志：分页查询
    *
    * @param findEntityLog
    *            实体日志对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityLog/edit.html" })
    public void editHtml(final EntityLogDto findEntityLog, final Model model) {
    final EntityLogDto entityLog = ContextHolder.getBean(EntityLog
    Service.class).findById(EntityLogDto.class, findEntityLog);

    model.addAttribute("entityLog", entityLog);
    }

    /**
    * 实体日志：查询
    *
    * @param findListEntityLog
    *            实体日志对象
    * @return 实体日志对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityLog/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final EntityLogDto findListEntityLog) {
    return () -> {
    final List<EntityLogDto> entityLogList = ContextHolder.getBean(EntityLog
    Service.class).findListByLikeCode(EntityLogDto.class, findListEntityLog);

    return ReturnDataUtils.newMap("entityLog", entityLogList);
    };
    }

    /**
    * 实体日志：保存
    *
    * @param saveEntityLog
    *            实体日志对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityLog/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final EntityLogDto saveEntityLog) {
    return () -> {
    ContextHolder.getBean(EntityLogService.class).saveByIdOrCode(EntityLogDto.class, saveEntityLog);

    return ReturnDataUtils.newMap();
    };
    }
    }
