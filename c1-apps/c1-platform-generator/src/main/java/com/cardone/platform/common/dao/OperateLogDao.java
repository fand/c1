package com.cardone.platform.common.dao;

import java.util.List;
import java.util.Map;

import com.cardone.common.dto.PaginationDto;
import com.cardone.platform.common.dto.OperateLogDto;
import com.cardone.common.dao.SimpleDao;

/**
* 操作日志
*
* @author yaohaitao
*
*/
public interface OperateLogDao extends SimpleDao
<OperateLogDto> {
    /**
    * sql标识
    *
    * @author yaohaitao
    *
    */
    public enum SqlIds {
    /**
    * 查询
    */
    findByCodes;

    /**
    * 根目录
    */
    public static final String ROOT = "/platform/common/operateLog/";

    /**
    * 标识
    */
    private final String id;

    /**
    * sql标识
    */
    private SqlIds() {
    this.id = SqlIds.ROOT + this.name();
    }

    /**
    * 获取
    *
    * @return sql标识
    */
    public String id() {
    return this.id;
    }
    }
    }