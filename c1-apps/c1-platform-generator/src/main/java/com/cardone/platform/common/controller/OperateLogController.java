package com.cardone.platform.common.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.common.dto.OperateLogDto;
import com.cardone.platform.common.service.OperateLogService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 操作日志
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class OperateLogController {
/**
* 操作日志：删除
*
* @param deleteOperateLog
*            操作日志对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/common/operateLog/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final OperateLogDto deleteOperateLog) {
    return () -> {
    ContextHolder.getBean(OperateLogService.class).deleteByIds(deleteOperateLog);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 操作日志：编辑
    *
    * @param findOperateLog
    *            操作日志对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/operateLog/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final OperateLogDto findOperateLog, final Model model) {
    return () -> {
    final OperateLogDto operateLog = ContextHolder.getBean(OperateLog
    Service.class).findById(OperateLogDto.class, findOperateLog);

    return ReturnDataUtils.newMap("operateLog", operateLog);
    };
    }

    /**
    * 操作日志：分页查询
    *
    * @param paginationOperateLog
    *            操作日志对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/operateLog/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final OperateLogDto paginationOperateLog) {
    return () -> {
    final PaginationDto
<OperateLogDto> operateLogPagination = ContextHolder.getBean(OperateLog
    Service.class).paginationByLikeCode(OperateLogDto.class, paginationOperateLog);

    return ReturnDataUtils.newMap("operateLogPagination", operateLogPagination);
    };
    }

    /**
    * 操作日志：分页查询
    *
    * @param paginationOperateLog
    *            操作日志对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/operateLog/pagination.html" })
    public void paginationHtml(final OperateLogDto paginationOperateLog, final Model model) {
    final PaginationDto
<OperateLogDto> operateLogPagination = ContextHolder.getBean(OperateLog
    Service.class).paginationByLikeCode(OperateLogDto.class, paginationOperateLog);

    model.addAttribute("operateLogPagination", operateLogPagination);
    }

    /**
    * 操作日志：分页查询
    *
    * @param findOperateLog
    *            操作日志对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/operateLog/edit.html" })
    public void editHtml(final OperateLogDto findOperateLog, final Model model) {
    final OperateLogDto operateLog = ContextHolder.getBean(OperateLog
    Service.class).findById(OperateLogDto.class, findOperateLog);

    model.addAttribute("operateLog", operateLog);
    }

    /**
    * 操作日志：查询
    *
    * @param findListOperateLog
    *            操作日志对象
    * @return 操作日志对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/operateLog/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final OperateLogDto findListOperateLog) {
    return () -> {
    final List<OperateLogDto> operateLogList = ContextHolder.getBean(OperateLog
    Service.class).findListByLikeCode(OperateLogDto.class, findListOperateLog);

    return ReturnDataUtils.newMap("operateLog", operateLogList);
    };
    }

    /**
    * 操作日志：保存
    *
    * @param saveOperateLog
    *            操作日志对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/operateLog/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final OperateLogDto saveOperateLog) {
    return () -> {
    ContextHolder.getBean(OperateLogService.class).saveByIdOrCode(OperateLogDto.class, saveOperateLog);

    return ReturnDataUtils.newMap();
    };
    }
    }
