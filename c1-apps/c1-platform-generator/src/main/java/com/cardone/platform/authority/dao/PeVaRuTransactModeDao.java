package com.cardone.platform.authority.dao;

import java.util.List;
import java.util.Map;

import com.cardone.common.dto.PaginationDto;
import com.cardone.platform.authority.dto.PeVaRuTransactModeDto;
import com.cardone.common.dao.SimpleDao;

/**
* 许可、验证规则与处理模式
*
* @author yaohaitao
*
*/
public interface PeVaRuTransactModeDao extends SimpleDao
<PeVaRuTransactModeDto> {
    /**
    * sql标识
    *
    * @author yaohaitao
    *
    */
    public enum SqlIds {
    /**
    * 查询
    */
    findByCodes;

    /**
    * 根目录
    */
    public static final String ROOT = "/platform/authority/peVaRuTransactMode/";

    /**
    * 标识
    */
    private final String id;

    /**
    * sql标识
    */
    private SqlIds() {
    this.id = SqlIds.ROOT + this.name();
    }

    /**
    * 获取
    *
    * @return sql标识
    */
    public String id() {
    return this.id;
    }
    }
    }