package com.cardone.platform.authority.service;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import com.cardone.common.service.SimpleService;


import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
import com.cardone.platform.authority.dto.NavigationDto;

/**
* 导航服务
*
* @author yaohaitao
*/
public interface NavigationService extends SimpleService
<NavigationDto> {
    /**
    * spring bean id
    */
    String BEAN_ID = "com.cardone.platform.authority.service.NavigationService";
    }