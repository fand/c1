package com.cardone.platform.configuration.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.configuration.dto.SiteUrlDto;
import com.cardone.platform.configuration.service.SiteUrlService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 站点位置
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class SiteUrlController {
/**
* 站点位置：删除
*
* @param deleteSiteUrl
*            站点位置对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/configuration/siteUrl/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final SiteUrlDto deleteSiteUrl) {
    return () -> {
    ContextHolder.getBean(SiteUrlService.class).deleteByIds(deleteSiteUrl);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 站点位置：编辑
    *
    * @param findSiteUrl
    *            站点位置对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/siteUrl/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final SiteUrlDto findSiteUrl, final Model model) {
    return () -> {
    final SiteUrlDto siteUrl = ContextHolder.getBean(SiteUrl
    Service.class).findById(SiteUrlDto.class, findSiteUrl);

    return ReturnDataUtils.newMap("siteUrl", siteUrl);
    };
    }

    /**
    * 站点位置：分页查询
    *
    * @param paginationSiteUrl
    *            站点位置对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/siteUrl/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final SiteUrlDto paginationSiteUrl) {
    return () -> {
    final PaginationDto
<SiteUrlDto> siteUrlPagination = ContextHolder.getBean(SiteUrl
    Service.class).paginationByLikeCode(SiteUrlDto.class, paginationSiteUrl);

    return ReturnDataUtils.newMap("siteUrlPagination", siteUrlPagination);
    };
    }

    /**
    * 站点位置：分页查询
    *
    * @param paginationSiteUrl
    *            站点位置对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/siteUrl/pagination.html" })
    public void paginationHtml(final SiteUrlDto paginationSiteUrl, final Model model) {
    final PaginationDto
<SiteUrlDto> siteUrlPagination = ContextHolder.getBean(SiteUrl
    Service.class).paginationByLikeCode(SiteUrlDto.class, paginationSiteUrl);

    model.addAttribute("siteUrlPagination", siteUrlPagination);
    }

    /**
    * 站点位置：分页查询
    *
    * @param findSiteUrl
    *            站点位置对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/siteUrl/edit.html" })
    public void editHtml(final SiteUrlDto findSiteUrl, final Model model) {
    final SiteUrlDto siteUrl = ContextHolder.getBean(SiteUrl
    Service.class).findById(SiteUrlDto.class, findSiteUrl);

    model.addAttribute("siteUrl", siteUrl);
    }

    /**
    * 站点位置：查询
    *
    * @param findListSiteUrl
    *            站点位置对象
    * @return 站点位置对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/siteUrl/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final SiteUrlDto findListSiteUrl) {
    return () -> {
    final List<SiteUrlDto> siteUrlList = ContextHolder.getBean(SiteUrl
    Service.class).findListByLikeCode(SiteUrlDto.class, findListSiteUrl);

    return ReturnDataUtils.newMap("siteUrl", siteUrlList);
    };
    }

    /**
    * 站点位置：保存
    *
    * @param saveSiteUrl
    *            站点位置对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/siteUrl/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final SiteUrlDto saveSiteUrl) {
    return () -> {
    ContextHolder.getBean(SiteUrlService.class).saveByIdOrCode(SiteUrlDto.class, saveSiteUrl);

    return ReturnDataUtils.newMap();
    };
    }
    }
