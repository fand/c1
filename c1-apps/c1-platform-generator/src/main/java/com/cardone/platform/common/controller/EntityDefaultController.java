package com.cardone.platform.common.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.common.dto.EntityDefaultDto;
import com.cardone.platform.common.service.EntityDefaultService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 实体与默认
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class EntityDefaultController {
/**
* 实体与默认：删除
*
* @param deleteEntityDefault
*            实体与默认对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/common/entityDefault/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final EntityDefaultDto deleteEntityDefault) {
    return () -> {
    ContextHolder.getBean(EntityDefaultService.class).deleteByIds(deleteEntityDefault);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 实体与默认：编辑
    *
    * @param findEntityDefault
    *            实体与默认对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityDefault/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final EntityDefaultDto findEntityDefault, final Model model) {
    return () -> {
    final EntityDefaultDto entityDefault = ContextHolder.getBean(EntityDefault
    Service.class).findById(EntityDefaultDto.class, findEntityDefault);

    return ReturnDataUtils.newMap("entityDefault", entityDefault);
    };
    }

    /**
    * 实体与默认：分页查询
    *
    * @param paginationEntityDefault
    *            实体与默认对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityDefault/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final EntityDefaultDto paginationEntityDefault) {
    return () -> {
    final PaginationDto
<EntityDefaultDto> entityDefaultPagination = ContextHolder.getBean(EntityDefault
    Service.class).paginationByLikeCode(EntityDefaultDto.class, paginationEntityDefault);

    return ReturnDataUtils.newMap("entityDefaultPagination", entityDefaultPagination);
    };
    }

    /**
    * 实体与默认：分页查询
    *
    * @param paginationEntityDefault
    *            实体与默认对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityDefault/pagination.html" })
    public void paginationHtml(final EntityDefaultDto paginationEntityDefault, final Model model) {
    final PaginationDto
<EntityDefaultDto> entityDefaultPagination = ContextHolder.getBean(EntityDefault
    Service.class).paginationByLikeCode(EntityDefaultDto.class, paginationEntityDefault);

    model.addAttribute("entityDefaultPagination", entityDefaultPagination);
    }

    /**
    * 实体与默认：分页查询
    *
    * @param findEntityDefault
    *            实体与默认对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityDefault/edit.html" })
    public void editHtml(final EntityDefaultDto findEntityDefault, final Model model) {
    final EntityDefaultDto entityDefault = ContextHolder.getBean(EntityDefault
    Service.class).findById(EntityDefaultDto.class, findEntityDefault);

    model.addAttribute("entityDefault", entityDefault);
    }

    /**
    * 实体与默认：查询
    *
    * @param findListEntityDefault
    *            实体与默认对象
    * @return 实体与默认对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityDefault/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final EntityDefaultDto findListEntityDefault) {
    return () -> {
    final List<EntityDefaultDto> entityDefaultList = ContextHolder.getBean(EntityDefault
    Service.class).findListByLikeCode(EntityDefaultDto.class, findListEntityDefault);

    return ReturnDataUtils.newMap("entityDefault", entityDefaultList);
    };
    }

    /**
    * 实体与默认：保存
    *
    * @param saveEntityDefault
    *            实体与默认对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityDefault/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final EntityDefaultDto saveEntityDefault) {
    return () -> {
    ContextHolder.getBean(EntityDefaultService.class).saveByIdOrCode(EntityDefaultDto.class, saveEntityDefault);

    return ReturnDataUtils.newMap();
    };
    }
    }
