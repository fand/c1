package com.cardone.platform.authority.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 许可、验证规则与处理模式
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class PeVaRuTransactMode extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 757571967681326153L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 许可标识
    */
    private String permissionId;

    /**
    * 字典.验证失败代码
    */
    private String validateErrorCode;

    /**
    * 字典.验证失败标识
    */
    private String validateErrorId;

    /**
    * 字典.验证失败名称
    */
    private String validateErrorName;

    /**
    * 字典.验证规则代码
    */
    private String validateRuleCode;

    /**
    * 字典.验证规则标识
    */
    private String validateRuleId;

    /**
    * 字典.验证规则名称
    */
    private String validateRuleName;

    /**
    * 字典.验证成功代码
    */
    private String validateSuccessCode;

    /**
    * 字典.验证成功标识
    */
    private String validateSuccessId;

    /**
    * 字典.验证成功名称
    */
    private String validateSuccessName;
}