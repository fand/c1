package com.cardone.platform.usercenter.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.usercenter.dto.UserActivationDto;
import com.cardone.platform.usercenter.service.UserActivationService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 用户激活
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class UserActivationController {
/**
* 用户激活：删除
*
* @param deleteUserActivation
*            用户激活对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/usercenter/userActivation/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final UserActivationDto deleteUserActivation) {
    return () -> {
    ContextHolder.getBean(UserActivationService.class).deleteByIds(deleteUserActivation);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 用户激活：编辑
    *
    * @param findUserActivation
    *            用户激活对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/userActivation/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final UserActivationDto findUserActivation, final Model model) {
    return () -> {
    final UserActivationDto userActivation = ContextHolder.getBean(UserActivation
    Service.class).findById(UserActivationDto.class, findUserActivation);

    return ReturnDataUtils.newMap("userActivation", userActivation);
    };
    }

    /**
    * 用户激活：分页查询
    *
    * @param paginationUserActivation
    *            用户激活对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/userActivation/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final UserActivationDto paginationUserActivation) {
    return () -> {
    final PaginationDto
<UserActivationDto> userActivationPagination = ContextHolder.getBean(UserActivation
    Service.class).paginationByLikeCode(UserActivationDto.class, paginationUserActivation);

    return ReturnDataUtils.newMap("userActivationPagination", userActivationPagination);
    };
    }

    /**
    * 用户激活：分页查询
    *
    * @param paginationUserActivation
    *            用户激活对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/userActivation/pagination.html" })
    public void paginationHtml(final UserActivationDto paginationUserActivation, final Model model) {
    final PaginationDto
<UserActivationDto> userActivationPagination = ContextHolder.getBean(UserActivation
    Service.class).paginationByLikeCode(UserActivationDto.class, paginationUserActivation);

    model.addAttribute("userActivationPagination", userActivationPagination);
    }

    /**
    * 用户激活：分页查询
    *
    * @param findUserActivation
    *            用户激活对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/userActivation/edit.html" })
    public void editHtml(final UserActivationDto findUserActivation, final Model model) {
    final UserActivationDto userActivation = ContextHolder.getBean(UserActivation
    Service.class).findById(UserActivationDto.class, findUserActivation);

    model.addAttribute("userActivation", userActivation);
    }

    /**
    * 用户激活：查询
    *
    * @param findListUserActivation
    *            用户激活对象
    * @return 用户激活对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/userActivation/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final UserActivationDto findListUserActivation) {
    return () -> {
    final List<UserActivationDto> userActivationList = ContextHolder.getBean(UserActivation
    Service.class).findListByLikeCode(UserActivationDto.class, findListUserActivation);

    return ReturnDataUtils.newMap("userActivation", userActivationList);
    };
    }

    /**
    * 用户激活：保存
    *
    * @param saveUserActivation
    *            用户激活对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/userActivation/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final UserActivationDto saveUserActivation) {
    return () -> {
    ContextHolder.getBean(UserActivationService.class).saveByIdOrCode(UserActivationDto.class, saveUserActivation);

    return ReturnDataUtils.newMap();
    };
    }
    }
