package com.cardone.platform.authority.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.authority.dto.PermissionOrgDto;
import com.cardone.platform.authority.service.PermissionOrgService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 许可与组织
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class PermissionOrgController {
/**
* 许可与组织：删除
*
* @param deletePermissionOrg
*            许可与组织对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/authority/permissionOrg/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final PermissionOrgDto deletePermissionOrg) {
    return () -> {
    ContextHolder.getBean(PermissionOrgService.class).deleteByIds(deletePermissionOrg);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 许可与组织：编辑
    *
    * @param findPermissionOrg
    *            许可与组织对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permissionOrg/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final PermissionOrgDto findPermissionOrg, final Model model) {
    return () -> {
    final PermissionOrgDto permissionOrg = ContextHolder.getBean(PermissionOrg
    Service.class).findById(PermissionOrgDto.class, findPermissionOrg);

    return ReturnDataUtils.newMap("permissionOrg", permissionOrg);
    };
    }

    /**
    * 许可与组织：分页查询
    *
    * @param paginationPermissionOrg
    *            许可与组织对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permissionOrg/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final PermissionOrgDto paginationPermissionOrg) {
    return () -> {
    final PaginationDto
<PermissionOrgDto> permissionOrgPagination = ContextHolder.getBean(PermissionOrg
    Service.class).paginationByLikeCode(PermissionOrgDto.class, paginationPermissionOrg);

    return ReturnDataUtils.newMap("permissionOrgPagination", permissionOrgPagination);
    };
    }

    /**
    * 许可与组织：分页查询
    *
    * @param paginationPermissionOrg
    *            许可与组织对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permissionOrg/pagination.html" })
    public void paginationHtml(final PermissionOrgDto paginationPermissionOrg, final Model model) {
    final PaginationDto
<PermissionOrgDto> permissionOrgPagination = ContextHolder.getBean(PermissionOrg
    Service.class).paginationByLikeCode(PermissionOrgDto.class, paginationPermissionOrg);

    model.addAttribute("permissionOrgPagination", permissionOrgPagination);
    }

    /**
    * 许可与组织：分页查询
    *
    * @param findPermissionOrg
    *            许可与组织对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permissionOrg/edit.html" })
    public void editHtml(final PermissionOrgDto findPermissionOrg, final Model model) {
    final PermissionOrgDto permissionOrg = ContextHolder.getBean(PermissionOrg
    Service.class).findById(PermissionOrgDto.class, findPermissionOrg);

    model.addAttribute("permissionOrg", permissionOrg);
    }

    /**
    * 许可与组织：查询
    *
    * @param findListPermissionOrg
    *            许可与组织对象
    * @return 许可与组织对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permissionOrg/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final PermissionOrgDto findListPermissionOrg) {
    return () -> {
    final List<PermissionOrgDto> permissionOrgList = ContextHolder.getBean(PermissionOrg
    Service.class).findListByLikeCode(PermissionOrgDto.class, findListPermissionOrg);

    return ReturnDataUtils.newMap("permissionOrg", permissionOrgList);
    };
    }

    /**
    * 许可与组织：保存
    *
    * @param savePermissionOrg
    *            许可与组织对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permissionOrg/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final PermissionOrgDto savePermissionOrg) {
    return () -> {
    ContextHolder.getBean(PermissionOrgService.class).saveByIdOrCode(PermissionOrgDto.class, savePermissionOrg);

    return ReturnDataUtils.newMap();
    };
    }
    }
