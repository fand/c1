package com.cardone.platform.configuration.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 字典项
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class DictionaryItem extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 252856433929638391L;

    /**
    * 字典代码
    */
    private String dictionaryCode;

    /**
    * 字典标识
    */
    private String dictionaryId;

    /**
    * 字典名称
    */
    private String dictionaryName;

    /**
    * 字典类别代码
    */
    private String dictionaryTypeCode;

    /**
    * 字典类别标识
    */
    private String dictionaryTypeId;

    /**
    * 字典类别名称
    */
    private String dictionaryTypeName;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 说明
    */
    private String remark;

    /**
    * 值
    */
    private String value;
}