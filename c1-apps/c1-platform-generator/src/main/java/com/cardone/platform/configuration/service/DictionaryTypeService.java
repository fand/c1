package com.cardone.platform.configuration.service;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import com.cardone.common.service.SimpleService;


import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
import com.cardone.platform.configuration.dto.DictionaryTypeDto;

/**
* 字典类型服务
*
* @author yaohaitao
*/
public interface DictionaryTypeService extends SimpleService
<DictionaryTypeDto> {
    /**
    * spring bean id
    */
    String BEAN_ID = "com.cardone.platform.configuration.service.DictionaryTypeService";
    }