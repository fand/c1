package com.cardone.platform.cms.service;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import com.cardone.common.service.SimpleService;


import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
import com.cardone.platform.cms.dto.NoticeDto;

/**
* 公告服务
*
* @author yaohaitao
*/
public interface NoticeService extends SimpleService
<NoticeDto> {
    /**
    * spring bean id
    */
    String BEAN_ID = "com.cardone.platform.cms.service.NoticeService";
    }