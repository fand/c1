package com.cardone.platform.common.service;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import com.cardone.common.service.SimpleService;


import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
import com.cardone.platform.common.dto.EntityExtendDto;

/**
* 实体扩展服务
*
* @author yaohaitao
*/
public interface EntityExtendService extends SimpleService
<EntityExtendDto> {
    /**
    * spring bean id
    */
    String BEAN_ID = "com.cardone.platform.common.service.EntityExtendService";
    }