package com.cardone.platform.usercenter.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 部门
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class Department extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 374094768671744286L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 组织代码
    */
    private String orgCode;

    /**
    * 组织标识
    */
    private String orgId;

    /**
    * 组织名称
    */
    private String orgName;
}