package com.cardone.platform.common.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 操作日志
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class OperateLog extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 854006236599894302L;

    /**
    * 业务代码
    */
    private String businessCode;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 消息
    */
    private String message;

    /**
    * 字典.类别标识
    */
    private String typeId;

    /**
    * 用户标识
    */
    private String userId;

    /**
    * 值
    */
    private String value;
}