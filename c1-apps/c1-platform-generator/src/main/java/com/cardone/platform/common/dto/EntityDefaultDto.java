package com.cardone.platform.common.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.common.po.EntityDefault;

/**
* 实体与默认
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityDefaultDto extends EntityDefault {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 751130253187778000L;
}