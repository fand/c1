package com.cardone.platform.usercenter.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 用户激活
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UserActivation extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 348801890786621019L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 用户标识
    */
    private String userId;

    /**
    * 验证码
    */
    private String verificationCode;
}