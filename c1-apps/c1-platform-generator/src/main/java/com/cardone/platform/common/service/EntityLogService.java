package com.cardone.platform.common.service;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import com.cardone.common.service.SimpleService;


import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
import com.cardone.platform.common.dto.EntityLogDto;

/**
* 实体日志服务
*
* @author yaohaitao
*/
public interface EntityLogService extends SimpleService
<EntityLogDto> {
    /**
    * spring bean id
    */
    String BEAN_ID = "com.cardone.platform.common.service.EntityLogService";
    }