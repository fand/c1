package com.cardone.platform.authority.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 用户组
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UserGroup extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 116171408733930745L;

    /**
    * 业务代码
    */
    private String businessCode;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 父级代码
    */
    private String parentCode;

    /**
    * 父级标识
    */
    private String parentId;

    /**
    * 父级名称
    */
    private String parentName;

    /**
    * 树代码
    */
    private String treeCode;

    /**
    * 树标识
    */
    private String treeId;

    /**
    * 树名称
    */
    private String treeName;

    /**
    * 字典.类型标识
    */
    private String typeId;
}