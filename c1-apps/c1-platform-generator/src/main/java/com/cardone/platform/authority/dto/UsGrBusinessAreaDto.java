package com.cardone.platform.authority.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.authority.po.UsGrBusinessArea;

/**
* 用户组与业务范围
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGrBusinessAreaDto extends UsGrBusinessArea {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 284816263809005515L;
}