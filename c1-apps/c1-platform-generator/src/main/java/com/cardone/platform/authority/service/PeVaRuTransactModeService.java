package com.cardone.platform.authority.service;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import com.cardone.common.service.SimpleService;


import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
import com.cardone.platform.authority.dto.PeVaRuTransactModeDto;

/**
* 许可、验证规则与处理模式服务
*
* @author yaohaitao
*/
public interface PeVaRuTransactModeService extends SimpleService
<PeVaRuTransactModeDto> {
    /**
    * spring bean id
    */
    String BEAN_ID = "com.cardone.platform.authority.service.PeVaRuTransactModeService";
    }