package com.cardone.platform.common.dao;

import java.util.List;
import java.util.Map;

import com.cardone.common.dto.PaginationDto;
import com.cardone.platform.common.dto.EntityLogDto;
import com.cardone.common.dao.SimpleDao;

/**
* 实体日志
*
* @author yaohaitao
*
*/
public interface EntityLogDao extends SimpleDao
<EntityLogDto> {
    /**
    * sql标识
    *
    * @author yaohaitao
    *
    */
    public enum SqlIds {
    /**
    * 查询
    */
    findByCodes;

    /**
    * 根目录
    */
    public static final String ROOT = "/platform/common/entityLog/";

    /**
    * 标识
    */
    private final String id;

    /**
    * sql标识
    */
    private SqlIds() {
    this.id = SqlIds.ROOT + this.name();
    }

    /**
    * 获取
    *
    * @return sql标识
    */
    public String id() {
    return this.id;
    }
    }
    }