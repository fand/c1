package com.cardone.platform.cms.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 文章
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class Article extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 703918423215767006L;

    /**
    * 正文
    */
    private String content;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 组织代码
    */
    private String orgCode;

    /**
    * 组织标识
    */
        @Id
    private String orgId;

    /**
    * 组织名称
    */
    private String orgName;

    /**
    * 发布日期
    */
    private Date releaseDate;

    /**
    * 标题
    */
    private String title;

    /**
    * 类别代码
    */
    private String typeCode;

    /**
    * 类别标识
    */
    private String typeId;

    /**
    * 类别名称
    */
    private String typeName;
}