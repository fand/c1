package com.cardone.platform.configuration.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.configuration.po.SiteUrl;

/**
* 站点位置
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class SiteUrlDto extends SiteUrl {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 818741802426963270L;
}