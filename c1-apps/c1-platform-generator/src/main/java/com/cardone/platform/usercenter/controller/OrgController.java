package com.cardone.platform.usercenter.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.usercenter.dto.OrgDto;
import com.cardone.platform.usercenter.service.OrgService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 组织
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class OrgController {
/**
* 组织：删除
*
* @param deleteOrg
*            组织对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/usercenter/org/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final OrgDto deleteOrg) {
    return () -> {
    ContextHolder.getBean(OrgService.class).deleteByIds(deleteOrg);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 组织：编辑
    *
    * @param findOrg
    *            组织对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/org/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final OrgDto findOrg, final Model model) {
    return () -> {
    final OrgDto org = ContextHolder.getBean(Org
    Service.class).findById(OrgDto.class, findOrg);

    return ReturnDataUtils.newMap("org", org);
    };
    }

    /**
    * 组织：分页查询
    *
    * @param paginationOrg
    *            组织对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/org/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final OrgDto paginationOrg) {
    return () -> {
    final PaginationDto
<OrgDto> orgPagination = ContextHolder.getBean(Org
    Service.class).paginationByLikeCode(OrgDto.class, paginationOrg);

    return ReturnDataUtils.newMap("orgPagination", orgPagination);
    };
    }

    /**
    * 组织：分页查询
    *
    * @param paginationOrg
    *            组织对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/org/pagination.html" })
    public void paginationHtml(final OrgDto paginationOrg, final Model model) {
    final PaginationDto
<OrgDto> orgPagination = ContextHolder.getBean(Org
    Service.class).paginationByLikeCode(OrgDto.class, paginationOrg);

    model.addAttribute("orgPagination", orgPagination);
    }

    /**
    * 组织：分页查询
    *
    * @param findOrg
    *            组织对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/org/edit.html" })
    public void editHtml(final OrgDto findOrg, final Model model) {
    final OrgDto org = ContextHolder.getBean(Org
    Service.class).findById(OrgDto.class, findOrg);

    model.addAttribute("org", org);
    }

    /**
    * 组织：查询
    *
    * @param findListOrg
    *            组织对象
    * @return 组织对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/org/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final OrgDto findListOrg) {
    return () -> {
    final List<OrgDto> orgList = ContextHolder.getBean(Org
    Service.class).findListByLikeCode(OrgDto.class, findListOrg);

    return ReturnDataUtils.newMap("org", orgList);
    };
    }

    /**
    * 组织：保存
    *
    * @param saveOrg
    *            组织对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/org/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final OrgDto saveOrg) {
    return () -> {
    ContextHolder.getBean(OrgService.class).saveByIdOrCode(OrgDto.class, saveOrg);

    return ReturnDataUtils.newMap();
    };
    }
    }
