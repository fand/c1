package com.cardone.platform.common.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.common.po.EntityLog;

/**
* 实体日志
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityLogDto extends EntityLog {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 252437224155030805L;
}