package com.cardone.platform.common.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.common.dto.EntityExtendDto;
import com.cardone.platform.common.service.EntityExtendService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 实体扩展
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class EntityExtendController {
/**
* 实体扩展：删除
*
* @param deleteEntityExtend
*            实体扩展对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/common/entityExtend/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final EntityExtendDto deleteEntityExtend) {
    return () -> {
    ContextHolder.getBean(EntityExtendService.class).deleteByIds(deleteEntityExtend);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 实体扩展：编辑
    *
    * @param findEntityExtend
    *            实体扩展对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityExtend/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final EntityExtendDto findEntityExtend, final Model model) {
    return () -> {
    final EntityExtendDto entityExtend = ContextHolder.getBean(EntityExtend
    Service.class).findById(EntityExtendDto.class, findEntityExtend);

    return ReturnDataUtils.newMap("entityExtend", entityExtend);
    };
    }

    /**
    * 实体扩展：分页查询
    *
    * @param paginationEntityExtend
    *            实体扩展对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityExtend/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final EntityExtendDto paginationEntityExtend) {
    return () -> {
    final PaginationDto
<EntityExtendDto> entityExtendPagination = ContextHolder.getBean(EntityExtend
    Service.class).paginationByLikeCode(EntityExtendDto.class, paginationEntityExtend);

    return ReturnDataUtils.newMap("entityExtendPagination", entityExtendPagination);
    };
    }

    /**
    * 实体扩展：分页查询
    *
    * @param paginationEntityExtend
    *            实体扩展对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityExtend/pagination.html" })
    public void paginationHtml(final EntityExtendDto paginationEntityExtend, final Model model) {
    final PaginationDto
<EntityExtendDto> entityExtendPagination = ContextHolder.getBean(EntityExtend
    Service.class).paginationByLikeCode(EntityExtendDto.class, paginationEntityExtend);

    model.addAttribute("entityExtendPagination", entityExtendPagination);
    }

    /**
    * 实体扩展：分页查询
    *
    * @param findEntityExtend
    *            实体扩展对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityExtend/edit.html" })
    public void editHtml(final EntityExtendDto findEntityExtend, final Model model) {
    final EntityExtendDto entityExtend = ContextHolder.getBean(EntityExtend
    Service.class).findById(EntityExtendDto.class, findEntityExtend);

    model.addAttribute("entityExtend", entityExtend);
    }

    /**
    * 实体扩展：查询
    *
    * @param findListEntityExtend
    *            实体扩展对象
    * @return 实体扩展对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityExtend/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final EntityExtendDto findListEntityExtend) {
    return () -> {
    final List<EntityExtendDto> entityExtendList = ContextHolder.getBean(EntityExtend
    Service.class).findListByLikeCode(EntityExtendDto.class, findListEntityExtend);

    return ReturnDataUtils.newMap("entityExtend", entityExtendList);
    };
    }

    /**
    * 实体扩展：保存
    *
    * @param saveEntityExtend
    *            实体扩展对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityExtend/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final EntityExtendDto saveEntityExtend) {
    return () -> {
    ContextHolder.getBean(EntityExtendService.class).saveByIdOrCode(EntityExtendDto.class, saveEntityExtend);

    return ReturnDataUtils.newMap();
    };
    }
    }
