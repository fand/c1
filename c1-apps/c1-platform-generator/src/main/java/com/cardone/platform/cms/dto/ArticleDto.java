package com.cardone.platform.cms.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.cms.po.Article;

/**
* 文章
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class ArticleDto extends Article {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 756273202181063761L;
}