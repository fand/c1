package com.cardone.platform.authority.service;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import com.cardone.common.service.SimpleService;


import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
import com.cardone.platform.authority.dto.UsGroupOrgDto;

/**
* 用户组与组织服务
*
* @author yaohaitao
*/
public interface UsGroupOrgService extends SimpleService
<UsGroupOrgDto> {
    /**
    * spring bean id
    */
    String BEAN_ID = "com.cardone.platform.authority.service.UsGroupOrgService";
    }