package com.cardone.platform.usercenter.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.usercenter.po.UserActivation;

/**
* 用户激活
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UserActivationDto extends UserActivation {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 767651440774347666L;
}