package com.cardone.platform.configuration.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.configuration.dto.DictionaryDto;
import com.cardone.platform.configuration.service.DictionaryService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 字典
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class DictionaryController {
/**
* 字典：删除
*
* @param deleteDictionary
*            字典对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/configuration/dictionary/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final DictionaryDto deleteDictionary) {
    return () -> {
    ContextHolder.getBean(DictionaryService.class).deleteByIds(deleteDictionary);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 字典：编辑
    *
    * @param findDictionary
    *            字典对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionary/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final DictionaryDto findDictionary, final Model model) {
    return () -> {
    final DictionaryDto dictionary = ContextHolder.getBean(Dictionary
    Service.class).findById(DictionaryDto.class, findDictionary);

    return ReturnDataUtils.newMap("dictionary", dictionary);
    };
    }

    /**
    * 字典：分页查询
    *
    * @param paginationDictionary
    *            字典对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionary/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final DictionaryDto paginationDictionary) {
    return () -> {
    final PaginationDto
<DictionaryDto> dictionaryPagination = ContextHolder.getBean(Dictionary
    Service.class).paginationByLikeCode(DictionaryDto.class, paginationDictionary);

    return ReturnDataUtils.newMap("dictionaryPagination", dictionaryPagination);
    };
    }

    /**
    * 字典：分页查询
    *
    * @param paginationDictionary
    *            字典对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionary/pagination.html" })
    public void paginationHtml(final DictionaryDto paginationDictionary, final Model model) {
    final PaginationDto
<DictionaryDto> dictionaryPagination = ContextHolder.getBean(Dictionary
    Service.class).paginationByLikeCode(DictionaryDto.class, paginationDictionary);

    model.addAttribute("dictionaryPagination", dictionaryPagination);
    }

    /**
    * 字典：分页查询
    *
    * @param findDictionary
    *            字典对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionary/edit.html" })
    public void editHtml(final DictionaryDto findDictionary, final Model model) {
    final DictionaryDto dictionary = ContextHolder.getBean(Dictionary
    Service.class).findById(DictionaryDto.class, findDictionary);

    model.addAttribute("dictionary", dictionary);
    }

    /**
    * 字典：查询
    *
    * @param findListDictionary
    *            字典对象
    * @return 字典对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionary/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final DictionaryDto findListDictionary) {
    return () -> {
    final List<DictionaryDto> dictionaryList = ContextHolder.getBean(Dictionary
    Service.class).findListByLikeCode(DictionaryDto.class, findListDictionary);

    return ReturnDataUtils.newMap("dictionary", dictionaryList);
    };
    }

    /**
    * 字典：保存
    *
    * @param saveDictionary
    *            字典对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionary/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final DictionaryDto saveDictionary) {
    return () -> {
    ContextHolder.getBean(DictionaryService.class).saveByIdOrCode(DictionaryDto.class, saveDictionary);

    return ReturnDataUtils.newMap();
    };
    }
    }
