package com.cardone.platform.authority.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 角色与许可
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class RolePermission extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 286644578190078701L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 许可标识
    */
    private String permissionId;

    /**
    * 角色标识
    */
    private String roleId;
}