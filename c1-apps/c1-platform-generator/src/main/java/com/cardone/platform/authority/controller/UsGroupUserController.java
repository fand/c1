package com.cardone.platform.authority.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.authority.dto.UsGroupUserDto;
import com.cardone.platform.authority.service.UsGroupUserService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 用户组与用户
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class UsGroupUserController {
/**
* 用户组与用户：删除
*
* @param deleteUsGroupUser
*            用户组与用户对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/authority/usGroupUser/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final UsGroupUserDto deleteUsGroupUser) {
    return () -> {
    ContextHolder.getBean(UsGroupUserService.class).deleteByIds(deleteUsGroupUser);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 用户组与用户：编辑
    *
    * @param findUsGroupUser
    *            用户组与用户对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupUser/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final UsGroupUserDto findUsGroupUser, final Model model) {
    return () -> {
    final UsGroupUserDto usGroupUser = ContextHolder.getBean(UsGroupUser
    Service.class).findById(UsGroupUserDto.class, findUsGroupUser);

    return ReturnDataUtils.newMap("usGroupUser", usGroupUser);
    };
    }

    /**
    * 用户组与用户：分页查询
    *
    * @param paginationUsGroupUser
    *            用户组与用户对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupUser/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final UsGroupUserDto paginationUsGroupUser) {
    return () -> {
    final PaginationDto
<UsGroupUserDto> usGroupUserPagination = ContextHolder.getBean(UsGroupUser
    Service.class).paginationByLikeCode(UsGroupUserDto.class, paginationUsGroupUser);

    return ReturnDataUtils.newMap("usGroupUserPagination", usGroupUserPagination);
    };
    }

    /**
    * 用户组与用户：分页查询
    *
    * @param paginationUsGroupUser
    *            用户组与用户对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupUser/pagination.html" })
    public void paginationHtml(final UsGroupUserDto paginationUsGroupUser, final Model model) {
    final PaginationDto
<UsGroupUserDto> usGroupUserPagination = ContextHolder.getBean(UsGroupUser
    Service.class).paginationByLikeCode(UsGroupUserDto.class, paginationUsGroupUser);

    model.addAttribute("usGroupUserPagination", usGroupUserPagination);
    }

    /**
    * 用户组与用户：分页查询
    *
    * @param findUsGroupUser
    *            用户组与用户对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupUser/edit.html" })
    public void editHtml(final UsGroupUserDto findUsGroupUser, final Model model) {
    final UsGroupUserDto usGroupUser = ContextHolder.getBean(UsGroupUser
    Service.class).findById(UsGroupUserDto.class, findUsGroupUser);

    model.addAttribute("usGroupUser", usGroupUser);
    }

    /**
    * 用户组与用户：查询
    *
    * @param findListUsGroupUser
    *            用户组与用户对象
    * @return 用户组与用户对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupUser/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final UsGroupUserDto findListUsGroupUser) {
    return () -> {
    final List<UsGroupUserDto> usGroupUserList = ContextHolder.getBean(UsGroupUser
    Service.class).findListByLikeCode(UsGroupUserDto.class, findListUsGroupUser);

    return ReturnDataUtils.newMap("usGroupUser", usGroupUserList);
    };
    }

    /**
    * 用户组与用户：保存
    *
    * @param saveUsGroupUser
    *            用户组与用户对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGroupUser/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final UsGroupUserDto saveUsGroupUser) {
    return () -> {
    ContextHolder.getBean(UsGroupUserService.class).saveByIdOrCode(UsGroupUserDto.class, saveUsGroupUser);

    return ReturnDataUtils.newMap();
    };
    }
    }
