package com.cardone.platform.common.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 实体文件
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityFile extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 508201620973436592L;

    /**
    * 内容类型
    */
    private String contentType;

    /**
    * 实体标识
    */
    private String entityId;

    /**
    * 扩展
    */
    private String extension;

    /**
    * 文件名
    */
    private String filename;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 原文件名
    */
    private String originalFilename;

    /**
    * 父级标识
    */
    private String parentId;

    /**
    * 小图尺寸
    */
    private String thumbnailValue;

    /**
    * 字典.类别标识
    */
    private String typeId;
}