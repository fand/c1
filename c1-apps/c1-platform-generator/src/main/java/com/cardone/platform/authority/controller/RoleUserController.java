package com.cardone.platform.authority.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.authority.dto.RoleUserDto;
import com.cardone.platform.authority.service.RoleUserService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 角色与用户
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class RoleUserController {
/**
* 角色与用户：删除
*
* @param deleteRoleUser
*            角色与用户对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/authority/roleUser/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final RoleUserDto deleteRoleUser) {
    return () -> {
    ContextHolder.getBean(RoleUserService.class).deleteByIds(deleteRoleUser);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 角色与用户：编辑
    *
    * @param findRoleUser
    *            角色与用户对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/roleUser/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final RoleUserDto findRoleUser, final Model model) {
    return () -> {
    final RoleUserDto roleUser = ContextHolder.getBean(RoleUser
    Service.class).findById(RoleUserDto.class, findRoleUser);

    return ReturnDataUtils.newMap("roleUser", roleUser);
    };
    }

    /**
    * 角色与用户：分页查询
    *
    * @param paginationRoleUser
    *            角色与用户对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/roleUser/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final RoleUserDto paginationRoleUser) {
    return () -> {
    final PaginationDto
<RoleUserDto> roleUserPagination = ContextHolder.getBean(RoleUser
    Service.class).paginationByLikeCode(RoleUserDto.class, paginationRoleUser);

    return ReturnDataUtils.newMap("roleUserPagination", roleUserPagination);
    };
    }

    /**
    * 角色与用户：分页查询
    *
    * @param paginationRoleUser
    *            角色与用户对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/roleUser/pagination.html" })
    public void paginationHtml(final RoleUserDto paginationRoleUser, final Model model) {
    final PaginationDto
<RoleUserDto> roleUserPagination = ContextHolder.getBean(RoleUser
    Service.class).paginationByLikeCode(RoleUserDto.class, paginationRoleUser);

    model.addAttribute("roleUserPagination", roleUserPagination);
    }

    /**
    * 角色与用户：分页查询
    *
    * @param findRoleUser
    *            角色与用户对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/roleUser/edit.html" })
    public void editHtml(final RoleUserDto findRoleUser, final Model model) {
    final RoleUserDto roleUser = ContextHolder.getBean(RoleUser
    Service.class).findById(RoleUserDto.class, findRoleUser);

    model.addAttribute("roleUser", roleUser);
    }

    /**
    * 角色与用户：查询
    *
    * @param findListRoleUser
    *            角色与用户对象
    * @return 角色与用户对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/roleUser/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final RoleUserDto findListRoleUser) {
    return () -> {
    final List<RoleUserDto> roleUserList = ContextHolder.getBean(RoleUser
    Service.class).findListByLikeCode(RoleUserDto.class, findListRoleUser);

    return ReturnDataUtils.newMap("roleUser", roleUserList);
    };
    }

    /**
    * 角色与用户：保存
    *
    * @param saveRoleUser
    *            角色与用户对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/roleUser/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final RoleUserDto saveRoleUser) {
    return () -> {
    ContextHolder.getBean(RoleUserService.class).saveByIdOrCode(RoleUserDto.class, saveRoleUser);

    return ReturnDataUtils.newMap();
    };
    }
    }
