package com.cardone.platform.usercenter.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.usercenter.dto.UserDto;
import com.cardone.platform.usercenter.service.UserService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 用户
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class UserController {
/**
* 用户：删除
*
* @param deleteUser
*            用户对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/usercenter/user/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final UserDto deleteUser) {
    return () -> {
    ContextHolder.getBean(UserService.class).deleteByIds(deleteUser);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 用户：编辑
    *
    * @param findUser
    *            用户对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/user/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final UserDto findUser, final Model model) {
    return () -> {
    final UserDto user = ContextHolder.getBean(User
    Service.class).findById(UserDto.class, findUser);

    return ReturnDataUtils.newMap("user", user);
    };
    }

    /**
    * 用户：分页查询
    *
    * @param paginationUser
    *            用户对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/user/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final UserDto paginationUser) {
    return () -> {
    final PaginationDto
<UserDto> userPagination = ContextHolder.getBean(User
    Service.class).paginationByLikeCode(UserDto.class, paginationUser);

    return ReturnDataUtils.newMap("userPagination", userPagination);
    };
    }

    /**
    * 用户：分页查询
    *
    * @param paginationUser
    *            用户对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/user/pagination.html" })
    public void paginationHtml(final UserDto paginationUser, final Model model) {
    final PaginationDto
<UserDto> userPagination = ContextHolder.getBean(User
    Service.class).paginationByLikeCode(UserDto.class, paginationUser);

    model.addAttribute("userPagination", userPagination);
    }

    /**
    * 用户：分页查询
    *
    * @param findUser
    *            用户对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/user/edit.html" })
    public void editHtml(final UserDto findUser, final Model model) {
    final UserDto user = ContextHolder.getBean(User
    Service.class).findById(UserDto.class, findUser);

    model.addAttribute("user", user);
    }

    /**
    * 用户：查询
    *
    * @param findListUser
    *            用户对象
    * @return 用户对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/user/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final UserDto findListUser) {
    return () -> {
    final List<UserDto> userList = ContextHolder.getBean(User
    Service.class).findListByLikeCode(UserDto.class, findListUser);

    return ReturnDataUtils.newMap("user", userList);
    };
    }

    /**
    * 用户：保存
    *
    * @param saveUser
    *            用户对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/user/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final UserDto saveUser) {
    return () -> {
    ContextHolder.getBean(UserService.class).saveByIdOrCode(UserDto.class, saveUser);

    return ReturnDataUtils.newMap();
    };
    }
    }
