package com.cardone.platform.configuration.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.ArrayUtils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.beanutils.PropertyUtils;

import org.joda.time.DateTime;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.collections.MapUtils;
import com.google.common.collect.Maps;

import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.EntityUtils;
import com.cardone.context.Attributes;
import com.cardone.persistent.builder.ModelArgs;
import com.cardone.persistent.builder.Model;
import com.cardone.persistent.builder.ModelUtils;
import com.cardone.persistent.support.JdbcTemplateSupport;
import com.cardone.persistent.support.PaginationArgs;
import com.cardone.context.ContextHolder;
import com.cardone.context.DictionaryException;
import com.cardone.common.dao.SimpleDao;
import com.cardone.persistent.support.SimpleJdbcDao;

import java.lang.reflect.InvocationTargetException;

/**
* 配置
*
* @author yaohaitao
*
*/
@Getter
@Setter
@Slf4j
    public class ConfigurationJdbcDao implements ConfigurationDao {
    }