package com.cardone.platform.authority.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.authority.po.UserGroup;

/**
* 用户组
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UserGroupDto extends UserGroup {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 390016480219003290L;
}