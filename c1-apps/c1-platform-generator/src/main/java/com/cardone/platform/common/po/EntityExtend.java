package com.cardone.platform.common.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 实体扩展
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityExtend extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 424312098353028825L;

    /**
    * 实体标识
    */
    private String entityId;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 说明
    */
    private String remark;

    /**
    * 字典.类别标识
    */
    private String typeId;

    /**
    * 值
    */
    private String value;
}