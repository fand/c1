package com.cardone.platform.authority.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.authority.po.PermissionOrg;

/**
* 许可与组织
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class PermissionOrgDto extends PermissionOrg {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 565608069470076978L;
}