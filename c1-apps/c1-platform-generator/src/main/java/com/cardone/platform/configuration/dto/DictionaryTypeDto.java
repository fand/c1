package com.cardone.platform.configuration.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.configuration.po.DictionaryType;

/**
* 字典类型
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class DictionaryTypeDto extends DictionaryType {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 884976870052015032L;
}