package com.cardone.platform.authority.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 许可与组织
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class PermissionOrg extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 106565838149193210L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 组织标识
    */
    private String orgId;

    /**
    * 许可标识
    */
    private String permissionId;
}