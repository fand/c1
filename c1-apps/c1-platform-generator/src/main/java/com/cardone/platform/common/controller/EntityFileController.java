package com.cardone.platform.common.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.common.dto.EntityFileDto;
import com.cardone.platform.common.service.EntityFileService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 实体文件
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class EntityFileController {
/**
* 实体文件：删除
*
* @param deleteEntityFile
*            实体文件对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/common/entityFile/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final EntityFileDto deleteEntityFile) {
    return () -> {
    ContextHolder.getBean(EntityFileService.class).deleteByIds(deleteEntityFile);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 实体文件：编辑
    *
    * @param findEntityFile
    *            实体文件对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityFile/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final EntityFileDto findEntityFile, final Model model) {
    return () -> {
    final EntityFileDto entityFile = ContextHolder.getBean(EntityFile
    Service.class).findById(EntityFileDto.class, findEntityFile);

    return ReturnDataUtils.newMap("entityFile", entityFile);
    };
    }

    /**
    * 实体文件：分页查询
    *
    * @param paginationEntityFile
    *            实体文件对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityFile/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final EntityFileDto paginationEntityFile) {
    return () -> {
    final PaginationDto
<EntityFileDto> entityFilePagination = ContextHolder.getBean(EntityFile
    Service.class).paginationByLikeCode(EntityFileDto.class, paginationEntityFile);

    return ReturnDataUtils.newMap("entityFilePagination", entityFilePagination);
    };
    }

    /**
    * 实体文件：分页查询
    *
    * @param paginationEntityFile
    *            实体文件对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityFile/pagination.html" })
    public void paginationHtml(final EntityFileDto paginationEntityFile, final Model model) {
    final PaginationDto
<EntityFileDto> entityFilePagination = ContextHolder.getBean(EntityFile
    Service.class).paginationByLikeCode(EntityFileDto.class, paginationEntityFile);

    model.addAttribute("entityFilePagination", entityFilePagination);
    }

    /**
    * 实体文件：分页查询
    *
    * @param findEntityFile
    *            实体文件对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityFile/edit.html" })
    public void editHtml(final EntityFileDto findEntityFile, final Model model) {
    final EntityFileDto entityFile = ContextHolder.getBean(EntityFile
    Service.class).findById(EntityFileDto.class, findEntityFile);

    model.addAttribute("entityFile", entityFile);
    }

    /**
    * 实体文件：查询
    *
    * @param findListEntityFile
    *            实体文件对象
    * @return 实体文件对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityFile/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final EntityFileDto findListEntityFile) {
    return () -> {
    final List<EntityFileDto> entityFileList = ContextHolder.getBean(EntityFile
    Service.class).findListByLikeCode(EntityFileDto.class, findListEntityFile);

    return ReturnDataUtils.newMap("entityFile", entityFileList);
    };
    }

    /**
    * 实体文件：保存
    *
    * @param saveEntityFile
    *            实体文件对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/common/entityFile/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final EntityFileDto saveEntityFile) {
    return () -> {
    ContextHolder.getBean(EntityFileService.class).saveByIdOrCode(EntityFileDto.class, saveEntityFile);

    return ReturnDataUtils.newMap();
    };
    }
    }
