package com.cardone.platform.usercenter.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 用户组织调动
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsOrgTranslate extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 290936550919574807L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 组织标识
    */
    private String orgId;

    /**
    * 用户标识
    */
    private String userId;
}