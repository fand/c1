package com.cardone.platform.configuration.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 字典
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class Dictionary extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 371263470188167029L;

    /**
    * 解释
    */
    private String explainText;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 父级代码
    */
    private String parentCode;

    /**
    * 父级标识
    */
    private String parentId;

    /**
    * 父级名称
    */
    private String parentName;

    /**
    * 说明
    */
    private String remark;

    /**
    * 树代码
    */
    private String treeCode;

    /**
    * 树标识
    */
    private String treeId;

    /**
    * 树名称
    */
    private String treeName;

    /**
    * 类别代码
    */
    private String typeCode;

    /**
    * 字典类型.类型标识
    */
    private String typeId;

    /**
    * 类别名称
    */
    private String typeName;

    /**
    * 类别树代码
    */
    private String typeTreeCode;

    /**
    * 类别树标识
    */
    private String typeTreeId;

    /**
    * 类别树名称
    */
    private String typeTreeName;

    /**
    * 值
    */
    private String value;
}