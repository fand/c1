package com.cardone.platform.authority.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 导航
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class Navigation extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 763649194424959726L;

    /**
    * 数据选项
    */
    private String dataOption;

    /**
    * 图标样式
    */
    private String iconStyle;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 父级代码
    */
    private String parentCode;

    /**
    * 父级标识
    */
    private String parentId;

    /**
    * 父级名称
    */
    private String parentName;

    /**
    * 站点代码
    */
    private String siteCode;

    /**
    * 站点标识
    */
    private String siteId;

    /**
    * 站点名称
    */
    private String siteName;

    /**
    * 目标
    */
    private String target;

    /**
    * 树代码
    */
    private String treeCode;

    /**
    * 树标识
    */
    private String treeId;

    /**
    * 树名称
    */
    private String treeName;

    /**
    * 类别代码
    */
    private String typeCode;

    /**
    * 类别标识
    */
    private String typeId;

    /**
    * 类别名称
    */
    private String typeName;

    /**
    * URL
    */
    private String url;
}