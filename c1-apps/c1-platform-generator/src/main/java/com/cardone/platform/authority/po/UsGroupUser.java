package com.cardone.platform.authority.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 用户组与用户
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGroupUser extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 224074373274142210L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 用户组标识
    */
    private String userGroupId;

    /**
    * 用户标识
    */
    private String userId;
}