package com.cardone.platform.authority.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.authority.dto.UsGrBusinessAreaDto;
import com.cardone.platform.authority.service.UsGrBusinessAreaService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 用户组与业务范围
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class UsGrBusinessAreaController {
/**
* 用户组与业务范围：删除
*
* @param deleteUsGrBusinessArea
*            用户组与业务范围对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/authority/usGrBusinessArea/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final UsGrBusinessAreaDto deleteUsGrBusinessArea) {
    return () -> {
    ContextHolder.getBean(UsGrBusinessAreaService.class).deleteByIds(deleteUsGrBusinessArea);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 用户组与业务范围：编辑
    *
    * @param findUsGrBusinessArea
    *            用户组与业务范围对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGrBusinessArea/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final UsGrBusinessAreaDto findUsGrBusinessArea, final Model model) {
    return () -> {
    final UsGrBusinessAreaDto usGrBusinessArea = ContextHolder.getBean(UsGrBusinessArea
    Service.class).findById(UsGrBusinessAreaDto.class, findUsGrBusinessArea);

    return ReturnDataUtils.newMap("usGrBusinessArea", usGrBusinessArea);
    };
    }

    /**
    * 用户组与业务范围：分页查询
    *
    * @param paginationUsGrBusinessArea
    *            用户组与业务范围对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGrBusinessArea/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final UsGrBusinessAreaDto paginationUsGrBusinessArea) {
    return () -> {
    final PaginationDto
<UsGrBusinessAreaDto> usGrBusinessAreaPagination = ContextHolder.getBean(UsGrBusinessArea
    Service.class).paginationByLikeCode(UsGrBusinessAreaDto.class, paginationUsGrBusinessArea);

    return ReturnDataUtils.newMap("usGrBusinessAreaPagination", usGrBusinessAreaPagination);
    };
    }

    /**
    * 用户组与业务范围：分页查询
    *
    * @param paginationUsGrBusinessArea
    *            用户组与业务范围对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGrBusinessArea/pagination.html" })
    public void paginationHtml(final UsGrBusinessAreaDto paginationUsGrBusinessArea, final Model model) {
    final PaginationDto
<UsGrBusinessAreaDto> usGrBusinessAreaPagination = ContextHolder.getBean(UsGrBusinessArea
    Service.class).paginationByLikeCode(UsGrBusinessAreaDto.class, paginationUsGrBusinessArea);

    model.addAttribute("usGrBusinessAreaPagination", usGrBusinessAreaPagination);
    }

    /**
    * 用户组与业务范围：分页查询
    *
    * @param findUsGrBusinessArea
    *            用户组与业务范围对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGrBusinessArea/edit.html" })
    public void editHtml(final UsGrBusinessAreaDto findUsGrBusinessArea, final Model model) {
    final UsGrBusinessAreaDto usGrBusinessArea = ContextHolder.getBean(UsGrBusinessArea
    Service.class).findById(UsGrBusinessAreaDto.class, findUsGrBusinessArea);

    model.addAttribute("usGrBusinessArea", usGrBusinessArea);
    }

    /**
    * 用户组与业务范围：查询
    *
    * @param findListUsGrBusinessArea
    *            用户组与业务范围对象
    * @return 用户组与业务范围对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGrBusinessArea/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final UsGrBusinessAreaDto findListUsGrBusinessArea) {
    return () -> {
    final List<UsGrBusinessAreaDto> usGrBusinessAreaList = ContextHolder.getBean(UsGrBusinessArea
    Service.class).findListByLikeCode(UsGrBusinessAreaDto.class, findListUsGrBusinessArea);

    return ReturnDataUtils.newMap("usGrBusinessArea", usGrBusinessAreaList);
    };
    }

    /**
    * 用户组与业务范围：保存
    *
    * @param saveUsGrBusinessArea
    *            用户组与业务范围对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/usGrBusinessArea/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final UsGrBusinessAreaDto saveUsGrBusinessArea) {
    return () -> {
    ContextHolder.getBean(UsGrBusinessAreaService.class).saveByIdOrCode(UsGrBusinessAreaDto.class, saveUsGrBusinessArea);

    return ReturnDataUtils.newMap();
    };
    }
    }
