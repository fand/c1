package com.cardone.platform.usercenter.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.usercenter.dto.UsOrgTranslateDto;
import com.cardone.platform.usercenter.service.UsOrgTranslateService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 用户组织调动
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class UsOrgTranslateController {
/**
* 用户组织调动：删除
*
* @param deleteUsOrgTranslate
*            用户组织调动对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/usercenter/usOrgTranslate/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final UsOrgTranslateDto deleteUsOrgTranslate) {
    return () -> {
    ContextHolder.getBean(UsOrgTranslateService.class).deleteByIds(deleteUsOrgTranslate);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 用户组织调动：编辑
    *
    * @param findUsOrgTranslate
    *            用户组织调动对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/usOrgTranslate/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final UsOrgTranslateDto findUsOrgTranslate, final Model model) {
    return () -> {
    final UsOrgTranslateDto usOrgTranslate = ContextHolder.getBean(UsOrgTranslate
    Service.class).findById(UsOrgTranslateDto.class, findUsOrgTranslate);

    return ReturnDataUtils.newMap("usOrgTranslate", usOrgTranslate);
    };
    }

    /**
    * 用户组织调动：分页查询
    *
    * @param paginationUsOrgTranslate
    *            用户组织调动对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/usOrgTranslate/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final UsOrgTranslateDto paginationUsOrgTranslate) {
    return () -> {
    final PaginationDto
<UsOrgTranslateDto> usOrgTranslatePagination = ContextHolder.getBean(UsOrgTranslate
    Service.class).paginationByLikeCode(UsOrgTranslateDto.class, paginationUsOrgTranslate);

    return ReturnDataUtils.newMap("usOrgTranslatePagination", usOrgTranslatePagination);
    };
    }

    /**
    * 用户组织调动：分页查询
    *
    * @param paginationUsOrgTranslate
    *            用户组织调动对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/usOrgTranslate/pagination.html" })
    public void paginationHtml(final UsOrgTranslateDto paginationUsOrgTranslate, final Model model) {
    final PaginationDto
<UsOrgTranslateDto> usOrgTranslatePagination = ContextHolder.getBean(UsOrgTranslate
    Service.class).paginationByLikeCode(UsOrgTranslateDto.class, paginationUsOrgTranslate);

    model.addAttribute("usOrgTranslatePagination", usOrgTranslatePagination);
    }

    /**
    * 用户组织调动：分页查询
    *
    * @param findUsOrgTranslate
    *            用户组织调动对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/usOrgTranslate/edit.html" })
    public void editHtml(final UsOrgTranslateDto findUsOrgTranslate, final Model model) {
    final UsOrgTranslateDto usOrgTranslate = ContextHolder.getBean(UsOrgTranslate
    Service.class).findById(UsOrgTranslateDto.class, findUsOrgTranslate);

    model.addAttribute("usOrgTranslate", usOrgTranslate);
    }

    /**
    * 用户组织调动：查询
    *
    * @param findListUsOrgTranslate
    *            用户组织调动对象
    * @return 用户组织调动对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/usOrgTranslate/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final UsOrgTranslateDto findListUsOrgTranslate) {
    return () -> {
    final List<UsOrgTranslateDto> usOrgTranslateList = ContextHolder.getBean(UsOrgTranslate
    Service.class).findListByLikeCode(UsOrgTranslateDto.class, findListUsOrgTranslate);

    return ReturnDataUtils.newMap("usOrgTranslate", usOrgTranslateList);
    };
    }

    /**
    * 用户组织调动：保存
    *
    * @param saveUsOrgTranslate
    *            用户组织调动对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/usOrgTranslate/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final UsOrgTranslateDto saveUsOrgTranslate) {
    return () -> {
    ContextHolder.getBean(UsOrgTranslateService.class).saveByIdOrCode(UsOrgTranslateDto.class, saveUsOrgTranslate);

    return ReturnDataUtils.newMap();
    };
    }
    }
