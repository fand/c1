package com.cardone.platform.configuration.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 站点位置
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class SiteUrl extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 131620588638995875L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 站点代码
    */
    private String siteCode;

    /**
    * 站点标识
    */
    private String siteId;

    /**
    * 站点名称
    */
    private String siteName;
}