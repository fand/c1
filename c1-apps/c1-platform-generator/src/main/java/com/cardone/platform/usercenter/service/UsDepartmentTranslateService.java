package com.cardone.platform.usercenter.service;

import java.util.List;
import java.util.Map;

import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import com.cardone.common.service.SimpleService;


import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
import com.cardone.platform.usercenter.dto.UsDepartmentTranslateDto;

/**
* 用户部门调动服务
*
* @author yaohaitao
*/
public interface UsDepartmentTranslateService extends SimpleService
<UsDepartmentTranslateDto> {
    /**
    * spring bean id
    */
    String BEAN_ID = "com.cardone.platform.usercenter.service.UsDepartmentTranslateService";
    }