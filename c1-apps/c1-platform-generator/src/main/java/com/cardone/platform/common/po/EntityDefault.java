package com.cardone.platform.common.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 实体与默认
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityDefault extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 142397707638550210L;

    /**
    * 实体标识
    */
    private String entityId;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 字典.类别标识
    */
    private String typeId;
}