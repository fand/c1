package com.cardone.platform.usercenter.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.usercenter.dto.DepartmentDto;
import com.cardone.platform.usercenter.service.DepartmentService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 部门
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class DepartmentController {
/**
* 部门：删除
*
* @param deleteDepartment
*            部门对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/usercenter/department/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final DepartmentDto deleteDepartment) {
    return () -> {
    ContextHolder.getBean(DepartmentService.class).deleteByIds(deleteDepartment);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 部门：编辑
    *
    * @param findDepartment
    *            部门对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/department/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final DepartmentDto findDepartment, final Model model) {
    return () -> {
    final DepartmentDto department = ContextHolder.getBean(Department
    Service.class).findById(DepartmentDto.class, findDepartment);

    return ReturnDataUtils.newMap("department", department);
    };
    }

    /**
    * 部门：分页查询
    *
    * @param paginationDepartment
    *            部门对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/department/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final DepartmentDto paginationDepartment) {
    return () -> {
    final PaginationDto
<DepartmentDto> departmentPagination = ContextHolder.getBean(Department
    Service.class).paginationByLikeCode(DepartmentDto.class, paginationDepartment);

    return ReturnDataUtils.newMap("departmentPagination", departmentPagination);
    };
    }

    /**
    * 部门：分页查询
    *
    * @param paginationDepartment
    *            部门对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/department/pagination.html" })
    public void paginationHtml(final DepartmentDto paginationDepartment, final Model model) {
    final PaginationDto
<DepartmentDto> departmentPagination = ContextHolder.getBean(Department
    Service.class).paginationByLikeCode(DepartmentDto.class, paginationDepartment);

    model.addAttribute("departmentPagination", departmentPagination);
    }

    /**
    * 部门：分页查询
    *
    * @param findDepartment
    *            部门对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/department/edit.html" })
    public void editHtml(final DepartmentDto findDepartment, final Model model) {
    final DepartmentDto department = ContextHolder.getBean(Department
    Service.class).findById(DepartmentDto.class, findDepartment);

    model.addAttribute("department", department);
    }

    /**
    * 部门：查询
    *
    * @param findListDepartment
    *            部门对象
    * @return 部门对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/department/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final DepartmentDto findListDepartment) {
    return () -> {
    final List<DepartmentDto> departmentList = ContextHolder.getBean(Department
    Service.class).findListByLikeCode(DepartmentDto.class, findListDepartment);

    return ReturnDataUtils.newMap("department", departmentList);
    };
    }

    /**
    * 部门：保存
    *
    * @param saveDepartment
    *            部门对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/usercenter/department/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final DepartmentDto saveDepartment) {
    return () -> {
    ContextHolder.getBean(DepartmentService.class).saveByIdOrCode(DepartmentDto.class, saveDepartment);

    return ReturnDataUtils.newMap();
    };
    }
    }
