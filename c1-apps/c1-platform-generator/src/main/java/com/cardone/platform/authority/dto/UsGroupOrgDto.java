package com.cardone.platform.authority.dto;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import org.springframework.data.annotation.Transient;

import com.cardone.platform.authority.po.UsGroupOrg;

/**
* 用户组与组织
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGroupOrgDto extends UsGroupOrg {
/**
* 版本号
*/
@Transient
private static final long serialVersionUID = 553060389021103206L;
}