package com.cardone.platform.authority.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.authority.dto.PermissionDto;
import com.cardone.platform.authority.service.PermissionService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 许可
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class PermissionController {
/**
* 许可：删除
*
* @param deletePermission
*            许可对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/authority/permission/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final PermissionDto deletePermission) {
    return () -> {
    ContextHolder.getBean(PermissionService.class).deleteByIds(deletePermission);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 许可：编辑
    *
    * @param findPermission
    *            许可对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permission/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final PermissionDto findPermission, final Model model) {
    return () -> {
    final PermissionDto permission = ContextHolder.getBean(Permission
    Service.class).findById(PermissionDto.class, findPermission);

    return ReturnDataUtils.newMap("permission", permission);
    };
    }

    /**
    * 许可：分页查询
    *
    * @param paginationPermission
    *            许可对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permission/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final PermissionDto paginationPermission) {
    return () -> {
    final PaginationDto
<PermissionDto> permissionPagination = ContextHolder.getBean(Permission
    Service.class).paginationByLikeCode(PermissionDto.class, paginationPermission);

    return ReturnDataUtils.newMap("permissionPagination", permissionPagination);
    };
    }

    /**
    * 许可：分页查询
    *
    * @param paginationPermission
    *            许可对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permission/pagination.html" })
    public void paginationHtml(final PermissionDto paginationPermission, final Model model) {
    final PaginationDto
<PermissionDto> permissionPagination = ContextHolder.getBean(Permission
    Service.class).paginationByLikeCode(PermissionDto.class, paginationPermission);

    model.addAttribute("permissionPagination", permissionPagination);
    }

    /**
    * 许可：分页查询
    *
    * @param findPermission
    *            许可对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permission/edit.html" })
    public void editHtml(final PermissionDto findPermission, final Model model) {
    final PermissionDto permission = ContextHolder.getBean(Permission
    Service.class).findById(PermissionDto.class, findPermission);

    model.addAttribute("permission", permission);
    }

    /**
    * 许可：查询
    *
    * @param findListPermission
    *            许可对象
    * @return 许可对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permission/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final PermissionDto findListPermission) {
    return () -> {
    final List<PermissionDto> permissionList = ContextHolder.getBean(Permission
    Service.class).findListByLikeCode(PermissionDto.class, findListPermission);

    return ReturnDataUtils.newMap("permission", permissionList);
    };
    }

    /**
    * 许可：保存
    *
    * @param savePermission
    *            许可对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/permission/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final PermissionDto savePermission) {
    return () -> {
    ContextHolder.getBean(PermissionService.class).saveByIdOrCode(PermissionDto.class, savePermission);

    return ReturnDataUtils.newMap();
    };
    }
    }
