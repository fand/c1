package com.cardone.platform.common.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 实体编号
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class EntityCode extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 879510014462496679L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 类别代码
    */
    private String typeCode;
}