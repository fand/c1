package com.cardone.platform.authority.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.authority.dto.RoleDto;
import com.cardone.platform.authority.service.RoleService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 角色
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class RoleController {
/**
* 角色：删除
*
* @param deleteRole
*            角色对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/authority/role/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final RoleDto deleteRole) {
    return () -> {
    ContextHolder.getBean(RoleService.class).deleteByIds(deleteRole);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 角色：编辑
    *
    * @param findRole
    *            角色对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/role/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final RoleDto findRole, final Model model) {
    return () -> {
    final RoleDto role = ContextHolder.getBean(Role
    Service.class).findById(RoleDto.class, findRole);

    return ReturnDataUtils.newMap("role", role);
    };
    }

    /**
    * 角色：分页查询
    *
    * @param paginationRole
    *            角色对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/role/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final RoleDto paginationRole) {
    return () -> {
    final PaginationDto
<RoleDto> rolePagination = ContextHolder.getBean(Role
    Service.class).paginationByLikeCode(RoleDto.class, paginationRole);

    return ReturnDataUtils.newMap("rolePagination", rolePagination);
    };
    }

    /**
    * 角色：分页查询
    *
    * @param paginationRole
    *            角色对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/role/pagination.html" })
    public void paginationHtml(final RoleDto paginationRole, final Model model) {
    final PaginationDto
<RoleDto> rolePagination = ContextHolder.getBean(Role
    Service.class).paginationByLikeCode(RoleDto.class, paginationRole);

    model.addAttribute("rolePagination", rolePagination);
    }

    /**
    * 角色：分页查询
    *
    * @param findRole
    *            角色对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/role/edit.html" })
    public void editHtml(final RoleDto findRole, final Model model) {
    final RoleDto role = ContextHolder.getBean(Role
    Service.class).findById(RoleDto.class, findRole);

    model.addAttribute("role", role);
    }

    /**
    * 角色：查询
    *
    * @param findListRole
    *            角色对象
    * @return 角色对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/role/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final RoleDto findListRole) {
    return () -> {
    final List<RoleDto> roleList = ContextHolder.getBean(Role
    Service.class).findListByLikeCode(RoleDto.class, findListRole);

    return ReturnDataUtils.newMap("role", roleList);
    };
    }

    /**
    * 角色：保存
    *
    * @param saveRole
    *            角色对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/role/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final RoleDto saveRole) {
    return () -> {
    ContextHolder.getBean(RoleService.class).saveByIdOrCode(RoleDto.class, saveRole);

    return ReturnDataUtils.newMap();
    };
    }
    }
