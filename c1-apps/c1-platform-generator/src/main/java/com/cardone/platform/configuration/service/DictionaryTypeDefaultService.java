package com.cardone.platform.configuration.service;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;


import lombok.Getter;


import lombok.Setter;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.transaction.annotation.Transactional;

import com.cardone.common.dao.SimpleDao;
import com.cardone.common.service.SimpleDefaultService;

import com.cardone.common.dto.PaginationDto;
import com.cardone.common.cache.Caches;
import com.cardone.context.Attributes;
import com.cardone.context.ContextHolder;
import com.cardone.common.util.EntityUtils;
import com.cardone.platform.configuration.dao.DictionaryTypeDao;
import com.cardone.platform.configuration.dto.DictionaryTypeDto;

/**
* 字典类型服务
*
* @author yaohaitao
*
*/
@Getter
@Setter
@Transactional(readOnly = true)
public class DictionaryTypeDefaultService extends SimpleDefaultService
<DictionaryTypeDto> implements DictionaryTypeService {
    @Override
    public DictionaryTypeDao getDao() {
    return ContextHolder.getBean(DictionaryTypeDao.class);
    }
    }