package com.cardone.platform.authority.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.authority.dto.RolePermissionDto;
import com.cardone.platform.authority.service.RolePermissionService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 角色与许可
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class RolePermissionController {
/**
* 角色与许可：删除
*
* @param deleteRolePermission
*            角色与许可对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/authority/rolePermission/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final RolePermissionDto deleteRolePermission) {
    return () -> {
    ContextHolder.getBean(RolePermissionService.class).deleteByIds(deleteRolePermission);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 角色与许可：编辑
    *
    * @param findRolePermission
    *            角色与许可对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/rolePermission/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final RolePermissionDto findRolePermission, final Model model) {
    return () -> {
    final RolePermissionDto rolePermission = ContextHolder.getBean(RolePermission
    Service.class).findById(RolePermissionDto.class, findRolePermission);

    return ReturnDataUtils.newMap("rolePermission", rolePermission);
    };
    }

    /**
    * 角色与许可：分页查询
    *
    * @param paginationRolePermission
    *            角色与许可对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/rolePermission/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final RolePermissionDto paginationRolePermission) {
    return () -> {
    final PaginationDto
<RolePermissionDto> rolePermissionPagination = ContextHolder.getBean(RolePermission
    Service.class).paginationByLikeCode(RolePermissionDto.class, paginationRolePermission);

    return ReturnDataUtils.newMap("rolePermissionPagination", rolePermissionPagination);
    };
    }

    /**
    * 角色与许可：分页查询
    *
    * @param paginationRolePermission
    *            角色与许可对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/rolePermission/pagination.html" })
    public void paginationHtml(final RolePermissionDto paginationRolePermission, final Model model) {
    final PaginationDto
<RolePermissionDto> rolePermissionPagination = ContextHolder.getBean(RolePermission
    Service.class).paginationByLikeCode(RolePermissionDto.class, paginationRolePermission);

    model.addAttribute("rolePermissionPagination", rolePermissionPagination);
    }

    /**
    * 角色与许可：分页查询
    *
    * @param findRolePermission
    *            角色与许可对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/rolePermission/edit.html" })
    public void editHtml(final RolePermissionDto findRolePermission, final Model model) {
    final RolePermissionDto rolePermission = ContextHolder.getBean(RolePermission
    Service.class).findById(RolePermissionDto.class, findRolePermission);

    model.addAttribute("rolePermission", rolePermission);
    }

    /**
    * 角色与许可：查询
    *
    * @param findListRolePermission
    *            角色与许可对象
    * @return 角色与许可对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/rolePermission/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final RolePermissionDto findListRolePermission) {
    return () -> {
    final List<RolePermissionDto> rolePermissionList = ContextHolder.getBean(RolePermission
    Service.class).findListByLikeCode(RolePermissionDto.class, findListRolePermission);

    return ReturnDataUtils.newMap("rolePermission", rolePermissionList);
    };
    }

    /**
    * 角色与许可：保存
    *
    * @param saveRolePermission
    *            角色与许可对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/rolePermission/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final RolePermissionDto saveRolePermission) {
    return () -> {
    ContextHolder.getBean(RolePermissionService.class).saveByIdOrCode(RolePermissionDto.class, saveRolePermission);

    return ReturnDataUtils.newMap();
    };
    }
    }
