package com.cardone.platform.authority.po;

    import java.util.Date;

    import org.springframework.data.annotation.Id;


import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import com.cardone.common.po.PoBase;

/**
* 用户组与角色
*
* @author yaohaitao
*
*/
@Getter
@Setter
@lombok.ToString(callSuper = true)
@Accessors(chain = true)
public class UsGroupRole extends PoBase {
/**
* 版本号
*/
private static final long serialVersionUID = 786634423293952600L;

    /**
    * 标识
    */
        @Id
    private String id;

    /**
    * 角色标识
    */
    private String roleId;

    /**
    * 用户组标识
    */
    private String userGroupId;
}