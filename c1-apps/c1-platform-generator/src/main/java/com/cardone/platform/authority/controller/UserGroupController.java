package com.cardone.platform.authority.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.authority.dto.UserGroupDto;
import com.cardone.platform.authority.service.UserGroupService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 用户组
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class UserGroupController {
/**
* 用户组：删除
*
* @param deleteUserGroup
*            用户组对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/authority/userGroup/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final UserGroupDto deleteUserGroup) {
    return () -> {
    ContextHolder.getBean(UserGroupService.class).deleteByIds(deleteUserGroup);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 用户组：编辑
    *
    * @param findUserGroup
    *            用户组对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/userGroup/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final UserGroupDto findUserGroup, final Model model) {
    return () -> {
    final UserGroupDto userGroup = ContextHolder.getBean(UserGroup
    Service.class).findById(UserGroupDto.class, findUserGroup);

    return ReturnDataUtils.newMap("userGroup", userGroup);
    };
    }

    /**
    * 用户组：分页查询
    *
    * @param paginationUserGroup
    *            用户组对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/userGroup/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final UserGroupDto paginationUserGroup) {
    return () -> {
    final PaginationDto
<UserGroupDto> userGroupPagination = ContextHolder.getBean(UserGroup
    Service.class).paginationByLikeCode(UserGroupDto.class, paginationUserGroup);

    return ReturnDataUtils.newMap("userGroupPagination", userGroupPagination);
    };
    }

    /**
    * 用户组：分页查询
    *
    * @param paginationUserGroup
    *            用户组对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/userGroup/pagination.html" })
    public void paginationHtml(final UserGroupDto paginationUserGroup, final Model model) {
    final PaginationDto
<UserGroupDto> userGroupPagination = ContextHolder.getBean(UserGroup
    Service.class).paginationByLikeCode(UserGroupDto.class, paginationUserGroup);

    model.addAttribute("userGroupPagination", userGroupPagination);
    }

    /**
    * 用户组：分页查询
    *
    * @param findUserGroup
    *            用户组对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/authority/userGroup/edit.html" })
    public void editHtml(final UserGroupDto findUserGroup, final Model model) {
    final UserGroupDto userGroup = ContextHolder.getBean(UserGroup
    Service.class).findById(UserGroupDto.class, findUserGroup);

    model.addAttribute("userGroup", userGroup);
    }

    /**
    * 用户组：查询
    *
    * @param findListUserGroup
    *            用户组对象
    * @return 用户组对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/userGroup/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final UserGroupDto findListUserGroup) {
    return () -> {
    final List<UserGroupDto> userGroupList = ContextHolder.getBean(UserGroup
    Service.class).findListByLikeCode(UserGroupDto.class, findListUserGroup);

    return ReturnDataUtils.newMap("userGroup", userGroupList);
    };
    }

    /**
    * 用户组：保存
    *
    * @param saveUserGroup
    *            用户组对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/authority/userGroup/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final UserGroupDto saveUserGroup) {
    return () -> {
    ContextHolder.getBean(UserGroupService.class).saveByIdOrCode(UserGroupDto.class, saveUserGroup);

    return ReturnDataUtils.newMap();
    };
    }
    }
