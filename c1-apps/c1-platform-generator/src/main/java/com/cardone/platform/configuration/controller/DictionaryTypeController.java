package com.cardone.platform.configuration.controller;

import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cardone.context.Contexts;
import com.cardone.common.dto.PaginationDto;
import com.cardone.common.util.ReturnDataUtils;
import com.cardone.context.ContextHolder;
import com.cardone.platform.configuration.dto.DictionaryTypeDto;
import com.cardone.platform.configuration.service.DictionaryTypeService;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
* 字典类型
*
* @author yaohaitao
*/
@Setter
@Slf4j
@Controller
public class DictionaryTypeController {
/**
* 字典类型：删除
*
* @param deleteDictionaryType
*            字典类型对象
* @return 处理结果，json格式
*/
@Deprecated
@RequestMapping(value = { "/configuration/dictionaryType/delete.json" }, method = RequestMethod.POST)
@ResponseBody
public Callable<Map<String, Object>> deleteJson(final DictionaryTypeDto deleteDictionaryType) {
    return () -> {
    ContextHolder.getBean(DictionaryTypeService.class).deleteByIds(deleteDictionaryType);

    return ReturnDataUtils.newMap();
    };
    }

    /**
    * 字典类型：编辑
    *
    * @param findDictionaryType
    *            字典类型对象
    * @return 实体对象集，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryType/findById.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findByIdJson(final DictionaryTypeDto findDictionaryType, final Model model) {
    return () -> {
    final DictionaryTypeDto dictionaryType = ContextHolder.getBean(DictionaryType
    Service.class).findById(DictionaryTypeDto.class, findDictionaryType);

    return ReturnDataUtils.newMap("dictionaryType", dictionaryType);
    };
    }

    /**
    * 字典类型：分页查询
    *
    * @param paginationDictionaryType
    *            字典类型对象
    * @return 分页，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryType/paginationByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> paginationByLikeCodeJson(final DictionaryTypeDto paginationDictionaryType) {
    return () -> {
    final PaginationDto
<DictionaryTypeDto> dictionaryTypePagination = ContextHolder.getBean(DictionaryType
    Service.class).paginationByLikeCode(DictionaryTypeDto.class, paginationDictionaryType);

    return ReturnDataUtils.newMap("dictionaryTypePagination", dictionaryTypePagination);
    };
    }

    /**
    * 字典类型：分页查询
    *
    * @param paginationDictionaryType
    *            字典类型对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryType/pagination.html" })
    public void paginationHtml(final DictionaryTypeDto paginationDictionaryType, final Model model) {
    final PaginationDto
<DictionaryTypeDto> dictionaryTypePagination = ContextHolder.getBean(DictionaryType
    Service.class).paginationByLikeCode(DictionaryTypeDto.class, paginationDictionaryType);

    model.addAttribute("dictionaryTypePagination", dictionaryTypePagination);
    }

    /**
    * 字典类型：分页查询
    *
    * @param findDictionaryType
    *            字典类型对象
    * @param model
    *            上下文映射
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryType/edit.html" })
    public void editHtml(final DictionaryTypeDto findDictionaryType, final Model model) {
    final DictionaryTypeDto dictionaryType = ContextHolder.getBean(DictionaryType
    Service.class).findById(DictionaryTypeDto.class, findDictionaryType);

    model.addAttribute("dictionaryType", dictionaryType);
    }

    /**
    * 字典类型：查询
    *
    * @param findListDictionaryType
    *            字典类型对象
    * @return 字典类型对象集合，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryType/findListByLikeCode.json" })
    @ResponseBody
    public Callable<Map<String, Object>> findListByLikeCodeJson(final DictionaryTypeDto findListDictionaryType) {
    return () -> {
    final List<DictionaryTypeDto> dictionaryTypeList = ContextHolder.getBean(DictionaryType
    Service.class).findListByLikeCode(DictionaryTypeDto.class, findListDictionaryType);

    return ReturnDataUtils.newMap("dictionaryType", dictionaryTypeList);
    };
    }

    /**
    * 字典类型：保存
    *
    * @param saveDictionaryType
    *            字典类型对象
    * @return 处理结果，json格式
    */
    @Deprecated
    @RequestMapping(value = { "/configuration/dictionaryType/save.json" })
    @ResponseBody
    public Callable<Map<String, Object>> saveJson(final DictionaryTypeDto saveDictionaryType) {
    return () -> {
    ContextHolder.getBean(DictionaryTypeService.class).saveByIdOrCode(DictionaryTypeDto.class, saveDictionaryType);

    return ReturnDataUtils.newMap();
    };
    }
    }
