package com.cardone.platform.configuration.dao;

import java.util.*;

/**
 * 配置
 *
 * @author yaohaitao
 */
public interface ConfigurationDao {
    /**
     * 重置序列
     *
     * @param seqNameList 序列名称集合
     */
    public void resetSeq(final List<String> seqNameList);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        createSeq,

        /**
         * 删除
         */
        @Deprecated
        deleteByIds,

        dropSeq,

        /**
         * 查询
         */
        @Deprecated
        findByCode,

        /**
         * 查询
         */
        @Deprecated
        findById,

        /**
         * 查询
         */
        @Deprecated
        findByLikeCode,

        /**
         * 插入
         */
        @Deprecated
        insertByCode,

        /**
         * 读取
         */
        @Deprecated
        readByCode,

        /**
         * 读取
         */
        @Deprecated
        readByLikeCode,

        /**
         * 更新
         */
        @Deprecated
        updateByCode;

        /**
         * 根目录
         */
        private static final String ROOT = "/platform/configuration/configuration/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}