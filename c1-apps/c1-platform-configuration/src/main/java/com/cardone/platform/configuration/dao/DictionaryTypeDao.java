package com.cardone.platform.configuration.dao;

import com.cardone.common.dao.*;
import com.cardone.platform.configuration.dto.*;

import java.util.*;

/**
 * 字典类型
 *
 * @author yaohaitao
 */
public interface DictionaryTypeDao extends SimpleDao<DictionaryTypeDto> {
    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param parentId    父级标识
     * @return 字典类型对象集合
     */
    <P> List<P> findListByParentId(final Class<P> mappedClass, final String parentId);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param parentCode  父级代码
     * @return 字典类型对象集合
     */
    <P> List<P> findListByParentCode(final Class<P> mappedClass, final String parentCode);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByParentCodeOrParentId,

        /**
         * 查询
         */
        findByParentId;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/configuration/dictionaryType/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}