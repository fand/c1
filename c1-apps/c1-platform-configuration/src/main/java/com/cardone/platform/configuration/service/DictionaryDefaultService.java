package com.cardone.platform.configuration.service;

import com.cardone.common.service.SimpleDefaultService;
import com.cardone.context.ContextHolder;
import com.cardone.platform.configuration.dao.DictionaryDao;
import com.cardone.platform.configuration.dto.DictionaryDto;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang.StringUtils;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.List;
import java.util.Map;

/**
 * 字典服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Transactional(readOnly = true)
public class DictionaryDefaultService extends SimpleDefaultService<DictionaryDto> implements DictionaryService {


    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public <V> Map<String, List<V>> findListMapByTypeCodeList(final Class<V> mappedClass, final List<String> typeCodeList) {
        if (CollectionUtils.isEmpty(typeCodeList)) {
            return null;
        }

        final Map<String, List<V>> dictionaryListMap = Maps.newLinkedHashMap();

        for (final String typeCode : typeCodeList) {
            dictionaryListMap.put(typeCode, ContextHolder.getBean(DictionaryDao.class).findListByTypeCode(mappedClass, typeCode));
        }

        return dictionaryListMap;
    }

    @Override
    public <V> Map<String, List<V>> findListMapByTypeCodeList(Class<V> mappedClass, String typeCodes) {
        return findListMapByTypeCodeList(mappedClass, Lists.newArrayList(StringUtils.split(typeCodes, ",")));
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public Map<String, List<Map<String, Object>>> findListMapByTypeCodeList(List<String> typeCodeList) {
        if (CollectionUtils.isEmpty(typeCodeList)) {
            return null;
        }

        final Map<String, List<Map<String, Object>>> dictionaryListMap = Maps.newLinkedHashMap();

        for (final String typeCode : typeCodeList) {
            dictionaryListMap.put(typeCode, ContextHolder.getBean(DictionaryDao.class).findListByTypeCode(typeCode));
        }

        return dictionaryListMap;
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public String readIdByTypeCodeAndCode(final String defaultId, final String typeCode, final String code) {
        final String id = ContextHolder.getBean(DictionaryService.class).readIdByTypeCodeAndCode(typeCode, code);

        return StringUtils.defaultIfBlank(id, defaultId);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public String readNameByTypeCodeAndCode(final String defaultName, final String typeCode, final String code) {
        final String name = ContextHolder.getBean(DictionaryService.class).readNameByTypeCodeAndCode(typeCode, code);

        return StringUtils.defaultIfBlank(name, defaultName);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public String readValueByTypeCodeAndCode(final String defaultValue, final String typeCode, final String code) {
        final String value = ContextHolder.getBean(DictionaryService.class).readValueByTypeCodeAndCode(typeCode, code);

        return StringUtils.defaultIfBlank(value, defaultValue);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public String readValueByTypeCodeAndCode(final String typeCode, final String code) {
        final DictionaryDto dictionary = ContextHolder.getBean(DictionaryService.class).findByTypeCodeAndCode(DictionaryDto.class, typeCode, code);

        if (dictionary == null) {
            return null;
        }

        return dictionary.getValue();
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public String readNameByTypeCodeAndCode(final String typeCode, final String code) {
        final DictionaryDto dictionary = ContextHolder.getBean(DictionaryService.class).findByTypeCodeAndCode(DictionaryDto.class, typeCode, code);

        if (dictionary == null) {
            return null;
        }

        return dictionary.getName();
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public String readIdByTypeCodeAndCode(final String typeCode, final String code) {
        final DictionaryDto dictionary = ContextHolder.getBean(DictionaryService.class).findByTypeCodeAndCode(DictionaryDto.class, typeCode, code);

        if (dictionary == null) {
            return null;
        }

        return dictionary.getId();
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public <P> P findByTypeCodeAndCode(final Class<P> mappedClass, final String typeCode, final String code) {
        return ContextHolder.getBean(DictionaryDao.class).findByTypeCodeAndCode(mappedClass, typeCode, code);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public <P> List<P> findListByTypeCode(final Class<P> mappedClass, final String typeCode) {
        return ContextHolder.getBean(DictionaryDao.class).findListByTypeCode(mappedClass, typeCode);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public <P> List<P> findListByTypeId(final Class<P> mappedClass, final String typeId) {
        return ContextHolder.getBean(DictionaryDao.class).findListByTypeId(mappedClass, typeId);
    }


    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public List<Map<String, Object>> findListByTypeId(String typeId) {
        return this.getDao().findListByTypeId(typeId);
    }

    @Override
    public DictionaryDao getDao() {
        return ContextHolder.getBean(DictionaryDao.class);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public Integer readByCodeNqIdForCount(final DictionaryDto readDictionary) {
        return ContextHolder.getBean(DictionaryDao.class).readByCodeNqIdForCount(readDictionary);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public List<Map<String, Object>> findListByTypeCode(String typeCode) {
        return this.getDao().findListByTypeCode(typeCode);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public Map<String, Object> findByTypeCodeAndCode(String typeCode, String code) {
        return this.getDao().findByTypeCodeAndCode(typeCode, code);
    }
}