package com.cardone.platform.configuration.dao;

/**
 * 站点位置
 *
 * @author yaohaitao
 */
public interface SiteUrlDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.configuration.dto.SiteUrlDto> {

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 更新
         */
        @Deprecated
        updateByCode;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/configuration/siteUrl/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}