package com.cardone.platform.configuration.dao;

/**
 * 字典项
 *
 * @author yaohaitao
 */
public interface DictionaryItemDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.configuration.dto.DictionaryItemDto> {
    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 更新
         */
        updateByCode;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/configuration/dictionaryItem/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}