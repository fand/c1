package com.cardone.platform.configuration.dao;

import com.cardone.common.dao.*;
import com.cardone.platform.configuration.dto.*;

import java.util.*;

/**
 * 字典
 *
 * @author yaohaitao
 */
public interface DictionaryDao extends SimpleDao<DictionaryDto> {
    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param typeId      类型标识
     * @return 字典对象集合
     */
    <P> List<P> findListByTypeId(final Class<P> mappedClass, final String typeId);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param typeCode    类型代码
     * @return 字典对象集合
     */
    <P> List<P> findListByTypeCode(final Class<P> mappedClass, final String typeCode);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param typeCode    类别代码
     * @param code        代码
     * @return 返回对象
     */
    <P> P findByTypeCodeAndCode(final Class<P> mappedClass, final String typeCode, final String code);


    /**
     * 查询
     *
     * @param typeId 类型标识
     * @return 字典对象集合
     */
    List<Map<String, Object>> findListByTypeId(final String typeId);

    /**
     * 查询
     *
     * @param typeCode 类型代码
     * @return 字典对象集合
     */
    List<Map<String, Object>> findListByTypeCode(final String typeCode);

    /**
     * 查询
     *
     * @param typeCode 类别代码
     * @param code     代码
     * @return 返回对象
     */
    Map<String, Object> findByTypeCodeAndCode(final String typeCode, final String code);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByTypeCode,

        /**
         * 查询
         */
        findByTypeCodeAndCode,

        /**
         * 查询
         */
        findByTypeId;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/configuration/dictionary/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}