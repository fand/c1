package com.cardone.platform.configuration.dao;

import com.cardone.common.dao.*;
import com.cardone.platform.configuration.dto.*;

import java.util.*;

/**
 * 站
 *
 * @author yaohaitao
 */
public interface SiteDao extends SimpleDao<SiteDto> {
    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param siteUrlCode 站路径代码
     * @return 返回对象
     */
    <P> P findBySiteUrlCode(final Class<P> mappedClass, final String siteUrlCode);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param projectCode 项目代码
     * @return 站对象集合
     */
    <P> List<P> findListByProjectCode(final Class<P> mappedClass, final String projectCode);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param serverName  服务名
     * @param projectCode 项目代码
     * @param code        站点代码
     * @return 返回对象
     */
    <P> P findByServerName(final Class<P> mappedClass, final String serverName, final String projectCode, final String code);

    /**
     * 查询
     *
     * @param id 标识
     * @return 返回对象
     */
    <P> P findById(final Class<P> mappedClass, String id);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByProjectCode,

        /**
         * 查询
         */
        findByServerName,

        /**
         * 查询
         */
        findBySiteUrlCode;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/configuration/site/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}