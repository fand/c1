package com.cardone.platform.configuration.service;

import com.cardone.context.*;
import com.cardone.platform.configuration.dao.*;
import lombok.*;

/**
 * 站点位置服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class SiteUrlDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.configuration.dto.SiteUrlDto> implements SiteUrlService {
    @Override
    public SiteUrlDao getDao() {
        return ContextHolder.getBean(SiteUrlDao.class);
    }
}