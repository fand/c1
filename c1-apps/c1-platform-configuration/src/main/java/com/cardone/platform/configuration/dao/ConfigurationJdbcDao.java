package com.cardone.platform.configuration.dao;

import com.cardone.context.*;
import com.cardone.persistent.support.*;
import com.google.common.collect.*;
import lombok.*;
import lombok.extern.slf4j.*;

import java.util.*;

/**
 * 配置
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Slf4j
public class ConfigurationJdbcDao implements ConfigurationDao {

    @Override
    public void resetSeq(final List<String> seqNameList) {
        for (final String seqName : seqNameList) {
            final Map<String, Object> model = Maps.newHashMap();

            model.put("seqName", seqName);

            try {
                ContextHolder.getBean(JdbcTemplateSupport.class).update(Lists.newArrayList(SqlIds.dropSeq.id(), SqlIds.createSeq.id()), model);
            } catch (final Exception e) {
                ConfigurationJdbcDao.log.error(e.getMessage(), e);
            }
        }
    }
}