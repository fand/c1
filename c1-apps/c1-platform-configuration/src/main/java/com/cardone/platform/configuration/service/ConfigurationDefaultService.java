package com.cardone.platform.configuration.service;

import com.cardone.context.*;
import com.cardone.platform.configuration.dao.*;
import lombok.*;
import org.springframework.util.*;

import java.util.*;

/**
 * 配置服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class ConfigurationDefaultService implements ConfigurationService {
    /**
     * 序列名称集合
     */
    private List<String> seqNameList;

    @Override
    @org.springframework.transaction.annotation.Transactional
    public void resetSeq() {
        if (CollectionUtils.isEmpty(this.seqNameList)) {
            return;
        }

        ContextHolder.getBean(ConfigurationDao.class).resetSeq(this.seqNameList);
    }
}