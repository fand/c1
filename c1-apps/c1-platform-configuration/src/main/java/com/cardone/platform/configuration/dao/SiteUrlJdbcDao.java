package com.cardone.platform.configuration.dao;

import com.cardone.platform.configuration.dto.*;
import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 站点位置
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.extern.slf4j.Slf4j
public class SiteUrlJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.configuration.dto.SiteUrlDto> implements SiteUrlDao {
    public SiteUrlJdbcDao() {
        super(SiteUrlDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(SiteUrlDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(SiteUrlDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }
}