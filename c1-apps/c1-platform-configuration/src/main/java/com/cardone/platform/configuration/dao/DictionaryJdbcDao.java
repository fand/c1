package com.cardone.platform.configuration.dao;

import com.cardone.context.*;
import com.cardone.persistent.support.*;
import com.cardone.platform.configuration.dto.*;
import com.google.common.collect.*;
import lombok.*;
import lombok.extern.slf4j.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 字典
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Slf4j
public class DictionaryJdbcDao extends SimpleJdbcDao<DictionaryDto> implements DictionaryDao {
    public DictionaryJdbcDao() {
        super(DictionaryDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(DictionaryDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(DictionaryDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }

    @Override
    public <P> List<P> findListByTypeId(final Class<P> mappedClass, final String typeId) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.typeId.name(), typeId);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findByTypeId.id(), model);
    }

    @Override
    public <P> List<P> findListByTypeCode(final Class<P> mappedClass, final String typeCode) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.typeCode.name(), typeCode);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findByTypeCode.id(), model);
    }

    @Override
    public <P> P findByTypeCodeAndCode(final Class<P> mappedClass, final String typeCode, final String code) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.typeCode.name(), typeCode);
        model.put(Attributes.code.name(), code);

        return ContextHolder.getBean(JdbcTemplateSupport.class).find(mappedClass, SqlIds.findByTypeCodeAndCode.id(), model);
    }

    @Override
    public List<Map<String, Object>> findListByTypeId(String typeId) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.typeId.name(), typeId);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(SqlIds.findByTypeId.id(), model);
    }

    @Override
    public List<Map<String, Object>> findListByTypeCode(String typeCode) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.typeCode.name(), typeCode);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(SqlIds.findByTypeCode.id(), model);
    }

    @Override
    public Map<String, Object> findByTypeCodeAndCode(String typeCode, String code) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.typeCode.name(), typeCode);
        model.put(Attributes.code.name(), code);

        return ContextHolder.getBean(JdbcTemplateSupport.class).find(SqlIds.findByTypeCodeAndCode.id(), model);
    }
}