package com.cardone.platform.configuration.dao;

import com.cardone.context.*;
import com.cardone.persistent.support.*;
import com.cardone.platform.configuration.dto.*;
import com.google.common.collect.*;
import lombok.*;
import lombok.extern.slf4j.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 站
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Slf4j
public class SiteJdbcDao extends SimpleJdbcDao<SiteDto> implements SiteDao {
    public SiteJdbcDao() {
        super(SiteDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(SiteDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(SiteDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }

    @Override
    public <P> P findBySiteUrlCode(final Class<P> mappedClass, final String siteUrlCode) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(SiteDto.Attributes.siteUrlCode.name(), siteUrlCode);

        return ContextHolder.getBean(JdbcTemplateSupport.class).find(mappedClass, SqlIds.findBySiteUrlCode.id(), model);
    }

    @Override
    public <P> List<P> findListByProjectCode(final Class<P> mappedClass, final String projectCode) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.projectCode.name(), projectCode);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findByProjectCode.id(), model);
    }

    @Override
    public <P> P findByServerName(final Class<P> mappedClass, final String serverName, final String projectCode, final String code) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.serverName.name(), serverName);
        model.put(Attributes.projectCode.name(), projectCode);
        model.put(Attributes.code.name(), code);

        return ContextHolder.getBean(JdbcTemplateSupport.class).find(mappedClass, SqlIds.findByServerName.id(), model);
    }

    @Override
    public <P> P findById(Class<P> mappedClass, String id) {
        final com.cardone.persistent.builder.Model model = com.cardone.persistent.builder.ModelUtils.newModel();

        model.putExtend(null, Attributes.id.name(), null, id);

        return ContextHolder.getBean(JdbcTemplateSupport.class).find(mappedClass, this.findById, model);
    }
}