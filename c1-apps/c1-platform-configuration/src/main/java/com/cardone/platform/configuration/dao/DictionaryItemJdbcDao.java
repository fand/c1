package com.cardone.platform.configuration.dao;

import com.cardone.platform.configuration.dto.*;
import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 字典项
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.extern.slf4j.Slf4j
public class DictionaryItemJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.configuration.dto.DictionaryItemDto> implements DictionaryItemDao {
    public DictionaryItemJdbcDao() {
        super(DictionaryItemDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(DictionaryItemDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(DictionaryItemDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }
}