package com.cardone.platform.configuration.dao;

import com.cardone.context.*;
import com.cardone.persistent.support.*;
import com.cardone.platform.configuration.dto.*;
import com.google.common.collect.*;
import lombok.*;
import lombok.extern.slf4j.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 字典类型
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Slf4j
public class DictionaryTypeJdbcDao extends SimpleJdbcDao<DictionaryTypeDto> implements DictionaryTypeDao {
    public DictionaryTypeJdbcDao() {
        super(DictionaryTypeDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(DictionaryTypeDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(DictionaryTypeDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }

    @Override
    public <P> List<P> findListByParentId(final Class<P> mappedClass, final String parentId) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.parentId.name(), parentId);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findByParentCodeOrParentId.id(), model);
    }

    @Override
    public <P> List<P> findListByParentCode(final Class<P> mappedClass, final String parentCode) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.parentCode.name(), parentCode);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findByParentCodeOrParentId.id(), model);
    }
}