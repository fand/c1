package com.cardone.platform.configuration.service;

import com.cardone.context.*;
import com.cardone.platform.configuration.dao.*;
import lombok.*;

/**
 * 字典项服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class DictionaryItemDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.configuration.dto.DictionaryItemDto> implements DictionaryItemService {
    @Override
    public DictionaryItemDao getDao() {
        return ContextHolder.getBean(DictionaryItemDao.class);
    }
}