package com.cardone.platform.configuration.service;

import com.cardone.common.service.*;
import com.cardone.context.*;
import com.cardone.platform.authority.po.Navigation;
import com.cardone.platform.configuration.dao.*;
import com.cardone.platform.configuration.dto.*;
import com.cardone.platform.configuration.po.Site;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.cache.annotation.*;
import org.springframework.transaction.annotation.*;
import org.springframework.util.*;

import java.util.*;

/**
 * 站服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Transactional(readOnly = true)
public class SiteDefaultService extends SimpleDefaultService<SiteDto> implements SiteService {
    @Override
    public SiteDao getDao() {
        return ContextHolder.getBean(SiteDao.class);
    }

    @Override
    @CacheEvict(value = SiteService.BEAN_ID, allEntries = true)
    public void init() {
        this.init(Site.class.getName() + ".initList");
    }

    @Transactional
    public void init(final List<SiteDto> initSiteList) {
        if (CollectionUtils.isEmpty(initSiteList)) {
            return;
        }

        for (final SiteDto initSite : initSiteList) {
            final String projectId = ContextHolder.getBean(DictionaryService.class).readIdByTypeCodeAndCode(initSite.getProjectTypeCode(), initSite.getProjectCode());

            if (StringUtils.isBlank(projectId)) {
                continue;
            }

            initSite.setProjectId(projectId);

            final String styleId = ContextHolder.getBean(DictionaryService.class).readIdByTypeCodeAndCode(initSite.getStyleTypeCode(), initSite.getStyleCode());

            if (StringUtils.isBlank(styleId)) {
                continue;
            }

            initSite.setStyleId(styleId);

            ContextHolder.getBean(SiteDao.class).saveByIdOrCode(SiteDto.class, initSite);
        }
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public <P> P findByServerName(final Class<P> mappedClass, final String serverName, final String projectCode, final String code) {
        return ContextHolder.getBean(SiteDao.class).findByServerName(mappedClass, serverName, projectCode, code);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public <P> P findById(Class<P> mappedClass, String id) {
        return  ContextHolder.getBean(SiteDao.class).findById(mappedClass, id);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public <P> P findBySiteUrlCode(final Class<P> mappedClass, final String siteUrlCode) {
        return ContextHolder.getBean(SiteDao.class).findBySiteUrlCode(mappedClass, siteUrlCode);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public <P> List<P> findListByProjectCode(final Class<P> mappedClass, final String projectCode) {
        return ContextHolder.getBean(SiteDao.class).findListByProjectCode(mappedClass, projectCode);
    }
}