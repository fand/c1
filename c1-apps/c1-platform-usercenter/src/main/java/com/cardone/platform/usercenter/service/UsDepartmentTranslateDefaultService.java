package com.cardone.platform.usercenter.service;

/**
 * 用户部门调动服务
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class UsDepartmentTranslateDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.usercenter.dto.UsDepartmentTranslateDto> implements com.cardone.platform.usercenter.service.UsDepartmentTranslateService {
    @Override
    public com.cardone.platform.usercenter.dao.UsDepartmentTranslateDao getDao() {
        return com.cardone.context.ContextHolder.getBean(com.cardone.platform.usercenter.dao.UsDepartmentTranslateDao.class);
    }
}