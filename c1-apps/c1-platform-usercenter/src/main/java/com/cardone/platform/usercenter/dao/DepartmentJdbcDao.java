package com.cardone.platform.usercenter.dao;

/**
 * 部门
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class DepartmentJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.usercenter.dto.DepartmentDto> implements com.cardone.platform.usercenter.dao.DepartmentDao {
    public DepartmentJdbcDao() {
        super(com.cardone.platform.usercenter.dao.DepartmentDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(com.cardone.platform.usercenter.dto.DepartmentDto dto) {
        return org.apache.commons.lang3.StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(com.cardone.platform.usercenter.dto.DepartmentDto dto) {
        dto.setId(java.util.UUID.randomUUID().toString());
    }
}