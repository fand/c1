package com.cardone.platform.usercenter.service;

/**
 * 用户组织调动服务
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class UsOrgTranslateDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.usercenter.dto.UsOrgTranslateDto> implements com.cardone.platform.usercenter.service.UsOrgTranslateService {
    @Override
    public com.cardone.platform.usercenter.dao.UsOrgTranslateDao getDao() {
        return com.cardone.context.ContextHolder.getBean(com.cardone.platform.usercenter.dao.UsOrgTranslateDao.class);
    }
}