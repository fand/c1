package com.cardone.platform.usercenter.service;

/**
 * 用户激活服务
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class UserActivationDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.usercenter.dto.UserActivationDto> implements com.cardone.platform.usercenter.service.UserActivationService {
    @Override
    public com.cardone.platform.usercenter.dao.UserActivationDao getDao() {
        return com.cardone.context.ContextHolder.getBean(com.cardone.platform.usercenter.dao.UserActivationDao.class);
    }
}