package com.cardone.platform.usercenter.service;

/**
 * 部门服务
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class DepartmentDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.usercenter.dto.DepartmentDto> implements com.cardone.platform.usercenter.service.DepartmentService {
    @Override
    public com.cardone.platform.usercenter.dao.DepartmentDao getDao() {
        return com.cardone.context.ContextHolder.getBean(com.cardone.platform.usercenter.dao.DepartmentDao.class);
    }
}