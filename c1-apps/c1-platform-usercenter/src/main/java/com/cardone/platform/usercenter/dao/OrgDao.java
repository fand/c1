package com.cardone.platform.usercenter.dao;

import java.util.*;

/**
 * 组织
 *
 * @author yaohaitao
 */
public interface OrgDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.usercenter.dto.OrgDto> {
    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param id          组织标识
     * @param isSuperRole 是否超级角色
     * @return 组织对象集合
     */
    <P> List<P> findListByIdForTree(final Class<P> mappedClass, final String id, final boolean isSuperRole);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByIdForTree;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/usercenter/org/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}