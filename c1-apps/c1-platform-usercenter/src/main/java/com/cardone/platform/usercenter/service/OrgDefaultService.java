package com.cardone.platform.usercenter.service;

import com.cardone.context.*;
import com.cardone.platform.usercenter.dao.*;
import lombok.*;

import java.util.*;

/**
 * 组织服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class OrgDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.usercenter.dto.OrgDto> implements OrgService {
    @Override
    public OrgDao getDao() {
        return ContextHolder.getBean(OrgDao.class);
    }

    @Override
    @org.springframework.transaction.annotation.Transactional(readOnly = true)
    public <P> List<P> findListByIdForTree(final Class<P> mappedClass, final String id, final boolean isSuperRole) {
        return ContextHolder.getBean(OrgDao.class).findListByIdForTree(mappedClass, id, isSuperRole);
    }
}