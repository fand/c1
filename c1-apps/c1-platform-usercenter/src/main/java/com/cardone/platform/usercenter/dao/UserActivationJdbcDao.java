package com.cardone.platform.usercenter.dao;

/**
 * 用户激活
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class UserActivationJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.usercenter.dto.UserActivationDto> implements com.cardone.platform.usercenter.dao.UserActivationDao {
    public UserActivationJdbcDao() {
        super(com.cardone.platform.usercenter.dao.UserActivationDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(com.cardone.platform.usercenter.dto.UserActivationDto dto) {
        return org.apache.commons.lang3.StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(com.cardone.platform.usercenter.dto.UserActivationDto dto) {
        dto.setId(java.util.UUID.randomUUID().toString());
    }
}