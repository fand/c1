package com.cardone.platform.usercenter.dao;

import com.cardone.common.dao.*;
import com.cardone.common.dto.*;
import com.cardone.platform.usercenter.dto.*;

/**
 * 用户
 *
 * @author yaohaitao
 */
public interface UserDao extends SimpleDao<UserDto> {
    /**
     * 分页
     *
     * @param mappedClass    返回类型
     * @param paginationUser 用户对象
     * @return 用户分页对象
     */
    <P> PaginationDto<P> paginationByOrgIdAndLikeCode(final Class<P> mappedClass, final UserDto paginationUser);

    /**
     * 查询
     *
     * @param id 标识
     * @return 返回对象
     */
    UserDto findById(final String id);

    /**
     * 更新
     *
     * @param updateUser 用户对象
     * @return 影响行数
     */
    int updateForDetail(final UserDto updateUser);

    /**
     * 更新
     *
     * @param id 标识
     * @return 影响行数
     */
    int updateEndDateByIdForInvalid(final String id);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param findUser    用户对象
     * @return 返回对象
     */
    <P> P findByCodeForLogin(final Class<P> mappedClass, final UserDto findUser);

    /**
     * 更新手机号码
     *
     * @param id          标识
     * @param mobilePhone 手机号码
     * @return 影响行数
     */
    int updateMobilePhone(final String id, final String mobilePhone);

    /**
     * 更新
     *
     * @param updateUser        用户对象
     * @param updateMobilePhone 更新手机号码
     * @return 影响行数
     */
    int updateForBaseInfo(final UserDto updateUser, final Boolean updateMobilePhone);

    /**
     * 更新
     *
     * @param id             用户标识
     * @param oldPassword    旧密码
     * @param updatePassword 更新密码
     * @return 影响行数
     */
    int updatePasswordById(final String id, final String oldPassword, final String updatePassword);

    /**
     * 更新
     *
     * @param updateUser        用户对象
     * @param updateMobilePhone 更新手机号码
     * @return 影响行数
     */
    int updateForFullInfo(final UserDto updateUser, final Boolean updateMobilePhone);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByCodeForLogin,

        /**
         * 查询
         */
        findByOrgIdAndLikeCode,

        /**
         * 读取
         */
        readByOrgIdAndLikeCode,

        /**
         * 更新
         */
        updateEndDateByIdForInvalid;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/usercenter/user/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}