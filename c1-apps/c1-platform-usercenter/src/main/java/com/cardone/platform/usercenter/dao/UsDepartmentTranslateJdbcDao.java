package com.cardone.platform.usercenter.dao;

/**
 * 用户部门调动
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class UsDepartmentTranslateJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.usercenter.dto.UsDepartmentTranslateDto> implements com.cardone.platform.usercenter.dao.UsDepartmentTranslateDao {
    public UsDepartmentTranslateJdbcDao() {
        super(com.cardone.platform.usercenter.dao.UsDepartmentTranslateDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(com.cardone.platform.usercenter.dto.UsDepartmentTranslateDto dto) {
        return org.apache.commons.lang3.StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(com.cardone.platform.usercenter.dto.UsDepartmentTranslateDto dto) {
        dto.setId(java.util.UUID.randomUUID().toString());
    }
}