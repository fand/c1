package com.cardone.platform.usercenter.dao;

/**
 * 用户组织调动
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class UsOrgTranslateJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.usercenter.dto.UsOrgTranslateDto> implements com.cardone.platform.usercenter.dao.UsOrgTranslateDao {
    public UsOrgTranslateJdbcDao() {
        super(com.cardone.platform.usercenter.dao.UsOrgTranslateDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(com.cardone.platform.usercenter.dto.UsOrgTranslateDto dto) {
        return org.apache.commons.lang3.StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(com.cardone.platform.usercenter.dto.UsOrgTranslateDto dto) {
        dto.setId(java.util.UUID.randomUUID().toString());
    }
}