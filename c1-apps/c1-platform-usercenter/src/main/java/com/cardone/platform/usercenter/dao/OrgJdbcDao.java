package com.cardone.platform.usercenter.dao;

import com.cardone.context.*;
import com.cardone.persistent.support.*;
import com.cardone.platform.usercenter.dto.*;
import com.google.common.collect.*;
import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 组织
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.extern.slf4j.Slf4j
public class OrgJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.usercenter.dto.OrgDto> implements OrgDao {
    public OrgJdbcDao() {
        super(OrgDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(OrgDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(OrgDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }

    @Override
    public <P> List<P> findListByIdForTree(final Class<P> mappedClass, final String id, final boolean isSuperRole) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.id.name(), id);
        model.put("isSuperRole", isSuperRole);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findByIdForTree.id(), model);
    }
}