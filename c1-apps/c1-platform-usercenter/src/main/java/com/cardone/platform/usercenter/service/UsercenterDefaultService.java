package com.cardone.platform.usercenter.service;

/**
 * 用户中心服务
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class UsercenterDefaultService implements com.cardone.platform.usercenter.service.UsercenterService {
}