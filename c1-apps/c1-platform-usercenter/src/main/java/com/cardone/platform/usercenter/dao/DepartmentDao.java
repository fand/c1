package com.cardone.platform.usercenter.dao;

/**
 * 部门
 *
 * @author yaohaitao
 */
public interface DepartmentDao extends com.cardone.common.dao.SimpleDao
        <com.cardone.platform.usercenter.dto.DepartmentDto> {
    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByCodes;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/usercenter/department/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = com.cardone.platform.usercenter.dao.DepartmentDao.SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}