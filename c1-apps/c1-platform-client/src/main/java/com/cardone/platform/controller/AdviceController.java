package com.cardone.platform.controller;

import com.cardone.context.ContextHolder;
import com.cardone.context.DictionaryException;
import com.cardone.platform.configuration.util.SiteUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;

/**
 * 高级配置
 *
 * @author yaohaitao
 */
@ControllerAdvice
@Slf4j
public class AdviceController {
    @InitBinder
    public void initBinder(final WebDataBinder webDataBinder) {
        webDataBinder.registerCustomEditor(Date.class, ContextHolder.getBean(com.cardone.common.beans.propertyeditors.CardOneDateEditor.class));
    }

    /**
     * 500页面或500json数据
     *
     * @param e
     * @param request
     * @param response
     * @return
     */
    @ExceptionHandler({Throwable.class, Exception.class, DictionaryException.class})
    public String exception500(final Throwable e, final HttpServletRequest request, final HttpServletResponse response) {
        return SiteUtils.exception500(e, request, response);
    }
}
