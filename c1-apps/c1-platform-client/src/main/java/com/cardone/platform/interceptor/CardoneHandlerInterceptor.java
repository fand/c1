package com.cardone.platform.interceptor;

import com.cardone.platform.configuration.util.SiteUtils;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author yaohaitao
 */
public class CardoneHandlerInterceptor extends HandlerInterceptorAdapter implements AsyncHandlerInterceptor {
    @Override
    public void postHandle(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final ModelAndView modelAndView) throws Exception {
        SiteUtils.postHandle(request, response, handler, modelAndView);
    }

    @Override
    public void afterCompletion(final HttpServletRequest request, final HttpServletResponse response, final Object handler, final Exception ex) throws Exception {
        SiteUtils.afterCompletion(request, response, handler, ex);
    }
}