package com.cardone.platform.configuration.function

import com.cardone.common.function.ReadDictionaryValueByTypeCodeAndCodeFunction
import com.cardone.context.ContextHolder
import com.cardone.platform.configuration.service.DictionaryService

/**
 * 读取字典值
 *
 * @author yaohaitao
 */
class DefaultReadDictionaryValueByTypeCodeAndCodeFunction implements ReadDictionaryValueByTypeCodeAndCodeFunction {
    @Override
    String execution(final String defaultValue, final String typeCode, final String code) {
        ContextHolder.getBean(DictionaryService.class).readValueByTypeCodeAndCode(defaultValue, typeCode, code);
    }
}
