INSERT
INTO
c1_entity_code
(
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} `BEGIN_DATE`
    <#assign prefixName=','>
</#if>
<#if (insert_code??) && (insert_code_value??)>
${prefixName} `CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} `CREATED_BY_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} `CREATED_BY_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} `CREATED_BY_NAME`
    <#assign prefixName=','>
</#if>
${prefixName} `CREATED_DATE`
<#assign prefixName=','>
<#if (insert_dataStateCode??) && (insert_dataStateCode_value??)>
${prefixName} `DATA_STATE_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_dataStateId??) && (insert_dataStateId_value??)>
${prefixName} `DATA_STATE_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_dataStateName??) && (insert_dataStateName_value??)>
${prefixName} `DATA_STATE_NAME`
    <#assign prefixName=','>
</#if>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} `END_DATE`
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} `ID`
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} `LAST_MODIFIED_BY_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} `LAST_MODIFIED_BY_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} `LAST_MODIFIED_BY_NAME`
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} `LAST_MODIFIED_DATE`
    <#assign prefixName=','>
</#if>
<#if (insert_stateCode??) && (insert_stateCode_value??)>
${prefixName} `STATE_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} `STATE_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_stateName??) && (insert_stateName_value??)>
${prefixName} `STATE_NAME`
    <#assign prefixName=','>
</#if>
<#if (insert_typeCode??) && (insert_typeCode_value??)>
${prefixName} `TYPE_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_versionInt??) && (insert_versionInt_value??)>
${prefixName} `VERSION_INT`
    <#assign prefixName=','>
</#if>
)
(
SELECT
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} :insert_beginDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_code??) && (insert_code_value??)>
${prefixName} :insert_code_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} :insert_createdByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} :insert_createdById_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} :insert_createdByName_value
    <#assign prefixName=','>
</#if>
${prefixName} NOW()
<#assign prefixName=','>
<#if (insert_dataStateCode??) && (insert_dataStateCode_value??)>
${prefixName} :insert_dataStateCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_dataStateId??) && (insert_dataStateId_value??)>
${prefixName} :insert_dataStateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_dataStateName??) && (insert_dataStateName_value??)>
${prefixName} :insert_dataStateName_value
    <#assign prefixName=','>
</#if>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} :insert_endDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} :insert_id_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} :insert_lastModifiedByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} :insert_lastModifiedById_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} :insert_lastModifiedByName_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} :insert_lastModifiedDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateCode??) && (insert_stateCode_value??)>
${prefixName} :insert_stateCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} :insert_stateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateName??) && (insert_stateName_value??)>
${prefixName} :insert_stateName_value
    <#assign prefixName=','>
</#if>
<#if (insert_typeCode??) && (insert_typeCode_value??)>
${prefixName} :insert_typeCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_versionInt??) && (insert_versionInt_value??)>
${prefixName} :insert_versionInt_value
    <#assign prefixName=','>
</#if>
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM c1_entity_code E
<#assign prefixName='WHERE'>
<#if (where_and_eq_beginDate??)>
    <#if (where_and_eq_beginDate_value??)>
    ${prefixName} E.BEGIN_DATE = :where_and_eq_beginDate_value
    <#else>
    ${prefixName} E.BEGIN_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_code??)>
    <#if (where_and_eq_code_value??)>
    ${prefixName} E.CODE = :where_and_eq_code_value
    <#else>
    ${prefixName} E.CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdByCode??)>
    <#if (where_and_eq_createdByCode_value??)>
    ${prefixName} E.CREATED_BY_CODE = :where_and_eq_createdByCode_value
    <#else>
    ${prefixName} E.CREATED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdById??)>
    <#if (where_and_eq_createdById_value??)>
    ${prefixName} E.CREATED_BY_ID = :where_and_eq_createdById_value
    <#else>
    ${prefixName} E.CREATED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdByName??)>
    <#if (where_and_eq_createdByName_value??)>
    ${prefixName} E.CREATED_BY_NAME = :where_and_eq_createdByName_value
    <#else>
    ${prefixName} E.CREATED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdDate??)>
    <#if (where_and_eq_createdDate_value??)>
    ${prefixName} E.CREATED_DATE = :where_and_eq_createdDate_value
    <#else>
    ${prefixName} E.CREATED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_dataStateCode??)>
    <#if (where_and_eq_dataStateCode_value??)>
    ${prefixName} E.DATA_STATE_CODE = :where_and_eq_dataStateCode_value
    <#else>
    ${prefixName} E.DATA_STATE_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_dataStateId??)>
    <#if (where_and_eq_dataStateId_value??)>
    ${prefixName} E.DATA_STATE_ID = :where_and_eq_dataStateId_value
    <#else>
    ${prefixName} E.DATA_STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_dataStateName??)>
    <#if (where_and_eq_dataStateName_value??)>
    ${prefixName} E.DATA_STATE_NAME = :where_and_eq_dataStateName_value
    <#else>
    ${prefixName} E.DATA_STATE_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_endDate??)>
    <#if (where_and_eq_endDate_value??)>
    ${prefixName} E.END_DATE = :where_and_eq_endDate_value
    <#else>
    ${prefixName} E.END_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if (where_and_between_sysdate??)>
    <#if (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
${prefixName} NOW() BETWEEN IFNULL(E.BEGIN_DATE, NOW()) AND IFNULL(E.END_DATE, NOW())
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_id??)>
    <#if (where_and_eq_id_value??)>
    ${prefixName} E.ID = :where_and_eq_id_value
    <#else>
    ${prefixName} E.ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedByCode??)>
    <#if (where_and_eq_lastModifiedByCode_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_CODE = :where_and_eq_lastModifiedByCode_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedById??)>
    <#if (where_and_eq_lastModifiedById_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_ID = :where_and_eq_lastModifiedById_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedByName??)>
    <#if (where_and_eq_lastModifiedByName_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_NAME = :where_and_eq_lastModifiedByName_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedDate??)>
    <#if (where_and_eq_lastModifiedDate_value??)>
    ${prefixName} E.LAST_MODIFIED_DATE = :where_and_eq_lastModifiedDate_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_stateCode??)>
    <#if (where_and_eq_stateCode_value??)>
    ${prefixName} E.STATE_CODE = :where_and_eq_stateCode_value
    <#else>
    ${prefixName} E.STATE_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_stateId??)>
    <#if (where_and_eq_stateId_value??)>
    ${prefixName} E.STATE_ID = :where_and_eq_stateId_value
    <#else>
    ${prefixName} E.STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_stateName??)>
    <#if (where_and_eq_stateName_value??)>
    ${prefixName} E.STATE_NAME = :where_and_eq_stateName_value
    <#else>
    ${prefixName} E.STATE_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_typeCode??)>
    <#if (where_and_eq_typeCode_value??)>
    ${prefixName} E.TYPE_CODE = :where_and_eq_typeCode_value
    <#else>
    ${prefixName} E.TYPE_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_versionInt??)>
    <#if (where_and_eq_versionInt_value??)>
    ${prefixName} E.VERSION_INT = :where_and_eq_versionInt_value
    <#else>
    ${prefixName} E.VERSION_INT IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
)
)