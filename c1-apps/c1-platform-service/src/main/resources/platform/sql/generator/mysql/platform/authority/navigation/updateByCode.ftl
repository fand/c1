UPDATE c1_navigation
<#assign prefixName='SET'>
<#if
(update_beginDate??)>
    <#if
    (update_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` = :update_beginDate_value
    <#else>
    ${prefixName} `BEGIN_DATE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_code??)>
    <#if
    (update_code_value??)>
    ${prefixName} `CODE` = :update_code_value
    <#else>
    ${prefixName} `CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByCode??)>
    <#if
    (update_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` = :update_createdByCode_value
    <#else>
    ${prefixName} `CREATED_BY_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdById??)>
    <#if
    (update_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` = :update_createdById_value
    <#else>
    ${prefixName} `CREATED_BY_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByName??)>
    <#if
    (update_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` = :update_createdByName_value
    <#else>
    ${prefixName} `CREATED_BY_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdDate??)>
    <#if
    (update_createdDate_value??)>
    ${prefixName} `CREATED_DATE` = :update_createdDate_value
    <#else>
    ${prefixName} `CREATED_DATE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_dataOption??)>
    <#if
    (update_dataOption_value??)>
    ${prefixName} `DATA_OPTION` = :update_dataOption_value
    <#else>
    ${prefixName} `DATA_OPTION` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_dataStateCode??)>
    <#if
    (update_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` = :update_dataStateCode_value
    <#else>
    ${prefixName} `DATA_STATE_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_dataStateId??)>
    <#if
    (update_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` = :update_dataStateId_value
    <#else>
    ${prefixName} `DATA_STATE_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_dataStateName??)>
    <#if
    (update_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` = :update_dataStateName_value
    <#else>
    ${prefixName} `DATA_STATE_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_endDate??)>
    <#if
    (update_endDate_value??)>
    ${prefixName} `END_DATE` = :update_endDate_value
    <#else>
    ${prefixName} `END_DATE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_iconStyle??)>
    <#if
    (update_iconStyle_value??)>
    ${prefixName} `ICON_STYLE` = :update_iconStyle_value
    <#else>
    ${prefixName} `ICON_STYLE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_id??)>
    <#if
    (update_id_value??)>
    ${prefixName} `ID` = :update_id_value
    <#else>
    ${prefixName} `ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByCode??)>
    <#if
    (update_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = :update_lastModifiedByCode_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedById??)>
    <#if
    (update_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` = :update_lastModifiedById_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByName??)>
    <#if
    (update_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = :update_lastModifiedByName_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedDate??)>
    <#if
    (update_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` = :update_lastModifiedDate_value
    <#else>
    ${prefixName} `LAST_MODIFIED_DATE` = NOW()
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_name??)>
    <#if
    (update_name_value??)>
    ${prefixName} `NAME` = :update_name_value
    <#else>
    ${prefixName} `NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_orderNum??)>
    <#if
    (update_orderNum_value??)>
    ${prefixName} `ORDER_NUM` = :update_orderNum_value
    <#else>
    ${prefixName} `ORDER_NUM` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_parentCode??)>
    <#if
    (update_parentCode_value??)>
    ${prefixName} `PARENT_CODE` = :update_parentCode_value
    <#else>
    ${prefixName} `PARENT_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_parentId??)>
    <#if
    (update_parentId_value??)>
    ${prefixName} `PARENT_ID` = :update_parentId_value
    <#else>
    ${prefixName} `PARENT_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_parentName??)>
    <#if
    (update_parentName_value??)>
    ${prefixName} `PARENT_NAME` = :update_parentName_value
    <#else>
    ${prefixName} `PARENT_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_siteCode??)>
    <#if
    (update_siteCode_value??)>
    ${prefixName} `SITE_CODE` = :update_siteCode_value
    <#else>
    ${prefixName} `SITE_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_siteId??)>
    <#if
    (update_siteId_value??)>
    ${prefixName} `SITE_ID` = :update_siteId_value
    <#else>
    ${prefixName} `SITE_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_siteName??)>
    <#if
    (update_siteName_value??)>
    ${prefixName} `SITE_NAME` = :update_siteName_value
    <#else>
    ${prefixName} `SITE_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_stateCode??)>
    <#if
    (update_stateCode_value??)>
    ${prefixName} `STATE_CODE` = :update_stateCode_value
    <#else>
    ${prefixName} `STATE_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_stateId??)>
    <#if
    (update_stateId_value??)>
    ${prefixName} `STATE_ID` = :update_stateId_value
    <#else>
    ${prefixName} `STATE_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_stateName??)>
    <#if
    (update_stateName_value??)>
    ${prefixName} `STATE_NAME` = :update_stateName_value
    <#else>
    ${prefixName} `STATE_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_target??)>
    <#if
    (update_target_value??)>
    ${prefixName} `TARGET` = :update_target_value
    <#else>
    ${prefixName} `TARGET` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_treeCode??)>
    <#if
    (update_treeCode_value??)>
    ${prefixName} `TREE_CODE` = :update_treeCode_value
    <#else>
    ${prefixName} `TREE_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_treeId??)>
    <#if
    (update_treeId_value??)>
    ${prefixName} `TREE_ID` = :update_treeId_value
    <#else>
    ${prefixName} `TREE_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_treeName??)>
    <#if
    (update_treeName_value??)>
    ${prefixName} `TREE_NAME` = :update_treeName_value
    <#else>
    ${prefixName} `TREE_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_typeCode??)>
    <#if
    (update_typeCode_value??)>
    ${prefixName} `TYPE_CODE` = :update_typeCode_value
    <#else>
    ${prefixName} `TYPE_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_typeId??)>
    <#if
    (update_typeId_value??)>
    ${prefixName} `TYPE_ID` = :update_typeId_value
    <#else>
    ${prefixName} `TYPE_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_typeName??)>
    <#if
    (update_typeName_value??)>
    ${prefixName} `TYPE_NAME` = :update_typeName_value
    <#else>
    ${prefixName} `TYPE_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_url??)>
    <#if
    (update_url_value??)>
    ${prefixName} `URL` = :update_url_value
    <#else>
    ${prefixName} `URL` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_versionInt??)>
    <#if
    (update_versionInt_value??)>
    ${prefixName} `VERSION_INT` = :update_versionInt_value
    <#else>
    ${prefixName} `VERSION_INT` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#include
"whereByCode.ftl">