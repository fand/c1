INSERT
INTO
C1_USER
(
<#assign prefixName=' '>
<#if (insert_accountName??) && (insert_accountName_value??)>
${prefixName} ACCOUNT_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_address??) && (insert_address_value??)>
${prefixName} ADDRESS
    <#assign prefixName=','>
</#if>
<#if (insert_age??) && (insert_age_value??)>
${prefixName} AGE
    <#assign prefixName=','>
</#if>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} BEGIN_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_birthday??) && (insert_birthday_value??)>
${prefixName} BIRTHDAY
    <#assign prefixName=','>
</#if>
<#if (insert_callName??) && (insert_callName_value??)>
${prefixName} CALL_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_cityId??) && (insert_cityId_value??)>
${prefixName} CITY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_code??) && (insert_code_value??)>
${prefixName} CODE
    <#assign prefixName=','>
</#if>
<#if (insert_companyName??) && (insert_companyName_value??)>
${prefixName} COMPANY_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_countryId??) && (insert_countryId_value??)>
${prefixName} COUNTRY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} CREATED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} CREATED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} CREATED_BY_NAME
    <#assign prefixName=','>
</#if>
${prefixName} CREATED_DATE
<#assign prefixName=','>
<#if (insert_departmentId??) && (insert_departmentId_value??)>
${prefixName} DEPARTMENT_ID
    <#assign prefixName=','>
</#if>
<#if (insert_diplomaId??) && (insert_diplomaId_value??)>
${prefixName} DIPLOMA_ID
    <#assign prefixName=','>
</#if>
<#if (insert_districtId??) && (insert_districtId_value??)>
${prefixName} DISTRICT_ID
    <#assign prefixName=','>
</#if>
<#if (insert_email??) && (insert_email_value??)>
${prefixName} EMAIL
    <#assign prefixName=','>
</#if>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} END_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_flagId??) && (insert_flagId_value??)>
${prefixName} FLAG_ID
    <#assign prefixName=','>
</#if>
<#if (insert_folkId??) && (insert_folkId_value??)>
${prefixName} FOLK_ID
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} ID
    <#assign prefixName=','>
</#if>
<#if (insert_intro??) && (insert_intro_value??)>
${prefixName} INTRO
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} LAST_MODIFIED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} LAST_MODIFIED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} LAST_MODIFIED_BY_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} LAST_MODIFIED_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_locus??) && (insert_locus_value??)>
${prefixName} LOCUS
    <#assign prefixName=','>
</#if>
<#if (insert_marryStateId??) && (insert_marryStateId_value??)>
${prefixName} MARRY_STATE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_mobilePhone??) && (insert_mobilePhone_value??)>
${prefixName} MOBILE_PHONE
    <#assign prefixName=','>
</#if>
<#if (insert_name??) && (insert_name_value??)>
${prefixName} NAME
    <#assign prefixName=','>
</#if>
<#if (insert_orgId??) && (insert_orgId_value??)>
${prefixName} ORG_ID
    <#assign prefixName=','>
</#if>
<#if (insert_password??) && (insert_password_value??)>
${prefixName} PASSWORD
    <#assign prefixName=','>
</#if>
<#if (insert_professionId??) && (insert_professionId_value??)>
${prefixName} PROFESSION_ID
    <#assign prefixName=','>
</#if>
<#if (insert_provinceId??) && (insert_provinceId_value??)>
${prefixName} PROVINCE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_sexId??) && (insert_sexId_value??)>
${prefixName} SEX_ID
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} STATE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_telephone??) && (insert_telephone_value??)>
${prefixName} TELEPHONE
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} VERSION
    <#assign prefixName=','>
</#if>
)
(
SELECT
<#assign prefixName=' '>
<#if (insert_accountName??) && (insert_accountName_value??)>
${prefixName} :insert_accountName_value
    <#assign prefixName=','>
</#if>
<#if (insert_address??) && (insert_address_value??)>
${prefixName} :insert_address_value
    <#assign prefixName=','>
</#if>
<#if (insert_age??) && (insert_age_value??)>
${prefixName} :insert_age_value
    <#assign prefixName=','>
</#if>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} :insert_beginDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_birthday??) && (insert_birthday_value??)>
${prefixName} :insert_birthday_value
    <#assign prefixName=','>
</#if>
<#if (insert_callName??) && (insert_callName_value??)>
${prefixName} :insert_callName_value
    <#assign prefixName=','>
</#if>
<#if (insert_cityId??) && (insert_cityId_value??)>
${prefixName} :insert_cityId_value
    <#assign prefixName=','>
</#if>
<#if (insert_code??) && (insert_code_value??)>
${prefixName} :insert_code_value
    <#assign prefixName=','>
</#if>
<#if (insert_companyName??) && (insert_companyName_value??)>
${prefixName} :insert_companyName_value
    <#assign prefixName=','>
</#if>
<#if (insert_countryId??) && (insert_countryId_value??)>
${prefixName} :insert_countryId_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} :insert_createdByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} :insert_createdById_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} :insert_createdByName_value
    <#assign prefixName=','>
</#if>
${prefixName} SYSDATE
<#assign prefixName=','>
<#if (insert_departmentId??) && (insert_departmentId_value??)>
${prefixName} :insert_departmentId_value
    <#assign prefixName=','>
</#if>
<#if (insert_diplomaId??) && (insert_diplomaId_value??)>
${prefixName} :insert_diplomaId_value
    <#assign prefixName=','>
</#if>
<#if (insert_districtId??) && (insert_districtId_value??)>
${prefixName} :insert_districtId_value
    <#assign prefixName=','>
</#if>
<#if (insert_email??) && (insert_email_value??)>
${prefixName} :insert_email_value
    <#assign prefixName=','>
</#if>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} :insert_endDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_flagId??) && (insert_flagId_value??)>
${prefixName} :insert_flagId_value
    <#assign prefixName=','>
</#if>
<#if (insert_folkId??) && (insert_folkId_value??)>
${prefixName} :insert_folkId_value
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} :insert_id_value
    <#assign prefixName=','>
</#if>
<#if (insert_intro??) && (insert_intro_value??)>
${prefixName} :insert_intro_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} :insert_lastModifiedByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} :insert_lastModifiedById_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} :insert_lastModifiedByName_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} :insert_lastModifiedDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_locus??) && (insert_locus_value??)>
${prefixName} :insert_locus_value
    <#assign prefixName=','>
</#if>
<#if (insert_marryStateId??) && (insert_marryStateId_value??)>
${prefixName} :insert_marryStateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_mobilePhone??) && (insert_mobilePhone_value??)>
${prefixName} :insert_mobilePhone_value
    <#assign prefixName=','>
</#if>
<#if (insert_name??) && (insert_name_value??)>
${prefixName} :insert_name_value
    <#assign prefixName=','>
</#if>
<#if (insert_orgId??) && (insert_orgId_value??)>
${prefixName} :insert_orgId_value
    <#assign prefixName=','>
</#if>
<#if (insert_password??) && (insert_password_value??)>
${prefixName} :insert_password_value
    <#assign prefixName=','>
</#if>
<#if (insert_professionId??) && (insert_professionId_value??)>
${prefixName} :insert_professionId_value
    <#assign prefixName=','>
</#if>
<#if (insert_provinceId??) && (insert_provinceId_value??)>
${prefixName} :insert_provinceId_value
    <#assign prefixName=','>
</#if>
<#if (insert_sexId??) && (insert_sexId_value??)>
${prefixName} :insert_sexId_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} :insert_stateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_telephone??) && (insert_telephone_value??)>
${prefixName} :insert_telephone_value
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} :insert_version_value
    <#assign prefixName=','>
</#if>
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM C1_USER E
<#assign prefixName='WHERE'>
<#if (where_and_eq_accountName??)>
    <#if (where_and_eq_accountName_value??)>
    ${prefixName} E.ACCOUNT_NAME = :where_and_eq_accountName_value
    <#else>
    ${prefixName} E.ACCOUNT_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_address??)>
    <#if (where_and_eq_address_value??)>
    ${prefixName} E.ADDRESS = :where_and_eq_address_value
    <#else>
    ${prefixName} E.ADDRESS IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_age??)>
    <#if (where_and_eq_age_value??)>
    ${prefixName} E.AGE = :where_and_eq_age_value
    <#else>
    ${prefixName} E.AGE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_beginDate??)>
    <#if (where_and_eq_beginDate_value??)>
    ${prefixName} E.BEGIN_DATE = :where_and_eq_beginDate_value
    <#else>
    ${prefixName} E.BEGIN_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_birthday??)>
    <#if (where_and_eq_birthday_value??)>
    ${prefixName} E.BIRTHDAY = :where_and_eq_birthday_value
    <#else>
    ${prefixName} E.BIRTHDAY IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_callName??)>
    <#if (where_and_eq_callName_value??)>
    ${prefixName} E.CALL_NAME = :where_and_eq_callName_value
    <#else>
    ${prefixName} E.CALL_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_cityId??)>
    <#if (where_and_eq_cityId_value??)>
    ${prefixName} E.CITY_ID = :where_and_eq_cityId_value
    <#else>
    ${prefixName} E.CITY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_code??)>
    <#if (where_and_eq_code_value??)>
    ${prefixName} E.CODE = :where_and_eq_code_value
    <#else>
    ${prefixName} E.CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_companyName??)>
    <#if (where_and_eq_companyName_value??)>
    ${prefixName} E.COMPANY_NAME = :where_and_eq_companyName_value
    <#else>
    ${prefixName} E.COMPANY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_countryId??)>
    <#if (where_and_eq_countryId_value??)>
    ${prefixName} E.COUNTRY_ID = :where_and_eq_countryId_value
    <#else>
    ${prefixName} E.COUNTRY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdByCode??)>
    <#if (where_and_eq_createdByCode_value??)>
    ${prefixName} E.CREATED_BY_CODE = :where_and_eq_createdByCode_value
    <#else>
    ${prefixName} E.CREATED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdById??)>
    <#if (where_and_eq_createdById_value??)>
    ${prefixName} E.CREATED_BY_ID = :where_and_eq_createdById_value
    <#else>
    ${prefixName} E.CREATED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdByName??)>
    <#if (where_and_eq_createdByName_value??)>
    ${prefixName} E.CREATED_BY_NAME = :where_and_eq_createdByName_value
    <#else>
    ${prefixName} E.CREATED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdDate??)>
    <#if (where_and_eq_createdDate_value??)>
    ${prefixName} E.CREATED_DATE = :where_and_eq_createdDate_value
    <#else>
    ${prefixName} E.CREATED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_departmentId??)>
    <#if (where_and_eq_departmentId_value??)>
    ${prefixName} E.DEPARTMENT_ID = :where_and_eq_departmentId_value
    <#else>
    ${prefixName} E.DEPARTMENT_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_diplomaId??)>
    <#if (where_and_eq_diplomaId_value??)>
    ${prefixName} E.DIPLOMA_ID = :where_and_eq_diplomaId_value
    <#else>
    ${prefixName} E.DIPLOMA_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_districtId??)>
    <#if (where_and_eq_districtId_value??)>
    ${prefixName} E.DISTRICT_ID = :where_and_eq_districtId_value
    <#else>
    ${prefixName} E.DISTRICT_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_email??)>
    <#if (where_and_eq_email_value??)>
    ${prefixName} E.EMAIL = :where_and_eq_email_value
    <#else>
    ${prefixName} E.EMAIL IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_endDate??)>
    <#if (where_and_eq_endDate_value??)>
    ${prefixName} E.END_DATE = :where_and_eq_endDate_value
    <#else>
    ${prefixName} E.END_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if (where_and_between_sysdate??)>
    <#if (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
${prefixName} SYSDATE BETWEEN NVL(E.BEGIN_DATE, SYSDATE) AND NVL(E.END_DATE, SYSDATE)
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_flagId??)>
    <#if (where_and_eq_flagId_value??)>
    ${prefixName} E.FLAG_ID = :where_and_eq_flagId_value
    <#else>
    ${prefixName} E.FLAG_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_folkId??)>
    <#if (where_and_eq_folkId_value??)>
    ${prefixName} E.FOLK_ID = :where_and_eq_folkId_value
    <#else>
    ${prefixName} E.FOLK_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_id??)>
    <#if (where_and_eq_id_value??)>
    ${prefixName} E.ID = :where_and_eq_id_value
    <#else>
    ${prefixName} E.ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_intro??)>
    <#if (where_and_eq_intro_value??)>
    ${prefixName} E.INTRO = :where_and_eq_intro_value
    <#else>
    ${prefixName} E.INTRO IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedByCode??)>
    <#if (where_and_eq_lastModifiedByCode_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_CODE = :where_and_eq_lastModifiedByCode_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedById??)>
    <#if (where_and_eq_lastModifiedById_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_ID = :where_and_eq_lastModifiedById_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedByName??)>
    <#if (where_and_eq_lastModifiedByName_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_NAME = :where_and_eq_lastModifiedByName_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedDate??)>
    <#if (where_and_eq_lastModifiedDate_value??)>
    ${prefixName} E.LAST_MODIFIED_DATE = :where_and_eq_lastModifiedDate_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_locus??)>
    <#if (where_and_eq_locus_value??)>
    ${prefixName} E.LOCUS = :where_and_eq_locus_value
    <#else>
    ${prefixName} E.LOCUS IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_marryStateId??)>
    <#if (where_and_eq_marryStateId_value??)>
    ${prefixName} E.MARRY_STATE_ID = :where_and_eq_marryStateId_value
    <#else>
    ${prefixName} E.MARRY_STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_mobilePhone??)>
    <#if (where_and_eq_mobilePhone_value??)>
    ${prefixName} E.MOBILE_PHONE = :where_and_eq_mobilePhone_value
    <#else>
    ${prefixName} E.MOBILE_PHONE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_name??)>
    <#if (where_and_eq_name_value??)>
    ${prefixName} E.NAME = :where_and_eq_name_value
    <#else>
    ${prefixName} E.NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_orgId??)>
    <#if (where_and_eq_orgId_value??)>
    ${prefixName} E.ORG_ID = :where_and_eq_orgId_value
    <#else>
    ${prefixName} E.ORG_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_password??)>
    <#if (where_and_eq_password_value??)>
    ${prefixName} E.PASSWORD = :where_and_eq_password_value
    <#else>
    ${prefixName} E.PASSWORD IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_professionId??)>
    <#if (where_and_eq_professionId_value??)>
    ${prefixName} E.PROFESSION_ID = :where_and_eq_professionId_value
    <#else>
    ${prefixName} E.PROFESSION_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_provinceId??)>
    <#if (where_and_eq_provinceId_value??)>
    ${prefixName} E.PROVINCE_ID = :where_and_eq_provinceId_value
    <#else>
    ${prefixName} E.PROVINCE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_sexId??)>
    <#if (where_and_eq_sexId_value??)>
    ${prefixName} E.SEX_ID = :where_and_eq_sexId_value
    <#else>
    ${prefixName} E.SEX_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_stateId??)>
    <#if (where_and_eq_stateId_value??)>
    ${prefixName} E.STATE_ID = :where_and_eq_stateId_value
    <#else>
    ${prefixName} E.STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_telephone??)>
    <#if (where_and_eq_telephone_value??)>
    ${prefixName} E.TELEPHONE = :where_and_eq_telephone_value
    <#else>
    ${prefixName} E.TELEPHONE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_version??)>
    <#if (where_and_eq_version_value??)>
    ${prefixName} E.VERSION = :where_and_eq_version_value
    <#else>
    ${prefixName} E.VERSION IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
)
)