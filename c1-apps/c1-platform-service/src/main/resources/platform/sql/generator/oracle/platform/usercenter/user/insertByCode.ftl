INSERT
INTO
C1_USER
(
<#assign prefixName=' '>
<#if (insert_accountName??) && (insert_accountName_value??)>
${prefixName} ACCOUNT_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_address??) && (insert_address_value??)>
${prefixName} ADDRESS
    <#assign prefixName=','>
</#if>
<#if (insert_age??) && (insert_age_value??)>
${prefixName} AGE
    <#assign prefixName=','>
</#if>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} BEGIN_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_birthday??) && (insert_birthday_value??)>
${prefixName} BIRTHDAY
    <#assign prefixName=','>
</#if>
<#if (insert_callName??) && (insert_callName_value??)>
${prefixName} CALL_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_cityId??) && (insert_cityId_value??)>
${prefixName} CITY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_code??) && (insert_code_value??)>
${prefixName} CODE
    <#assign prefixName=','>
</#if>
<#if (insert_companyName??) && (insert_companyName_value??)>
${prefixName} COMPANY_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_countryId??) && (insert_countryId_value??)>
${prefixName} COUNTRY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} CREATED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} CREATED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} CREATED_BY_NAME
    <#assign prefixName=','>
</#if>
${prefixName} CREATED_DATE
<#assign prefixName=','>
<#if (insert_departmentId??) && (insert_departmentId_value??)>
${prefixName} DEPARTMENT_ID
    <#assign prefixName=','>
</#if>
<#if (insert_diplomaId??) && (insert_diplomaId_value??)>
${prefixName} DIPLOMA_ID
    <#assign prefixName=','>
</#if>
<#if (insert_districtId??) && (insert_districtId_value??)>
${prefixName} DISTRICT_ID
    <#assign prefixName=','>
</#if>
<#if (insert_email??) && (insert_email_value??)>
${prefixName} EMAIL
    <#assign prefixName=','>
</#if>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} END_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_flagId??) && (insert_flagId_value??)>
${prefixName} FLAG_ID
    <#assign prefixName=','>
</#if>
<#if (insert_folkId??) && (insert_folkId_value??)>
${prefixName} FOLK_ID
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} ID
    <#assign prefixName=','>
</#if>
<#if (insert_intro??) && (insert_intro_value??)>
${prefixName} INTRO
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} LAST_MODIFIED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} LAST_MODIFIED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} LAST_MODIFIED_BY_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} LAST_MODIFIED_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_locus??) && (insert_locus_value??)>
${prefixName} LOCUS
    <#assign prefixName=','>
</#if>
<#if (insert_marryStateId??) && (insert_marryStateId_value??)>
${prefixName} MARRY_STATE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_mobilePhone??) && (insert_mobilePhone_value??)>
${prefixName} MOBILE_PHONE
    <#assign prefixName=','>
</#if>
<#if (insert_name??) && (insert_name_value??)>
${prefixName} NAME
    <#assign prefixName=','>
</#if>
<#if (insert_orgId??) && (insert_orgId_value??)>
${prefixName} ORG_ID
    <#assign prefixName=','>
</#if>
<#if (insert_password??) && (insert_password_value??)>
${prefixName} PASSWORD
    <#assign prefixName=','>
</#if>
<#if (insert_professionId??) && (insert_professionId_value??)>
${prefixName} PROFESSION_ID
    <#assign prefixName=','>
</#if>
<#if (insert_provinceId??) && (insert_provinceId_value??)>
${prefixName} PROVINCE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_sexId??) && (insert_sexId_value??)>
${prefixName} SEX_ID
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} STATE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_telephone??) && (insert_telephone_value??)>
${prefixName} TELEPHONE
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} VERSION
    <#assign prefixName=','>
</#if>
)
VALUES
(
<#assign prefixName=' '>
<#if (insert_accountName??) && (insert_accountName_value??)>
${prefixName} :insert_accountName_value
    <#assign prefixName=','>
</#if>
<#if (insert_address??) && (insert_address_value??)>
${prefixName} :insert_address_value
    <#assign prefixName=','>
</#if>
<#if (insert_age??) && (insert_age_value??)>
${prefixName} :insert_age_value
    <#assign prefixName=','>
</#if>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} :insert_beginDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_birthday??) && (insert_birthday_value??)>
${prefixName} :insert_birthday_value
    <#assign prefixName=','>
</#if>
<#if (insert_callName??) && (insert_callName_value??)>
${prefixName} :insert_callName_value
    <#assign prefixName=','>
</#if>
<#if (insert_cityId??) && (insert_cityId_value??)>
${prefixName} :insert_cityId_value
    <#assign prefixName=','>
</#if>
<#if (insert_code??) && (insert_code_value??)>
${prefixName} :insert_code_value
    <#assign prefixName=','>
</#if>
<#if (insert_companyName??) && (insert_companyName_value??)>
${prefixName} :insert_companyName_value
    <#assign prefixName=','>
</#if>
<#if (insert_countryId??) && (insert_countryId_value??)>
${prefixName} :insert_countryId_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} :insert_createdByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} :insert_createdById_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} :insert_createdByName_value
    <#assign prefixName=','>
</#if>
${prefixName} SYSDATE
<#assign prefixName=','>
<#if (insert_departmentId??) && (insert_departmentId_value??)>
${prefixName} :insert_departmentId_value
    <#assign prefixName=','>
</#if>
<#if (insert_diplomaId??) && (insert_diplomaId_value??)>
${prefixName} :insert_diplomaId_value
    <#assign prefixName=','>
</#if>
<#if (insert_districtId??) && (insert_districtId_value??)>
${prefixName} :insert_districtId_value
    <#assign prefixName=','>
</#if>
<#if (insert_email??) && (insert_email_value??)>
${prefixName} :insert_email_value
    <#assign prefixName=','>
</#if>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} :insert_endDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_flagId??) && (insert_flagId_value??)>
${prefixName} :insert_flagId_value
    <#assign prefixName=','>
</#if>
<#if (insert_folkId??) && (insert_folkId_value??)>
${prefixName} :insert_folkId_value
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} :insert_id_value
    <#assign prefixName=','>
</#if>
<#if (insert_intro??) && (insert_intro_value??)>
${prefixName} :insert_intro_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} :insert_lastModifiedByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} :insert_lastModifiedById_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} :insert_lastModifiedByName_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} :insert_lastModifiedDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_locus??) && (insert_locus_value??)>
${prefixName} :insert_locus_value
    <#assign prefixName=','>
</#if>
<#if (insert_marryStateId??) && (insert_marryStateId_value??)>
${prefixName} :insert_marryStateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_mobilePhone??) && (insert_mobilePhone_value??)>
${prefixName} :insert_mobilePhone_value
    <#assign prefixName=','>
</#if>
<#if (insert_name??) && (insert_name_value??)>
${prefixName} :insert_name_value
    <#assign prefixName=','>
</#if>
<#if (insert_orgId??) && (insert_orgId_value??)>
${prefixName} :insert_orgId_value
    <#assign prefixName=','>
</#if>
<#if (insert_password??) && (insert_password_value??)>
${prefixName} :insert_password_value
    <#assign prefixName=','>
</#if>
<#if (insert_professionId??) && (insert_professionId_value??)>
${prefixName} :insert_professionId_value
    <#assign prefixName=','>
</#if>
<#if (insert_provinceId??) && (insert_provinceId_value??)>
${prefixName} :insert_provinceId_value
    <#assign prefixName=','>
</#if>
<#if (insert_sexId??) && (insert_sexId_value??)>
${prefixName} :insert_sexId_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} :insert_stateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_telephone??) && (insert_telephone_value??)>
${prefixName} :insert_telephone_value
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} :insert_version_value
    <#assign prefixName=','>
</#if>
)