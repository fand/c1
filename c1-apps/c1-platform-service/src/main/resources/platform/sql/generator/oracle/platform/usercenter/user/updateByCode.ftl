UPDATE C1_USER T
<#assign prefixName='SET'>
<#if
(update_accountName??)>
    <#if
    (update_accountName_value??)>
    ${prefixName} T.ACCOUNT_NAME = :update_accountName_value
    <#else>
    ${prefixName} T.ACCOUNT_NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_address??)>
    <#if
    (update_address_value??)>
    ${prefixName} T.ADDRESS = :update_address_value
    <#else>
    ${prefixName} T.ADDRESS = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_age??)>
    <#if
    (update_age_value??)>
    ${prefixName} T.AGE = :update_age_value
    <#else>
    ${prefixName} T.AGE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_beginDate??)>
    <#if
    (update_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE = :update_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_birthday??)>
    <#if
    (update_birthday_value??)>
    ${prefixName} T.BIRTHDAY = :update_birthday_value
    <#else>
    ${prefixName} T.BIRTHDAY = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_callName??)>
    <#if
    (update_callName_value??)>
    ${prefixName} T.CALL_NAME = :update_callName_value
    <#else>
    ${prefixName} T.CALL_NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_cityId??)>
    <#if
    (update_cityId_value??)>
    ${prefixName} T.CITY_ID = :update_cityId_value
    <#else>
    ${prefixName} T.CITY_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_code??)>
    <#if
    (update_code_value??)>
    ${prefixName} T.CODE = :update_code_value
    <#else>
    ${prefixName} T.CODE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_companyName??)>
    <#if
    (update_companyName_value??)>
    ${prefixName} T.COMPANY_NAME = :update_companyName_value
    <#else>
    ${prefixName} T.COMPANY_NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_countryId??)>
    <#if
    (update_countryId_value??)>
    ${prefixName} T.COUNTRY_ID = :update_countryId_value
    <#else>
    ${prefixName} T.COUNTRY_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByCode??)>
    <#if
    (update_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE = :update_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdById??)>
    <#if
    (update_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID = :update_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByName??)>
    <#if
    (update_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME = :update_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdDate??)>
    <#if
    (update_createdDate_value??)>
    ${prefixName} T.CREATED_DATE = :update_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_departmentId??)>
    <#if
    (update_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID = :update_departmentId_value
    <#else>
    ${prefixName} T.DEPARTMENT_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_diplomaId??)>
    <#if
    (update_diplomaId_value??)>
    ${prefixName} T.DIPLOMA_ID = :update_diplomaId_value
    <#else>
    ${prefixName} T.DIPLOMA_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_districtId??)>
    <#if
    (update_districtId_value??)>
    ${prefixName} T.DISTRICT_ID = :update_districtId_value
    <#else>
    ${prefixName} T.DISTRICT_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_email??)>
    <#if
    (update_email_value??)>
    ${prefixName} T.EMAIL = :update_email_value
    <#else>
    ${prefixName} T.EMAIL = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_endDate??)>
    <#if
    (update_endDate_value??)>
    ${prefixName} T.END_DATE = :update_endDate_value
    <#else>
    ${prefixName} T.END_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_flagId??)>
    <#if
    (update_flagId_value??)>
    ${prefixName} T.FLAG_ID = :update_flagId_value
    <#else>
    ${prefixName} T.FLAG_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_folkId??)>
    <#if
    (update_folkId_value??)>
    ${prefixName} T.FOLK_ID = :update_folkId_value
    <#else>
    ${prefixName} T.FOLK_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_id??)>
    <#if
    (update_id_value??)>
    ${prefixName} T.ID = :update_id_value
    <#else>
    ${prefixName} T.ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_intro??)>
    <#if
    (update_intro_value??)>
    ${prefixName} T.INTRO = :update_intro_value
    <#else>
    ${prefixName} T.INTRO = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByCode??)>
    <#if
    (update_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = :update_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedById??)>
    <#if
    (update_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID = :update_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByName??)>
    <#if
    (update_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = :update_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedDate??)>
    <#if
    (update_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE = :update_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE = SYSDATE
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_locus??)>
    <#if
    (update_locus_value??)>
    ${prefixName} T.LOCUS = :update_locus_value
    <#else>
    ${prefixName} T.LOCUS = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_marryStateId??)>
    <#if
    (update_marryStateId_value??)>
    ${prefixName} T.MARRY_STATE_ID = :update_marryStateId_value
    <#else>
    ${prefixName} T.MARRY_STATE_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_mobilePhone??)>
    <#if
    (update_mobilePhone_value??)>
    ${prefixName} T.MOBILE_PHONE = :update_mobilePhone_value
    <#else>
    ${prefixName} T.MOBILE_PHONE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_name??)>
    <#if
    (update_name_value??)>
    ${prefixName} T.NAME = :update_name_value
    <#else>
    ${prefixName} T.NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_orgId??)>
    <#if
    (update_orgId_value??)>
    ${prefixName} T.ORG_ID = :update_orgId_value
    <#else>
    ${prefixName} T.ORG_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_password??)>
    <#if
    (update_password_value??)>
    ${prefixName} T.PASSWORD = :update_password_value
    <#else>
    ${prefixName} T.PASSWORD = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_professionId??)>
    <#if
    (update_professionId_value??)>
    ${prefixName} T.PROFESSION_ID = :update_professionId_value
    <#else>
    ${prefixName} T.PROFESSION_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_provinceId??)>
    <#if
    (update_provinceId_value??)>
    ${prefixName} T.PROVINCE_ID = :update_provinceId_value
    <#else>
    ${prefixName} T.PROVINCE_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_sexId??)>
    <#if
    (update_sexId_value??)>
    ${prefixName} T.SEX_ID = :update_sexId_value
    <#else>
    ${prefixName} T.SEX_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_stateId??)>
    <#if
    (update_stateId_value??)>
    ${prefixName} T.STATE_ID = :update_stateId_value
    <#else>
    ${prefixName} T.STATE_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_telephone??)>
    <#if
    (update_telephone_value??)>
    ${prefixName} T.TELEPHONE = :update_telephone_value
    <#else>
    ${prefixName} T.TELEPHONE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_version??)>
    <#if
    (update_version_value??)>
    ${prefixName} T.VERSION = :update_version_value
    <#else>
    ${prefixName} T.VERSION = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#include
"whereByCode.ftl">