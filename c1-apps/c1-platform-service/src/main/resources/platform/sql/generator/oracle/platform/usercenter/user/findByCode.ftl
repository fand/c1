SELECT
<#assign prefixName=' '>
<#if
(select_accountName??)>
${prefixName} T.ACCOUNT_NAME AS "accountName"
    <#assign prefixName=','>
</#if>
<#if
(select_address??)>
${prefixName} T.ADDRESS AS "address"
    <#assign prefixName=','>
</#if>
<#if
(select_age??)>
${prefixName} T.AGE AS "age"
    <#assign prefixName=','>
</#if>
<#if
(select_beginDate??)>
${prefixName} T.BEGIN_DATE AS "beginDate"
    <#assign prefixName=','>
</#if>
<#if
(select_birthday??)>
${prefixName} T.BIRTHDAY AS "birthday"
    <#assign prefixName=','>
</#if>
<#if
(select_callName??)>
${prefixName} T.CALL_NAME AS "callName"
    <#assign prefixName=','>
</#if>
<#if
(select_cityId??)>
${prefixName} T.CITY_ID AS "cityId"
    <#assign prefixName=','>
</#if>
<#if
(select_code??)>
${prefixName} T.CODE AS "code"
    <#assign prefixName=','>
</#if>
<#if
(select_companyName??)>
${prefixName} T.COMPANY_NAME AS "companyName"
    <#assign prefixName=','>
</#if>
<#if
(select_countryId??)>
${prefixName} T.COUNTRY_ID AS "countryId"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByCode??)>
${prefixName} T.CREATED_BY_CODE AS "createdByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_createdById??)>
${prefixName} T.CREATED_BY_ID AS "createdById"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByName??)>
${prefixName} T.CREATED_BY_NAME AS "createdByName"
    <#assign prefixName=','>
</#if>
<#if
(select_createdDate??)>
${prefixName} T.CREATED_DATE AS "createdDate"
    <#assign prefixName=','>
</#if>
<#if
(select_departmentId??)>
${prefixName} T.DEPARTMENT_ID AS "departmentId"
    <#assign prefixName=','>
</#if>
<#if
(select_diplomaId??)>
${prefixName} T.DIPLOMA_ID AS "diplomaId"
    <#assign prefixName=','>
</#if>
<#if
(select_districtId??)>
${prefixName} T.DISTRICT_ID AS "districtId"
    <#assign prefixName=','>
</#if>
<#if
(select_email??)>
${prefixName} T.EMAIL AS "email"
    <#assign prefixName=','>
</#if>
<#if
(select_endDate??)>
${prefixName} T.END_DATE AS "endDate"
    <#assign prefixName=','>
</#if>
<#if
(select_flagId??)>
${prefixName} T.FLAG_ID AS "flagId"
    <#assign prefixName=','>
</#if>
<#if
(select_folkId??)>
${prefixName} T.FOLK_ID AS "folkId"
    <#assign prefixName=','>
</#if>
<#if
(select_id??)>
${prefixName} T.ID AS "id"
    <#assign prefixName=','>
</#if>
<#if
(select_intro??)>
${prefixName} T.INTRO AS "intro"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByCode??)>
${prefixName} T.LAST_MODIFIED_BY_CODE AS "lastModifiedByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedById??)>
${prefixName} T.LAST_MODIFIED_BY_ID AS "lastModifiedById"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByName??)>
${prefixName} T.LAST_MODIFIED_BY_NAME AS "lastModifiedByName"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedDate??)>
${prefixName} T.LAST_MODIFIED_DATE AS "lastModifiedDate"
    <#assign prefixName=','>
</#if>
<#if
(select_locus??)>
${prefixName} T.LOCUS AS "locus"
    <#assign prefixName=','>
</#if>
<#if
(select_marryStateId??)>
${prefixName} T.MARRY_STATE_ID AS "marryStateId"
    <#assign prefixName=','>
</#if>
<#if
(select_mobilePhone??)>
${prefixName} T.MOBILE_PHONE AS "mobilePhone"
    <#assign prefixName=','>
</#if>
<#if
(select_name??)>
${prefixName} T.NAME AS "name"
    <#assign prefixName=','>
</#if>
<#if
(select_orgId??)>
${prefixName} T.ORG_ID AS "orgId"
    <#assign prefixName=','>
</#if>
<#if
(select_password??)>
${prefixName} T.PASSWORD AS "password"
    <#assign prefixName=','>
</#if>
<#if
(select_professionId??)>
${prefixName} T.PROFESSION_ID AS "professionId"
    <#assign prefixName=','>
</#if>
<#if
(select_provinceId??)>
${prefixName} T.PROVINCE_ID AS "provinceId"
    <#assign prefixName=','>
</#if>
<#if
(select_sexId??)>
${prefixName} T.SEX_ID AS "sexId"
    <#assign prefixName=','>
</#if>
<#if
(select_stateId??)>
${prefixName} T.STATE_ID AS "stateId"
    <#assign prefixName=','>
</#if>
<#if
(select_telephone??)>
${prefixName} T.TELEPHONE AS "telephone"
    <#assign prefixName=','>
</#if>
<#if
(select_version??)>
${prefixName} T.VERSION AS "version"
    <#assign prefixName=','>
</#if>
<#if prefixName== ' '>
T.ACCOUNT_NAME AS "accountName"
, T.ADDRESS AS "address"
, T.AGE AS "age"
, T.BEGIN_DATE AS "beginDate"
, T.BIRTHDAY AS "birthday"
, T.CALL_NAME AS "callName"
, T.CITY_ID AS "cityId"
, T.CODE AS "code"
, T.COMPANY_NAME AS "companyName"
, T.COUNTRY_ID AS "countryId"
, T.CREATED_BY_CODE AS "createdByCode"
, T.CREATED_BY_ID AS "createdById"
, T.CREATED_BY_NAME AS "createdByName"
, T.CREATED_DATE AS "createdDate"
, T.DEPARTMENT_ID AS "departmentId"
, T.DIPLOMA_ID AS "diplomaId"
, T.DISTRICT_ID AS "districtId"
, T.EMAIL AS "email"
, T.END_DATE AS "endDate"
, T.FLAG_ID AS "flagId"
, T.FOLK_ID AS "folkId"
, T.ID AS "id"
, T.INTRO AS "intro"
, T.LAST_MODIFIED_BY_CODE AS "lastModifiedByCode"
, T.LAST_MODIFIED_BY_ID AS "lastModifiedById"
, T.LAST_MODIFIED_BY_NAME AS "lastModifiedByName"
, T.LAST_MODIFIED_DATE AS "lastModifiedDate"
, T.LOCUS AS "locus"
, T.MARRY_STATE_ID AS "marryStateId"
, T.MOBILE_PHONE AS "mobilePhone"
, T.NAME AS "name"
, T.ORG_ID AS "orgId"
, T.PASSWORD AS "password"
, T.PROFESSION_ID AS "professionId"
, T.PROVINCE_ID AS "provinceId"
, T.SEX_ID AS "sexId"
, T.STATE_ID AS "stateId"
, T.TELEPHONE AS "telephone"
, T.VERSION AS "version"
</#if>
FROM C1_USER T
<#include "whereByCode.ftl">
<#assign prefixName='ORDER BY'>
<#if
(order_by_accountName??)>
${prefixName} T.ACCOUNT_NAME ${order_by_accountName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_address??)>
${prefixName} T.ADDRESS ${order_by_address_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_age??)>
${prefixName} T.AGE ${order_by_age_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_beginDate??)>
${prefixName} T.BEGIN_DATE ${order_by_beginDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_birthday??)>
${prefixName} T.BIRTHDAY ${order_by_birthday_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_callName??)>
${prefixName} T.CALL_NAME ${order_by_callName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_cityId??)>
${prefixName} T.CITY_ID ${order_by_cityId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_code??)>
${prefixName} T.CODE ${order_by_code_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_companyName??)>
${prefixName} T.COMPANY_NAME ${order_by_companyName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_countryId??)>
${prefixName} T.COUNTRY_ID ${order_by_countryId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByCode??)>
${prefixName} T.CREATED_BY_CODE ${order_by_createdByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdById??)>
${prefixName} T.CREATED_BY_ID ${order_by_createdById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByName??)>
${prefixName} T.CREATED_BY_NAME ${order_by_createdByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdDate??)>
${prefixName} T.CREATED_DATE ${order_by_createdDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_departmentId??)>
${prefixName} T.DEPARTMENT_ID ${order_by_departmentId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_diplomaId??)>
${prefixName} T.DIPLOMA_ID ${order_by_diplomaId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_districtId??)>
${prefixName} T.DISTRICT_ID ${order_by_districtId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_email??)>
${prefixName} T.EMAIL ${order_by_email_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_endDate??)>
${prefixName} T.END_DATE ${order_by_endDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_flagId??)>
${prefixName} T.FLAG_ID ${order_by_flagId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_folkId??)>
${prefixName} T.FOLK_ID ${order_by_folkId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_id??)>
${prefixName} T.ID ${order_by_id_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_intro??)>
${prefixName} T.INTRO ${order_by_intro_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByCode??)>
${prefixName} T.LAST_MODIFIED_BY_CODE ${order_by_lastModifiedByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedById??)>
${prefixName} T.LAST_MODIFIED_BY_ID ${order_by_lastModifiedById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByName??)>
${prefixName} T.LAST_MODIFIED_BY_NAME ${order_by_lastModifiedByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedDate??)>
${prefixName} T.LAST_MODIFIED_DATE ${order_by_lastModifiedDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_locus??)>
${prefixName} T.LOCUS ${order_by_locus_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_marryStateId??)>
${prefixName} T.MARRY_STATE_ID ${order_by_marryStateId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_mobilePhone??)>
${prefixName} T.MOBILE_PHONE ${order_by_mobilePhone_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_name??)>
${prefixName} T.NAME ${order_by_name_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_orgId??)>
${prefixName} T.ORG_ID ${order_by_orgId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_password??)>
${prefixName} T.PASSWORD ${order_by_password_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_professionId??)>
${prefixName} T.PROFESSION_ID ${order_by_professionId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_provinceId??)>
${prefixName} T.PROVINCE_ID ${order_by_provinceId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_sexId??)>
${prefixName} T.SEX_ID ${order_by_sexId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateId??)>
${prefixName} T.STATE_ID ${order_by_stateId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_telephone??)>
${prefixName} T.TELEPHONE ${order_by_telephone_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_version??)>
${prefixName} T.VERSION ${order_by_version_value!}
    <#assign prefixName=','>
</#if>
