SELECT
<#switch (object_id!)>
    <#case "beginDate">
    T.BEGIN_DATE AS beginDate
        <#break>
    <#case "createdByCode">
    T.CREATED_BY_CODE AS createdByCode
        <#break>
    <#case "createdById">
    T.CREATED_BY_ID AS createdById
        <#break>
    <#case "createdByName">
    T.CREATED_BY_NAME AS createdByName
        <#break>
    <#case "createdDate">
    T.CREATED_DATE AS createdDate
        <#break>
    <#case "endDate">
    T.END_DATE AS endDate
        <#break>
    <#case "id">
    T.ID AS id
        <#break>
    <#case "lastModifiedByCode">
    T.LAST_MODIFIED_BY_CODE AS lastModifiedByCode
        <#break>
    <#case "lastModifiedById">
    T.LAST_MODIFIED_BY_ID AS lastModifiedById
        <#break>
    <#case "lastModifiedByName">
    T.LAST_MODIFIED_BY_NAME AS lastModifiedByName
        <#break>
    <#case "lastModifiedDate">
    T.LAST_MODIFIED_DATE AS lastModifiedDate
        <#break>
    <#case "orderNum">
    T.ORDER_NUM AS orderNum
        <#break>
    <#case "permissionId">
    T.PERMISSION_ID AS permissionId
        <#break>
    <#case "stateId">
    T.STATE_ID AS stateId
        <#break>
    <#case "validateErrorId">
    T.VALIDATE_ERROR_ID AS validateErrorId
        <#break>
    <#case "validateRuleId">
    T.VALIDATE_RULE_ID AS validateRuleId
        <#break>
    <#case "validateSuccessId">
    T.VALIDATE_SUCCESS_ID AS validateSuccessId
        <#break>
    <#case "version">
    T.VERSION AS version
        <#break>
    <#default>
    COUNT(1) AS COUNT_
</#switch>
FROM C1_PE_VA_RU_TRANSACT_MODE T
<#include "whereByCode.ftl">