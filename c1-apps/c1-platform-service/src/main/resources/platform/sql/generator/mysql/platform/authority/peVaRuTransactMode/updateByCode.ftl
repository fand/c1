UPDATE c1_pe_va_ru_transact_mode
<#assign prefixName='SET'>
<#if
(update_beginDate??)>
    <#if
    (update_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` = :update_beginDate_value
    <#else>
    ${prefixName} `BEGIN_DATE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByCode??)>
    <#if
    (update_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` = :update_createdByCode_value
    <#else>
    ${prefixName} `CREATED_BY_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdById??)>
    <#if
    (update_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` = :update_createdById_value
    <#else>
    ${prefixName} `CREATED_BY_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByName??)>
    <#if
    (update_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` = :update_createdByName_value
    <#else>
    ${prefixName} `CREATED_BY_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdDate??)>
    <#if
    (update_createdDate_value??)>
    ${prefixName} `CREATED_DATE` = :update_createdDate_value
    <#else>
    ${prefixName} `CREATED_DATE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_dataStateCode??)>
    <#if
    (update_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` = :update_dataStateCode_value
    <#else>
    ${prefixName} `DATA_STATE_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_dataStateId??)>
    <#if
    (update_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` = :update_dataStateId_value
    <#else>
    ${prefixName} `DATA_STATE_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_dataStateName??)>
    <#if
    (update_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` = :update_dataStateName_value
    <#else>
    ${prefixName} `DATA_STATE_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_endDate??)>
    <#if
    (update_endDate_value??)>
    ${prefixName} `END_DATE` = :update_endDate_value
    <#else>
    ${prefixName} `END_DATE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_id??)>
    <#if
    (update_id_value??)>
    ${prefixName} `ID` = :update_id_value
    <#else>
    ${prefixName} `ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByCode??)>
    <#if
    (update_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = :update_lastModifiedByCode_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedById??)>
    <#if
    (update_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` = :update_lastModifiedById_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByName??)>
    <#if
    (update_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = :update_lastModifiedByName_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedDate??)>
    <#if
    (update_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` = :update_lastModifiedDate_value
    <#else>
    ${prefixName} `LAST_MODIFIED_DATE` = NOW()
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_orderNum??)>
    <#if
    (update_orderNum_value??)>
    ${prefixName} `ORDER_NUM` = :update_orderNum_value
    <#else>
    ${prefixName} `ORDER_NUM` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_permissionId??)>
    <#if
    (update_permissionId_value??)>
    ${prefixName} `PERMISSION_ID` = :update_permissionId_value
    <#else>
    ${prefixName} `PERMISSION_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_stateCode??)>
    <#if
    (update_stateCode_value??)>
    ${prefixName} `STATE_CODE` = :update_stateCode_value
    <#else>
    ${prefixName} `STATE_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_stateId??)>
    <#if
    (update_stateId_value??)>
    ${prefixName} `STATE_ID` = :update_stateId_value
    <#else>
    ${prefixName} `STATE_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_stateName??)>
    <#if
    (update_stateName_value??)>
    ${prefixName} `STATE_NAME` = :update_stateName_value
    <#else>
    ${prefixName} `STATE_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateErrorCode??)>
    <#if
    (update_validateErrorCode_value??)>
    ${prefixName} `VALIDATE_ERROR_CODE` = :update_validateErrorCode_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateErrorId??)>
    <#if
    (update_validateErrorId_value??)>
    ${prefixName} `VALIDATE_ERROR_ID` = :update_validateErrorId_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateErrorName??)>
    <#if
    (update_validateErrorName_value??)>
    ${prefixName} `VALIDATE_ERROR_NAME` = :update_validateErrorName_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateRuleCode??)>
    <#if
    (update_validateRuleCode_value??)>
    ${prefixName} `VALIDATE_RULE_CODE` = :update_validateRuleCode_value
    <#else>
    ${prefixName} `VALIDATE_RULE_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateRuleId??)>
    <#if
    (update_validateRuleId_value??)>
    ${prefixName} `VALIDATE_RULE_ID` = :update_validateRuleId_value
    <#else>
    ${prefixName} `VALIDATE_RULE_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateRuleName??)>
    <#if
    (update_validateRuleName_value??)>
    ${prefixName} `VALIDATE_RULE_NAME` = :update_validateRuleName_value
    <#else>
    ${prefixName} `VALIDATE_RULE_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateSuccessCode??)>
    <#if
    (update_validateSuccessCode_value??)>
    ${prefixName} `VALIDATE_SUCCESS_CODE` = :update_validateSuccessCode_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_CODE` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateSuccessId??)>
    <#if
    (update_validateSuccessId_value??)>
    ${prefixName} `VALIDATE_SUCCESS_ID` = :update_validateSuccessId_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_ID` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateSuccessName??)>
    <#if
    (update_validateSuccessName_value??)>
    ${prefixName} `VALIDATE_SUCCESS_NAME` = :update_validateSuccessName_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_NAME` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_versionInt??)>
    <#if
    (update_versionInt_value??)>
    ${prefixName} `VERSION_INT` = :update_versionInt_value
    <#else>
    ${prefixName} `VERSION_INT` = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#include
"whereByCode.ftl">