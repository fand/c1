SELECT
<#switch (object_id!)>
    <#case "accountName">
    T.ACCOUNT_NAME AS accountName
        <#break>
    <#case "address">
    T.ADDRESS AS address
        <#break>
    <#case "age">
    T.AGE AS age
        <#break>
    <#case "beginDate">
    T.BEGIN_DATE AS beginDate
        <#break>
    <#case "birthday">
    T.BIRTHDAY AS birthday
        <#break>
    <#case "callName">
    T.CALL_NAME AS callName
        <#break>
    <#case "cityId">
    T.CITY_ID AS cityId
        <#break>
    <#case "code">
    T.CODE AS code
        <#break>
    <#case "companyName">
    T.COMPANY_NAME AS companyName
        <#break>
    <#case "countryId">
    T.COUNTRY_ID AS countryId
        <#break>
    <#case "createdByCode">
    T.CREATED_BY_CODE AS createdByCode
        <#break>
    <#case "createdById">
    T.CREATED_BY_ID AS createdById
        <#break>
    <#case "createdByName">
    T.CREATED_BY_NAME AS createdByName
        <#break>
    <#case "createdDate">
    T.CREATED_DATE AS createdDate
        <#break>
    <#case "departmentId">
    T.DEPARTMENT_ID AS departmentId
        <#break>
    <#case "diplomaId">
    T.DIPLOMA_ID AS diplomaId
        <#break>
    <#case "districtId">
    T.DISTRICT_ID AS districtId
        <#break>
    <#case "email">
    T.EMAIL AS email
        <#break>
    <#case "endDate">
    T.END_DATE AS endDate
        <#break>
    <#case "flagId">
    T.FLAG_ID AS flagId
        <#break>
    <#case "folkId">
    T.FOLK_ID AS folkId
        <#break>
    <#case "id">
    T.ID AS id
        <#break>
    <#case "intro">
    T.INTRO AS intro
        <#break>
    <#case "lastModifiedByCode">
    T.LAST_MODIFIED_BY_CODE AS lastModifiedByCode
        <#break>
    <#case "lastModifiedById">
    T.LAST_MODIFIED_BY_ID AS lastModifiedById
        <#break>
    <#case "lastModifiedByName">
    T.LAST_MODIFIED_BY_NAME AS lastModifiedByName
        <#break>
    <#case "lastModifiedDate">
    T.LAST_MODIFIED_DATE AS lastModifiedDate
        <#break>
    <#case "locus">
    T.LOCUS AS locus
        <#break>
    <#case "marryStateId">
    T.MARRY_STATE_ID AS marryStateId
        <#break>
    <#case "mobilePhone">
    T.MOBILE_PHONE AS mobilePhone
        <#break>
    <#case "name">
    T.NAME AS name
        <#break>
    <#case "orgId">
    T.ORG_ID AS orgId
        <#break>
    <#case "password">
    T.PASSWORD AS password
        <#break>
    <#case "professionId">
    T.PROFESSION_ID AS professionId
        <#break>
    <#case "provinceId">
    T.PROVINCE_ID AS provinceId
        <#break>
    <#case "sexId">
    T.SEX_ID AS sexId
        <#break>
    <#case "stateId">
    T.STATE_ID AS stateId
        <#break>
    <#case "telephone">
    T.TELEPHONE AS telephone
        <#break>
    <#case "version">
    T.VERSION AS version
        <#break>
    <#default>
    COUNT(1) AS COUNT_
</#switch>
FROM C1_USER T
<#include
"whereByCode.ftl">