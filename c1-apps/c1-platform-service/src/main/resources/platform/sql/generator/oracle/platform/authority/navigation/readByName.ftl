SELECT
<#switch (object_id!)>
    <#case "beginDate">
    T.BEGIN_DATE AS beginDate
        <#break>
    <#case "code">
    T.CODE AS code
        <#break>
    <#case "createdByCode">
    T.CREATED_BY_CODE AS createdByCode
        <#break>
    <#case "createdById">
    T.CREATED_BY_ID AS createdById
        <#break>
    <#case "createdByName">
    T.CREATED_BY_NAME AS createdByName
        <#break>
    <#case "createdDate">
    T.CREATED_DATE AS createdDate
        <#break>
    <#case "dataOption">
    T.DATA_OPTION AS dataOption
        <#break>
    <#case "endDate">
    T.END_DATE AS endDate
        <#break>
    <#case "iconStyle">
    T.ICON_STYLE AS iconStyle
        <#break>
    <#case "id">
    T.ID AS id
        <#break>
    <#case "lastModifiedByCode">
    T.LAST_MODIFIED_BY_CODE AS lastModifiedByCode
        <#break>
    <#case "lastModifiedById">
    T.LAST_MODIFIED_BY_ID AS lastModifiedById
        <#break>
    <#case "lastModifiedByName">
    T.LAST_MODIFIED_BY_NAME AS lastModifiedByName
        <#break>
    <#case "lastModifiedDate">
    T.LAST_MODIFIED_DATE AS lastModifiedDate
        <#break>
    <#case "name">
    T.NAME AS name
        <#break>
    <#case "orderNum">
    T.ORDER_NUM AS orderNum
        <#break>
    <#case "parentId">
    T.PARENT_ID AS parentId
        <#break>
    <#case "siteId">
    T.SITE_ID AS siteId
        <#break>
    <#case "stateId">
    T.STATE_ID AS stateId
        <#break>
    <#case "target">
    T.TARGET AS target
        <#break>
    <#case "typeId">
    T.TYPE_ID AS typeId
        <#break>
    <#case "url">
    T.URL AS url
        <#break>
    <#case "version">
    T.VERSION AS version
        <#break>
    <#default>
    COUNT(1) AS COUNT_
</#switch>
FROM C1_NAVIGATION T
<#include "whereByCode.ftl">