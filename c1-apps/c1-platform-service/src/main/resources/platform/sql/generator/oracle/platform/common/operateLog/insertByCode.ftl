INSERT
INTO
C1_OPERATE_LOG
(
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} BEGIN_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_businessCode??) && (insert_businessCode_value??)>
${prefixName} BUSINESS_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_code??) && (insert_code_value??)>
${prefixName} CODE
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} CREATED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} CREATED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} CREATED_BY_NAME
    <#assign prefixName=','>
</#if>
${prefixName} CREATED_DATE
<#assign prefixName=','>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} END_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} ID
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} LAST_MODIFIED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} LAST_MODIFIED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} LAST_MODIFIED_BY_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} LAST_MODIFIED_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_message??) && (insert_message_value??)>
${prefixName} MESSAGE
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} STATE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_typeId??) && (insert_typeId_value??)>
${prefixName} TYPE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_userId??) && (insert_userId_value??)>
${prefixName} USER_ID
    <#assign prefixName=','>
</#if>
<#if (insert_value??) && (insert_value_value??)>
${prefixName} VALUE
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} VERSION
    <#assign prefixName=','>
</#if>
)
VALUES
(
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} :insert_beginDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_businessCode??) && (insert_businessCode_value??)>
${prefixName} :insert_businessCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_code??) && (insert_code_value??)>
${prefixName} :insert_code_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} :insert_createdByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} :insert_createdById_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} :insert_createdByName_value
    <#assign prefixName=','>
</#if>
${prefixName} SYSDATE
<#assign prefixName=','>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} :insert_endDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} :insert_id_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} :insert_lastModifiedByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} :insert_lastModifiedById_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} :insert_lastModifiedByName_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} :insert_lastModifiedDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_message??) && (insert_message_value??)>
${prefixName} :insert_message_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} :insert_stateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_typeId??) && (insert_typeId_value??)>
${prefixName} :insert_typeId_value
    <#assign prefixName=','>
</#if>
<#if (insert_userId??) && (insert_userId_value??)>
${prefixName} :insert_userId_value
    <#assign prefixName=','>
</#if>
<#if (insert_value??) && (insert_value_value??)>
${prefixName} :insert_value_value
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} :insert_version_value
    <#assign prefixName=','>
</#if>
)