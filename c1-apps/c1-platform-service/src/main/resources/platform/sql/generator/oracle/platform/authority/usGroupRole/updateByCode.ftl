UPDATE C1_US_GROUP_ROLE T
<#assign prefixName='SET'>
<#if
(update_beginDate??)>
    <#if
    (update_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE = :update_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByCode??)>
    <#if
    (update_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE = :update_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdById??)>
    <#if
    (update_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID = :update_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByName??)>
    <#if
    (update_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME = :update_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdDate??)>
    <#if
    (update_createdDate_value??)>
    ${prefixName} T.CREATED_DATE = :update_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_endDate??)>
    <#if
    (update_endDate_value??)>
    ${prefixName} T.END_DATE = :update_endDate_value
    <#else>
    ${prefixName} T.END_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_id??)>
    <#if
    (update_id_value??)>
    ${prefixName} T.ID = :update_id_value
    <#else>
    ${prefixName} T.ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByCode??)>
    <#if
    (update_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = :update_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedById??)>
    <#if
    (update_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID = :update_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByName??)>
    <#if
    (update_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = :update_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedDate??)>
    <#if
    (update_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE = :update_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE = SYSDATE
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_roleId??)>
    <#if
    (update_roleId_value??)>
    ${prefixName} T.ROLE_ID = :update_roleId_value
    <#else>
    ${prefixName} T.ROLE_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_stateId??)>
    <#if
    (update_stateId_value??)>
    ${prefixName} T.STATE_ID = :update_stateId_value
    <#else>
    ${prefixName} T.STATE_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_userGroupId??)>
    <#if
    (update_userGroupId_value??)>
    ${prefixName} T.USER_GROUP_ID = :update_userGroupId_value
    <#else>
    ${prefixName} T.USER_GROUP_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_version??)>
    <#if
    (update_version_value??)>
    ${prefixName} T.VERSION = :update_version_value
    <#else>
    ${prefixName} T.VERSION = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#include
"whereByCode.ftl">