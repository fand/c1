SELECT
<#switch (object_id!)>
    <#case "beginDate">
    T.BEGIN_DATE AS beginDate
        <#break>
    <#case "contentType">
    T.CONTENT_TYPE AS contentType
        <#break>
    <#case "createdByCode">
    T.CREATED_BY_CODE AS createdByCode
        <#break>
    <#case "createdById">
    T.CREATED_BY_ID AS createdById
        <#break>
    <#case "createdByName">
    T.CREATED_BY_NAME AS createdByName
        <#break>
    <#case "createdDate">
    T.CREATED_DATE AS createdDate
        <#break>
    <#case "endDate">
    T.END_DATE AS endDate
        <#break>
    <#case "entityId">
    T.ENTITY_ID AS entityId
        <#break>
    <#case "extension">
    T.EXTENSION AS extension
        <#break>
    <#case "filename">
    T.FILENAME AS filename
        <#break>
    <#case "id">
    T.ID AS id
        <#break>
    <#case "lastModifiedByCode">
    T.LAST_MODIFIED_BY_CODE AS lastModifiedByCode
        <#break>
    <#case "lastModifiedById">
    T.LAST_MODIFIED_BY_ID AS lastModifiedById
        <#break>
    <#case "lastModifiedByName">
    T.LAST_MODIFIED_BY_NAME AS lastModifiedByName
        <#break>
    <#case "lastModifiedDate">
    T.LAST_MODIFIED_DATE AS lastModifiedDate
        <#break>
    <#case "name">
    T.NAME AS name
        <#break>
    <#case "originalFilename">
    T.ORIGINAL_FILENAME AS originalFilename
        <#break>
    <#case "parentId">
    T.PARENT_ID AS parentId
        <#break>
    <#case "stateId">
    T.STATE_ID AS stateId
        <#break>
    <#case "thumbnailValue">
    T.THUMBNAIL_VALUE AS thumbnailValue
        <#break>
    <#case "typeId">
    T.TYPE_ID AS typeId
        <#break>
    <#case "version">
    T.VERSION AS version
        <#break>
    <#default>
    COUNT(1) AS COUNT_
</#switch>
FROM C1_ENTITY_FILE T
<#include
"whereByCode.ftl">