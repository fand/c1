SELECT
<#switch (object_id!)>
    <#case "age">
    T.AGE AS age
        <#break>
    <#case "beginDate">
    T.BEGIN_DATE AS beginDate
        <#break>
    <#case "code">
    T.CODE AS code
        <#break>
    <#case "createdByCode">
    T.CREATED_BY_CODE AS createdByCode
        <#break>
    <#case "createdById">
    T.CREATED_BY_ID AS createdById
        <#break>
    <#case "createdByName">
    T.CREATED_BY_NAME AS createdByName
        <#break>
    <#case "createdDate">
    T.CREATED_DATE AS createdDate
        <#break>
    <#case "departmentId">
    T.DEPARTMENT_ID AS departmentId
        <#break>
    <#case "email">
    T.EMAIL AS email
        <#break>
    <#case "endDate">
    T.END_DATE AS endDate
        <#break>
    <#case "id">
    T.ID AS id
        <#break>
    <#case "intro">
    T.INTRO AS intro
        <#break>
    <#case "lastModifiedByCode">
    T.LAST_MODIFIED_BY_CODE AS lastModifiedByCode
        <#break>
    <#case "lastModifiedById">
    T.LAST_MODIFIED_BY_ID AS lastModifiedById
        <#break>
    <#case "lastModifiedByName">
    T.LAST_MODIFIED_BY_NAME AS lastModifiedByName
        <#break>
    <#case "lastModifiedDate">
    T.LAST_MODIFIED_DATE AS lastModifiedDate
        <#break>
    <#case "mobile">
    T.MOBILE AS mobile
        <#break>
    <#case "name">
    T.NAME AS name
        <#break>
    <#case "orgId">
    T.ORG_ID AS orgId
        <#break>
    <#case "password">
    T.PASSWORD AS password
        <#break>
    <#case "sexId">
    T.SEX_ID AS sexId
        <#break>
    <#case "stateId">
    T.STATE_ID AS stateId
        <#break>
    <#case "version">
    T.VERSION AS version
        <#break>
    <#default>
    COUNT(1) AS COUNT_
</#switch>
FROM C1_USER T
<#include "whereByCode.ftl">