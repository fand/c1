INSERT
INTO
C1_USER_ACTIVATION
(
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} BEGIN_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} CREATED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} CREATED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} CREATED_BY_NAME
    <#assign prefixName=','>
</#if>
${prefixName} CREATED_DATE
<#assign prefixName=','>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} END_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} ID
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} LAST_MODIFIED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} LAST_MODIFIED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} LAST_MODIFIED_BY_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} LAST_MODIFIED_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_password??) && (insert_password_value??)>
${prefixName} PASSWORD
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} STATE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_userId??) && (insert_userId_value??)>
${prefixName} USER_ID
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} VERSION
    <#assign prefixName=','>
</#if>
)
(
SELECT
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} :insert_beginDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} :insert_createdByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} :insert_createdById_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} :insert_createdByName_value
    <#assign prefixName=','>
</#if>
${prefixName} SYSDATE
<#assign prefixName=','>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} :insert_endDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} :insert_id_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} :insert_lastModifiedByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} :insert_lastModifiedById_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} :insert_lastModifiedByName_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} :insert_lastModifiedDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_password??) && (insert_password_value??)>
${prefixName} :insert_password_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} :insert_stateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_userId??) && (insert_userId_value??)>
${prefixName} :insert_userId_value
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} :insert_version_value
    <#assign prefixName=','>
</#if>
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM C1_USER_ACTIVATION E
<#assign prefixName='WHERE'>
<#if (where_and_eq_beginDate??)>
    <#if (where_and_eq_beginDate_value??)>
    ${prefixName} E.BEGIN_DATE = :where_and_eq_beginDate_value
    <#else>
    ${prefixName} E.BEGIN_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdByCode??)>
    <#if (where_and_eq_createdByCode_value??)>
    ${prefixName} E.CREATED_BY_CODE = :where_and_eq_createdByCode_value
    <#else>
    ${prefixName} E.CREATED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdById??)>
    <#if (where_and_eq_createdById_value??)>
    ${prefixName} E.CREATED_BY_ID = :where_and_eq_createdById_value
    <#else>
    ${prefixName} E.CREATED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdByName??)>
    <#if (where_and_eq_createdByName_value??)>
    ${prefixName} E.CREATED_BY_NAME = :where_and_eq_createdByName_value
    <#else>
    ${prefixName} E.CREATED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdDate??)>
    <#if (where_and_eq_createdDate_value??)>
    ${prefixName} E.CREATED_DATE = :where_and_eq_createdDate_value
    <#else>
    ${prefixName} E.CREATED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_endDate??)>
    <#if (where_and_eq_endDate_value??)>
    ${prefixName} E.END_DATE = :where_and_eq_endDate_value
    <#else>
    ${prefixName} E.END_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if (where_and_between_sysdate??)>
    <#if (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
${prefixName} SYSDATE BETWEEN NVL(E.BEGIN_DATE, SYSDATE) AND NVL(E.END_DATE, SYSDATE)
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_id??)>
    <#if (where_and_eq_id_value??)>
    ${prefixName} E.ID = :where_and_eq_id_value
    <#else>
    ${prefixName} E.ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedByCode??)>
    <#if (where_and_eq_lastModifiedByCode_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_CODE = :where_and_eq_lastModifiedByCode_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedById??)>
    <#if (where_and_eq_lastModifiedById_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_ID = :where_and_eq_lastModifiedById_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedByName??)>
    <#if (where_and_eq_lastModifiedByName_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_NAME = :where_and_eq_lastModifiedByName_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedDate??)>
    <#if (where_and_eq_lastModifiedDate_value??)>
    ${prefixName} E.LAST_MODIFIED_DATE = :where_and_eq_lastModifiedDate_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_password??)>
    <#if (where_and_eq_password_value??)>
    ${prefixName} E.PASSWORD = :where_and_eq_password_value
    <#else>
    ${prefixName} E.PASSWORD IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_stateId??)>
    <#if (where_and_eq_stateId_value??)>
    ${prefixName} E.STATE_ID = :where_and_eq_stateId_value
    <#else>
    ${prefixName} E.STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_userId??)>
    <#if (where_and_eq_userId_value??)>
    ${prefixName} E.USER_ID = :where_and_eq_userId_value
    <#else>
    ${prefixName} E.USER_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_version??)>
    <#if (where_and_eq_version_value??)>
    ${prefixName} E.VERSION = :where_and_eq_version_value
    <#else>
    ${prefixName} E.VERSION IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
)
)