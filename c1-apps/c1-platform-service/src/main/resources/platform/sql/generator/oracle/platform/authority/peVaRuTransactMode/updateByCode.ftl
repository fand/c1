UPDATE C1_PE_VA_RU_TRANSACT_MODE T
<#assign prefixName='SET'>
<#if
(update_beginDate??)>
    <#if
    (update_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE = :update_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByCode??)>
    <#if
    (update_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE = :update_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdById??)>
    <#if
    (update_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID = :update_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByName??)>
    <#if
    (update_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME = :update_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdDate??)>
    <#if
    (update_createdDate_value??)>
    ${prefixName} T.CREATED_DATE = :update_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_endDate??)>
    <#if
    (update_endDate_value??)>
    ${prefixName} T.END_DATE = :update_endDate_value
    <#else>
    ${prefixName} T.END_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_id??)>
    <#if
    (update_id_value??)>
    ${prefixName} T.ID = :update_id_value
    <#else>
    ${prefixName} T.ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByCode??)>
    <#if
    (update_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = :update_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedById??)>
    <#if
    (update_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID = :update_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByName??)>
    <#if
    (update_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = :update_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedDate??)>
    <#if
    (update_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE = :update_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE = SYSDATE
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_orderNum??)>
    <#if
    (update_orderNum_value??)>
    ${prefixName} T.ORDER_NUM = :update_orderNum_value
    <#else>
    ${prefixName} T.ORDER_NUM = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_permissionId??)>
    <#if
    (update_permissionId_value??)>
    ${prefixName} T.PERMISSION_ID = :update_permissionId_value
    <#else>
    ${prefixName} T.PERMISSION_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_stateId??)>
    <#if
    (update_stateId_value??)>
    ${prefixName} T.STATE_ID = :update_stateId_value
    <#else>
    ${prefixName} T.STATE_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateErrorId??)>
    <#if
    (update_validateErrorId_value??)>
    ${prefixName} T.VALIDATE_ERROR_ID = :update_validateErrorId_value
    <#else>
    ${prefixName} T.VALIDATE_ERROR_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateRuleId??)>
    <#if
    (update_validateRuleId_value??)>
    ${prefixName} T.VALIDATE_RULE_ID = :update_validateRuleId_value
    <#else>
    ${prefixName} T.VALIDATE_RULE_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_validateSuccessId??)>
    <#if
    (update_validateSuccessId_value??)>
    ${prefixName} T.VALIDATE_SUCCESS_ID = :update_validateSuccessId_value
    <#else>
    ${prefixName} T.VALIDATE_SUCCESS_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_version??)>
    <#if
    (update_version_value??)>
    ${prefixName} T.VERSION = :update_version_value
    <#else>
    ${prefixName} T.VERSION = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#include
"whereByCode.ftl">