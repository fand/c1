SELECT
<#assign prefixName=' '>
<#if
(select_beginDate??)>
${prefixName} T.BEGIN_DATE AS "beginDate"
    <#assign prefixName=','>
</#if>
<#if
(select_code??)>
${prefixName} T.CODE AS "code"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByCode??)>
${prefixName} T.CREATED_BY_CODE AS "createdByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_createdById??)>
${prefixName} T.CREATED_BY_ID AS "createdById"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByName??)>
${prefixName} T.CREATED_BY_NAME AS "createdByName"
    <#assign prefixName=','>
</#if>
<#if
(select_createdDate??)>
${prefixName} T.CREATED_DATE AS "createdDate"
    <#assign prefixName=','>
</#if>
<#if
(select_endDate??)>
${prefixName} T.END_DATE AS "endDate"
    <#assign prefixName=','>
</#if>
<#if
(select_id??)>
${prefixName} T.ID AS "id"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByCode??)>
${prefixName} T.LAST_MODIFIED_BY_CODE AS "lastModifiedByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedById??)>
${prefixName} T.LAST_MODIFIED_BY_ID AS "lastModifiedById"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByName??)>
${prefixName} T.LAST_MODIFIED_BY_NAME AS "lastModifiedByName"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedDate??)>
${prefixName} T.LAST_MODIFIED_DATE AS "lastModifiedDate"
    <#assign prefixName=','>
</#if>
<#if
(select_name??)>
${prefixName} T.NAME AS "name"
    <#assign prefixName=','>
</#if>
<#if
(select_orderNum??)>
${prefixName} T.ORDER_NUM AS "orderNum"
    <#assign prefixName=','>
</#if>
<#if
(select_siteId??)>
${prefixName} T.SITE_ID AS "siteId"
    <#assign prefixName=','>
</#if>
<#if
(select_stateId??)>
${prefixName} T.STATE_ID AS "stateId"
    <#assign prefixName=','>
</#if>
<#if
(select_version??)>
${prefixName} T.VERSION AS "version"
    <#assign prefixName=','>
</#if>
<#if prefixName== ' '>
T.BEGIN_DATE AS "beginDate"
, T.CODE AS "code"
, T.CREATED_BY_CODE AS "createdByCode"
, T.CREATED_BY_ID AS "createdById"
, T.CREATED_BY_NAME AS "createdByName"
, T.CREATED_DATE AS "createdDate"
, T.END_DATE AS "endDate"
, T.ID AS "id"
, T.LAST_MODIFIED_BY_CODE AS "lastModifiedByCode"
, T.LAST_MODIFIED_BY_ID AS "lastModifiedById"
, T.LAST_MODIFIED_BY_NAME AS "lastModifiedByName"
, T.LAST_MODIFIED_DATE AS "lastModifiedDate"
, T.NAME AS "name"
, T.ORDER_NUM AS "orderNum"
, T.SITE_ID AS "siteId"
, T.STATE_ID AS "stateId"
, T.VERSION AS "version"
</#if>
FROM C1_SITE_URL T
<#include "whereByCode.ftl">
<#assign prefixName='ORDER BY'>
<#if
(order_by_beginDate??)>
${prefixName} T.BEGIN_DATE ${order_by_beginDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_code??)>
${prefixName} T.CODE ${order_by_code_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByCode??)>
${prefixName} T.CREATED_BY_CODE ${order_by_createdByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdById??)>
${prefixName} T.CREATED_BY_ID ${order_by_createdById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByName??)>
${prefixName} T.CREATED_BY_NAME ${order_by_createdByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdDate??)>
${prefixName} T.CREATED_DATE ${order_by_createdDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_endDate??)>
${prefixName} T.END_DATE ${order_by_endDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_id??)>
${prefixName} T.ID ${order_by_id_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByCode??)>
${prefixName} T.LAST_MODIFIED_BY_CODE ${order_by_lastModifiedByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedById??)>
${prefixName} T.LAST_MODIFIED_BY_ID ${order_by_lastModifiedById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByName??)>
${prefixName} T.LAST_MODIFIED_BY_NAME ${order_by_lastModifiedByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedDate??)>
${prefixName} T.LAST_MODIFIED_DATE ${order_by_lastModifiedDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_name??)>
${prefixName} T.NAME ${order_by_name_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_orderNum??)>
${prefixName} T.ORDER_NUM ${order_by_orderNum_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_siteId??)>
${prefixName} T.SITE_ID ${order_by_siteId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateId??)>
${prefixName} T.STATE_ID ${order_by_stateId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_version??)>
${prefixName} T.VERSION ${order_by_version_value!}
    <#assign prefixName=','>
</#if>
