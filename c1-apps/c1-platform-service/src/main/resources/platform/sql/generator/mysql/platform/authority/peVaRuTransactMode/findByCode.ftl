SELECT
<#assign prefixName=' '>
<#if
(select_beginDate??)>
${prefixName} `BEGIN_DATE` AS "beginDate"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByCode??)>
${prefixName} `CREATED_BY_CODE` AS "createdByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_createdById??)>
${prefixName} `CREATED_BY_ID` AS "createdById"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByName??)>
${prefixName} `CREATED_BY_NAME` AS "createdByName"
    <#assign prefixName=','>
</#if>
<#if
(select_createdDate??)>
${prefixName} `CREATED_DATE` AS "createdDate"
    <#assign prefixName=','>
</#if>
<#if
(select_dataStateCode??)>
${prefixName} `DATA_STATE_CODE` AS "dataStateCode"
    <#assign prefixName=','>
</#if>
<#if
(select_dataStateId??)>
${prefixName} `DATA_STATE_ID` AS "dataStateId"
    <#assign prefixName=','>
</#if>
<#if
(select_dataStateName??)>
${prefixName} `DATA_STATE_NAME` AS "dataStateName"
    <#assign prefixName=','>
</#if>
<#if
(select_endDate??)>
${prefixName} `END_DATE` AS "endDate"
    <#assign prefixName=','>
</#if>
<#if
(select_id??)>
${prefixName} `ID` AS "id"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE` AS "lastModifiedByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID` AS "lastModifiedById"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME` AS "lastModifiedByName"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE` AS "lastModifiedDate"
    <#assign prefixName=','>
</#if>
<#if
(select_orderNum??)>
${prefixName} `ORDER_NUM` AS "orderNum"
    <#assign prefixName=','>
</#if>
<#if
(select_permissionId??)>
${prefixName} `PERMISSION_ID` AS "permissionId"
    <#assign prefixName=','>
</#if>
<#if
(select_stateCode??)>
${prefixName} `STATE_CODE` AS "stateCode"
    <#assign prefixName=','>
</#if>
<#if
(select_stateId??)>
${prefixName} `STATE_ID` AS "stateId"
    <#assign prefixName=','>
</#if>
<#if
(select_stateName??)>
${prefixName} `STATE_NAME` AS "stateName"
    <#assign prefixName=','>
</#if>
<#if
(select_validateErrorCode??)>
${prefixName} `VALIDATE_ERROR_CODE` AS "validateErrorCode"
    <#assign prefixName=','>
</#if>
<#if
(select_validateErrorId??)>
${prefixName} `VALIDATE_ERROR_ID` AS "validateErrorId"
    <#assign prefixName=','>
</#if>
<#if
(select_validateErrorName??)>
${prefixName} `VALIDATE_ERROR_NAME` AS "validateErrorName"
    <#assign prefixName=','>
</#if>
<#if
(select_validateRuleCode??)>
${prefixName} `VALIDATE_RULE_CODE` AS "validateRuleCode"
    <#assign prefixName=','>
</#if>
<#if
(select_validateRuleId??)>
${prefixName} `VALIDATE_RULE_ID` AS "validateRuleId"
    <#assign prefixName=','>
</#if>
<#if
(select_validateRuleName??)>
${prefixName} `VALIDATE_RULE_NAME` AS "validateRuleName"
    <#assign prefixName=','>
</#if>
<#if
(select_validateSuccessCode??)>
${prefixName} `VALIDATE_SUCCESS_CODE` AS "validateSuccessCode"
    <#assign prefixName=','>
</#if>
<#if
(select_validateSuccessId??)>
${prefixName} `VALIDATE_SUCCESS_ID` AS "validateSuccessId"
    <#assign prefixName=','>
</#if>
<#if
(select_validateSuccessName??)>
${prefixName} `VALIDATE_SUCCESS_NAME` AS "validateSuccessName"
    <#assign prefixName=','>
</#if>
<#if
(select_versionInt??)>
${prefixName} `VERSION_INT` AS "versionInt"
    <#assign prefixName=','>
</#if>
<#if prefixName== ' '>
`BEGIN_DATE` AS "beginDate"
, `CREATED_BY_CODE` AS "createdByCode"
, `CREATED_BY_ID` AS "createdById"
, `CREATED_BY_NAME` AS "createdByName"
, `CREATED_DATE` AS "createdDate"
, `DATA_STATE_CODE` AS "dataStateCode"
, `DATA_STATE_ID` AS "dataStateId"
, `DATA_STATE_NAME` AS "dataStateName"
, `END_DATE` AS "endDate"
, `ID` AS "id"
, `LAST_MODIFIED_BY_CODE` AS "lastModifiedByCode"
, `LAST_MODIFIED_BY_ID` AS "lastModifiedById"
, `LAST_MODIFIED_BY_NAME` AS "lastModifiedByName"
, `LAST_MODIFIED_DATE` AS "lastModifiedDate"
, `ORDER_NUM` AS "orderNum"
, `PERMISSION_ID` AS "permissionId"
, `STATE_CODE` AS "stateCode"
, `STATE_ID` AS "stateId"
, `STATE_NAME` AS "stateName"
, `VALIDATE_ERROR_CODE` AS "validateErrorCode"
, `VALIDATE_ERROR_ID` AS "validateErrorId"
, `VALIDATE_ERROR_NAME` AS "validateErrorName"
, `VALIDATE_RULE_CODE` AS "validateRuleCode"
, `VALIDATE_RULE_ID` AS "validateRuleId"
, `VALIDATE_RULE_NAME` AS "validateRuleName"
, `VALIDATE_SUCCESS_CODE` AS "validateSuccessCode"
, `VALIDATE_SUCCESS_ID` AS "validateSuccessId"
, `VALIDATE_SUCCESS_NAME` AS "validateSuccessName"
, `VERSION_INT` AS "versionInt"
</#if>
FROM c1_pe_va_ru_transact_mode
<#include "whereByCode.ftl">
<#assign prefixName='ORDER BY'>
<#if
(order_by_beginDate??)>
${prefixName} `BEGIN_DATE` ${order_by_beginDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByCode??)>
${prefixName} `CREATED_BY_CODE` ${order_by_createdByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdById??)>
${prefixName} `CREATED_BY_ID` ${order_by_createdById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByName??)>
${prefixName} `CREATED_BY_NAME` ${order_by_createdByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdDate??)>
${prefixName} `CREATED_DATE` ${order_by_createdDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_dataStateCode??)>
${prefixName} `DATA_STATE_CODE` ${order_by_dataStateCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_dataStateId??)>
${prefixName} `DATA_STATE_ID` ${order_by_dataStateId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_dataStateName??)>
${prefixName} `DATA_STATE_NAME` ${order_by_dataStateName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_endDate??)>
${prefixName} `END_DATE` ${order_by_endDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_id??)>
${prefixName} `ID` ${order_by_id_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE` ${order_by_lastModifiedByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID` ${order_by_lastModifiedById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME` ${order_by_lastModifiedByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE` ${order_by_lastModifiedDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_orderNum??)>
${prefixName} `ORDER_NUM` ${order_by_orderNum_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_permissionId??)>
${prefixName} `PERMISSION_ID` ${order_by_permissionId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateCode??)>
${prefixName} `STATE_CODE` ${order_by_stateCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateId??)>
${prefixName} `STATE_ID` ${order_by_stateId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateName??)>
${prefixName} `STATE_NAME` ${order_by_stateName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_validateErrorCode??)>
${prefixName} `VALIDATE_ERROR_CODE` ${order_by_validateErrorCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_validateErrorId??)>
${prefixName} `VALIDATE_ERROR_ID` ${order_by_validateErrorId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_validateErrorName??)>
${prefixName} `VALIDATE_ERROR_NAME` ${order_by_validateErrorName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_validateRuleCode??)>
${prefixName} `VALIDATE_RULE_CODE` ${order_by_validateRuleCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_validateRuleId??)>
${prefixName} `VALIDATE_RULE_ID` ${order_by_validateRuleId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_validateRuleName??)>
${prefixName} `VALIDATE_RULE_NAME` ${order_by_validateRuleName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_validateSuccessCode??)>
${prefixName} `VALIDATE_SUCCESS_CODE` ${order_by_validateSuccessCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_validateSuccessId??)>
${prefixName} `VALIDATE_SUCCESS_ID` ${order_by_validateSuccessId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_validateSuccessName??)>
${prefixName} `VALIDATE_SUCCESS_NAME` ${order_by_validateSuccessName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_versionInt??)>
${prefixName} `VERSION_INT` ${order_by_versionInt_value!}
    <#assign prefixName=','>
</#if>
