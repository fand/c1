UPDATE C1_ENTITY_FILE T
<#assign prefixName='SET'>
<#if
(update_beginDate??)>
    <#if
    (update_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE = :update_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_contentType??)>
    <#if
    (update_contentType_value??)>
    ${prefixName} T.CONTENT_TYPE = :update_contentType_value
    <#else>
    ${prefixName} T.CONTENT_TYPE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByCode??)>
    <#if
    (update_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE = :update_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdById??)>
    <#if
    (update_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID = :update_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdByName??)>
    <#if
    (update_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME = :update_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_createdDate??)>
    <#if
    (update_createdDate_value??)>
    ${prefixName} T.CREATED_DATE = :update_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_endDate??)>
    <#if
    (update_endDate_value??)>
    ${prefixName} T.END_DATE = :update_endDate_value
    <#else>
    ${prefixName} T.END_DATE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_entityId??)>
    <#if
    (update_entityId_value??)>
    ${prefixName} T.ENTITY_ID = :update_entityId_value
    <#else>
    ${prefixName} T.ENTITY_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_extension??)>
    <#if
    (update_extension_value??)>
    ${prefixName} T.EXTENSION = :update_extension_value
    <#else>
    ${prefixName} T.EXTENSION = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_filename??)>
    <#if
    (update_filename_value??)>
    ${prefixName} T.FILENAME = :update_filename_value
    <#else>
    ${prefixName} T.FILENAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_id??)>
    <#if
    (update_id_value??)>
    ${prefixName} T.ID = :update_id_value
    <#else>
    ${prefixName} T.ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByCode??)>
    <#if
    (update_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = :update_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedById??)>
    <#if
    (update_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID = :update_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedByName??)>
    <#if
    (update_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = :update_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_lastModifiedDate??)>
    <#if
    (update_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE = :update_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE = SYSDATE
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_name??)>
    <#if
    (update_name_value??)>
    ${prefixName} T.NAME = :update_name_value
    <#else>
    ${prefixName} T.NAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_originalFilename??)>
    <#if
    (update_originalFilename_value??)>
    ${prefixName} T.ORIGINAL_FILENAME = :update_originalFilename_value
    <#else>
    ${prefixName} T.ORIGINAL_FILENAME = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_parentId??)>
    <#if
    (update_parentId_value??)>
    ${prefixName} T.PARENT_ID = :update_parentId_value
    <#else>
    ${prefixName} T.PARENT_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_stateId??)>
    <#if
    (update_stateId_value??)>
    ${prefixName} T.STATE_ID = :update_stateId_value
    <#else>
    ${prefixName} T.STATE_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_thumbnailValue??)>
    <#if
    (update_thumbnailValue_value??)>
    ${prefixName} T.THUMBNAIL_VALUE = :update_thumbnailValue_value
    <#else>
    ${prefixName} T.THUMBNAIL_VALUE = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_typeId??)>
    <#if
    (update_typeId_value??)>
    ${prefixName} T.TYPE_ID = :update_typeId_value
    <#else>
    ${prefixName} T.TYPE_ID = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#if
(update_version??)>
    <#if
    (update_version_value??)>
    ${prefixName} T.VERSION = :update_version_value
    <#else>
    ${prefixName} T.VERSION = NULL
    </#if>
    <#assign prefixName=','>
</#if>
<#include
"whereByCode.ftl">