SELECT
<#switch (object_id!)>
    <#case "beginDate">
    BEGIN_DATE AS beginDate
        <#break>
    <#case "content">
    CONTENT AS content
        <#break>
    <#case "createdByCode">
    CREATED_BY_CODE AS createdByCode
        <#break>
    <#case "createdById">
    CREATED_BY_ID AS createdById
        <#break>
    <#case "createdByName">
    CREATED_BY_NAME AS createdByName
        <#break>
    <#case "createdDate">
    CREATED_DATE AS createdDate
        <#break>
    <#case "dataStateCode">
    DATA_STATE_CODE AS dataStateCode
        <#break>
    <#case "dataStateId">
    DATA_STATE_ID AS dataStateId
        <#break>
    <#case "dataStateName">
    DATA_STATE_NAME AS dataStateName
        <#break>
    <#case "endDate">
    END_DATE AS endDate
        <#break>
    <#case "id">
    ID AS id
        <#break>
    <#case "lastModifiedByCode">
    LAST_MODIFIED_BY_CODE AS lastModifiedByCode
        <#break>
    <#case "lastModifiedById">
    LAST_MODIFIED_BY_ID AS lastModifiedById
        <#break>
    <#case "lastModifiedByName">
    LAST_MODIFIED_BY_NAME AS lastModifiedByName
        <#break>
    <#case "lastModifiedDate">
    LAST_MODIFIED_DATE AS lastModifiedDate
        <#break>
    <#case "orgCode">
    ORG_CODE AS orgCode
        <#break>
    <#case "orgId">
    ORG_ID AS orgId
        <#break>
    <#case "orgName">
    ORG_NAME AS orgName
        <#break>
    <#case "releaseDate">
    RELEASE_DATE AS releaseDate
        <#break>
    <#case "stateCode">
    STATE_CODE AS stateCode
        <#break>
    <#case "stateId">
    STATE_ID AS stateId
        <#break>
    <#case "stateName">
    STATE_NAME AS stateName
        <#break>
    <#case "title">
    TITLE AS title
        <#break>
    <#case "typeCode">
    TYPE_CODE AS typeCode
        <#break>
    <#case "typeId">
    TYPE_ID AS typeId
        <#break>
    <#case "typeName">
    TYPE_NAME AS typeName
        <#break>
    <#case "versionInt">
    VERSION_INT AS versionInt
        <#break>
    <#default>
    COUNT(1) AS COUNT_
</#switch>
FROM c1_article
<#include
"whereByCode.ftl">