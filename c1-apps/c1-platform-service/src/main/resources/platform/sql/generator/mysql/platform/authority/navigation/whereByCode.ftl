<#assign prefixName='WHERE'>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_beginDate??)>
    <#if
    (where_and_eq_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` = :where_and_eq_beginDate_value
    <#else>
    ${prefixName} `BEGIN_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` <> :where_and_nq_beginDate_value
    <#else>
    ${prefixName} `BEGIN_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` <> :where_and_like_beginDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` = :where_or_eq_beginDate
    _value
    <#else>
    ${prefixName} `BEGIN_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` <>
    :where_or_nq_beginDate_value
    <#else>
    ${prefixName} `BEGIN_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` like
    :where_or_like_beginDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_code??)>
    <#if
    (where_and_eq_code_value??)>
    ${prefixName} `CODE` = :where_and_eq_code_value
    <#else>
    ${prefixName} `CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_code??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_code_value??)>
    ${prefixName} `CODE` <> :where_and_nq_code_value
    <#else>
    ${prefixName} `CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_code??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_code_value??)>
    ${prefixName} `CODE` <> :where_and_like_code_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_code??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_code_value??)>
    ${prefixName} `CODE` = :where_or_eq_code
    _value
    <#else>
    ${prefixName} `CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_code??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_code_value??)>
    ${prefixName} `CODE` <>
    :where_or_nq_code_value
    <#else>
    ${prefixName} `CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_code??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_code_value??)>
    ${prefixName} `CODE` like
    :where_or_like_code_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdByCode??)>
    <#if
    (where_and_eq_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` = :where_and_eq_createdByCode_value
    <#else>
    ${prefixName} `CREATED_BY_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` <> :where_and_nq_createdByCode_value
    <#else>
    ${prefixName} `CREATED_BY_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` <> :where_and_like_createdByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` = :where_or_eq_createdByCode
    _value
    <#else>
    ${prefixName} `CREATED_BY_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` <>
    :where_or_nq_createdByCode_value
    <#else>
    ${prefixName} `CREATED_BY_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` like
    :where_or_like_createdByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdById??)>
    <#if
    (where_and_eq_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` = :where_and_eq_createdById_value
    <#else>
    ${prefixName} `CREATED_BY_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` <> :where_and_nq_createdById_value
    <#else>
    ${prefixName} `CREATED_BY_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` <> :where_and_like_createdById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` = :where_or_eq_createdById
    _value
    <#else>
    ${prefixName} `CREATED_BY_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` <>
    :where_or_nq_createdById_value
    <#else>
    ${prefixName} `CREATED_BY_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` like
    :where_or_like_createdById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdByName??)>
    <#if
    (where_and_eq_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` = :where_and_eq_createdByName_value
    <#else>
    ${prefixName} `CREATED_BY_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` <> :where_and_nq_createdByName_value
    <#else>
    ${prefixName} `CREATED_BY_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` <> :where_and_like_createdByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` = :where_or_eq_createdByName
    _value
    <#else>
    ${prefixName} `CREATED_BY_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` <>
    :where_or_nq_createdByName_value
    <#else>
    ${prefixName} `CREATED_BY_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` like
    :where_or_like_createdByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdDate??)>
    <#if
    (where_and_eq_createdDate_value??)>
    ${prefixName} `CREATED_DATE` = :where_and_eq_createdDate_value
    <#else>
    ${prefixName} `CREATED_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdDate_value??)>
    ${prefixName} `CREATED_DATE` <> :where_and_nq_createdDate_value
    <#else>
    ${prefixName} `CREATED_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdDate_value??)>
    ${prefixName} `CREATED_DATE` <> :where_and_like_createdDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdDate_value??)>
    ${prefixName} `CREATED_DATE` = :where_or_eq_createdDate
    _value
    <#else>
    ${prefixName} `CREATED_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdDate_value??)>
    ${prefixName} `CREATED_DATE` <>
    :where_or_nq_createdDate_value
    <#else>
    ${prefixName} `CREATED_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdDate_value??)>
    ${prefixName} `CREATED_DATE` like
    :where_or_like_createdDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_dataOption??)>
    <#if
    (where_and_eq_dataOption_value??)>
    ${prefixName} `DATA_OPTION` = :where_and_eq_dataOption_value
    <#else>
    ${prefixName} `DATA_OPTION` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_dataOption??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_dataOption_value??)>
    ${prefixName} `DATA_OPTION` <> :where_and_nq_dataOption_value
    <#else>
    ${prefixName} `DATA_OPTION` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_dataOption??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_dataOption_value??)>
    ${prefixName} `DATA_OPTION` <> :where_and_like_dataOption_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_dataOption??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_dataOption_value??)>
    ${prefixName} `DATA_OPTION` = :where_or_eq_dataOption
    _value
    <#else>
    ${prefixName} `DATA_OPTION` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_dataOption??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_dataOption_value??)>
    ${prefixName} `DATA_OPTION` <>
    :where_or_nq_dataOption_value
    <#else>
    ${prefixName} `DATA_OPTION` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_dataOption??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_dataOption_value??)>
    ${prefixName} `DATA_OPTION` like
    :where_or_like_dataOption_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_dataStateCode??)>
    <#if
    (where_and_eq_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` = :where_and_eq_dataStateCode_value
    <#else>
    ${prefixName} `DATA_STATE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_dataStateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` <> :where_and_nq_dataStateCode_value
    <#else>
    ${prefixName} `DATA_STATE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_dataStateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` <> :where_and_like_dataStateCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_dataStateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` = :where_or_eq_dataStateCode
    _value
    <#else>
    ${prefixName} `DATA_STATE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_dataStateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` <>
    :where_or_nq_dataStateCode_value
    <#else>
    ${prefixName} `DATA_STATE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_dataStateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` like
    :where_or_like_dataStateCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_dataStateId??)>
    <#if
    (where_and_eq_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` = :where_and_eq_dataStateId_value
    <#else>
    ${prefixName} `DATA_STATE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_dataStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` <> :where_and_nq_dataStateId_value
    <#else>
    ${prefixName} `DATA_STATE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_dataStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` <> :where_and_like_dataStateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_dataStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` = :where_or_eq_dataStateId
    _value
    <#else>
    ${prefixName} `DATA_STATE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_dataStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` <>
    :where_or_nq_dataStateId_value
    <#else>
    ${prefixName} `DATA_STATE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_dataStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` like
    :where_or_like_dataStateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_dataStateName??)>
    <#if
    (where_and_eq_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` = :where_and_eq_dataStateName_value
    <#else>
    ${prefixName} `DATA_STATE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_dataStateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` <> :where_and_nq_dataStateName_value
    <#else>
    ${prefixName} `DATA_STATE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_dataStateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` <> :where_and_like_dataStateName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_dataStateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` = :where_or_eq_dataStateName
    _value
    <#else>
    ${prefixName} `DATA_STATE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_dataStateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` <>
    :where_or_nq_dataStateName_value
    <#else>
    ${prefixName} `DATA_STATE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_dataStateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` like
    :where_or_like_dataStateName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_endDate??)>
    <#if
    (where_and_eq_endDate_value??)>
    ${prefixName} `END_DATE` = :where_and_eq_endDate_value
    <#else>
    ${prefixName} `END_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_endDate_value??)>
    ${prefixName} `END_DATE` <> :where_and_nq_endDate_value
    <#else>
    ${prefixName} `END_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_endDate_value??)>
    ${prefixName} `END_DATE` <> :where_and_like_endDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_endDate_value??)>
    ${prefixName} `END_DATE` = :where_or_eq_endDate
    _value
    <#else>
    ${prefixName} `END_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_endDate_value??)>
    ${prefixName} `END_DATE` <>
    :where_or_nq_endDate_value
    <#else>
    ${prefixName} `END_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_endDate_value??)>
    ${prefixName} `END_DATE` like
    :where_or_like_endDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_between_sysdate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
${prefixName} (NOW() BETWEEN
IFNULL(BEGIN_DATE, NOW()) AND IFNULL(END_DATE,
NOW()))
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_iconStyle??)>
    <#if
    (where_and_eq_iconStyle_value??)>
    ${prefixName} `ICON_STYLE` = :where_and_eq_iconStyle_value
    <#else>
    ${prefixName} `ICON_STYLE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_iconStyle??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_iconStyle_value??)>
    ${prefixName} `ICON_STYLE` <> :where_and_nq_iconStyle_value
    <#else>
    ${prefixName} `ICON_STYLE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_iconStyle??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_iconStyle_value??)>
    ${prefixName} `ICON_STYLE` <> :where_and_like_iconStyle_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_iconStyle??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_iconStyle_value??)>
    ${prefixName} `ICON_STYLE` = :where_or_eq_iconStyle
    _value
    <#else>
    ${prefixName} `ICON_STYLE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_iconStyle??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_iconStyle_value??)>
    ${prefixName} `ICON_STYLE` <>
    :where_or_nq_iconStyle_value
    <#else>
    ${prefixName} `ICON_STYLE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_iconStyle??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_iconStyle_value??)>
    ${prefixName} `ICON_STYLE` like
    :where_or_like_iconStyle_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_id??)>
    <#if
    (where_and_eq_id_value??)>
    ${prefixName} `ID` = :where_and_eq_id_value
    <#else>
    ${prefixName} `ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_id_value??)>
    ${prefixName} `ID` <> :where_and_nq_id_value
    <#else>
    ${prefixName} `ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_id_value??)>
    ${prefixName} `ID` <> :where_and_like_id_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_id_value??)>
    ${prefixName} `ID` = :where_or_eq_id
    _value
    <#else>
    ${prefixName} `ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_id_value??)>
    ${prefixName} `ID` <>
    :where_or_nq_id_value
    <#else>
    ${prefixName} `ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_id_value??)>
    ${prefixName} `ID` like
    :where_or_like_id_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedByCode??)>
    <#if
    (where_and_eq_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = :where_and_eq_lastModifiedByCode_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` <> :where_and_nq_lastModifiedByCode_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` <> :where_and_like_lastModifiedByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = :where_or_eq_lastModifiedByCode
    _value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` <>
    :where_or_nq_lastModifiedByCode_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` like
    :where_or_like_lastModifiedByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedById??)>
    <#if
    (where_and_eq_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` = :where_and_eq_lastModifiedById_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` <> :where_and_nq_lastModifiedById_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` <> :where_and_like_lastModifiedById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` = :where_or_eq_lastModifiedById
    _value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` <>
    :where_or_nq_lastModifiedById_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` like
    :where_or_like_lastModifiedById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedByName??)>
    <#if
    (where_and_eq_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = :where_and_eq_lastModifiedByName_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` <> :where_and_nq_lastModifiedByName_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` <> :where_and_like_lastModifiedByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = :where_or_eq_lastModifiedByName
    _value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` <>
    :where_or_nq_lastModifiedByName_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` like
    :where_or_like_lastModifiedByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedDate??)>
    <#if
    (where_and_eq_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` = :where_and_eq_lastModifiedDate_value
    <#else>
    ${prefixName} `LAST_MODIFIED_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` <> :where_and_nq_lastModifiedDate_value
    <#else>
    ${prefixName} `LAST_MODIFIED_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` <> :where_and_like_lastModifiedDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` = :where_or_eq_lastModifiedDate
    _value
    <#else>
    ${prefixName} `LAST_MODIFIED_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` <>
    :where_or_nq_lastModifiedDate_value
    <#else>
    ${prefixName} `LAST_MODIFIED_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` like
    :where_or_like_lastModifiedDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_name??)>
    <#if
    (where_and_eq_name_value??)>
    ${prefixName} `NAME` = :where_and_eq_name_value
    <#else>
    ${prefixName} `NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_name??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_name_value??)>
    ${prefixName} `NAME` <> :where_and_nq_name_value
    <#else>
    ${prefixName} `NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_name??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_name_value??)>
    ${prefixName} `NAME` <> :where_and_like_name_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_name??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_name_value??)>
    ${prefixName} `NAME` = :where_or_eq_name
    _value
    <#else>
    ${prefixName} `NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_name??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_name_value??)>
    ${prefixName} `NAME` <>
    :where_or_nq_name_value
    <#else>
    ${prefixName} `NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_name??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_name_value??)>
    ${prefixName} `NAME` like
    :where_or_like_name_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_orderNum??)>
    <#if
    (where_and_eq_orderNum_value??)>
    ${prefixName} `ORDER_NUM` = :where_and_eq_orderNum_value
    <#else>
    ${prefixName} `ORDER_NUM` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_orderNum??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_orderNum_value??)>
    ${prefixName} `ORDER_NUM` <> :where_and_nq_orderNum_value
    <#else>
    ${prefixName} `ORDER_NUM` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_orderNum??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_orderNum_value??)>
    ${prefixName} `ORDER_NUM` <> :where_and_like_orderNum_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_orderNum??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_orderNum_value??)>
    ${prefixName} `ORDER_NUM` = :where_or_eq_orderNum
    _value
    <#else>
    ${prefixName} `ORDER_NUM` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_orderNum??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_orderNum_value??)>
    ${prefixName} `ORDER_NUM` <>
    :where_or_nq_orderNum_value
    <#else>
    ${prefixName} `ORDER_NUM` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_orderNum??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_orderNum_value??)>
    ${prefixName} `ORDER_NUM` like
    :where_or_like_orderNum_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_parentCode??)>
    <#if
    (where_and_eq_parentCode_value??)>
    ${prefixName} `PARENT_CODE` = :where_and_eq_parentCode_value
    <#else>
    ${prefixName} `PARENT_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_parentCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_parentCode_value??)>
    ${prefixName} `PARENT_CODE` <> :where_and_nq_parentCode_value
    <#else>
    ${prefixName} `PARENT_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_parentCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_parentCode_value??)>
    ${prefixName} `PARENT_CODE` <> :where_and_like_parentCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_parentCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_parentCode_value??)>
    ${prefixName} `PARENT_CODE` = :where_or_eq_parentCode
    _value
    <#else>
    ${prefixName} `PARENT_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_parentCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_parentCode_value??)>
    ${prefixName} `PARENT_CODE` <>
    :where_or_nq_parentCode_value
    <#else>
    ${prefixName} `PARENT_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_parentCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_parentCode_value??)>
    ${prefixName} `PARENT_CODE` like
    :where_or_like_parentCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_parentId??)>
    <#if
    (where_and_eq_parentId_value??)>
    ${prefixName} `PARENT_ID` = :where_and_eq_parentId_value
    <#else>
    ${prefixName} `PARENT_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_parentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_parentId_value??)>
    ${prefixName} `PARENT_ID` <> :where_and_nq_parentId_value
    <#else>
    ${prefixName} `PARENT_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_parentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_parentId_value??)>
    ${prefixName} `PARENT_ID` <> :where_and_like_parentId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_parentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_parentId_value??)>
    ${prefixName} `PARENT_ID` = :where_or_eq_parentId
    _value
    <#else>
    ${prefixName} `PARENT_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_parentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_parentId_value??)>
    ${prefixName} `PARENT_ID` <>
    :where_or_nq_parentId_value
    <#else>
    ${prefixName} `PARENT_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_parentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_parentId_value??)>
    ${prefixName} `PARENT_ID` like
    :where_or_like_parentId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_parentName??)>
    <#if
    (where_and_eq_parentName_value??)>
    ${prefixName} `PARENT_NAME` = :where_and_eq_parentName_value
    <#else>
    ${prefixName} `PARENT_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_parentName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_parentName_value??)>
    ${prefixName} `PARENT_NAME` <> :where_and_nq_parentName_value
    <#else>
    ${prefixName} `PARENT_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_parentName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_parentName_value??)>
    ${prefixName} `PARENT_NAME` <> :where_and_like_parentName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_parentName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_parentName_value??)>
    ${prefixName} `PARENT_NAME` = :where_or_eq_parentName
    _value
    <#else>
    ${prefixName} `PARENT_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_parentName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_parentName_value??)>
    ${prefixName} `PARENT_NAME` <>
    :where_or_nq_parentName_value
    <#else>
    ${prefixName} `PARENT_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_parentName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_parentName_value??)>
    ${prefixName} `PARENT_NAME` like
    :where_or_like_parentName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_siteCode??)>
    <#if
    (where_and_eq_siteCode_value??)>
    ${prefixName} `SITE_CODE` = :where_and_eq_siteCode_value
    <#else>
    ${prefixName} `SITE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_siteCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_siteCode_value??)>
    ${prefixName} `SITE_CODE` <> :where_and_nq_siteCode_value
    <#else>
    ${prefixName} `SITE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_siteCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_siteCode_value??)>
    ${prefixName} `SITE_CODE` <> :where_and_like_siteCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_siteCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_siteCode_value??)>
    ${prefixName} `SITE_CODE` = :where_or_eq_siteCode
    _value
    <#else>
    ${prefixName} `SITE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_siteCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_siteCode_value??)>
    ${prefixName} `SITE_CODE` <>
    :where_or_nq_siteCode_value
    <#else>
    ${prefixName} `SITE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_siteCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_siteCode_value??)>
    ${prefixName} `SITE_CODE` like
    :where_or_like_siteCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_siteId??)>
    <#if
    (where_and_eq_siteId_value??)>
    ${prefixName} `SITE_ID` = :where_and_eq_siteId_value
    <#else>
    ${prefixName} `SITE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_siteId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_siteId_value??)>
    ${prefixName} `SITE_ID` <> :where_and_nq_siteId_value
    <#else>
    ${prefixName} `SITE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_siteId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_siteId_value??)>
    ${prefixName} `SITE_ID` <> :where_and_like_siteId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_siteId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_siteId_value??)>
    ${prefixName} `SITE_ID` = :where_or_eq_siteId
    _value
    <#else>
    ${prefixName} `SITE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_siteId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_siteId_value??)>
    ${prefixName} `SITE_ID` <>
    :where_or_nq_siteId_value
    <#else>
    ${prefixName} `SITE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_siteId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_siteId_value??)>
    ${prefixName} `SITE_ID` like
    :where_or_like_siteId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_siteName??)>
    <#if
    (where_and_eq_siteName_value??)>
    ${prefixName} `SITE_NAME` = :where_and_eq_siteName_value
    <#else>
    ${prefixName} `SITE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_siteName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_siteName_value??)>
    ${prefixName} `SITE_NAME` <> :where_and_nq_siteName_value
    <#else>
    ${prefixName} `SITE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_siteName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_siteName_value??)>
    ${prefixName} `SITE_NAME` <> :where_and_like_siteName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_siteName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_siteName_value??)>
    ${prefixName} `SITE_NAME` = :where_or_eq_siteName
    _value
    <#else>
    ${prefixName} `SITE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_siteName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_siteName_value??)>
    ${prefixName} `SITE_NAME` <>
    :where_or_nq_siteName_value
    <#else>
    ${prefixName} `SITE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_siteName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_siteName_value??)>
    ${prefixName} `SITE_NAME` like
    :where_or_like_siteName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_stateCode??)>
    <#if
    (where_and_eq_stateCode_value??)>
    ${prefixName} `STATE_CODE` = :where_and_eq_stateCode_value
    <#else>
    ${prefixName} `STATE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_stateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_stateCode_value??)>
    ${prefixName} `STATE_CODE` <> :where_and_nq_stateCode_value
    <#else>
    ${prefixName} `STATE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_stateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_stateCode_value??)>
    ${prefixName} `STATE_CODE` <> :where_and_like_stateCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_stateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_stateCode_value??)>
    ${prefixName} `STATE_CODE` = :where_or_eq_stateCode
    _value
    <#else>
    ${prefixName} `STATE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_stateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_stateCode_value??)>
    ${prefixName} `STATE_CODE` <>
    :where_or_nq_stateCode_value
    <#else>
    ${prefixName} `STATE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_stateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_stateCode_value??)>
    ${prefixName} `STATE_CODE` like
    :where_or_like_stateCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_stateId??)>
    <#if
    (where_and_eq_stateId_value??)>
    ${prefixName} `STATE_ID` = :where_and_eq_stateId_value
    <#else>
    ${prefixName} `STATE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_stateId_value??)>
    ${prefixName} `STATE_ID` <> :where_and_nq_stateId_value
    <#else>
    ${prefixName} `STATE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_stateId_value??)>
    ${prefixName} `STATE_ID` <> :where_and_like_stateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_stateId_value??)>
    ${prefixName} `STATE_ID` = :where_or_eq_stateId
    _value
    <#else>
    ${prefixName} `STATE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_stateId_value??)>
    ${prefixName} `STATE_ID` <>
    :where_or_nq_stateId_value
    <#else>
    ${prefixName} `STATE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_stateId_value??)>
    ${prefixName} `STATE_ID` like
    :where_or_like_stateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_stateName??)>
    <#if
    (where_and_eq_stateName_value??)>
    ${prefixName} `STATE_NAME` = :where_and_eq_stateName_value
    <#else>
    ${prefixName} `STATE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_stateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_stateName_value??)>
    ${prefixName} `STATE_NAME` <> :where_and_nq_stateName_value
    <#else>
    ${prefixName} `STATE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_stateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_stateName_value??)>
    ${prefixName} `STATE_NAME` <> :where_and_like_stateName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_stateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_stateName_value??)>
    ${prefixName} `STATE_NAME` = :where_or_eq_stateName
    _value
    <#else>
    ${prefixName} `STATE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_stateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_stateName_value??)>
    ${prefixName} `STATE_NAME` <>
    :where_or_nq_stateName_value
    <#else>
    ${prefixName} `STATE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_stateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_stateName_value??)>
    ${prefixName} `STATE_NAME` like
    :where_or_like_stateName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_target??)>
    <#if
    (where_and_eq_target_value??)>
    ${prefixName} `TARGET` = :where_and_eq_target_value
    <#else>
    ${prefixName} `TARGET` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_target??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_target_value??)>
    ${prefixName} `TARGET` <> :where_and_nq_target_value
    <#else>
    ${prefixName} `TARGET` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_target??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_target_value??)>
    ${prefixName} `TARGET` <> :where_and_like_target_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_target??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_target_value??)>
    ${prefixName} `TARGET` = :where_or_eq_target
    _value
    <#else>
    ${prefixName} `TARGET` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_target??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_target_value??)>
    ${prefixName} `TARGET` <>
    :where_or_nq_target_value
    <#else>
    ${prefixName} `TARGET` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_target??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_target_value??)>
    ${prefixName} `TARGET` like
    :where_or_like_target_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_treeCode??)>
    <#if
    (where_and_eq_treeCode_value??)>
    ${prefixName} `TREE_CODE` = :where_and_eq_treeCode_value
    <#else>
    ${prefixName} `TREE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_treeCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_treeCode_value??)>
    ${prefixName} `TREE_CODE` <> :where_and_nq_treeCode_value
    <#else>
    ${prefixName} `TREE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_treeCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_treeCode_value??)>
    ${prefixName} `TREE_CODE` <> :where_and_like_treeCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_treeCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_treeCode_value??)>
    ${prefixName} `TREE_CODE` = :where_or_eq_treeCode
    _value
    <#else>
    ${prefixName} `TREE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_treeCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_treeCode_value??)>
    ${prefixName} `TREE_CODE` <>
    :where_or_nq_treeCode_value
    <#else>
    ${prefixName} `TREE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_treeCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_treeCode_value??)>
    ${prefixName} `TREE_CODE` like
    :where_or_like_treeCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_treeId??)>
    <#if
    (where_and_eq_treeId_value??)>
    ${prefixName} `TREE_ID` = :where_and_eq_treeId_value
    <#else>
    ${prefixName} `TREE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_treeId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_treeId_value??)>
    ${prefixName} `TREE_ID` <> :where_and_nq_treeId_value
    <#else>
    ${prefixName} `TREE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_treeId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_treeId_value??)>
    ${prefixName} `TREE_ID` <> :where_and_like_treeId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_treeId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_treeId_value??)>
    ${prefixName} `TREE_ID` = :where_or_eq_treeId
    _value
    <#else>
    ${prefixName} `TREE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_treeId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_treeId_value??)>
    ${prefixName} `TREE_ID` <>
    :where_or_nq_treeId_value
    <#else>
    ${prefixName} `TREE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_treeId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_treeId_value??)>
    ${prefixName} `TREE_ID` like
    :where_or_like_treeId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_treeName??)>
    <#if
    (where_and_eq_treeName_value??)>
    ${prefixName} `TREE_NAME` = :where_and_eq_treeName_value
    <#else>
    ${prefixName} `TREE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_treeName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_treeName_value??)>
    ${prefixName} `TREE_NAME` <> :where_and_nq_treeName_value
    <#else>
    ${prefixName} `TREE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_treeName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_treeName_value??)>
    ${prefixName} `TREE_NAME` <> :where_and_like_treeName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_treeName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_treeName_value??)>
    ${prefixName} `TREE_NAME` = :where_or_eq_treeName
    _value
    <#else>
    ${prefixName} `TREE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_treeName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_treeName_value??)>
    ${prefixName} `TREE_NAME` <>
    :where_or_nq_treeName_value
    <#else>
    ${prefixName} `TREE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_treeName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_treeName_value??)>
    ${prefixName} `TREE_NAME` like
    :where_or_like_treeName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_typeCode??)>
    <#if
    (where_and_eq_typeCode_value??)>
    ${prefixName} `TYPE_CODE` = :where_and_eq_typeCode_value
    <#else>
    ${prefixName} `TYPE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_typeCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_typeCode_value??)>
    ${prefixName} `TYPE_CODE` <> :where_and_nq_typeCode_value
    <#else>
    ${prefixName} `TYPE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_typeCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_typeCode_value??)>
    ${prefixName} `TYPE_CODE` <> :where_and_like_typeCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_typeCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_typeCode_value??)>
    ${prefixName} `TYPE_CODE` = :where_or_eq_typeCode
    _value
    <#else>
    ${prefixName} `TYPE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_typeCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_typeCode_value??)>
    ${prefixName} `TYPE_CODE` <>
    :where_or_nq_typeCode_value
    <#else>
    ${prefixName} `TYPE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_typeCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_typeCode_value??)>
    ${prefixName} `TYPE_CODE` like
    :where_or_like_typeCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_typeId??)>
    <#if
    (where_and_eq_typeId_value??)>
    ${prefixName} `TYPE_ID` = :where_and_eq_typeId_value
    <#else>
    ${prefixName} `TYPE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_typeId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_typeId_value??)>
    ${prefixName} `TYPE_ID` <> :where_and_nq_typeId_value
    <#else>
    ${prefixName} `TYPE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_typeId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_typeId_value??)>
    ${prefixName} `TYPE_ID` <> :where_and_like_typeId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_typeId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_typeId_value??)>
    ${prefixName} `TYPE_ID` = :where_or_eq_typeId
    _value
    <#else>
    ${prefixName} `TYPE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_typeId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_typeId_value??)>
    ${prefixName} `TYPE_ID` <>
    :where_or_nq_typeId_value
    <#else>
    ${prefixName} `TYPE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_typeId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_typeId_value??)>
    ${prefixName} `TYPE_ID` like
    :where_or_like_typeId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_typeName??)>
    <#if
    (where_and_eq_typeName_value??)>
    ${prefixName} `TYPE_NAME` = :where_and_eq_typeName_value
    <#else>
    ${prefixName} `TYPE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_typeName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_typeName_value??)>
    ${prefixName} `TYPE_NAME` <> :where_and_nq_typeName_value
    <#else>
    ${prefixName} `TYPE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_typeName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_typeName_value??)>
    ${prefixName} `TYPE_NAME` <> :where_and_like_typeName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_typeName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_typeName_value??)>
    ${prefixName} `TYPE_NAME` = :where_or_eq_typeName
    _value
    <#else>
    ${prefixName} `TYPE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_typeName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_typeName_value??)>
    ${prefixName} `TYPE_NAME` <>
    :where_or_nq_typeName_value
    <#else>
    ${prefixName} `TYPE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_typeName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_typeName_value??)>
    ${prefixName} `TYPE_NAME` like
    :where_or_like_typeName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_url??)>
    <#if
    (where_and_eq_url_value??)>
    ${prefixName} `URL` = :where_and_eq_url_value
    <#else>
    ${prefixName} `URL` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_url??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_url_value??)>
    ${prefixName} `URL` <> :where_and_nq_url_value
    <#else>
    ${prefixName} `URL` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_url??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_url_value??)>
    ${prefixName} `URL` <> :where_and_like_url_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_url??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_url_value??)>
    ${prefixName} `URL` = :where_or_eq_url
    _value
    <#else>
    ${prefixName} `URL` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_url??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_url_value??)>
    ${prefixName} `URL` <>
    :where_or_nq_url_value
    <#else>
    ${prefixName} `URL` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_url??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_url_value??)>
    ${prefixName} `URL` like
    :where_or_like_url_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_versionInt??)>
    <#if
    (where_and_eq_versionInt_value??)>
    ${prefixName} `VERSION_INT` = :where_and_eq_versionInt_value
    <#else>
    ${prefixName} `VERSION_INT` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_versionInt??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_versionInt_value??)>
    ${prefixName} `VERSION_INT` <> :where_and_nq_versionInt_value
    <#else>
    ${prefixName} `VERSION_INT` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_versionInt??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_versionInt_value??)>
    ${prefixName} `VERSION_INT` <> :where_and_like_versionInt_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_versionInt??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_versionInt_value??)>
    ${prefixName} `VERSION_INT` = :where_or_eq_versionInt
    _value
    <#else>
    ${prefixName} `VERSION_INT` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_versionInt??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_versionInt_value??)>
    ${prefixName} `VERSION_INT` <>
    :where_or_nq_versionInt_value
    <#else>
    ${prefixName} `VERSION_INT` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_versionInt??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_versionInt_value??)>
    ${prefixName} `VERSION_INT` like
    :where_or_like_versionInt_value
    </#if>
    <#assign prefixName=''>
</#if>
