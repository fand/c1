SELECT
<#switch (object_id!)>
    <#case "beginDate">
    BEGIN_DATE AS beginDate
        <#break>
    <#case "createdByCode">
    CREATED_BY_CODE AS createdByCode
        <#break>
    <#case "createdById">
    CREATED_BY_ID AS createdById
        <#break>
    <#case "createdByName">
    CREATED_BY_NAME AS createdByName
        <#break>
    <#case "createdDate">
    CREATED_DATE AS createdDate
        <#break>
    <#case "dataStateCode">
    DATA_STATE_CODE AS dataStateCode
        <#break>
    <#case "dataStateId">
    DATA_STATE_ID AS dataStateId
        <#break>
    <#case "dataStateName">
    DATA_STATE_NAME AS dataStateName
        <#break>
    <#case "endDate">
    END_DATE AS endDate
        <#break>
    <#case "id">
    ID AS id
        <#break>
    <#case "lastModifiedByCode">
    LAST_MODIFIED_BY_CODE AS lastModifiedByCode
        <#break>
    <#case "lastModifiedById">
    LAST_MODIFIED_BY_ID AS lastModifiedById
        <#break>
    <#case "lastModifiedByName">
    LAST_MODIFIED_BY_NAME AS lastModifiedByName
        <#break>
    <#case "lastModifiedDate">
    LAST_MODIFIED_DATE AS lastModifiedDate
        <#break>
    <#case "orderNum">
    ORDER_NUM AS orderNum
        <#break>
    <#case "permissionId">
    PERMISSION_ID AS permissionId
        <#break>
    <#case "stateCode">
    STATE_CODE AS stateCode
        <#break>
    <#case "stateId">
    STATE_ID AS stateId
        <#break>
    <#case "stateName">
    STATE_NAME AS stateName
        <#break>
    <#case "validateErrorCode">
    VALIDATE_ERROR_CODE AS validateErrorCode
        <#break>
    <#case "validateErrorId">
    VALIDATE_ERROR_ID AS validateErrorId
        <#break>
    <#case "validateErrorName">
    VALIDATE_ERROR_NAME AS validateErrorName
        <#break>
    <#case "validateRuleCode">
    VALIDATE_RULE_CODE AS validateRuleCode
        <#break>
    <#case "validateRuleId">
    VALIDATE_RULE_ID AS validateRuleId
        <#break>
    <#case "validateRuleName">
    VALIDATE_RULE_NAME AS validateRuleName
        <#break>
    <#case "validateSuccessCode">
    VALIDATE_SUCCESS_CODE AS validateSuccessCode
        <#break>
    <#case "validateSuccessId">
    VALIDATE_SUCCESS_ID AS validateSuccessId
        <#break>
    <#case "validateSuccessName">
    VALIDATE_SUCCESS_NAME AS validateSuccessName
        <#break>
    <#case "versionInt">
    VERSION_INT AS versionInt
        <#break>
    <#default>
    COUNT(1) AS COUNT_
</#switch>
FROM c1_pe_va_ru_transact_mode
<#include
"whereByCode.ftl">