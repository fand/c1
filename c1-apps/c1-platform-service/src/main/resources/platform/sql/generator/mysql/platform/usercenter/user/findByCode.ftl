SELECT
<#assign prefixName=' '>
<#if
(select_address??)>
${prefixName} `ADDRESS` AS "address"
    <#assign prefixName=','>
</#if>
<#if
(select_age??)>
${prefixName} `AGE` AS "age"
    <#assign prefixName=','>
</#if>
<#if
(select_beginDate??)>
${prefixName} `BEGIN_DATE` AS "beginDate"
    <#assign prefixName=','>
</#if>
<#if
(select_birthday??)>
${prefixName} `BIRTHDAY` AS "birthday"
    <#assign prefixName=','>
</#if>
<#if
(select_callName??)>
${prefixName} `CALL_NAME` AS "callName"
    <#assign prefixName=','>
</#if>
<#if
(select_cityCode??)>
${prefixName} `CITY_CODE` AS "cityCode"
    <#assign prefixName=','>
</#if>
<#if
(select_cityId??)>
${prefixName} `CITY_ID` AS "cityId"
    <#assign prefixName=','>
</#if>
<#if
(select_cityName??)>
${prefixName} `CITY_NAME` AS "cityName"
    <#assign prefixName=','>
</#if>
<#if
(select_code??)>
${prefixName} `CODE` AS "code"
    <#assign prefixName=','>
</#if>
<#if
(select_companyName??)>
${prefixName} `COMPANY_NAME` AS "companyName"
    <#assign prefixName=','>
</#if>
<#if
(select_countryCode??)>
${prefixName} `COUNTRY_CODE` AS "countryCode"
    <#assign prefixName=','>
</#if>
<#if
(select_countryId??)>
${prefixName} `COUNTRY_ID` AS "countryId"
    <#assign prefixName=','>
</#if>
<#if
(select_countryName??)>
${prefixName} `COUNTRY_NAME` AS "countryName"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByCode??)>
${prefixName} `CREATED_BY_CODE` AS "createdByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_createdById??)>
${prefixName} `CREATED_BY_ID` AS "createdById"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByName??)>
${prefixName} `CREATED_BY_NAME` AS "createdByName"
    <#assign prefixName=','>
</#if>
<#if
(select_createdDate??)>
${prefixName} `CREATED_DATE` AS "createdDate"
    <#assign prefixName=','>
</#if>
<#if
(select_dataStateCode??)>
${prefixName} `DATA_STATE_CODE` AS "dataStateCode"
    <#assign prefixName=','>
</#if>
<#if
(select_dataStateId??)>
${prefixName} `DATA_STATE_ID` AS "dataStateId"
    <#assign prefixName=','>
</#if>
<#if
(select_dataStateName??)>
${prefixName} `DATA_STATE_NAME` AS "dataStateName"
    <#assign prefixName=','>
</#if>
<#if
(select_departmentCode??)>
${prefixName} `DEPARTMENT_CODE` AS "departmentCode"
    <#assign prefixName=','>
</#if>
<#if
(select_departmentId??)>
${prefixName} `DEPARTMENT_ID` AS "departmentId"
    <#assign prefixName=','>
</#if>
<#if
(select_departmentName??)>
${prefixName} `DEPARTMENT_NAME` AS "departmentName"
    <#assign prefixName=','>
</#if>
<#if
(select_diplomaCode??)>
${prefixName} `DIPLOMA_CODE` AS "diplomaCode"
    <#assign prefixName=','>
</#if>
<#if
(select_diplomaId??)>
${prefixName} `DIPLOMA_ID` AS "diplomaId"
    <#assign prefixName=','>
</#if>
<#if
(select_diplomaName??)>
${prefixName} `DIPLOMA_NAME` AS "diplomaName"
    <#assign prefixName=','>
</#if>
<#if
(select_districtCode??)>
${prefixName} `DISTRICT_CODE` AS "districtCode"
    <#assign prefixName=','>
</#if>
<#if
(select_districtId??)>
${prefixName} `DISTRICT_ID` AS "districtId"
    <#assign prefixName=','>
</#if>
<#if
(select_districtName??)>
${prefixName} `DISTRICT_NAME` AS "districtName"
    <#assign prefixName=','>
</#if>
<#if
(select_email??)>
${prefixName} `EMAIL` AS "email"
    <#assign prefixName=','>
</#if>
<#if
(select_endDate??)>
${prefixName} `END_DATE` AS "endDate"
    <#assign prefixName=','>
</#if>
<#if
(select_flagCode??)>
${prefixName} `FLAG_CODE` AS "flagCode"
    <#assign prefixName=','>
</#if>
<#if
(select_flagId??)>
${prefixName} `FLAG_ID` AS "flagId"
    <#assign prefixName=','>
</#if>
<#if
(select_flagName??)>
${prefixName} `FLAG_NAME` AS "flagName"
    <#assign prefixName=','>
</#if>
<#if
(select_folkCode??)>
${prefixName} `FOLK_CODE` AS "folkCode"
    <#assign prefixName=','>
</#if>
<#if
(select_folkId??)>
${prefixName} `FOLK_ID` AS "folkId"
    <#assign prefixName=','>
</#if>
<#if
(select_folkName??)>
${prefixName} `FOLK_NAME` AS "folkName"
    <#assign prefixName=','>
</#if>
<#if
(select_id??)>
${prefixName} `ID` AS "id"
    <#assign prefixName=','>
</#if>
<#if
(select_intro??)>
${prefixName} `INTRO` AS "intro"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE` AS "lastModifiedByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID` AS "lastModifiedById"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME` AS "lastModifiedByName"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE` AS "lastModifiedDate"
    <#assign prefixName=','>
</#if>
<#if
(select_locus??)>
${prefixName} `LOCUS` AS "locus"
    <#assign prefixName=','>
</#if>
<#if
(select_marryStateCode??)>
${prefixName} `MARRY_STATE_CODE` AS "marryStateCode"
    <#assign prefixName=','>
</#if>
<#if
(select_marryStateId??)>
${prefixName} `MARRY_STATE_ID` AS "marryStateId"
    <#assign prefixName=','>
</#if>
<#if
(select_marryStateName??)>
${prefixName} `MARRY_STATE_NAME` AS "marryStateName"
    <#assign prefixName=','>
</#if>
<#if
(select_mobilePhone??)>
${prefixName} `MOBILE_PHONE` AS "mobilePhone"
    <#assign prefixName=','>
</#if>
<#if
(select_name??)>
${prefixName} `NAME` AS "name"
    <#assign prefixName=','>
</#if>
<#if
(select_no??)>
${prefixName} `NO` AS "no"
    <#assign prefixName=','>
</#if>
<#if
(select_orgCode??)>
${prefixName} `ORG_CODE` AS "orgCode"
    <#assign prefixName=','>
</#if>
<#if
(select_orgId??)>
${prefixName} `ORG_ID` AS "orgId"
    <#assign prefixName=','>
</#if>
<#if
(select_orgName??)>
${prefixName} `ORG_NAME` AS "orgName"
    <#assign prefixName=','>
</#if>
<#if
(select_password??)>
${prefixName} `PASSWORD` AS "password"
    <#assign prefixName=','>
</#if>
<#if
(select_professionCode??)>
${prefixName} `PROFESSION_CODE` AS "professionCode"
    <#assign prefixName=','>
</#if>
<#if
(select_professionId??)>
${prefixName} `PROFESSION_ID` AS "professionId"
    <#assign prefixName=','>
</#if>
<#if
(select_professionName??)>
${prefixName} `PROFESSION_NAME` AS "professionName"
    <#assign prefixName=','>
</#if>
<#if
(select_provinceCode??)>
${prefixName} `PROVINCE_CODE` AS "provinceCode"
    <#assign prefixName=','>
</#if>
<#if
(select_provinceId??)>
${prefixName} `PROVINCE_ID` AS "provinceId"
    <#assign prefixName=','>
</#if>
<#if
(select_provinceName??)>
${prefixName} `PROVINCE_NAME` AS "provinceName"
    <#assign prefixName=','>
</#if>
<#if
(select_sexCode??)>
${prefixName} `SEX_CODE` AS "sexCode"
    <#assign prefixName=','>
</#if>
<#if
(select_sexId??)>
${prefixName} `SEX_ID` AS "sexId"
    <#assign prefixName=','>
</#if>
<#if
(select_sexName??)>
${prefixName} `SEX_NAME` AS "sexName"
    <#assign prefixName=','>
</#if>
<#if
(select_stateCode??)>
${prefixName} `STATE_CODE` AS "stateCode"
    <#assign prefixName=','>
</#if>
<#if
(select_stateId??)>
${prefixName} `STATE_ID` AS "stateId"
    <#assign prefixName=','>
</#if>
<#if
(select_stateName??)>
${prefixName} `STATE_NAME` AS "stateName"
    <#assign prefixName=','>
</#if>
<#if
(select_telephone??)>
${prefixName} `TELEPHONE` AS "telephone"
    <#assign prefixName=','>
</#if>
<#if
(select_versionInt??)>
${prefixName} `VERSION_INT` AS "versionInt"
    <#assign prefixName=','>
</#if>
<#if prefixName== ' '>
`ADDRESS` AS "address"
, `AGE` AS "age"
, `BEGIN_DATE` AS "beginDate"
, `BIRTHDAY` AS "birthday"
, `CALL_NAME` AS "callName"
, `CITY_CODE` AS "cityCode"
, `CITY_ID` AS "cityId"
, `CITY_NAME` AS "cityName"
, `CODE` AS "code"
, `COMPANY_NAME` AS "companyName"
, `COUNTRY_CODE` AS "countryCode"
, `COUNTRY_ID` AS "countryId"
, `COUNTRY_NAME` AS "countryName"
, `CREATED_BY_CODE` AS "createdByCode"
, `CREATED_BY_ID` AS "createdById"
, `CREATED_BY_NAME` AS "createdByName"
, `CREATED_DATE` AS "createdDate"
, `DATA_STATE_CODE` AS "dataStateCode"
, `DATA_STATE_ID` AS "dataStateId"
, `DATA_STATE_NAME` AS "dataStateName"
, `DEPARTMENT_CODE` AS "departmentCode"
, `DEPARTMENT_ID` AS "departmentId"
, `DEPARTMENT_NAME` AS "departmentName"
, `DIPLOMA_CODE` AS "diplomaCode"
, `DIPLOMA_ID` AS "diplomaId"
, `DIPLOMA_NAME` AS "diplomaName"
, `DISTRICT_CODE` AS "districtCode"
, `DISTRICT_ID` AS "districtId"
, `DISTRICT_NAME` AS "districtName"
, `EMAIL` AS "email"
, `END_DATE` AS "endDate"
, `FLAG_CODE` AS "flagCode"
, `FLAG_ID` AS "flagId"
, `FLAG_NAME` AS "flagName"
, `FOLK_CODE` AS "folkCode"
, `FOLK_ID` AS "folkId"
, `FOLK_NAME` AS "folkName"
, `ID` AS "id"
, `INTRO` AS "intro"
, `LAST_MODIFIED_BY_CODE` AS "lastModifiedByCode"
, `LAST_MODIFIED_BY_ID` AS "lastModifiedById"
, `LAST_MODIFIED_BY_NAME` AS "lastModifiedByName"
, `LAST_MODIFIED_DATE` AS "lastModifiedDate"
, `LOCUS` AS "locus"
, `MARRY_STATE_CODE` AS "marryStateCode"
, `MARRY_STATE_ID` AS "marryStateId"
, `MARRY_STATE_NAME` AS "marryStateName"
, `MOBILE_PHONE` AS "mobilePhone"
, `NAME` AS "name"
, `NO` AS "no"
, `ORG_CODE` AS "orgCode"
, `ORG_ID` AS "orgId"
, `ORG_NAME` AS "orgName"
, `PASSWORD` AS "password"
, `PROFESSION_CODE` AS "professionCode"
, `PROFESSION_ID` AS "professionId"
, `PROFESSION_NAME` AS "professionName"
, `PROVINCE_CODE` AS "provinceCode"
, `PROVINCE_ID` AS "provinceId"
, `PROVINCE_NAME` AS "provinceName"
, `SEX_CODE` AS "sexCode"
, `SEX_ID` AS "sexId"
, `SEX_NAME` AS "sexName"
, `STATE_CODE` AS "stateCode"
, `STATE_ID` AS "stateId"
, `STATE_NAME` AS "stateName"
, `TELEPHONE` AS "telephone"
, `VERSION_INT` AS "versionInt"
</#if>
FROM c1_user
<#include "whereByCode.ftl">
<#assign prefixName='ORDER BY'>
<#if
(order_by_address??)>
${prefixName} `ADDRESS` ${order_by_address_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_age??)>
${prefixName} `AGE` ${order_by_age_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_beginDate??)>
${prefixName} `BEGIN_DATE` ${order_by_beginDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_birthday??)>
${prefixName} `BIRTHDAY` ${order_by_birthday_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_callName??)>
${prefixName} `CALL_NAME` ${order_by_callName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_cityCode??)>
${prefixName} `CITY_CODE` ${order_by_cityCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_cityId??)>
${prefixName} `CITY_ID` ${order_by_cityId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_cityName??)>
${prefixName} `CITY_NAME` ${order_by_cityName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_code??)>
${prefixName} `CODE` ${order_by_code_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_companyName??)>
${prefixName} `COMPANY_NAME` ${order_by_companyName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_countryCode??)>
${prefixName} `COUNTRY_CODE` ${order_by_countryCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_countryId??)>
${prefixName} `COUNTRY_ID` ${order_by_countryId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_countryName??)>
${prefixName} `COUNTRY_NAME` ${order_by_countryName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByCode??)>
${prefixName} `CREATED_BY_CODE` ${order_by_createdByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdById??)>
${prefixName} `CREATED_BY_ID` ${order_by_createdById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByName??)>
${prefixName} `CREATED_BY_NAME` ${order_by_createdByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdDate??)>
${prefixName} `CREATED_DATE` ${order_by_createdDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_dataStateCode??)>
${prefixName} `DATA_STATE_CODE` ${order_by_dataStateCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_dataStateId??)>
${prefixName} `DATA_STATE_ID` ${order_by_dataStateId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_dataStateName??)>
${prefixName} `DATA_STATE_NAME` ${order_by_dataStateName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_departmentCode??)>
${prefixName} `DEPARTMENT_CODE` ${order_by_departmentCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_departmentId??)>
${prefixName} `DEPARTMENT_ID` ${order_by_departmentId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_departmentName??)>
${prefixName} `DEPARTMENT_NAME` ${order_by_departmentName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_diplomaCode??)>
${prefixName} `DIPLOMA_CODE` ${order_by_diplomaCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_diplomaId??)>
${prefixName} `DIPLOMA_ID` ${order_by_diplomaId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_diplomaName??)>
${prefixName} `DIPLOMA_NAME` ${order_by_diplomaName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_districtCode??)>
${prefixName} `DISTRICT_CODE` ${order_by_districtCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_districtId??)>
${prefixName} `DISTRICT_ID` ${order_by_districtId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_districtName??)>
${prefixName} `DISTRICT_NAME` ${order_by_districtName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_email??)>
${prefixName} `EMAIL` ${order_by_email_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_endDate??)>
${prefixName} `END_DATE` ${order_by_endDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_flagCode??)>
${prefixName} `FLAG_CODE` ${order_by_flagCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_flagId??)>
${prefixName} `FLAG_ID` ${order_by_flagId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_flagName??)>
${prefixName} `FLAG_NAME` ${order_by_flagName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_folkCode??)>
${prefixName} `FOLK_CODE` ${order_by_folkCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_folkId??)>
${prefixName} `FOLK_ID` ${order_by_folkId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_folkName??)>
${prefixName} `FOLK_NAME` ${order_by_folkName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_id??)>
${prefixName} `ID` ${order_by_id_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_intro??)>
${prefixName} `INTRO` ${order_by_intro_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE` ${order_by_lastModifiedByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID` ${order_by_lastModifiedById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME` ${order_by_lastModifiedByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE` ${order_by_lastModifiedDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_locus??)>
${prefixName} `LOCUS` ${order_by_locus_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_marryStateCode??)>
${prefixName} `MARRY_STATE_CODE` ${order_by_marryStateCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_marryStateId??)>
${prefixName} `MARRY_STATE_ID` ${order_by_marryStateId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_marryStateName??)>
${prefixName} `MARRY_STATE_NAME` ${order_by_marryStateName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_mobilePhone??)>
${prefixName} `MOBILE_PHONE` ${order_by_mobilePhone_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_name??)>
${prefixName} `NAME` ${order_by_name_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_no??)>
${prefixName} `NO` ${order_by_no_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_orgCode??)>
${prefixName} `ORG_CODE` ${order_by_orgCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_orgId??)>
${prefixName} `ORG_ID` ${order_by_orgId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_orgName??)>
${prefixName} `ORG_NAME` ${order_by_orgName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_password??)>
${prefixName} `PASSWORD` ${order_by_password_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_professionCode??)>
${prefixName} `PROFESSION_CODE` ${order_by_professionCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_professionId??)>
${prefixName} `PROFESSION_ID` ${order_by_professionId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_professionName??)>
${prefixName} `PROFESSION_NAME` ${order_by_professionName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_provinceCode??)>
${prefixName} `PROVINCE_CODE` ${order_by_provinceCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_provinceId??)>
${prefixName} `PROVINCE_ID` ${order_by_provinceId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_provinceName??)>
${prefixName} `PROVINCE_NAME` ${order_by_provinceName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_sexCode??)>
${prefixName} `SEX_CODE` ${order_by_sexCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_sexId??)>
${prefixName} `SEX_ID` ${order_by_sexId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_sexName??)>
${prefixName} `SEX_NAME` ${order_by_sexName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateCode??)>
${prefixName} `STATE_CODE` ${order_by_stateCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateId??)>
${prefixName} `STATE_ID` ${order_by_stateId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateName??)>
${prefixName} `STATE_NAME` ${order_by_stateName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_telephone??)>
${prefixName} `TELEPHONE` ${order_by_telephone_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_versionInt??)>
${prefixName} `VERSION_INT` ${order_by_versionInt_value!}
    <#assign prefixName=','>
</#if>
