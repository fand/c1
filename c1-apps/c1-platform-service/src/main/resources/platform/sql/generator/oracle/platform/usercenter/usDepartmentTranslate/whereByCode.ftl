<#assign prefixName='WHERE'>
<#if
(where_and_eq_beginDate??)>
    <#if
    (where_and_eq_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE = :where_and_eq_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE <> :where_and_nq_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE <> :where_and_like_beginDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE = :where_or_eq_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE <>
    :where_or_nq_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE like
    :where_or_like_beginDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdByCode??)>
    <#if
    (where_and_eq_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE = :where_and_eq_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE <> :where_and_nq_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE <> :where_and_like_createdByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE = :where_or_eq_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE <>
    :where_or_nq_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE like
    :where_or_like_createdByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdById??)>
    <#if
    (where_and_eq_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID = :where_and_eq_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID <> :where_and_nq_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID <> :where_and_like_createdById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID = :where_or_eq_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID <>
    :where_or_nq_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID like
    :where_or_like_createdById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdByName??)>
    <#if
    (where_and_eq_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME = :where_and_eq_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME <> :where_and_nq_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME <> :where_and_like_createdByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME = :where_or_eq_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME <>
    :where_or_nq_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME like
    :where_or_like_createdByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdDate??)>
    <#if
    (where_and_eq_createdDate_value??)>
    ${prefixName} T.CREATED_DATE = :where_and_eq_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdDate_value??)>
    ${prefixName} T.CREATED_DATE <> :where_and_nq_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdDate_value??)>
    ${prefixName} T.CREATED_DATE <> :where_and_like_createdDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdDate_value??)>
    ${prefixName} T.CREATED_DATE = :where_or_eq_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdDate_value??)>
    ${prefixName} T.CREATED_DATE <>
    :where_or_nq_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdDate_value??)>
    ${prefixName} T.CREATED_DATE like
    :where_or_like_createdDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_departmentId??)>
    <#if
    (where_and_eq_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID = :where_and_eq_departmentId_value
    <#else>
    ${prefixName} T.DEPARTMENT_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_departmentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID <> :where_and_nq_departmentId_value
    <#else>
    ${prefixName} T.DEPARTMENT_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_departmentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID <> :where_and_like_departmentId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_departmentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID = :where_or_eq_departmentId_value
    <#else>
    ${prefixName} T.DEPARTMENT_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_departmentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID <>
    :where_or_nq_departmentId_value
    <#else>
    ${prefixName} T.DEPARTMENT_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_departmentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID like
    :where_or_like_departmentId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_endDate??)>
    <#if
    (where_and_eq_endDate_value??)>
    ${prefixName} T.END_DATE = :where_and_eq_endDate_value
    <#else>
    ${prefixName} T.END_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_endDate_value??)>
    ${prefixName} T.END_DATE <> :where_and_nq_endDate_value
    <#else>
    ${prefixName} T.END_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_endDate_value??)>
    ${prefixName} T.END_DATE <> :where_and_like_endDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_endDate_value??)>
    ${prefixName} T.END_DATE = :where_or_eq_endDate_value
    <#else>
    ${prefixName} T.END_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_endDate_value??)>
    ${prefixName} T.END_DATE <>
    :where_or_nq_endDate_value
    <#else>
    ${prefixName} T.END_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_endDate_value??)>
    ${prefixName} T.END_DATE like
    :where_or_like_endDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_between_sysdate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
${prefixName} SYSDATE BETWEEN NVL(T.BEGIN_DATE,
SYSDATE) AND NVL(T.END_DATE, SYSDATE)
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_id??)>
    <#if
    (where_and_eq_id_value??)>
    ${prefixName} T.ID = :where_and_eq_id_value
    <#else>
    ${prefixName} T.ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_id_value??)>
    ${prefixName} T.ID <> :where_and_nq_id_value
    <#else>
    ${prefixName} T.ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_id_value??)>
    ${prefixName} T.ID <> :where_and_like_id_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_id_value??)>
    ${prefixName} T.ID = :where_or_eq_id_value
    <#else>
    ${prefixName} T.ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_id_value??)>
    ${prefixName} T.ID <>
    :where_or_nq_id_value
    <#else>
    ${prefixName} T.ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_id_value??)>
    ${prefixName} T.ID like
    :where_or_like_id_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedByCode??)>
    <#if
    (where_and_eq_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = :where_and_eq_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE <> :where_and_nq_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE <> :where_and_like_lastModifiedByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = :where_or_eq_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE <>
    :where_or_nq_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE like
    :where_or_like_lastModifiedByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedById??)>
    <#if
    (where_and_eq_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID = :where_and_eq_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID <> :where_and_nq_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID <> :where_and_like_lastModifiedById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID = :where_or_eq_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID <>
    :where_or_nq_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID like
    :where_or_like_lastModifiedById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedByName??)>
    <#if
    (where_and_eq_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = :where_and_eq_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME <> :where_and_nq_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME <> :where_and_like_lastModifiedByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = :where_or_eq_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME <>
    :where_or_nq_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME like
    :where_or_like_lastModifiedByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedDate??)>
    <#if
    (where_and_eq_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE = :where_and_eq_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE <> :where_and_nq_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE <> :where_and_like_lastModifiedDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE = :where_or_eq_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE <>
    :where_or_nq_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE like
    :where_or_like_lastModifiedDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_stateId??)>
    <#if
    (where_and_eq_stateId_value??)>
    ${prefixName} T.STATE_ID = :where_and_eq_stateId_value
    <#else>
    ${prefixName} T.STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_stateId_value??)>
    ${prefixName} T.STATE_ID <> :where_and_nq_stateId_value
    <#else>
    ${prefixName} T.STATE_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_stateId_value??)>
    ${prefixName} T.STATE_ID <> :where_and_like_stateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_stateId_value??)>
    ${prefixName} T.STATE_ID = :where_or_eq_stateId_value
    <#else>
    ${prefixName} T.STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_stateId_value??)>
    ${prefixName} T.STATE_ID <>
    :where_or_nq_stateId_value
    <#else>
    ${prefixName} T.STATE_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_stateId_value??)>
    ${prefixName} T.STATE_ID like
    :where_or_like_stateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_userId??)>
    <#if
    (where_and_eq_userId_value??)>
    ${prefixName} T.USER_ID = :where_and_eq_userId_value
    <#else>
    ${prefixName} T.USER_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_userId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_userId_value??)>
    ${prefixName} T.USER_ID <> :where_and_nq_userId_value
    <#else>
    ${prefixName} T.USER_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_userId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_userId_value??)>
    ${prefixName} T.USER_ID <> :where_and_like_userId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_userId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_userId_value??)>
    ${prefixName} T.USER_ID = :where_or_eq_userId_value
    <#else>
    ${prefixName} T.USER_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_userId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_userId_value??)>
    ${prefixName} T.USER_ID <>
    :where_or_nq_userId_value
    <#else>
    ${prefixName} T.USER_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_userId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_userId_value??)>
    ${prefixName} T.USER_ID like
    :where_or_like_userId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_version??)>
    <#if
    (where_and_eq_version_value??)>
    ${prefixName} T.VERSION = :where_and_eq_version_value
    <#else>
    ${prefixName} T.VERSION IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_version??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_version_value??)>
    ${prefixName} T.VERSION <> :where_and_nq_version_value
    <#else>
    ${prefixName} T.VERSION IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_version??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_version_value??)>
    ${prefixName} T.VERSION <> :where_and_like_version_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_version??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_version_value??)>
    ${prefixName} T.VERSION = :where_or_eq_version_value
    <#else>
    ${prefixName} T.VERSION IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_version??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_version_value??)>
    ${prefixName} T.VERSION <>
    :where_or_nq_version_value
    <#else>
    ${prefixName} T.VERSION IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_version??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_version_value??)>
    ${prefixName} T.VERSION like
    :where_or_like_version_value
    </#if>
    <#assign prefixName=''>
</#if>
