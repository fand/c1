<#assign prefixName='WHERE'>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_beginDate??)>
    <#if
    (where_and_eq_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` = :where_and_eq_beginDate_value
    <#else>
    ${prefixName} `BEGIN_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` <> :where_and_nq_beginDate_value
    <#else>
    ${prefixName} `BEGIN_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` <> :where_and_like_beginDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` = :where_or_eq_beginDate
    _value
    <#else>
    ${prefixName} `BEGIN_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` <>
    :where_or_nq_beginDate_value
    <#else>
    ${prefixName} `BEGIN_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_beginDate_value??)>
    ${prefixName} `BEGIN_DATE` like
    :where_or_like_beginDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdByCode??)>
    <#if
    (where_and_eq_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` = :where_and_eq_createdByCode_value
    <#else>
    ${prefixName} `CREATED_BY_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` <> :where_and_nq_createdByCode_value
    <#else>
    ${prefixName} `CREATED_BY_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` <> :where_and_like_createdByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` = :where_or_eq_createdByCode
    _value
    <#else>
    ${prefixName} `CREATED_BY_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` <>
    :where_or_nq_createdByCode_value
    <#else>
    ${prefixName} `CREATED_BY_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdByCode_value??)>
    ${prefixName} `CREATED_BY_CODE` like
    :where_or_like_createdByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdById??)>
    <#if
    (where_and_eq_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` = :where_and_eq_createdById_value
    <#else>
    ${prefixName} `CREATED_BY_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` <> :where_and_nq_createdById_value
    <#else>
    ${prefixName} `CREATED_BY_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` <> :where_and_like_createdById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` = :where_or_eq_createdById
    _value
    <#else>
    ${prefixName} `CREATED_BY_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` <>
    :where_or_nq_createdById_value
    <#else>
    ${prefixName} `CREATED_BY_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdById_value??)>
    ${prefixName} `CREATED_BY_ID` like
    :where_or_like_createdById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdByName??)>
    <#if
    (where_and_eq_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` = :where_and_eq_createdByName_value
    <#else>
    ${prefixName} `CREATED_BY_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` <> :where_and_nq_createdByName_value
    <#else>
    ${prefixName} `CREATED_BY_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` <> :where_and_like_createdByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` = :where_or_eq_createdByName
    _value
    <#else>
    ${prefixName} `CREATED_BY_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` <>
    :where_or_nq_createdByName_value
    <#else>
    ${prefixName} `CREATED_BY_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdByName_value??)>
    ${prefixName} `CREATED_BY_NAME` like
    :where_or_like_createdByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdDate??)>
    <#if
    (where_and_eq_createdDate_value??)>
    ${prefixName} `CREATED_DATE` = :where_and_eq_createdDate_value
    <#else>
    ${prefixName} `CREATED_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdDate_value??)>
    ${prefixName} `CREATED_DATE` <> :where_and_nq_createdDate_value
    <#else>
    ${prefixName} `CREATED_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdDate_value??)>
    ${prefixName} `CREATED_DATE` <> :where_and_like_createdDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdDate_value??)>
    ${prefixName} `CREATED_DATE` = :where_or_eq_createdDate
    _value
    <#else>
    ${prefixName} `CREATED_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdDate_value??)>
    ${prefixName} `CREATED_DATE` <>
    :where_or_nq_createdDate_value
    <#else>
    ${prefixName} `CREATED_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdDate_value??)>
    ${prefixName} `CREATED_DATE` like
    :where_or_like_createdDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_dataStateCode??)>
    <#if
    (where_and_eq_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` = :where_and_eq_dataStateCode_value
    <#else>
    ${prefixName} `DATA_STATE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_dataStateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` <> :where_and_nq_dataStateCode_value
    <#else>
    ${prefixName} `DATA_STATE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_dataStateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` <> :where_and_like_dataStateCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_dataStateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` = :where_or_eq_dataStateCode
    _value
    <#else>
    ${prefixName} `DATA_STATE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_dataStateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` <>
    :where_or_nq_dataStateCode_value
    <#else>
    ${prefixName} `DATA_STATE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_dataStateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_dataStateCode_value??)>
    ${prefixName} `DATA_STATE_CODE` like
    :where_or_like_dataStateCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_dataStateId??)>
    <#if
    (where_and_eq_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` = :where_and_eq_dataStateId_value
    <#else>
    ${prefixName} `DATA_STATE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_dataStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` <> :where_and_nq_dataStateId_value
    <#else>
    ${prefixName} `DATA_STATE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_dataStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` <> :where_and_like_dataStateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_dataStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` = :where_or_eq_dataStateId
    _value
    <#else>
    ${prefixName} `DATA_STATE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_dataStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` <>
    :where_or_nq_dataStateId_value
    <#else>
    ${prefixName} `DATA_STATE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_dataStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_dataStateId_value??)>
    ${prefixName} `DATA_STATE_ID` like
    :where_or_like_dataStateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_dataStateName??)>
    <#if
    (where_and_eq_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` = :where_and_eq_dataStateName_value
    <#else>
    ${prefixName} `DATA_STATE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_dataStateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` <> :where_and_nq_dataStateName_value
    <#else>
    ${prefixName} `DATA_STATE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_dataStateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` <> :where_and_like_dataStateName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_dataStateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` = :where_or_eq_dataStateName
    _value
    <#else>
    ${prefixName} `DATA_STATE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_dataStateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` <>
    :where_or_nq_dataStateName_value
    <#else>
    ${prefixName} `DATA_STATE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_dataStateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_dataStateName_value??)>
    ${prefixName} `DATA_STATE_NAME` like
    :where_or_like_dataStateName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_endDate??)>
    <#if
    (where_and_eq_endDate_value??)>
    ${prefixName} `END_DATE` = :where_and_eq_endDate_value
    <#else>
    ${prefixName} `END_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_endDate_value??)>
    ${prefixName} `END_DATE` <> :where_and_nq_endDate_value
    <#else>
    ${prefixName} `END_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_endDate_value??)>
    ${prefixName} `END_DATE` <> :where_and_like_endDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_endDate_value??)>
    ${prefixName} `END_DATE` = :where_or_eq_endDate
    _value
    <#else>
    ${prefixName} `END_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_endDate_value??)>
    ${prefixName} `END_DATE` <>
    :where_or_nq_endDate_value
    <#else>
    ${prefixName} `END_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_endDate_value??)>
    ${prefixName} `END_DATE` like
    :where_or_like_endDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_between_sysdate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
${prefixName} (NOW() BETWEEN
IFNULL(BEGIN_DATE, NOW()) AND IFNULL(END_DATE,
NOW()))
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_id??)>
    <#if
    (where_and_eq_id_value??)>
    ${prefixName} `ID` = :where_and_eq_id_value
    <#else>
    ${prefixName} `ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_id_value??)>
    ${prefixName} `ID` <> :where_and_nq_id_value
    <#else>
    ${prefixName} `ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_id_value??)>
    ${prefixName} `ID` <> :where_and_like_id_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_id_value??)>
    ${prefixName} `ID` = :where_or_eq_id
    _value
    <#else>
    ${prefixName} `ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_id_value??)>
    ${prefixName} `ID` <>
    :where_or_nq_id_value
    <#else>
    ${prefixName} `ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_id_value??)>
    ${prefixName} `ID` like
    :where_or_like_id_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedByCode??)>
    <#if
    (where_and_eq_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = :where_and_eq_lastModifiedByCode_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` <> :where_and_nq_lastModifiedByCode_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` <> :where_and_like_lastModifiedByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` = :where_or_eq_lastModifiedByCode
    _value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` <>
    :where_or_nq_lastModifiedByCode_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedByCode_value??)>
    ${prefixName} `LAST_MODIFIED_BY_CODE` like
    :where_or_like_lastModifiedByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedById??)>
    <#if
    (where_and_eq_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` = :where_and_eq_lastModifiedById_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` <> :where_and_nq_lastModifiedById_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` <> :where_and_like_lastModifiedById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` = :where_or_eq_lastModifiedById
    _value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` <>
    :where_or_nq_lastModifiedById_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedById_value??)>
    ${prefixName} `LAST_MODIFIED_BY_ID` like
    :where_or_like_lastModifiedById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedByName??)>
    <#if
    (where_and_eq_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = :where_and_eq_lastModifiedByName_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` <> :where_and_nq_lastModifiedByName_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` <> :where_and_like_lastModifiedByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` = :where_or_eq_lastModifiedByName
    _value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` <>
    :where_or_nq_lastModifiedByName_value
    <#else>
    ${prefixName} `LAST_MODIFIED_BY_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedByName_value??)>
    ${prefixName} `LAST_MODIFIED_BY_NAME` like
    :where_or_like_lastModifiedByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedDate??)>
    <#if
    (where_and_eq_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` = :where_and_eq_lastModifiedDate_value
    <#else>
    ${prefixName} `LAST_MODIFIED_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` <> :where_and_nq_lastModifiedDate_value
    <#else>
    ${prefixName} `LAST_MODIFIED_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` <> :where_and_like_lastModifiedDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` = :where_or_eq_lastModifiedDate
    _value
    <#else>
    ${prefixName} `LAST_MODIFIED_DATE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` <>
    :where_or_nq_lastModifiedDate_value
    <#else>
    ${prefixName} `LAST_MODIFIED_DATE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedDate_value??)>
    ${prefixName} `LAST_MODIFIED_DATE` like
    :where_or_like_lastModifiedDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_orderNum??)>
    <#if
    (where_and_eq_orderNum_value??)>
    ${prefixName} `ORDER_NUM` = :where_and_eq_orderNum_value
    <#else>
    ${prefixName} `ORDER_NUM` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_orderNum??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_orderNum_value??)>
    ${prefixName} `ORDER_NUM` <> :where_and_nq_orderNum_value
    <#else>
    ${prefixName} `ORDER_NUM` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_orderNum??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_orderNum_value??)>
    ${prefixName} `ORDER_NUM` <> :where_and_like_orderNum_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_orderNum??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_orderNum_value??)>
    ${prefixName} `ORDER_NUM` = :where_or_eq_orderNum
    _value
    <#else>
    ${prefixName} `ORDER_NUM` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_orderNum??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_orderNum_value??)>
    ${prefixName} `ORDER_NUM` <>
    :where_or_nq_orderNum_value
    <#else>
    ${prefixName} `ORDER_NUM` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_orderNum??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_orderNum_value??)>
    ${prefixName} `ORDER_NUM` like
    :where_or_like_orderNum_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_permissionId??)>
    <#if
    (where_and_eq_permissionId_value??)>
    ${prefixName} `PERMISSION_ID` = :where_and_eq_permissionId_value
    <#else>
    ${prefixName} `PERMISSION_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_permissionId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_permissionId_value??)>
    ${prefixName} `PERMISSION_ID` <> :where_and_nq_permissionId_value
    <#else>
    ${prefixName} `PERMISSION_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_permissionId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_permissionId_value??)>
    ${prefixName} `PERMISSION_ID` <> :where_and_like_permissionId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_permissionId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_permissionId_value??)>
    ${prefixName} `PERMISSION_ID` = :where_or_eq_permissionId
    _value
    <#else>
    ${prefixName} `PERMISSION_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_permissionId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_permissionId_value??)>
    ${prefixName} `PERMISSION_ID` <>
    :where_or_nq_permissionId_value
    <#else>
    ${prefixName} `PERMISSION_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_permissionId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_permissionId_value??)>
    ${prefixName} `PERMISSION_ID` like
    :where_or_like_permissionId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_stateCode??)>
    <#if
    (where_and_eq_stateCode_value??)>
    ${prefixName} `STATE_CODE` = :where_and_eq_stateCode_value
    <#else>
    ${prefixName} `STATE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_stateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_stateCode_value??)>
    ${prefixName} `STATE_CODE` <> :where_and_nq_stateCode_value
    <#else>
    ${prefixName} `STATE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_stateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_stateCode_value??)>
    ${prefixName} `STATE_CODE` <> :where_and_like_stateCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_stateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_stateCode_value??)>
    ${prefixName} `STATE_CODE` = :where_or_eq_stateCode
    _value
    <#else>
    ${prefixName} `STATE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_stateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_stateCode_value??)>
    ${prefixName} `STATE_CODE` <>
    :where_or_nq_stateCode_value
    <#else>
    ${prefixName} `STATE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_stateCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_stateCode_value??)>
    ${prefixName} `STATE_CODE` like
    :where_or_like_stateCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_stateId??)>
    <#if
    (where_and_eq_stateId_value??)>
    ${prefixName} `STATE_ID` = :where_and_eq_stateId_value
    <#else>
    ${prefixName} `STATE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_stateId_value??)>
    ${prefixName} `STATE_ID` <> :where_and_nq_stateId_value
    <#else>
    ${prefixName} `STATE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_stateId_value??)>
    ${prefixName} `STATE_ID` <> :where_and_like_stateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_stateId_value??)>
    ${prefixName} `STATE_ID` = :where_or_eq_stateId
    _value
    <#else>
    ${prefixName} `STATE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_stateId_value??)>
    ${prefixName} `STATE_ID` <>
    :where_or_nq_stateId_value
    <#else>
    ${prefixName} `STATE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_stateId_value??)>
    ${prefixName} `STATE_ID` like
    :where_or_like_stateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_stateName??)>
    <#if
    (where_and_eq_stateName_value??)>
    ${prefixName} `STATE_NAME` = :where_and_eq_stateName_value
    <#else>
    ${prefixName} `STATE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_stateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_stateName_value??)>
    ${prefixName} `STATE_NAME` <> :where_and_nq_stateName_value
    <#else>
    ${prefixName} `STATE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_stateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_stateName_value??)>
    ${prefixName} `STATE_NAME` <> :where_and_like_stateName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_stateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_stateName_value??)>
    ${prefixName} `STATE_NAME` = :where_or_eq_stateName
    _value
    <#else>
    ${prefixName} `STATE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_stateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_stateName_value??)>
    ${prefixName} `STATE_NAME` <>
    :where_or_nq_stateName_value
    <#else>
    ${prefixName} `STATE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_stateName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_stateName_value??)>
    ${prefixName} `STATE_NAME` like
    :where_or_like_stateName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_validateErrorCode??)>
    <#if
    (where_and_eq_validateErrorCode_value??)>
    ${prefixName} `VALIDATE_ERROR_CODE` = :where_and_eq_validateErrorCode_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_validateErrorCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_validateErrorCode_value??)>
    ${prefixName} `VALIDATE_ERROR_CODE` <> :where_and_nq_validateErrorCode_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_validateErrorCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_validateErrorCode_value??)>
    ${prefixName} `VALIDATE_ERROR_CODE` <> :where_and_like_validateErrorCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_validateErrorCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_validateErrorCode_value??)>
    ${prefixName} `VALIDATE_ERROR_CODE` = :where_or_eq_validateErrorCode
    _value
    <#else>
    ${prefixName} `VALIDATE_ERROR_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_validateErrorCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_validateErrorCode_value??)>
    ${prefixName} `VALIDATE_ERROR_CODE` <>
    :where_or_nq_validateErrorCode_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_validateErrorCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_validateErrorCode_value??)>
    ${prefixName} `VALIDATE_ERROR_CODE` like
    :where_or_like_validateErrorCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_validateErrorId??)>
    <#if
    (where_and_eq_validateErrorId_value??)>
    ${prefixName} `VALIDATE_ERROR_ID` = :where_and_eq_validateErrorId_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_validateErrorId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_validateErrorId_value??)>
    ${prefixName} `VALIDATE_ERROR_ID` <> :where_and_nq_validateErrorId_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_validateErrorId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_validateErrorId_value??)>
    ${prefixName} `VALIDATE_ERROR_ID` <> :where_and_like_validateErrorId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_validateErrorId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_validateErrorId_value??)>
    ${prefixName} `VALIDATE_ERROR_ID` = :where_or_eq_validateErrorId
    _value
    <#else>
    ${prefixName} `VALIDATE_ERROR_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_validateErrorId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_validateErrorId_value??)>
    ${prefixName} `VALIDATE_ERROR_ID` <>
    :where_or_nq_validateErrorId_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_validateErrorId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_validateErrorId_value??)>
    ${prefixName} `VALIDATE_ERROR_ID` like
    :where_or_like_validateErrorId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_validateErrorName??)>
    <#if
    (where_and_eq_validateErrorName_value??)>
    ${prefixName} `VALIDATE_ERROR_NAME` = :where_and_eq_validateErrorName_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_validateErrorName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_validateErrorName_value??)>
    ${prefixName} `VALIDATE_ERROR_NAME` <> :where_and_nq_validateErrorName_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_validateErrorName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_validateErrorName_value??)>
    ${prefixName} `VALIDATE_ERROR_NAME` <> :where_and_like_validateErrorName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_validateErrorName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_validateErrorName_value??)>
    ${prefixName} `VALIDATE_ERROR_NAME` = :where_or_eq_validateErrorName
    _value
    <#else>
    ${prefixName} `VALIDATE_ERROR_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_validateErrorName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_validateErrorName_value??)>
    ${prefixName} `VALIDATE_ERROR_NAME` <>
    :where_or_nq_validateErrorName_value
    <#else>
    ${prefixName} `VALIDATE_ERROR_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_validateErrorName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_validateErrorName_value??)>
    ${prefixName} `VALIDATE_ERROR_NAME` like
    :where_or_like_validateErrorName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_validateRuleCode??)>
    <#if
    (where_and_eq_validateRuleCode_value??)>
    ${prefixName} `VALIDATE_RULE_CODE` = :where_and_eq_validateRuleCode_value
    <#else>
    ${prefixName} `VALIDATE_RULE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_validateRuleCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_validateRuleCode_value??)>
    ${prefixName} `VALIDATE_RULE_CODE` <> :where_and_nq_validateRuleCode_value
    <#else>
    ${prefixName} `VALIDATE_RULE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_validateRuleCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_validateRuleCode_value??)>
    ${prefixName} `VALIDATE_RULE_CODE` <> :where_and_like_validateRuleCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_validateRuleCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_validateRuleCode_value??)>
    ${prefixName} `VALIDATE_RULE_CODE` = :where_or_eq_validateRuleCode
    _value
    <#else>
    ${prefixName} `VALIDATE_RULE_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_validateRuleCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_validateRuleCode_value??)>
    ${prefixName} `VALIDATE_RULE_CODE` <>
    :where_or_nq_validateRuleCode_value
    <#else>
    ${prefixName} `VALIDATE_RULE_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_validateRuleCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_validateRuleCode_value??)>
    ${prefixName} `VALIDATE_RULE_CODE` like
    :where_or_like_validateRuleCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_validateRuleId??)>
    <#if
    (where_and_eq_validateRuleId_value??)>
    ${prefixName} `VALIDATE_RULE_ID` = :where_and_eq_validateRuleId_value
    <#else>
    ${prefixName} `VALIDATE_RULE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_validateRuleId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_validateRuleId_value??)>
    ${prefixName} `VALIDATE_RULE_ID` <> :where_and_nq_validateRuleId_value
    <#else>
    ${prefixName} `VALIDATE_RULE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_validateRuleId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_validateRuleId_value??)>
    ${prefixName} `VALIDATE_RULE_ID` <> :where_and_like_validateRuleId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_validateRuleId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_validateRuleId_value??)>
    ${prefixName} `VALIDATE_RULE_ID` = :where_or_eq_validateRuleId
    _value
    <#else>
    ${prefixName} `VALIDATE_RULE_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_validateRuleId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_validateRuleId_value??)>
    ${prefixName} `VALIDATE_RULE_ID` <>
    :where_or_nq_validateRuleId_value
    <#else>
    ${prefixName} `VALIDATE_RULE_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_validateRuleId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_validateRuleId_value??)>
    ${prefixName} `VALIDATE_RULE_ID` like
    :where_or_like_validateRuleId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_validateRuleName??)>
    <#if
    (where_and_eq_validateRuleName_value??)>
    ${prefixName} `VALIDATE_RULE_NAME` = :where_and_eq_validateRuleName_value
    <#else>
    ${prefixName} `VALIDATE_RULE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_validateRuleName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_validateRuleName_value??)>
    ${prefixName} `VALIDATE_RULE_NAME` <> :where_and_nq_validateRuleName_value
    <#else>
    ${prefixName} `VALIDATE_RULE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_validateRuleName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_validateRuleName_value??)>
    ${prefixName} `VALIDATE_RULE_NAME` <> :where_and_like_validateRuleName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_validateRuleName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_validateRuleName_value??)>
    ${prefixName} `VALIDATE_RULE_NAME` = :where_or_eq_validateRuleName
    _value
    <#else>
    ${prefixName} `VALIDATE_RULE_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_validateRuleName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_validateRuleName_value??)>
    ${prefixName} `VALIDATE_RULE_NAME` <>
    :where_or_nq_validateRuleName_value
    <#else>
    ${prefixName} `VALIDATE_RULE_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_validateRuleName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_validateRuleName_value??)>
    ${prefixName} `VALIDATE_RULE_NAME` like
    :where_or_like_validateRuleName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_validateSuccessCode??)>
    <#if
    (where_and_eq_validateSuccessCode_value??)>
    ${prefixName} `VALIDATE_SUCCESS_CODE` = :where_and_eq_validateSuccessCode_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_validateSuccessCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_validateSuccessCode_value??)>
    ${prefixName} `VALIDATE_SUCCESS_CODE` <> :where_and_nq_validateSuccessCode_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_validateSuccessCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_validateSuccessCode_value??)>
    ${prefixName} `VALIDATE_SUCCESS_CODE` <> :where_and_like_validateSuccessCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_validateSuccessCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_validateSuccessCode_value??)>
    ${prefixName} `VALIDATE_SUCCESS_CODE` = :where_or_eq_validateSuccessCode
    _value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_CODE` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_validateSuccessCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_validateSuccessCode_value??)>
    ${prefixName} `VALIDATE_SUCCESS_CODE` <>
    :where_or_nq_validateSuccessCode_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_CODE` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_validateSuccessCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_validateSuccessCode_value??)>
    ${prefixName} `VALIDATE_SUCCESS_CODE` like
    :where_or_like_validateSuccessCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_validateSuccessId??)>
    <#if
    (where_and_eq_validateSuccessId_value??)>
    ${prefixName} `VALIDATE_SUCCESS_ID` = :where_and_eq_validateSuccessId_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_validateSuccessId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_validateSuccessId_value??)>
    ${prefixName} `VALIDATE_SUCCESS_ID` <> :where_and_nq_validateSuccessId_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_validateSuccessId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_validateSuccessId_value??)>
    ${prefixName} `VALIDATE_SUCCESS_ID` <> :where_and_like_validateSuccessId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_validateSuccessId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_validateSuccessId_value??)>
    ${prefixName} `VALIDATE_SUCCESS_ID` = :where_or_eq_validateSuccessId
    _value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_ID` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_validateSuccessId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_validateSuccessId_value??)>
    ${prefixName} `VALIDATE_SUCCESS_ID` <>
    :where_or_nq_validateSuccessId_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_ID` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_validateSuccessId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_validateSuccessId_value??)>
    ${prefixName} `VALIDATE_SUCCESS_ID` like
    :where_or_like_validateSuccessId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_validateSuccessName??)>
    <#if
    (where_and_eq_validateSuccessName_value??)>
    ${prefixName} `VALIDATE_SUCCESS_NAME` = :where_and_eq_validateSuccessName_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_validateSuccessName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_validateSuccessName_value??)>
    ${prefixName} `VALIDATE_SUCCESS_NAME` <> :where_and_nq_validateSuccessName_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_validateSuccessName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_validateSuccessName_value??)>
    ${prefixName} `VALIDATE_SUCCESS_NAME` <> :where_and_like_validateSuccessName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_validateSuccessName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_validateSuccessName_value??)>
    ${prefixName} `VALIDATE_SUCCESS_NAME` = :where_or_eq_validateSuccessName
    _value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_NAME` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_validateSuccessName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_validateSuccessName_value??)>
    ${prefixName} `VALIDATE_SUCCESS_NAME` <>
    :where_or_nq_validateSuccessName_value
    <#else>
    ${prefixName} `VALIDATE_SUCCESS_NAME` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_validateSuccessName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_validateSuccessName_value??)>
    ${prefixName} `VALIDATE_SUCCESS_NAME` like
    :where_or_like_validateSuccessName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_versionInt??)>
    <#if
    (where_and_eq_versionInt_value??)>
    ${prefixName} `VERSION_INT` = :where_and_eq_versionInt_value
    <#else>
    ${prefixName} `VERSION_INT` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_versionInt??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_versionInt_value??)>
    ${prefixName} `VERSION_INT` <> :where_and_nq_versionInt_value
    <#else>
    ${prefixName} `VERSION_INT` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_versionInt??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_versionInt_value??)>
    ${prefixName} `VERSION_INT` <> :where_and_like_versionInt_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_versionInt??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_versionInt_value??)>
    ${prefixName} `VERSION_INT` = :where_or_eq_versionInt
    _value
    <#else>
    ${prefixName} `VERSION_INT` IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_versionInt??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_versionInt_value??)>
    ${prefixName} `VERSION_INT` <>
    :where_or_nq_versionInt_value
    <#else>
    ${prefixName} `VERSION_INT` IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_versionInt??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_versionInt_value??)>
    ${prefixName} `VERSION_INT` like
    :where_or_like_versionInt_value
    </#if>
    <#assign prefixName=''>
</#if>
