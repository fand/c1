SELECT
<#assign prefixName=' '>
<#if
(select_beginDate??)>
${prefixName} `BEGIN_DATE` AS "beginDate"
    <#assign prefixName=','>
</#if>
<#if
(select_code??)>
${prefixName} `CODE` AS "code"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByCode??)>
${prefixName} `CREATED_BY_CODE` AS "createdByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_createdById??)>
${prefixName} `CREATED_BY_ID` AS "createdById"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByName??)>
${prefixName} `CREATED_BY_NAME` AS "createdByName"
    <#assign prefixName=','>
</#if>
<#if
(select_createdDate??)>
${prefixName} `CREATED_DATE` AS "createdDate"
    <#assign prefixName=','>
</#if>
<#if
(select_dataStateCode??)>
${prefixName} `DATA_STATE_CODE` AS "dataStateCode"
    <#assign prefixName=','>
</#if>
<#if
(select_dataStateId??)>
${prefixName} `DATA_STATE_ID` AS "dataStateId"
    <#assign prefixName=','>
</#if>
<#if
(select_dataStateName??)>
${prefixName} `DATA_STATE_NAME` AS "dataStateName"
    <#assign prefixName=','>
</#if>
<#if
(select_endDate??)>
${prefixName} `END_DATE` AS "endDate"
    <#assign prefixName=','>
</#if>
<#if
(select_id??)>
${prefixName} `ID` AS "id"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE` AS "lastModifiedByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID` AS "lastModifiedById"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME` AS "lastModifiedByName"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE` AS "lastModifiedDate"
    <#assign prefixName=','>
</#if>
<#if
(select_name??)>
${prefixName} `NAME` AS "name"
    <#assign prefixName=','>
</#if>
<#if
(select_projectCode??)>
${prefixName} `PROJECT_CODE` AS "projectCode"
    <#assign prefixName=','>
</#if>
<#if
(select_projectId??)>
${prefixName} `PROJECT_ID` AS "projectId"
    <#assign prefixName=','>
</#if>
<#if
(select_projectName??)>
${prefixName} `PROJECT_NAME` AS "projectName"
    <#assign prefixName=','>
</#if>
<#if
(select_stateCode??)>
${prefixName} `STATE_CODE` AS "stateCode"
    <#assign prefixName=','>
</#if>
<#if
(select_stateId??)>
${prefixName} `STATE_ID` AS "stateId"
    <#assign prefixName=','>
</#if>
<#if
(select_stateName??)>
${prefixName} `STATE_NAME` AS "stateName"
    <#assign prefixName=','>
</#if>
<#if
(select_styleCode??)>
${prefixName} `STYLE_CODE` AS "styleCode"
    <#assign prefixName=','>
</#if>
<#if
(select_styleId??)>
${prefixName} `STYLE_ID` AS "styleId"
    <#assign prefixName=','>
</#if>
<#if
(select_styleName??)>
${prefixName} `STYLE_NAME` AS "styleName"
    <#assign prefixName=','>
</#if>
<#if
(select_versionInt??)>
${prefixName} `VERSION_INT` AS "versionInt"
    <#assign prefixName=','>
</#if>
<#if prefixName== ' '>
`BEGIN_DATE` AS "beginDate"
, `CODE` AS "code"
, `CREATED_BY_CODE` AS "createdByCode"
, `CREATED_BY_ID` AS "createdById"
, `CREATED_BY_NAME` AS "createdByName"
, `CREATED_DATE` AS "createdDate"
, `DATA_STATE_CODE` AS "dataStateCode"
, `DATA_STATE_ID` AS "dataStateId"
, `DATA_STATE_NAME` AS "dataStateName"
, `END_DATE` AS "endDate"
, `ID` AS "id"
, `LAST_MODIFIED_BY_CODE` AS "lastModifiedByCode"
, `LAST_MODIFIED_BY_ID` AS "lastModifiedById"
, `LAST_MODIFIED_BY_NAME` AS "lastModifiedByName"
, `LAST_MODIFIED_DATE` AS "lastModifiedDate"
, `NAME` AS "name"
, `PROJECT_CODE` AS "projectCode"
, `PROJECT_ID` AS "projectId"
, `PROJECT_NAME` AS "projectName"
, `STATE_CODE` AS "stateCode"
, `STATE_ID` AS "stateId"
, `STATE_NAME` AS "stateName"
, `STYLE_CODE` AS "styleCode"
, `STYLE_ID` AS "styleId"
, `STYLE_NAME` AS "styleName"
, `VERSION_INT` AS "versionInt"
</#if>
FROM c1_site
<#include "whereByCode.ftl">
<#assign prefixName='ORDER BY'>
<#if
(order_by_beginDate??)>
${prefixName} `BEGIN_DATE` ${order_by_beginDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_code??)>
${prefixName} `CODE` ${order_by_code_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByCode??)>
${prefixName} `CREATED_BY_CODE` ${order_by_createdByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdById??)>
${prefixName} `CREATED_BY_ID` ${order_by_createdById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByName??)>
${prefixName} `CREATED_BY_NAME` ${order_by_createdByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdDate??)>
${prefixName} `CREATED_DATE` ${order_by_createdDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_dataStateCode??)>
${prefixName} `DATA_STATE_CODE` ${order_by_dataStateCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_dataStateId??)>
${prefixName} `DATA_STATE_ID` ${order_by_dataStateId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_dataStateName??)>
${prefixName} `DATA_STATE_NAME` ${order_by_dataStateName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_endDate??)>
${prefixName} `END_DATE` ${order_by_endDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_id??)>
${prefixName} `ID` ${order_by_id_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByCode??)>
${prefixName} `LAST_MODIFIED_BY_CODE` ${order_by_lastModifiedByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedById??)>
${prefixName} `LAST_MODIFIED_BY_ID` ${order_by_lastModifiedById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByName??)>
${prefixName} `LAST_MODIFIED_BY_NAME` ${order_by_lastModifiedByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedDate??)>
${prefixName} `LAST_MODIFIED_DATE` ${order_by_lastModifiedDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_name??)>
${prefixName} `NAME` ${order_by_name_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_projectCode??)>
${prefixName} `PROJECT_CODE` ${order_by_projectCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_projectId??)>
${prefixName} `PROJECT_ID` ${order_by_projectId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_projectName??)>
${prefixName} `PROJECT_NAME` ${order_by_projectName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateCode??)>
${prefixName} `STATE_CODE` ${order_by_stateCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateId??)>
${prefixName} `STATE_ID` ${order_by_stateId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateName??)>
${prefixName} `STATE_NAME` ${order_by_stateName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_styleCode??)>
${prefixName} `STYLE_CODE` ${order_by_styleCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_styleId??)>
${prefixName} `STYLE_ID` ${order_by_styleId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_styleName??)>
${prefixName} `STYLE_NAME` ${order_by_styleName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_versionInt??)>
${prefixName} `VERSION_INT` ${order_by_versionInt_value!}
    <#assign prefixName=','>
</#if>
