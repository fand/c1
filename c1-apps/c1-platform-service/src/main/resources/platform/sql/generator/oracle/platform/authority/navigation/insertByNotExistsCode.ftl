INSERT
INTO
C1_NAVIGATION
(
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} BEGIN_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_code??) && (insert_code_value??)>
${prefixName} CODE
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} CREATED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} CREATED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} CREATED_BY_NAME
    <#assign prefixName=','>
</#if>
${prefixName} CREATED_DATE
<#assign prefixName=','>
<#if (insert_dataOption??) && (insert_dataOption_value??)>
${prefixName} DATA_OPTION
    <#assign prefixName=','>
</#if>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} END_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_iconStyle??) && (insert_iconStyle_value??)>
${prefixName} ICON_STYLE
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} ID
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} LAST_MODIFIED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} LAST_MODIFIED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} LAST_MODIFIED_BY_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} LAST_MODIFIED_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_name??) && (insert_name_value??)>
${prefixName} NAME
    <#assign prefixName=','>
</#if>
<#if (insert_orderNum??) && (insert_orderNum_value??)>
${prefixName} ORDER_NUM
    <#assign prefixName=','>
</#if>
<#if (insert_parentId??) && (insert_parentId_value??)>
${prefixName} PARENT_ID
    <#assign prefixName=','>
</#if>
<#if (insert_siteId??) && (insert_siteId_value??)>
${prefixName} SITE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} STATE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_target??) && (insert_target_value??)>
${prefixName} TARGET
    <#assign prefixName=','>
</#if>
<#if (insert_typeId??) && (insert_typeId_value??)>
${prefixName} TYPE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_url??) && (insert_url_value??)>
${prefixName} URL
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} VERSION
    <#assign prefixName=','>
</#if>
)
(
SELECT
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} :insert_beginDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_code??) && (insert_code_value??)>
${prefixName} :insert_code_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} :insert_createdByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} :insert_createdById_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} :insert_createdByName_value
    <#assign prefixName=','>
</#if>
${prefixName} SYSDATE
<#assign prefixName=','>
<#if (insert_dataOption??) && (insert_dataOption_value??)>
${prefixName} :insert_dataOption_value
    <#assign prefixName=','>
</#if>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} :insert_endDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_iconStyle??) && (insert_iconStyle_value??)>
${prefixName} :insert_iconStyle_value
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} :insert_id_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} :insert_lastModifiedByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} :insert_lastModifiedById_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} :insert_lastModifiedByName_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} :insert_lastModifiedDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_name??) && (insert_name_value??)>
${prefixName} :insert_name_value
    <#assign prefixName=','>
</#if>
<#if (insert_orderNum??) && (insert_orderNum_value??)>
${prefixName} :insert_orderNum_value
    <#assign prefixName=','>
</#if>
<#if (insert_parentId??) && (insert_parentId_value??)>
${prefixName} :insert_parentId_value
    <#assign prefixName=','>
</#if>
<#if (insert_siteId??) && (insert_siteId_value??)>
${prefixName} :insert_siteId_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} :insert_stateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_target??) && (insert_target_value??)>
${prefixName} :insert_target_value
    <#assign prefixName=','>
</#if>
<#if (insert_typeId??) && (insert_typeId_value??)>
${prefixName} :insert_typeId_value
    <#assign prefixName=','>
</#if>
<#if (insert_url??) && (insert_url_value??)>
${prefixName} :insert_url_value
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} :insert_version_value
    <#assign prefixName=','>
</#if>
FROM DUAL WHERE NOT EXISTS (SELECT 1 FROM C1_NAVIGATION E
<#assign prefixName='WHERE'>
<#if (where_and_eq_beginDate??)>
    <#if (where_and_eq_beginDate_value??)>
    ${prefixName} E.BEGIN_DATE = :where_and_eq_beginDate_value
    <#else>
    ${prefixName} E.BEGIN_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_code??)>
    <#if (where_and_eq_code_value??)>
    ${prefixName} E.CODE = :where_and_eq_code_value
    <#else>
    ${prefixName} E.CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdByCode??)>
    <#if (where_and_eq_createdByCode_value??)>
    ${prefixName} E.CREATED_BY_CODE = :where_and_eq_createdByCode_value
    <#else>
    ${prefixName} E.CREATED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdById??)>
    <#if (where_and_eq_createdById_value??)>
    ${prefixName} E.CREATED_BY_ID = :where_and_eq_createdById_value
    <#else>
    ${prefixName} E.CREATED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdByName??)>
    <#if (where_and_eq_createdByName_value??)>
    ${prefixName} E.CREATED_BY_NAME = :where_and_eq_createdByName_value
    <#else>
    ${prefixName} E.CREATED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_createdDate??)>
    <#if (where_and_eq_createdDate_value??)>
    ${prefixName} E.CREATED_DATE = :where_and_eq_createdDate_value
    <#else>
    ${prefixName} E.CREATED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_dataOption??)>
    <#if (where_and_eq_dataOption_value??)>
    ${prefixName} E.DATA_OPTION = :where_and_eq_dataOption_value
    <#else>
    ${prefixName} E.DATA_OPTION IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_endDate??)>
    <#if (where_and_eq_endDate_value??)>
    ${prefixName} E.END_DATE = :where_and_eq_endDate_value
    <#else>
    ${prefixName} E.END_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if (where_and_between_sysdate??)>
    <#if (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
${prefixName} SYSDATE BETWEEN NVL(E.BEGIN_DATE, SYSDATE) AND NVL(E.END_DATE, SYSDATE)
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_iconStyle??)>
    <#if (where_and_eq_iconStyle_value??)>
    ${prefixName} E.ICON_STYLE = :where_and_eq_iconStyle_value
    <#else>
    ${prefixName} E.ICON_STYLE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_id??)>
    <#if (where_and_eq_id_value??)>
    ${prefixName} E.ID = :where_and_eq_id_value
    <#else>
    ${prefixName} E.ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedByCode??)>
    <#if (where_and_eq_lastModifiedByCode_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_CODE = :where_and_eq_lastModifiedByCode_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedById??)>
    <#if (where_and_eq_lastModifiedById_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_ID = :where_and_eq_lastModifiedById_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedByName??)>
    <#if (where_and_eq_lastModifiedByName_value??)>
    ${prefixName} E.LAST_MODIFIED_BY_NAME = :where_and_eq_lastModifiedByName_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_lastModifiedDate??)>
    <#if (where_and_eq_lastModifiedDate_value??)>
    ${prefixName} E.LAST_MODIFIED_DATE = :where_and_eq_lastModifiedDate_value
    <#else>
    ${prefixName} E.LAST_MODIFIED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_name??)>
    <#if (where_and_eq_name_value??)>
    ${prefixName} E.NAME = :where_and_eq_name_value
    <#else>
    ${prefixName} E.NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_orderNum??)>
    <#if (where_and_eq_orderNum_value??)>
    ${prefixName} E.ORDER_NUM = :where_and_eq_orderNum_value
    <#else>
    ${prefixName} E.ORDER_NUM IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_parentId??)>
    <#if (where_and_eq_parentId_value??)>
    ${prefixName} E.PARENT_ID = :where_and_eq_parentId_value
    <#else>
    ${prefixName} E.PARENT_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_siteId??)>
    <#if (where_and_eq_siteId_value??)>
    ${prefixName} E.SITE_ID = :where_and_eq_siteId_value
    <#else>
    ${prefixName} E.SITE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_stateId??)>
    <#if (where_and_eq_stateId_value??)>
    ${prefixName} E.STATE_ID = :where_and_eq_stateId_value
    <#else>
    ${prefixName} E.STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_target??)>
    <#if (where_and_eq_target_value??)>
    ${prefixName} E.TARGET = :where_and_eq_target_value
    <#else>
    ${prefixName} E.TARGET IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_typeId??)>
    <#if (where_and_eq_typeId_value??)>
    ${prefixName} E.TYPE_ID = :where_and_eq_typeId_value
    <#else>
    ${prefixName} E.TYPE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_url??)>
    <#if (where_and_eq_url_value??)>
    ${prefixName} E.URL = :where_and_eq_url_value
    <#else>
    ${prefixName} E.URL IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if (where_and_eq_version??)>
    <#if (where_and_eq_version_value??)>
    ${prefixName} E.VERSION = :where_and_eq_version_value
    <#else>
    ${prefixName} E.VERSION IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
)
)