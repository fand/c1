INSERT
INTO
c1_pe_va_ru_transact_mode
(
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} `BEGIN_DATE`
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} `CREATED_BY_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} `CREATED_BY_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} `CREATED_BY_NAME`
    <#assign prefixName=','>
</#if>
${prefixName} CREATED_DATE
<#assign prefixName=','>
<#if (insert_dataStateCode??) && (insert_dataStateCode_value??)>
${prefixName} `DATA_STATE_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_dataStateId??) && (insert_dataStateId_value??)>
${prefixName} `DATA_STATE_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_dataStateName??) && (insert_dataStateName_value??)>
${prefixName} `DATA_STATE_NAME`
    <#assign prefixName=','>
</#if>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} `END_DATE`
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} `ID`
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} `LAST_MODIFIED_BY_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} `LAST_MODIFIED_BY_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} `LAST_MODIFIED_BY_NAME`
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} `LAST_MODIFIED_DATE`
    <#assign prefixName=','>
</#if>
<#if (insert_orderNum??) && (insert_orderNum_value??)>
${prefixName} `ORDER_NUM`
    <#assign prefixName=','>
</#if>
<#if (insert_permissionId??) && (insert_permissionId_value??)>
${prefixName} `PERMISSION_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_stateCode??) && (insert_stateCode_value??)>
${prefixName} `STATE_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} `STATE_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_stateName??) && (insert_stateName_value??)>
${prefixName} `STATE_NAME`
    <#assign prefixName=','>
</#if>
<#if (insert_validateErrorCode??) && (insert_validateErrorCode_value??)>
${prefixName} `VALIDATE_ERROR_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_validateErrorId??) && (insert_validateErrorId_value??)>
${prefixName} `VALIDATE_ERROR_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_validateErrorName??) && (insert_validateErrorName_value??)>
${prefixName} `VALIDATE_ERROR_NAME`
    <#assign prefixName=','>
</#if>
<#if (insert_validateRuleCode??) && (insert_validateRuleCode_value??)>
${prefixName} `VALIDATE_RULE_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_validateRuleId??) && (insert_validateRuleId_value??)>
${prefixName} `VALIDATE_RULE_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_validateRuleName??) && (insert_validateRuleName_value??)>
${prefixName} `VALIDATE_RULE_NAME`
    <#assign prefixName=','>
</#if>
<#if (insert_validateSuccessCode??) && (insert_validateSuccessCode_value??)>
${prefixName} `VALIDATE_SUCCESS_CODE`
    <#assign prefixName=','>
</#if>
<#if (insert_validateSuccessId??) && (insert_validateSuccessId_value??)>
${prefixName} `VALIDATE_SUCCESS_ID`
    <#assign prefixName=','>
</#if>
<#if (insert_validateSuccessName??) && (insert_validateSuccessName_value??)>
${prefixName} `VALIDATE_SUCCESS_NAME`
    <#assign prefixName=','>
</#if>
<#if (insert_versionInt??) && (insert_versionInt_value??)>
${prefixName} `VERSION_INT`
    <#assign prefixName=','>
</#if>
)
VALUES
(
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} :insert_beginDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} :insert_createdByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} :insert_createdById_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} :insert_createdByName_value
    <#assign prefixName=','>
</#if>
${prefixName} NOW()
<#assign prefixName=','>
<#if (insert_dataStateCode??) && (insert_dataStateCode_value??)>
${prefixName} :insert_dataStateCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_dataStateId??) && (insert_dataStateId_value??)>
${prefixName} :insert_dataStateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_dataStateName??) && (insert_dataStateName_value??)>
${prefixName} :insert_dataStateName_value
    <#assign prefixName=','>
</#if>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} :insert_endDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} :insert_id_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} :insert_lastModifiedByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} :insert_lastModifiedById_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} :insert_lastModifiedByName_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} :insert_lastModifiedDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_orderNum??) && (insert_orderNum_value??)>
${prefixName} :insert_orderNum_value
    <#assign prefixName=','>
</#if>
<#if (insert_permissionId??) && (insert_permissionId_value??)>
${prefixName} :insert_permissionId_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateCode??) && (insert_stateCode_value??)>
${prefixName} :insert_stateCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} :insert_stateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateName??) && (insert_stateName_value??)>
${prefixName} :insert_stateName_value
    <#assign prefixName=','>
</#if>
<#if (insert_validateErrorCode??) && (insert_validateErrorCode_value??)>
${prefixName} :insert_validateErrorCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_validateErrorId??) && (insert_validateErrorId_value??)>
${prefixName} :insert_validateErrorId_value
    <#assign prefixName=','>
</#if>
<#if (insert_validateErrorName??) && (insert_validateErrorName_value??)>
${prefixName} :insert_validateErrorName_value
    <#assign prefixName=','>
</#if>
<#if (insert_validateRuleCode??) && (insert_validateRuleCode_value??)>
${prefixName} :insert_validateRuleCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_validateRuleId??) && (insert_validateRuleId_value??)>
${prefixName} :insert_validateRuleId_value
    <#assign prefixName=','>
</#if>
<#if (insert_validateRuleName??) && (insert_validateRuleName_value??)>
${prefixName} :insert_validateRuleName_value
    <#assign prefixName=','>
</#if>
<#if (insert_validateSuccessCode??) && (insert_validateSuccessCode_value??)>
${prefixName} :insert_validateSuccessCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_validateSuccessId??) && (insert_validateSuccessId_value??)>
${prefixName} :insert_validateSuccessId_value
    <#assign prefixName=','>
</#if>
<#if (insert_validateSuccessName??) && (insert_validateSuccessName_value??)>
${prefixName} :insert_validateSuccessName_value
    <#assign prefixName=','>
</#if>
<#if (insert_versionInt??) && (insert_versionInt_value??)>
${prefixName} :insert_versionInt_value
    <#assign prefixName=','>
</#if>
)