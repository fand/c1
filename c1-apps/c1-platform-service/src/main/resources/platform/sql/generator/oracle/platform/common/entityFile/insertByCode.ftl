INSERT
INTO
C1_ENTITY_FILE
(
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} BEGIN_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_contentType??) && (insert_contentType_value??)>
${prefixName} CONTENT_TYPE
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} CREATED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} CREATED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} CREATED_BY_NAME
    <#assign prefixName=','>
</#if>
${prefixName} CREATED_DATE
<#assign prefixName=','>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} END_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_entityId??) && (insert_entityId_value??)>
${prefixName} ENTITY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_extension??) && (insert_extension_value??)>
${prefixName} EXTENSION
    <#assign prefixName=','>
</#if>
<#if (insert_filename??) && (insert_filename_value??)>
${prefixName} FILENAME
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} ID
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} LAST_MODIFIED_BY_CODE
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} LAST_MODIFIED_BY_ID
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} LAST_MODIFIED_BY_NAME
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} LAST_MODIFIED_DATE
    <#assign prefixName=','>
</#if>
<#if (insert_name??) && (insert_name_value??)>
${prefixName} NAME
    <#assign prefixName=','>
</#if>
<#if (insert_originalFilename??) && (insert_originalFilename_value??)>
${prefixName} ORIGINAL_FILENAME
    <#assign prefixName=','>
</#if>
<#if (insert_parentId??) && (insert_parentId_value??)>
${prefixName} PARENT_ID
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} STATE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_thumbnailValue??) && (insert_thumbnailValue_value??)>
${prefixName} THUMBNAIL_VALUE
    <#assign prefixName=','>
</#if>
<#if (insert_typeId??) && (insert_typeId_value??)>
${prefixName} TYPE_ID
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} VERSION
    <#assign prefixName=','>
</#if>
)
VALUES
(
<#assign prefixName=' '>
<#if (insert_beginDate??) && (insert_beginDate_value??)>
${prefixName} :insert_beginDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_contentType??) && (insert_contentType_value??)>
${prefixName} :insert_contentType_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByCode??) && (insert_createdByCode_value??)>
${prefixName} :insert_createdByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdById??) && (insert_createdById_value??)>
${prefixName} :insert_createdById_value
    <#assign prefixName=','>
</#if>
<#if (insert_createdByName??) && (insert_createdByName_value??)>
${prefixName} :insert_createdByName_value
    <#assign prefixName=','>
</#if>
${prefixName} SYSDATE
<#assign prefixName=','>
<#if (insert_endDate??) && (insert_endDate_value??)>
${prefixName} :insert_endDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_entityId??) && (insert_entityId_value??)>
${prefixName} :insert_entityId_value
    <#assign prefixName=','>
</#if>
<#if (insert_extension??) && (insert_extension_value??)>
${prefixName} :insert_extension_value
    <#assign prefixName=','>
</#if>
<#if (insert_filename??) && (insert_filename_value??)>
${prefixName} :insert_filename_value
    <#assign prefixName=','>
</#if>
<#if (insert_id??) && (insert_id_value??)>
${prefixName} :insert_id_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByCode??) && (insert_lastModifiedByCode_value??)>
${prefixName} :insert_lastModifiedByCode_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedById??) && (insert_lastModifiedById_value??)>
${prefixName} :insert_lastModifiedById_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedByName??) && (insert_lastModifiedByName_value??)>
${prefixName} :insert_lastModifiedByName_value
    <#assign prefixName=','>
</#if>
<#if (insert_lastModifiedDate??) && (insert_lastModifiedDate_value??)>
${prefixName} :insert_lastModifiedDate_value
    <#assign prefixName=','>
</#if>
<#if (insert_name??) && (insert_name_value??)>
${prefixName} :insert_name_value
    <#assign prefixName=','>
</#if>
<#if (insert_originalFilename??) && (insert_originalFilename_value??)>
${prefixName} :insert_originalFilename_value
    <#assign prefixName=','>
</#if>
<#if (insert_parentId??) && (insert_parentId_value??)>
${prefixName} :insert_parentId_value
    <#assign prefixName=','>
</#if>
<#if (insert_stateId??) && (insert_stateId_value??)>
${prefixName} :insert_stateId_value
    <#assign prefixName=','>
</#if>
<#if (insert_thumbnailValue??) && (insert_thumbnailValue_value??)>
${prefixName} :insert_thumbnailValue_value
    <#assign prefixName=','>
</#if>
<#if (insert_typeId??) && (insert_typeId_value??)>
${prefixName} :insert_typeId_value
    <#assign prefixName=','>
</#if>
<#if (insert_version??) && (insert_version_value??)>
${prefixName} :insert_version_value
    <#assign prefixName=','>
</#if>
)