SELECT
<#assign prefixName=' '>
<#if
(select_beginDate??)>
${prefixName} T.BEGIN_DATE AS "beginDate"
    <#assign prefixName=','>
</#if>
<#if
(select_contentType??)>
${prefixName} T.CONTENT_TYPE AS "contentType"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByCode??)>
${prefixName} T.CREATED_BY_CODE AS "createdByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_createdById??)>
${prefixName} T.CREATED_BY_ID AS "createdById"
    <#assign prefixName=','>
</#if>
<#if
(select_createdByName??)>
${prefixName} T.CREATED_BY_NAME AS "createdByName"
    <#assign prefixName=','>
</#if>
<#if
(select_createdDate??)>
${prefixName} T.CREATED_DATE AS "createdDate"
    <#assign prefixName=','>
</#if>
<#if
(select_endDate??)>
${prefixName} T.END_DATE AS "endDate"
    <#assign prefixName=','>
</#if>
<#if
(select_entityId??)>
${prefixName} T.ENTITY_ID AS "entityId"
    <#assign prefixName=','>
</#if>
<#if
(select_extension??)>
${prefixName} T.EXTENSION AS "extension"
    <#assign prefixName=','>
</#if>
<#if
(select_filename??)>
${prefixName} T.FILENAME AS "filename"
    <#assign prefixName=','>
</#if>
<#if
(select_id??)>
${prefixName} T.ID AS "id"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByCode??)>
${prefixName} T.LAST_MODIFIED_BY_CODE AS "lastModifiedByCode"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedById??)>
${prefixName} T.LAST_MODIFIED_BY_ID AS "lastModifiedById"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedByName??)>
${prefixName} T.LAST_MODIFIED_BY_NAME AS "lastModifiedByName"
    <#assign prefixName=','>
</#if>
<#if
(select_lastModifiedDate??)>
${prefixName} T.LAST_MODIFIED_DATE AS "lastModifiedDate"
    <#assign prefixName=','>
</#if>
<#if
(select_name??)>
${prefixName} T.NAME AS "name"
    <#assign prefixName=','>
</#if>
<#if
(select_originalFilename??)>
${prefixName} T.ORIGINAL_FILENAME AS "originalFilename"
    <#assign prefixName=','>
</#if>
<#if
(select_parentId??)>
${prefixName} T.PARENT_ID AS "parentId"
    <#assign prefixName=','>
</#if>
<#if
(select_stateId??)>
${prefixName} T.STATE_ID AS "stateId"
    <#assign prefixName=','>
</#if>
<#if
(select_thumbnailValue??)>
${prefixName} T.THUMBNAIL_VALUE AS "thumbnailValue"
    <#assign prefixName=','>
</#if>
<#if
(select_typeId??)>
${prefixName} T.TYPE_ID AS "typeId"
    <#assign prefixName=','>
</#if>
<#if
(select_version??)>
${prefixName} T.VERSION AS "version"
    <#assign prefixName=','>
</#if>
<#if prefixName== ' '>
T.BEGIN_DATE AS "beginDate"
, T.CONTENT_TYPE AS "contentType"
, T.CREATED_BY_CODE AS "createdByCode"
, T.CREATED_BY_ID AS "createdById"
, T.CREATED_BY_NAME AS "createdByName"
, T.CREATED_DATE AS "createdDate"
, T.END_DATE AS "endDate"
, T.ENTITY_ID AS "entityId"
, T.EXTENSION AS "extension"
, T.FILENAME AS "filename"
, T.ID AS "id"
, T.LAST_MODIFIED_BY_CODE AS "lastModifiedByCode"
, T.LAST_MODIFIED_BY_ID AS "lastModifiedById"
, T.LAST_MODIFIED_BY_NAME AS "lastModifiedByName"
, T.LAST_MODIFIED_DATE AS "lastModifiedDate"
, T.NAME AS "name"
, T.ORIGINAL_FILENAME AS "originalFilename"
, T.PARENT_ID AS "parentId"
, T.STATE_ID AS "stateId"
, T.THUMBNAIL_VALUE AS "thumbnailValue"
, T.TYPE_ID AS "typeId"
, T.VERSION AS "version"
</#if>
FROM C1_ENTITY_FILE T
<#include "whereByCode.ftl">
<#assign prefixName='ORDER BY'>
<#if
(order_by_beginDate??)>
${prefixName} T.BEGIN_DATE ${order_by_beginDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_contentType??)>
${prefixName} T.CONTENT_TYPE ${order_by_contentType_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByCode??)>
${prefixName} T.CREATED_BY_CODE ${order_by_createdByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdById??)>
${prefixName} T.CREATED_BY_ID ${order_by_createdById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdByName??)>
${prefixName} T.CREATED_BY_NAME ${order_by_createdByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_createdDate??)>
${prefixName} T.CREATED_DATE ${order_by_createdDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_endDate??)>
${prefixName} T.END_DATE ${order_by_endDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_entityId??)>
${prefixName} T.ENTITY_ID ${order_by_entityId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_extension??)>
${prefixName} T.EXTENSION ${order_by_extension_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_filename??)>
${prefixName} T.FILENAME ${order_by_filename_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_id??)>
${prefixName} T.ID ${order_by_id_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByCode??)>
${prefixName} T.LAST_MODIFIED_BY_CODE ${order_by_lastModifiedByCode_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedById??)>
${prefixName} T.LAST_MODIFIED_BY_ID ${order_by_lastModifiedById_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedByName??)>
${prefixName} T.LAST_MODIFIED_BY_NAME ${order_by_lastModifiedByName_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_lastModifiedDate??)>
${prefixName} T.LAST_MODIFIED_DATE ${order_by_lastModifiedDate_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_name??)>
${prefixName} T.NAME ${order_by_name_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_originalFilename??)>
${prefixName} T.ORIGINAL_FILENAME ${order_by_originalFilename_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_parentId??)>
${prefixName} T.PARENT_ID ${order_by_parentId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_stateId??)>
${prefixName} T.STATE_ID ${order_by_stateId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_thumbnailValue??)>
${prefixName} T.THUMBNAIL_VALUE ${order_by_thumbnailValue_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_typeId??)>
${prefixName} T.TYPE_ID ${order_by_typeId_value!}
    <#assign prefixName=','>
</#if>
<#if
(order_by_version??)>
${prefixName} T.VERSION ${order_by_version_value!}
    <#assign prefixName=','>
</#if>
