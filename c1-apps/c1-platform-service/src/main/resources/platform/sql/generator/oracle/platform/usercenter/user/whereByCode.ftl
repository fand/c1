<#assign prefixName='WHERE'>
<#if
(where_and_eq_accountName??)>
    <#if
    (where_and_eq_accountName_value??)>
    ${prefixName} T.ACCOUNT_NAME = :where_and_eq_accountName_value
    <#else>
    ${prefixName} T.ACCOUNT_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_accountName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_accountName_value??)>
    ${prefixName} T.ACCOUNT_NAME <> :where_and_nq_accountName_value
    <#else>
    ${prefixName} T.ACCOUNT_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_accountName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_accountName_value??)>
    ${prefixName} T.ACCOUNT_NAME <> :where_and_like_accountName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_accountName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_accountName_value??)>
    ${prefixName} T.ACCOUNT_NAME = :where_or_eq_accountName_value
    <#else>
    ${prefixName} T.ACCOUNT_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_accountName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_accountName_value??)>
    ${prefixName} T.ACCOUNT_NAME <>
    :where_or_nq_accountName_value
    <#else>
    ${prefixName} T.ACCOUNT_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_accountName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_accountName_value??)>
    ${prefixName} T.ACCOUNT_NAME like
    :where_or_like_accountName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_address??)>
    <#if
    (where_and_eq_address_value??)>
    ${prefixName} T.ADDRESS = :where_and_eq_address_value
    <#else>
    ${prefixName} T.ADDRESS IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_address??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_address_value??)>
    ${prefixName} T.ADDRESS <> :where_and_nq_address_value
    <#else>
    ${prefixName} T.ADDRESS IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_address??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_address_value??)>
    ${prefixName} T.ADDRESS <> :where_and_like_address_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_address??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_address_value??)>
    ${prefixName} T.ADDRESS = :where_or_eq_address_value
    <#else>
    ${prefixName} T.ADDRESS IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_address??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_address_value??)>
    ${prefixName} T.ADDRESS <>
    :where_or_nq_address_value
    <#else>
    ${prefixName} T.ADDRESS IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_address??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_address_value??)>
    ${prefixName} T.ADDRESS like
    :where_or_like_address_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_age??)>
    <#if
    (where_and_eq_age_value??)>
    ${prefixName} T.AGE = :where_and_eq_age_value
    <#else>
    ${prefixName} T.AGE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_age??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_age_value??)>
    ${prefixName} T.AGE <> :where_and_nq_age_value
    <#else>
    ${prefixName} T.AGE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_age??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_age_value??)>
    ${prefixName} T.AGE <> :where_and_like_age_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_age??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_age_value??)>
    ${prefixName} T.AGE = :where_or_eq_age_value
    <#else>
    ${prefixName} T.AGE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_age??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_age_value??)>
    ${prefixName} T.AGE <>
    :where_or_nq_age_value
    <#else>
    ${prefixName} T.AGE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_age??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_age_value??)>
    ${prefixName} T.AGE like
    :where_or_like_age_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_beginDate??)>
    <#if
    (where_and_eq_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE = :where_and_eq_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE <> :where_and_nq_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE <> :where_and_like_beginDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE = :where_or_eq_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE <>
    :where_or_nq_beginDate_value
    <#else>
    ${prefixName} T.BEGIN_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_beginDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_beginDate_value??)>
    ${prefixName} T.BEGIN_DATE like
    :where_or_like_beginDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_birthday??)>
    <#if
    (where_and_eq_birthday_value??)>
    ${prefixName} T.BIRTHDAY = :where_and_eq_birthday_value
    <#else>
    ${prefixName} T.BIRTHDAY IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_birthday??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_birthday_value??)>
    ${prefixName} T.BIRTHDAY <> :where_and_nq_birthday_value
    <#else>
    ${prefixName} T.BIRTHDAY IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_birthday??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_birthday_value??)>
    ${prefixName} T.BIRTHDAY <> :where_and_like_birthday_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_birthday??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_birthday_value??)>
    ${prefixName} T.BIRTHDAY = :where_or_eq_birthday_value
    <#else>
    ${prefixName} T.BIRTHDAY IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_birthday??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_birthday_value??)>
    ${prefixName} T.BIRTHDAY <>
    :where_or_nq_birthday_value
    <#else>
    ${prefixName} T.BIRTHDAY IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_birthday??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_birthday_value??)>
    ${prefixName} T.BIRTHDAY like
    :where_or_like_birthday_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_callName??)>
    <#if
    (where_and_eq_callName_value??)>
    ${prefixName} T.CALL_NAME = :where_and_eq_callName_value
    <#else>
    ${prefixName} T.CALL_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_callName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_callName_value??)>
    ${prefixName} T.CALL_NAME <> :where_and_nq_callName_value
    <#else>
    ${prefixName} T.CALL_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_callName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_callName_value??)>
    ${prefixName} T.CALL_NAME <> :where_and_like_callName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_callName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_callName_value??)>
    ${prefixName} T.CALL_NAME = :where_or_eq_callName_value
    <#else>
    ${prefixName} T.CALL_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_callName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_callName_value??)>
    ${prefixName} T.CALL_NAME <>
    :where_or_nq_callName_value
    <#else>
    ${prefixName} T.CALL_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_callName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_callName_value??)>
    ${prefixName} T.CALL_NAME like
    :where_or_like_callName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_cityId??)>
    <#if
    (where_and_eq_cityId_value??)>
    ${prefixName} T.CITY_ID = :where_and_eq_cityId_value
    <#else>
    ${prefixName} T.CITY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_cityId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_cityId_value??)>
    ${prefixName} T.CITY_ID <> :where_and_nq_cityId_value
    <#else>
    ${prefixName} T.CITY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_cityId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_cityId_value??)>
    ${prefixName} T.CITY_ID <> :where_and_like_cityId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_cityId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_cityId_value??)>
    ${prefixName} T.CITY_ID = :where_or_eq_cityId_value
    <#else>
    ${prefixName} T.CITY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_cityId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_cityId_value??)>
    ${prefixName} T.CITY_ID <>
    :where_or_nq_cityId_value
    <#else>
    ${prefixName} T.CITY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_cityId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_cityId_value??)>
    ${prefixName} T.CITY_ID like
    :where_or_like_cityId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_code??)>
    <#if
    (where_and_eq_code_value??)>
    ${prefixName} T.CODE = :where_and_eq_code_value
    <#else>
    ${prefixName} T.CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_code??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_code_value??)>
    ${prefixName} T.CODE <> :where_and_nq_code_value
    <#else>
    ${prefixName} T.CODE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_code??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_code_value??)>
    ${prefixName} T.CODE <> :where_and_like_code_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_code??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_code_value??)>
    ${prefixName} T.CODE = :where_or_eq_code_value
    <#else>
    ${prefixName} T.CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_code??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_code_value??)>
    ${prefixName} T.CODE <>
    :where_or_nq_code_value
    <#else>
    ${prefixName} T.CODE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_code??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_code_value??)>
    ${prefixName} T.CODE like
    :where_or_like_code_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_companyName??)>
    <#if
    (where_and_eq_companyName_value??)>
    ${prefixName} T.COMPANY_NAME = :where_and_eq_companyName_value
    <#else>
    ${prefixName} T.COMPANY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_companyName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_companyName_value??)>
    ${prefixName} T.COMPANY_NAME <> :where_and_nq_companyName_value
    <#else>
    ${prefixName} T.COMPANY_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_companyName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_companyName_value??)>
    ${prefixName} T.COMPANY_NAME <> :where_and_like_companyName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_companyName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_companyName_value??)>
    ${prefixName} T.COMPANY_NAME = :where_or_eq_companyName_value
    <#else>
    ${prefixName} T.COMPANY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_companyName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_companyName_value??)>
    ${prefixName} T.COMPANY_NAME <>
    :where_or_nq_companyName_value
    <#else>
    ${prefixName} T.COMPANY_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_companyName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_companyName_value??)>
    ${prefixName} T.COMPANY_NAME like
    :where_or_like_companyName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_countryId??)>
    <#if
    (where_and_eq_countryId_value??)>
    ${prefixName} T.COUNTRY_ID = :where_and_eq_countryId_value
    <#else>
    ${prefixName} T.COUNTRY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_countryId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_countryId_value??)>
    ${prefixName} T.COUNTRY_ID <> :where_and_nq_countryId_value
    <#else>
    ${prefixName} T.COUNTRY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_countryId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_countryId_value??)>
    ${prefixName} T.COUNTRY_ID <> :where_and_like_countryId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_countryId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_countryId_value??)>
    ${prefixName} T.COUNTRY_ID = :where_or_eq_countryId_value
    <#else>
    ${prefixName} T.COUNTRY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_countryId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_countryId_value??)>
    ${prefixName} T.COUNTRY_ID <>
    :where_or_nq_countryId_value
    <#else>
    ${prefixName} T.COUNTRY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_countryId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_countryId_value??)>
    ${prefixName} T.COUNTRY_ID like
    :where_or_like_countryId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdByCode??)>
    <#if
    (where_and_eq_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE = :where_and_eq_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE <> :where_and_nq_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE <> :where_and_like_createdByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE = :where_or_eq_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE <>
    :where_or_nq_createdByCode_value
    <#else>
    ${prefixName} T.CREATED_BY_CODE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdByCode_value??)>
    ${prefixName} T.CREATED_BY_CODE like
    :where_or_like_createdByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdById??)>
    <#if
    (where_and_eq_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID = :where_and_eq_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID <> :where_and_nq_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID <> :where_and_like_createdById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID = :where_or_eq_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID <>
    :where_or_nq_createdById_value
    <#else>
    ${prefixName} T.CREATED_BY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdById_value??)>
    ${prefixName} T.CREATED_BY_ID like
    :where_or_like_createdById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdByName??)>
    <#if
    (where_and_eq_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME = :where_and_eq_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME <> :where_and_nq_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME <> :where_and_like_createdByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME = :where_or_eq_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME <>
    :where_or_nq_createdByName_value
    <#else>
    ${prefixName} T.CREATED_BY_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdByName_value??)>
    ${prefixName} T.CREATED_BY_NAME like
    :where_or_like_createdByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_createdDate??)>
    <#if
    (where_and_eq_createdDate_value??)>
    ${prefixName} T.CREATED_DATE = :where_and_eq_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_createdDate_value??)>
    ${prefixName} T.CREATED_DATE <> :where_and_nq_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_createdDate_value??)>
    ${prefixName} T.CREATED_DATE <> :where_and_like_createdDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_createdDate_value??)>
    ${prefixName} T.CREATED_DATE = :where_or_eq_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_createdDate_value??)>
    ${prefixName} T.CREATED_DATE <>
    :where_or_nq_createdDate_value
    <#else>
    ${prefixName} T.CREATED_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_createdDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_createdDate_value??)>
    ${prefixName} T.CREATED_DATE like
    :where_or_like_createdDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_departmentId??)>
    <#if
    (where_and_eq_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID = :where_and_eq_departmentId_value
    <#else>
    ${prefixName} T.DEPARTMENT_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_departmentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID <> :where_and_nq_departmentId_value
    <#else>
    ${prefixName} T.DEPARTMENT_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_departmentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID <> :where_and_like_departmentId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_departmentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID = :where_or_eq_departmentId_value
    <#else>
    ${prefixName} T.DEPARTMENT_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_departmentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID <>
    :where_or_nq_departmentId_value
    <#else>
    ${prefixName} T.DEPARTMENT_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_departmentId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_departmentId_value??)>
    ${prefixName} T.DEPARTMENT_ID like
    :where_or_like_departmentId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_diplomaId??)>
    <#if
    (where_and_eq_diplomaId_value??)>
    ${prefixName} T.DIPLOMA_ID = :where_and_eq_diplomaId_value
    <#else>
    ${prefixName} T.DIPLOMA_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_diplomaId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_diplomaId_value??)>
    ${prefixName} T.DIPLOMA_ID <> :where_and_nq_diplomaId_value
    <#else>
    ${prefixName} T.DIPLOMA_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_diplomaId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_diplomaId_value??)>
    ${prefixName} T.DIPLOMA_ID <> :where_and_like_diplomaId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_diplomaId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_diplomaId_value??)>
    ${prefixName} T.DIPLOMA_ID = :where_or_eq_diplomaId_value
    <#else>
    ${prefixName} T.DIPLOMA_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_diplomaId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_diplomaId_value??)>
    ${prefixName} T.DIPLOMA_ID <>
    :where_or_nq_diplomaId_value
    <#else>
    ${prefixName} T.DIPLOMA_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_diplomaId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_diplomaId_value??)>
    ${prefixName} T.DIPLOMA_ID like
    :where_or_like_diplomaId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_districtId??)>
    <#if
    (where_and_eq_districtId_value??)>
    ${prefixName} T.DISTRICT_ID = :where_and_eq_districtId_value
    <#else>
    ${prefixName} T.DISTRICT_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_districtId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_districtId_value??)>
    ${prefixName} T.DISTRICT_ID <> :where_and_nq_districtId_value
    <#else>
    ${prefixName} T.DISTRICT_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_districtId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_districtId_value??)>
    ${prefixName} T.DISTRICT_ID <> :where_and_like_districtId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_districtId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_districtId_value??)>
    ${prefixName} T.DISTRICT_ID = :where_or_eq_districtId_value
    <#else>
    ${prefixName} T.DISTRICT_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_districtId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_districtId_value??)>
    ${prefixName} T.DISTRICT_ID <>
    :where_or_nq_districtId_value
    <#else>
    ${prefixName} T.DISTRICT_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_districtId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_districtId_value??)>
    ${prefixName} T.DISTRICT_ID like
    :where_or_like_districtId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_email??)>
    <#if
    (where_and_eq_email_value??)>
    ${prefixName} T.EMAIL = :where_and_eq_email_value
    <#else>
    ${prefixName} T.EMAIL IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_email??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_email_value??)>
    ${prefixName} T.EMAIL <> :where_and_nq_email_value
    <#else>
    ${prefixName} T.EMAIL IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_email??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_email_value??)>
    ${prefixName} T.EMAIL <> :where_and_like_email_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_email??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_email_value??)>
    ${prefixName} T.EMAIL = :where_or_eq_email_value
    <#else>
    ${prefixName} T.EMAIL IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_email??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_email_value??)>
    ${prefixName} T.EMAIL <>
    :where_or_nq_email_value
    <#else>
    ${prefixName} T.EMAIL IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_email??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_email_value??)>
    ${prefixName} T.EMAIL like
    :where_or_like_email_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_endDate??)>
    <#if
    (where_and_eq_endDate_value??)>
    ${prefixName} T.END_DATE = :where_and_eq_endDate_value
    <#else>
    ${prefixName} T.END_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_endDate_value??)>
    ${prefixName} T.END_DATE <> :where_and_nq_endDate_value
    <#else>
    ${prefixName} T.END_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_endDate_value??)>
    ${prefixName} T.END_DATE <> :where_and_like_endDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_endDate_value??)>
    ${prefixName} T.END_DATE = :where_or_eq_endDate_value
    <#else>
    ${prefixName} T.END_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_endDate_value??)>
    ${prefixName} T.END_DATE <>
    :where_or_nq_endDate_value
    <#else>
    ${prefixName} T.END_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_endDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_endDate_value??)>
    ${prefixName} T.END_DATE like
    :where_or_like_endDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_between_sysdate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
${prefixName} SYSDATE BETWEEN NVL(T.BEGIN_DATE,
SYSDATE) AND NVL(T.END_DATE, SYSDATE)
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_flagId??)>
    <#if
    (where_and_eq_flagId_value??)>
    ${prefixName} T.FLAG_ID = :where_and_eq_flagId_value
    <#else>
    ${prefixName} T.FLAG_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_flagId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_flagId_value??)>
    ${prefixName} T.FLAG_ID <> :where_and_nq_flagId_value
    <#else>
    ${prefixName} T.FLAG_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_flagId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_flagId_value??)>
    ${prefixName} T.FLAG_ID <> :where_and_like_flagId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_flagId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_flagId_value??)>
    ${prefixName} T.FLAG_ID = :where_or_eq_flagId_value
    <#else>
    ${prefixName} T.FLAG_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_flagId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_flagId_value??)>
    ${prefixName} T.FLAG_ID <>
    :where_or_nq_flagId_value
    <#else>
    ${prefixName} T.FLAG_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_flagId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_flagId_value??)>
    ${prefixName} T.FLAG_ID like
    :where_or_like_flagId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_folkId??)>
    <#if
    (where_and_eq_folkId_value??)>
    ${prefixName} T.FOLK_ID = :where_and_eq_folkId_value
    <#else>
    ${prefixName} T.FOLK_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_folkId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_folkId_value??)>
    ${prefixName} T.FOLK_ID <> :where_and_nq_folkId_value
    <#else>
    ${prefixName} T.FOLK_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_folkId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_folkId_value??)>
    ${prefixName} T.FOLK_ID <> :where_and_like_folkId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_folkId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_folkId_value??)>
    ${prefixName} T.FOLK_ID = :where_or_eq_folkId_value
    <#else>
    ${prefixName} T.FOLK_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_folkId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_folkId_value??)>
    ${prefixName} T.FOLK_ID <>
    :where_or_nq_folkId_value
    <#else>
    ${prefixName} T.FOLK_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_folkId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_folkId_value??)>
    ${prefixName} T.FOLK_ID like
    :where_or_like_folkId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_id??)>
    <#if
    (where_and_eq_id_value??)>
    ${prefixName} T.ID = :where_and_eq_id_value
    <#else>
    ${prefixName} T.ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_id_value??)>
    ${prefixName} T.ID <> :where_and_nq_id_value
    <#else>
    ${prefixName} T.ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_id_value??)>
    ${prefixName} T.ID <> :where_and_like_id_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_id_value??)>
    ${prefixName} T.ID = :where_or_eq_id_value
    <#else>
    ${prefixName} T.ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_id_value??)>
    ${prefixName} T.ID <>
    :where_or_nq_id_value
    <#else>
    ${prefixName} T.ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_id??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_id_value??)>
    ${prefixName} T.ID like
    :where_or_like_id_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_intro??)>
    <#if
    (where_and_eq_intro_value??)>
    ${prefixName} T.INTRO = :where_and_eq_intro_value
    <#else>
    ${prefixName} T.INTRO IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_intro??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_intro_value??)>
    ${prefixName} T.INTRO <> :where_and_nq_intro_value
    <#else>
    ${prefixName} T.INTRO IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_intro??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_intro_value??)>
    ${prefixName} T.INTRO <> :where_and_like_intro_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_intro??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_intro_value??)>
    ${prefixName} T.INTRO = :where_or_eq_intro_value
    <#else>
    ${prefixName} T.INTRO IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_intro??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_intro_value??)>
    ${prefixName} T.INTRO <>
    :where_or_nq_intro_value
    <#else>
    ${prefixName} T.INTRO IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_intro??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_intro_value??)>
    ${prefixName} T.INTRO like
    :where_or_like_intro_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedByCode??)>
    <#if
    (where_and_eq_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = :where_and_eq_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE <> :where_and_nq_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE <> :where_and_like_lastModifiedByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE = :where_or_eq_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE <>
    :where_or_nq_lastModifiedByCode_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_CODE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedByCode??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedByCode_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_CODE like
    :where_or_like_lastModifiedByCode_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedById??)>
    <#if
    (where_and_eq_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID = :where_and_eq_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID <> :where_and_nq_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID <> :where_and_like_lastModifiedById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID = :where_or_eq_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID <>
    :where_or_nq_lastModifiedById_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedById??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedById_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_ID like
    :where_or_like_lastModifiedById_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedByName??)>
    <#if
    (where_and_eq_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = :where_and_eq_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME <> :where_and_nq_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME <> :where_and_like_lastModifiedByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME = :where_or_eq_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME <>
    :where_or_nq_lastModifiedByName_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_BY_NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedByName??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedByName_value??)>
    ${prefixName} T.LAST_MODIFIED_BY_NAME like
    :where_or_like_lastModifiedByName_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_lastModifiedDate??)>
    <#if
    (where_and_eq_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE = :where_and_eq_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE <> :where_and_nq_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE <> :where_and_like_lastModifiedDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE = :where_or_eq_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE <>
    :where_or_nq_lastModifiedDate_value
    <#else>
    ${prefixName} T.LAST_MODIFIED_DATE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_lastModifiedDate??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_lastModifiedDate_value??)>
    ${prefixName} T.LAST_MODIFIED_DATE like
    :where_or_like_lastModifiedDate_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_locus??)>
    <#if
    (where_and_eq_locus_value??)>
    ${prefixName} T.LOCUS = :where_and_eq_locus_value
    <#else>
    ${prefixName} T.LOCUS IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_locus??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_locus_value??)>
    ${prefixName} T.LOCUS <> :where_and_nq_locus_value
    <#else>
    ${prefixName} T.LOCUS IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_locus??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_locus_value??)>
    ${prefixName} T.LOCUS <> :where_and_like_locus_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_locus??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_locus_value??)>
    ${prefixName} T.LOCUS = :where_or_eq_locus_value
    <#else>
    ${prefixName} T.LOCUS IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_locus??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_locus_value??)>
    ${prefixName} T.LOCUS <>
    :where_or_nq_locus_value
    <#else>
    ${prefixName} T.LOCUS IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_locus??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_locus_value??)>
    ${prefixName} T.LOCUS like
    :where_or_like_locus_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_marryStateId??)>
    <#if
    (where_and_eq_marryStateId_value??)>
    ${prefixName} T.MARRY_STATE_ID = :where_and_eq_marryStateId_value
    <#else>
    ${prefixName} T.MARRY_STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_marryStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_marryStateId_value??)>
    ${prefixName} T.MARRY_STATE_ID <> :where_and_nq_marryStateId_value
    <#else>
    ${prefixName} T.MARRY_STATE_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_marryStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_marryStateId_value??)>
    ${prefixName} T.MARRY_STATE_ID <> :where_and_like_marryStateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_marryStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_marryStateId_value??)>
    ${prefixName} T.MARRY_STATE_ID = :where_or_eq_marryStateId_value
    <#else>
    ${prefixName} T.MARRY_STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_marryStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_marryStateId_value??)>
    ${prefixName} T.MARRY_STATE_ID <>
    :where_or_nq_marryStateId_value
    <#else>
    ${prefixName} T.MARRY_STATE_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_marryStateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_marryStateId_value??)>
    ${prefixName} T.MARRY_STATE_ID like
    :where_or_like_marryStateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_mobilePhone??)>
    <#if
    (where_and_eq_mobilePhone_value??)>
    ${prefixName} T.MOBILE_PHONE = :where_and_eq_mobilePhone_value
    <#else>
    ${prefixName} T.MOBILE_PHONE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_mobilePhone??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_mobilePhone_value??)>
    ${prefixName} T.MOBILE_PHONE <> :where_and_nq_mobilePhone_value
    <#else>
    ${prefixName} T.MOBILE_PHONE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_mobilePhone??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_mobilePhone_value??)>
    ${prefixName} T.MOBILE_PHONE <> :where_and_like_mobilePhone_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_mobilePhone??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_mobilePhone_value??)>
    ${prefixName} T.MOBILE_PHONE = :where_or_eq_mobilePhone_value
    <#else>
    ${prefixName} T.MOBILE_PHONE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_mobilePhone??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_mobilePhone_value??)>
    ${prefixName} T.MOBILE_PHONE <>
    :where_or_nq_mobilePhone_value
    <#else>
    ${prefixName} T.MOBILE_PHONE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_mobilePhone??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_mobilePhone_value??)>
    ${prefixName} T.MOBILE_PHONE like
    :where_or_like_mobilePhone_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_name??)>
    <#if
    (where_and_eq_name_value??)>
    ${prefixName} T.NAME = :where_and_eq_name_value
    <#else>
    ${prefixName} T.NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_name??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_name_value??)>
    ${prefixName} T.NAME <> :where_and_nq_name_value
    <#else>
    ${prefixName} T.NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_name??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_name_value??)>
    ${prefixName} T.NAME <> :where_and_like_name_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_name??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_name_value??)>
    ${prefixName} T.NAME = :where_or_eq_name_value
    <#else>
    ${prefixName} T.NAME IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_name??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_name_value??)>
    ${prefixName} T.NAME <>
    :where_or_nq_name_value
    <#else>
    ${prefixName} T.NAME IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_name??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_name_value??)>
    ${prefixName} T.NAME like
    :where_or_like_name_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_orgId??)>
    <#if
    (where_and_eq_orgId_value??)>
    ${prefixName} T.ORG_ID = :where_and_eq_orgId_value
    <#else>
    ${prefixName} T.ORG_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_orgId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_orgId_value??)>
    ${prefixName} T.ORG_ID <> :where_and_nq_orgId_value
    <#else>
    ${prefixName} T.ORG_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_orgId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_orgId_value??)>
    ${prefixName} T.ORG_ID <> :where_and_like_orgId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_orgId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_orgId_value??)>
    ${prefixName} T.ORG_ID = :where_or_eq_orgId_value
    <#else>
    ${prefixName} T.ORG_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_orgId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_orgId_value??)>
    ${prefixName} T.ORG_ID <>
    :where_or_nq_orgId_value
    <#else>
    ${prefixName} T.ORG_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_orgId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_orgId_value??)>
    ${prefixName} T.ORG_ID like
    :where_or_like_orgId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_password??)>
    <#if
    (where_and_eq_password_value??)>
    ${prefixName} T.PASSWORD = :where_and_eq_password_value
    <#else>
    ${prefixName} T.PASSWORD IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_password??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_password_value??)>
    ${prefixName} T.PASSWORD <> :where_and_nq_password_value
    <#else>
    ${prefixName} T.PASSWORD IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_password??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_password_value??)>
    ${prefixName} T.PASSWORD <> :where_and_like_password_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_password??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_password_value??)>
    ${prefixName} T.PASSWORD = :where_or_eq_password_value
    <#else>
    ${prefixName} T.PASSWORD IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_password??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_password_value??)>
    ${prefixName} T.PASSWORD <>
    :where_or_nq_password_value
    <#else>
    ${prefixName} T.PASSWORD IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_password??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_password_value??)>
    ${prefixName} T.PASSWORD like
    :where_or_like_password_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_professionId??)>
    <#if
    (where_and_eq_professionId_value??)>
    ${prefixName} T.PROFESSION_ID = :where_and_eq_professionId_value
    <#else>
    ${prefixName} T.PROFESSION_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_professionId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_professionId_value??)>
    ${prefixName} T.PROFESSION_ID <> :where_and_nq_professionId_value
    <#else>
    ${prefixName} T.PROFESSION_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_professionId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_professionId_value??)>
    ${prefixName} T.PROFESSION_ID <> :where_and_like_professionId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_professionId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_professionId_value??)>
    ${prefixName} T.PROFESSION_ID = :where_or_eq_professionId_value
    <#else>
    ${prefixName} T.PROFESSION_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_professionId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_professionId_value??)>
    ${prefixName} T.PROFESSION_ID <>
    :where_or_nq_professionId_value
    <#else>
    ${prefixName} T.PROFESSION_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_professionId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_professionId_value??)>
    ${prefixName} T.PROFESSION_ID like
    :where_or_like_professionId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_provinceId??)>
    <#if
    (where_and_eq_provinceId_value??)>
    ${prefixName} T.PROVINCE_ID = :where_and_eq_provinceId_value
    <#else>
    ${prefixName} T.PROVINCE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_provinceId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_provinceId_value??)>
    ${prefixName} T.PROVINCE_ID <> :where_and_nq_provinceId_value
    <#else>
    ${prefixName} T.PROVINCE_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_provinceId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_provinceId_value??)>
    ${prefixName} T.PROVINCE_ID <> :where_and_like_provinceId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_provinceId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_provinceId_value??)>
    ${prefixName} T.PROVINCE_ID = :where_or_eq_provinceId_value
    <#else>
    ${prefixName} T.PROVINCE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_provinceId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_provinceId_value??)>
    ${prefixName} T.PROVINCE_ID <>
    :where_or_nq_provinceId_value
    <#else>
    ${prefixName} T.PROVINCE_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_provinceId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_provinceId_value??)>
    ${prefixName} T.PROVINCE_ID like
    :where_or_like_provinceId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_sexId??)>
    <#if
    (where_and_eq_sexId_value??)>
    ${prefixName} T.SEX_ID = :where_and_eq_sexId_value
    <#else>
    ${prefixName} T.SEX_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_sexId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_sexId_value??)>
    ${prefixName} T.SEX_ID <> :where_and_nq_sexId_value
    <#else>
    ${prefixName} T.SEX_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_sexId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_sexId_value??)>
    ${prefixName} T.SEX_ID <> :where_and_like_sexId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_sexId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_sexId_value??)>
    ${prefixName} T.SEX_ID = :where_or_eq_sexId_value
    <#else>
    ${prefixName} T.SEX_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_sexId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_sexId_value??)>
    ${prefixName} T.SEX_ID <>
    :where_or_nq_sexId_value
    <#else>
    ${prefixName} T.SEX_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_sexId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_sexId_value??)>
    ${prefixName} T.SEX_ID like
    :where_or_like_sexId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_stateId??)>
    <#if
    (where_and_eq_stateId_value??)>
    ${prefixName} T.STATE_ID = :where_and_eq_stateId_value
    <#else>
    ${prefixName} T.STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_stateId_value??)>
    ${prefixName} T.STATE_ID <> :where_and_nq_stateId_value
    <#else>
    ${prefixName} T.STATE_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_stateId_value??)>
    ${prefixName} T.STATE_ID <> :where_and_like_stateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_stateId_value??)>
    ${prefixName} T.STATE_ID = :where_or_eq_stateId_value
    <#else>
    ${prefixName} T.STATE_ID IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_stateId_value??)>
    ${prefixName} T.STATE_ID <>
    :where_or_nq_stateId_value
    <#else>
    ${prefixName} T.STATE_ID IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_stateId??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_stateId_value??)>
    ${prefixName} T.STATE_ID like
    :where_or_like_stateId_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_telephone??)>
    <#if
    (where_and_eq_telephone_value??)>
    ${prefixName} T.TELEPHONE = :where_and_eq_telephone_value
    <#else>
    ${prefixName} T.TELEPHONE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_telephone??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_telephone_value??)>
    ${prefixName} T.TELEPHONE <> :where_and_nq_telephone_value
    <#else>
    ${prefixName} T.TELEPHONE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_telephone??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_telephone_value??)>
    ${prefixName} T.TELEPHONE <> :where_and_like_telephone_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_telephone??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_telephone_value??)>
    ${prefixName} T.TELEPHONE = :where_or_eq_telephone_value
    <#else>
    ${prefixName} T.TELEPHONE IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_telephone??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_telephone_value??)>
    ${prefixName} T.TELEPHONE <>
    :where_or_nq_telephone_value
    <#else>
    ${prefixName} T.TELEPHONE IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_telephone??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_telephone_value??)>
    ${prefixName} T.TELEPHONE like
    :where_or_like_telephone_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(prefixName!) != 'WHERE'>
    <#assign prefixName='AND'>
</#if>
<#if
(where_and_eq_version??)>
    <#if
    (where_and_eq_version_value??)>
    ${prefixName} T.VERSION = :where_and_eq_version_value
    <#else>
    ${prefixName} T.VERSION IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_nq_version??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_nq_version_value??)>
    ${prefixName} T.VERSION <> :where_and_nq_version_value
    <#else>
    ${prefixName} T.VERSION IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_and_like_version??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='AND'>
    </#if>
    <#if
    (where_and_like_version_value??)>
    ${prefixName} T.VERSION <> :where_and_like_version_value
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_eq_version??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_eq_version_value??)>
    ${prefixName} T.VERSION = :where_or_eq_version_value
    <#else>
    ${prefixName} T.VERSION IS NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_nq_version??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_nq_version_value??)>
    ${prefixName} T.VERSION <>
    :where_or_nq_version_value
    <#else>
    ${prefixName} T.VERSION IS NOT NULL
    </#if>
    <#assign prefixName=''>
</#if>
<#if
(where_or_like_version??)>
    <#if
    (prefixName!) != 'WHERE'>
        <#assign prefixName='OR'>
    </#if>
    <#if
    (where_or_like_version_value??)>
    ${prefixName} T.VERSION like
    :where_or_like_version_value
    </#if>
    <#assign prefixName=''>
</#if>
