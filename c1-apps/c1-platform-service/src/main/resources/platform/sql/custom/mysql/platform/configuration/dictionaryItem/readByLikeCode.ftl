<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT count(1)
FROM C1_DICTIONARY_ITEM T
LEFT JOIN C1_DICTIONARY t_DICTIONARY
ON (NOW() BETWEEN IFNULL(t_DICTIONARY.begin_date, NOW()) AND IFNULL(t_DICTIONARY.end_date, NOW())
AND t_DICTIONARY.id = t.dictionary_id)
LEFT JOIN C1_DICTIONARY_TYPE T_DICTIONARY_TYPE
ON (NOW() BETWEEN IFNULL(T_DICTIONARY_TYPE.begin_date, NOW()) AND IFNULL(T_DICTIONARY_TYPE.end_date, NOW())
AND T_DICTIONARY_TYPE.ID = t_DICTIONARY.type_id)
WHERE NOW() BETWEEN IFNULL(t.begin_date, NOW()) AND IFNULL(t.end_date, NOW())
<#if StringUtils.isNotBlank(typeCode)>
and (instr(','||T_DICTIONARY_TYPE.code||',', ','||:typeCode||',') > 0 or instr(','||T_DICTIONARY_TYPE.name||',', ','||:typeCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(dictionaryCode)>
and (instr(','||T_DICTIONARY.code||',', ','||:dictionaryCode||',') > 0 or instr(','||T_DICTIONARY.name||',', ','||:dictionaryCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(code)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>