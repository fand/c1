SELECT t.entity_id
FROM C1_ENTITY_LOG t
WHERE t.id = :id
AND sysdate BETWEEN NVL(t.begin_date, sysdate) AND NVL(t.end_date, sysdate)