<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT COUNT(1)
FROM C1_ROLE_PERMISSION T
LEFT JOIN C1_ROLE T_ROLE
ON (T_ROLE.ID = T.ROLE_ID)
LEFT JOIN C1_PERMISSION T_PERMISSION
ON (T_PERMISSION.ID = T.PERMISSION_ID)
LEFT JOIN C1_DICTIONARY T_DICTIONARY
ON (T_DICTIONARY.ID = T_PERMISSION.TYPE_ID)
WHERE 1 = 1
<#if StringUtils.isNotBlank(roleCode)>
AND (instr(','||T_ROLE.code||',', ','||:roleCode||',') > 0 OR instr(','||T_ROLE.name||',', ','||:roleCode||',')   > 0)
</#if>
<#if StringUtils.isNotBlank(permissionCode)>
and (instr(','||T_PERMISSION.CODE||',', ','||:permissionCode||',') > 0 or instr(','||T_PERMISSION.NAME||',', ','||:permissionCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(permissionTypeCode)>
and (instr(','||T_DICTIONARY.code||',', ','||:permissionTypeCode||',') > 0 or instr(','||T_DICTIONARY.name||',', ','||:permissionTypeCode||',') > 0)
</#if>