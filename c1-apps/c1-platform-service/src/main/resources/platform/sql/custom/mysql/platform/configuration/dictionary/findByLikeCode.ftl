<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT T.* ,
T_DICTIONARY_TYPE.code AS type_Code,
T_DICTIONARY_TYPE.name AS type_Name
FROM C1_DICTIONARY T
LEFT JOIN C1_DICTIONARY_TYPE T_DICTIONARY_TYPE
ON (T_DICTIONARY_TYPE.ID = t.TYPE_id)
WHERE (NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW()))
<#if StringUtils.isNotBlank(typeCode)>
and (instr(','||T_DICTIONARY_TYPE.code||',', ','||:typeCode||',') > 0 or instr(','||T_DICTIONARY_TYPE.name||',', ','||:typeCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(typeId)>
and T.TYPE_ID = :typeId
</#if>
<#if StringUtils.isNotBlank(code)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>
order by T_DICTIONARY_TYPE.ORDER_NUM, T_DICTIONARY_TYPE.code, t.ORDER_NUM, t.CODE
LIMIT <#if (begin_row_num??)>:begin_row_num<#else>0</#if>, <#if (size??)>:size<#else>20</#if>