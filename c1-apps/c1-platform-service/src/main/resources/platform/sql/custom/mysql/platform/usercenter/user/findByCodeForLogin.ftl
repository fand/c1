SELECT T.*,
(
CASE
WHEN (NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW()))
THEN '1'
ELSE '2'
END) AS valid_code
FROM c1_user t
WHERE T.PASSWORD in (:password, :newPassword) AND t.code = :code
