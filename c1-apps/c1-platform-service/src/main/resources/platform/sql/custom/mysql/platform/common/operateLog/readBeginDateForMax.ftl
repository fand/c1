<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT sysdate FROM dual
UNION ALL
SELECT MAX(t.BEGIN_DATE)
FROM C1_OPERATE_LOG T
WHERE (NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW()))
AND t.code = :code
AND EXISTS
(SELECT 1
FROM c1_dictionary e
WHERE e.id = t.type_id
AND e.code = :typeCode
AND EXISTS
(SELECT 1
FROM c1_dictionary_type e_e
WHERE e_e.id = e.type_id
AND e_e.code = :typeTypeCode
)
)
<#if StringUtils.isNotBlank(businessCode)>
AND T.BUSINESS_CODE = :businessCode
</#if>