<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT count(1)
FROM C1_ROLE T
WHERE (Sysdate BETWEEN t.begin_date AND t.end_date )
<#if StringUtils.isNotBlank(orgId)>
AND (T.ORG_ID = :orgId
    <#if StringUtils.isNotBlank(notCodes)>
    or (T.ORG_ID IS NULL AND instr(','||:notCodes||',', ','||T.code||',') < 1)
    </#if>
)
</#if>
<#if StringUtils.isNotBlank(code)>
AND (instr(','||T.code||',', ','||:code||',') > 0
OR instr(','||T.name||',', ','||:code||',')   > 0)
</#if>