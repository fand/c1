SELECT
<#switch (object_id!)>
    <#case "beginDate">
    T.BEGIN_DATE AS beginDate
        <#break>
    <#case "code">
    T.CODE AS code
        <#break>
    <#case "createdByCode">
    T.CREATED_BY_CODE AS createdByCode
        <#break>
    <#case "createdById">
    T.CREATED_BY_ID AS createdById
        <#break>
    <#case "createdByName">
    T.CREATED_BY_NAME AS createdByName
        <#break>
    <#case "createdDate">
    T.CREATED_DATE AS createdDate
        <#break>
    <#case "endDate">
    T.END_DATE AS endDate
        <#break>
    <#case "id">
    T.ID AS id
        <#break>
    <#case "lastModifiedByCode">
    T.LAST_MODIFIED_BY_CODE AS lastModifiedByCode
        <#break>
    <#case "lastModifiedById">
    T.LAST_MODIFIED_BY_ID AS lastModifiedById
        <#break>
    <#case "lastModifiedByName">
    T.LAST_MODIFIED_BY_NAME AS lastModifiedByName
        <#break>
    <#case "lastModifiedDate">
    T.LAST_MODIFIED_DATE AS lastModifiedDate
        <#break>
    <#case "name">
    T.NAME AS name
        <#break>
    <#case "orderNum">
    T.ORDER_NUM AS orderNum
        <#break>
    <#case "parentId">
    T.PARENT_ID AS parentId
        <#break>
    <#case "remark">
    T.REMARK AS remark
        <#break>
    <#case "stateId">
    T.STATE_ID AS stateId
        <#break>
    <#case "typeId">
    T.TYPE_ID AS typeId
        <#break>
    <#case "value">
    T.VALUE AS value
        <#break>
    <#case "version">
    T.VERSION AS version
        <#break>
    <#default>
    COUNT(1) AS COUNT_
</#switch>
FROM C1_DICTIONARY T WHERE T.CODE = :code
AND (NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW())) AND EXISTS
(SELECT 1 FROM C1_DICTIONARY_TYPE e WHERE e.code = :typeCode AND (NOW() BETWEEN IFNULL(e.begin_date, NOW()) AND IFNULL(e.end_date, NOW())) AND e.id = t.type_id
)