UPDATE `${updateTableName}`
SET `${updateFieldName}` = :UPDATE_FIELD
WHERE `${updateForeignKeyName}` = :UPDATE_FOREIGN_KEY and (`${updateFieldName}` IS NULL OR `${updateFieldName}` <> :UPDATE_FIELD)