SELECT
T.*
FROM
C1_SITE T
WHERE
EXISTS
(
SELECT
1
FROM
C1_site_url e
WHERE
e.site_id = t.id
AND t.code  = :siteUrlCode
)