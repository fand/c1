INSERT
INTO C1_ENTITY_CODE
(
ID,
CODE,
TYPE_CODE
)
(select
:id,
:code,
:typeCode
from dual where not exists (select 1 from C1_ENTITY_CODE e where e.type_code = :typeCode and e.code = :code)
)