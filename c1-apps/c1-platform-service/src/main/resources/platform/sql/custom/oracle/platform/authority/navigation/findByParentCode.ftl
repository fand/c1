SELECT T.* ,
(SELECT COUNT(1)
FROM C1_NAVIGATION s
WHERE sysdate BETWEEN s.BEGIN_DATE AND s.END_DATE
AND s.PARENT_ID = t.id
) AS childTotal
FROM C1_NAVIGATION T
JOIN c1_site t_site
ON (t_site.id = t.SITE_ID)
JOIN c1_dictionary t_dictionary
ON ((sysdate BETWEEN t_dictionary.BEGIN_DATE AND t_dictionary.END_DATE)
AND t_dictionary.id = t.type_id )
WHERE (sysdate BETWEEN t.BEGIN_DATE AND t.END_DATE)
AND (sysdate BETWEEN t_site.BEGIN_DATE AND t_site.END_DATE)
AND t_site.code = :siteCode
AND t_dictionary.code = :typeCode
AND <#if (parentCode??)>EXISTS (SELECT 1 FROM C1_NAVIGATION e WHERE e.code = :parentCode AND e.id = t.parent_id)<#else>t.parent_id IS NULL</#if>
ORDER BY t.ORDER_NUM