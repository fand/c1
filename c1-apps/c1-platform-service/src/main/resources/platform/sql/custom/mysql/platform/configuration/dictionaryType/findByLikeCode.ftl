<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT T.ID,
T.PARENT_ID,
T.CODE,
T.NAME,
T.VALUE,
T.ORDER_NUM,
T.BEGIN_DATE,
T.END_DATE ,
t_dictionary_type.code AS parentCode,
t_dictionary_type.name AS parentName
FROM C1_DICTIONARY_TYPE T
LEFT JOIN C1_DICTIONARY_TYPE T_DICTIONARY_TYPE
ON (NOW() BETWEEN IFNULL(t_dictionary_type.begin_date, NOW()) AND IFNULL(t_dictionary_type.end_date, NOW())
AND T_DICTIONARY_TYPE.ID = t.parent_id)
WHERE NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW())
<#if StringUtils.isNotBlank(parentCode)>
and (instr(','||T_DICTIONARY_TYPE.code||',', ','||:parentCode||',') > 0 or instr(','||T_DICTIONARY_TYPE.name||',', ','||:parentCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(parentId)>
and T.PARENT_ID = :parentId
</#if>
<#if StringUtils.isNotBlank(code)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>
order by T_DICTIONARY_TYPE.ORDER_NUM, T_DICTIONARY_TYPE.code, t.ORDER_NUM, t.CODE
LIMIT <#if (begin_row_num??)>:begin_row_num<#else>0</#if>, <#if (size??)>:size<#else>20</#if>


