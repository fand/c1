SELECT T.code
FROM c1_permission t
WHERE T.SITE_ID = :siteId
AND EXISTS
(SELECT 1 FROM c1_dictionary e WHERE e.id = t.type_id AND e.code = :typeCode
)
ORDER BY t.order_num,
LENGTH(t.code) DESC