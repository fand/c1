<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT COUNT(1)
FROM C1_US_GROUP_USER T
LEFT JOIN C1_USER_GROUP T_USER_GROUP
ON (T_USER_GROUP.id = t.user_group_id)
LEFT JOIN C1_USER T_USER
ON (T_USER.id = t.user_id)
WHERE 1 = 1
<#if StringUtils.isNotBlank(userGroupCode)>
AND (instr(','||T_USER_GROUP.code||',', ','||:userGroupCode||',') > 0 OR instr(','||T_USER_GROUP.name||',', ','||:userGroupCode||',')   > 0)
</#if>
<#if StringUtils.isNotBlank(userCode)>
and (instr(','||T_USER.code||',', ','||:userCode||',') > 0 or instr(','||T_USER.name||',', ','||:userCode||',') > 0)
</#if>