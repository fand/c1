SELECT t.*
FROM C1_SITE T
left join C1_DICTIONARY project_DICTIONARY on (project_DICTIONARY.id = t.project_ID)
where ( NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW()) )
and project_DICTIONARY.code = :projectCode
order by t.code
