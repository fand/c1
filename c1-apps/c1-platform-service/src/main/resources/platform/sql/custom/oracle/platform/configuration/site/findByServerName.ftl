SELECT t.*
FROM c1_site t
WHERE EXISTS
(SELECT 1 FROM c1_site_url e WHERE e.site_id = t.id AND e.code = :serverName
)
UNION ALL
SELECT t.*
FROM c1_site t
WHERE t.code = :code
AND EXISTS
(SELECT 1
FROM c1_dictionary e
WHERE e.id = t.project_id and e.code = :projectCode
)
