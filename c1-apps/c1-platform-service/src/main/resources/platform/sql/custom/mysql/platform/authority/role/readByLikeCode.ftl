<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT count(1)
FROM C1_ROLE T
WHERE (NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW()) )
<#if StringUtils.isNotBlank(orgId)>
AND T.ORG_ID = :orgId
</#if>
<#if StringUtils.isNotBlank(code)>
AND (instr(','||T.code||',', ','||:code||',') > 0
OR instr(','||T.name||',', ','||:code||',')   > 0)
</#if>