<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
WITH W_US_GR_BUSINESS_AREA AS
(SELECT T.*,
ROW_NUMBER() OVER(ORDER BY T_USER_GROUP.ORDER_NUM, T_USER_GROUP.CODE, T_DICTIONARY_TYPE.CODE, T_DICTIONARY.CODE) RN ,
T_USER_GROUP.CODE      AS USER_GROUP_CODE,
T_USER_GROUP.NAME      AS USER_GROUP_NAME,
T_DICTIONARY.CODE      AS BUSINESS_AREA_CODE,
T_DICTIONARY.NAME      AS BUSINESS_AREA_NAME,
T_DICTIONARY.TYPE_ID   AS BU_AREA_TYPE_ID,
T_DICTIONARY_TYPE.CODE AS BU_AREA_TYPE_CODE,
T_DICTIONARY_TYPE.NAME AS BU_AREA_TYPE_NAME
FROM C1_US_GR_BUSINESS_AREA T
LEFT JOIN C1_USER_GROUP T_USER_GROUP
ON (T_USER_GROUP.ID = T.USER_GROUP_ID)
LEFT JOIN C1_DICTIONARY T_DICTIONARY
ON (T_DICTIONARY.ID = T.BUSINESS_AREA_ID)
LEFT JOIN C1_DICTIONARY_TYPE T_DICTIONARY_TYPE
ON (T_DICTIONARY_TYPE.ID = T_DICTIONARY.TYPE_ID)
WHERE 1 = 1
<#if StringUtils.isNotBlank(userGroupCode)>
AND (instr(','||T_USER_GROUP.code||',', ','||:userGroupCode||',') > 0 OR instr(','||T_USER_GROUP.name||',', ','||:userGroupCode||',')   > 0)
</#if>
<#if StringUtils.isNotBlank(buAreaTypeCode)>
and (instr(','||T_DICTIONARY_TYPE.code||',', ','||:buAreaTypeCode||',') > 0 or instr(','||T_DICTIONARY_TYPE.name||',', ','||:buAreaTypeCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(businessAreaCode)>
and (instr(','||T_DICTIONARY.code||',', ','||:businessAreaCode||',') > 0 or instr(','||T_DICTIONARY.name||',', ','||:businessAreaCode||',') > 0)
</#if>
)
SELECT T.*
FROM W_US_GR_BUSINESS_AREA T
WHERE t.rn BETWEEN <#if (begin_row_num??)>:begin_row_num<#else>1</#if> AND <#if (end_row_num??)>:end_row_num<#else>20</#if>