SELECT T.* ,
t_dictionary_type.code AS parent_Code,
t_dictionary_type.name AS parent_Name
FROM C1_DICTIONARY_TYPE T
LEFT JOIN C1_DICTIONARY_TYPE T_DICTIONARY_TYPE
ON (NOW() BETWEEN IFNULL(t_dictionary_type.begin_date, NOW()) AND IFNULL(t_dictionary_type.end_date, NOW())
AND T_DICTIONARY_TYPE.ID = t.parent_id)
WHERE t.Id = :id