<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT count(1)
FROM C1_DICTIONARY T
LEFT JOIN C1_DICTIONARY_TYPE T_DICTIONARY_TYPE
ON ((NOW() BETWEEN IFNULL(t_dictionary_type.begin_date, NOW()) AND IFNULL(t_dictionary_type.end_date, NOW())) AND T_DICTIONARY_TYPE.ID = t.TYPE_id)
WHERE (NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW()))
<#if StringUtils.isNotBlank(typeCode)>
and (instr(','||T_DICTIONARY_TYPE.code||',', ','||:typeCode||',') > 0 or instr(','||T_DICTIONARY_TYPE.name||',', ','||:typeCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(typeId)>
and T.TYPE_ID = :typeId
</#if>
<#if StringUtils.isNotBlank(code)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>