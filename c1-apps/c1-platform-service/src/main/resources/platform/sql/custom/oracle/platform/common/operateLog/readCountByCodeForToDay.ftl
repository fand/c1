<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT COUNT(1)
FROM C1_OPERATE_LOG T
WHERE TO_CHAR(sysdate, 'yyyymmdd') = TO_CHAR(T.BEGIN_DATE, 'yyyymmdd')
AND t.code = :code
AND t.type_id = :typeId