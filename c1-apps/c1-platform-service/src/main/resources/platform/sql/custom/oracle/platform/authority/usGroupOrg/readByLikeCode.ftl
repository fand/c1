<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT COUNT(1)
FROM C1_US_GROUP_ORG T
LEFT JOIN C1_USER_GROUP T_USER_GROUP
ON (T_USER_GROUP.id = t.user_group_id)
LEFT JOIN C1_ORG T_ORG
ON (T_ORG.id = t.ORG_ID)
WHERE 1 = 1
<#if StringUtils.isNotBlank(userGroupCode)>
AND (instr(','||T_USER_GROUP.code||',', ','||:userGroupCode||',') > 0 OR instr(','||T_USER_GROUP.name||',', ','||:userGroupCode||',')   > 0)
</#if>
<#if StringUtils.isNotBlank(orgCode)>
and (instr(','||T_ORG.code||',', ','||:orgCode||',') > 0 or instr(','||T_ORG.name||',', ','||:orgCode||',') > 0)
</#if>