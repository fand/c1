<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT count(1)
FROM C1_USER_GROUP T
LEFT JOIN C1_USER_GROUP T_USER_GROUP ON ((sysdate BETWEEN T_USER_GROUP.BEGIN_DATE AND T_USER_GROUP.END_DATE)
AND T_USER_GROUP.id = t.parent_id)
WHERE (Sysdate BETWEEN t.begin_date AND t.end_date )
<#if StringUtils.isNotBlank(typeId)>
AND T.type_id                              = :typeId
</#if>
<#if StringUtils.isNotBlank(parentCode)>
AND (instr(','||T_USER_GROUP.code||',', ','||:parentCode||',') > 0 OR instr(','||T_USER_GROUP.name||',', ','||:parentCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(code)>
AND (instr(','||T.code||',', ','||:code||',') > 0 OR instr(','||T.name||',', ','||:code||',') > 0)
</#if>