<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT t.*,
project_DICTIONARY.code as projectCode ,
project_DICTIONARY.name as projectName,
style_DICTIONARY.code as styleCode ,
style_DICTIONARY.name as styleName
FROM C1_SITE T
left join C1_DICTIONARY project_DICTIONARY on (project_DICTIONARY.id = t.project_ID)
left join C1_DICTIONARY style_DICTIONARY on (style_DICTIONARY.id = t.style_ID)
where (NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW()))
<#if StringUtils.isNotBlank(projectCode)>
and (instr(','||project_DICTIONARY.code||',', ','||:projectCode||',') > 0 or instr(','||project_DICTIONARY.name||',', ','||:projectCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(code)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>
order by project_DICTIONARY.code, t.code
LIMIT <#if (begin_row_num??)>:begin_row_num<#else>0</#if>, <#if (size??)>:size<#else>20</#if>