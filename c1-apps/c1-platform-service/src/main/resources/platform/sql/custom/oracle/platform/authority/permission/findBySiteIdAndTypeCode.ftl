SELECT t.*,
t_dictionary.code AS type_code,
t_dictionary.name AS type_name
FROM c1_permission t
LEFT JOIN c1_dictionary t_dictionary
ON (t_dictionary.id = t.type_id)
WHERE T.SITE_ID = :siteId
AND EXISTS
(SELECT 1 FROM c1_dictionary e WHERE e.id = t.type_id AND e.code = :typeCode
)
ORDER BY t.order_num,
LENGTH(t.code) DESC