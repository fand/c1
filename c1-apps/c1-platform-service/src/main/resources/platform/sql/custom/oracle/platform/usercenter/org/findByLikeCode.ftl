<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
WITH W_ORG AS
(SELECT T.*,
row_number() over(order by T_ORG.code, T.CODE) rn ,
T_ORG.code AS parentCode ,
T_ORG.name AS parentName
FROM C1_ORG T
LEFT JOIN C1_ORG T_ORG
ON ((sysdate BETWEEN T_ORG.BEGIN_DATE AND T_ORG.END_DATE)
AND T_ORG.id = t.parent_id)
WHERE (Sysdate BETWEEN t.begin_date AND t.end_date )
<#if StringUtils.isNotBlank(code)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>
<#if StringUtils.isNotBlank(parentCode)>
AND (instr(','||T_ORG.code||',', ','||:parentCode||',') > 0 OR instr(','||T_ORG.name||',', ','||:parentCode||',')   > 0)
</#if>
)
SELECT T.*,
T.parentCode ,
T.parentName
FROM W_ORG T
WHERE t.rn BETWEEN <#if (begin_row_num??)>:begin_row_num<#else>1</#if> AND <#if (end_row_num??)>:end_row_num<#else>20</#if>