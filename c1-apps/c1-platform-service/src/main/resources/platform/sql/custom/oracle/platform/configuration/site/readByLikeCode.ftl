<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT count(1)
FROM C1_SITE T
left join C1_DICTIONARY project_DICTIONARY on (project_DICTIONARY.id = t.project_ID)
where ( Sysdate BETWEEN t.begin_date AND t.end_date )
<#if StringUtils.isNotBlank(projectCode)>
and (instr(','||project_DICTIONARY.code||',', ','||:projectCode||',') > 0 or instr(','||project_DICTIONARY.name||',', ','||:projectCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(code)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>