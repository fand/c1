SELECT t.*,
t_dictionary.code AS type_code,
t_dictionary.name AS type_name,
(SELECT WMSYS.WM_CONCAT(s.role_id)
FROM c1_role_permission s
WHERE s.permission_id = t.id
) AS role_ids
FROM c1_permission t
LEFT JOIN c1_dictionary t_dictionary
ON (t_dictionary.id = t.type_id)
WHERE t.site_id     = :siteId
ORDER BY T.site_id,
T.type_id,
LENGTH(t.code) DESC,
t.order_num