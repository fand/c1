SELECT T.* ,
T_USER_GROUP.code AS parent_Code ,
T_USER_GROUP.name AS parent_Name
FROM C1_USER_GROUP T
LEFT JOIN C1_USER_GROUP T_USER_GROUP
ON (T_USER_GROUP.id = T.parent_id)
WHERE T.id          = :id