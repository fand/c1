SELECT T.* ,
T_NAVIGATION.code              AS parentCode ,
T_NAVIGATION.name              AS parentName
FROM C1_NAVIGATION T
LEFT JOIN C1_NAVIGATION T_NAVIGATION
ON (T_NAVIGATION.id = T.parent_id)
WHERE T.id          = :id