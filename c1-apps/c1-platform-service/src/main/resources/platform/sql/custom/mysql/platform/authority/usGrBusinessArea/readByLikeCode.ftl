<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT COUNT(1)
FROM C1_US_GR_BUSINESS_AREA T
LEFT JOIN C1_USER_GROUP T_USER_GROUP
ON (T_USER_GROUP.ID = T.USER_GROUP_ID)
LEFT JOIN C1_DICTIONARY T_DICTIONARY
ON (T_DICTIONARY.ID = T.BUSINESS_AREA_ID)
LEFT JOIN C1_DICTIONARY_TYPE T_DICTIONARY_TYPE
ON (T_DICTIONARY_TYPE.ID = T_DICTIONARY.TYPE_ID)
WHERE 1 = 1
<#if StringUtils.isNotBlank(userGroupCode)>
AND (instr(','||T_USER_GROUP.code||',', ','||:userGroupCode||',') > 0 OR instr(','||T_USER_GROUP.name||',', ','||:userGroupCode||',')   > 0)
</#if>
<#if StringUtils.isNotBlank(buAreaTypeCode)>
and (instr(','||T_DICTIONARY_TYPE.code||',', ','||:buAreaTypeCode||',') > 0 or instr(','||T_DICTIONARY_TYPE.name||',', ','||:buAreaTypeCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(businessAreaCode)>
and (instr(','||T_DICTIONARY.code||',', ','||:businessAreaCode||',') > 0 or instr(','||T_DICTIONARY.name||',', ','||:businessAreaCode||',') > 0)
</#if>