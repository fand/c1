SELECT t.*,
t_dictionary.code AS type_code,
t_dictionary.name AS type_name
FROM c1_permission t
LEFT JOIN c1_dictionary t_dictionary
ON (t_dictionary.id = t.type_id)
WHERE T.SITE_ID = :siteId
AND T.type_id = :typeId
ORDER BY T.site_id, T.type_id, LENGTH(t.code) DESC, t.order_num