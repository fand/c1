<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT COUNT(1)
FROM C1_DICTIONARY_TYPE T
LEFT JOIN C1_DICTIONARY_TYPE T_DICTIONARY_TYPE
ON (NOW() BETWEEN IFNULL(t_dictionary_type.begin_date, NOW()) AND IFNULL(t_dictionary_type.end_date, NOW())
AND T_DICTIONARY_TYPE.ID = t.parent_id)
WHERE NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW())
<#if StringUtils.isNotBlank(parentCode)>
and (instr(','||T_DICTIONARY_TYPE.code||',', ','||:parentCode||',') > 0 or instr(','||T_DICTIONARY_TYPE.name||',', ','||:parentCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(parentId)>
and T.PARENT_ID = :parentId
</#if>
<#if StringUtils.isNotBlank(code)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>