SELECT `D`.`${dataPrimaryKeyName}` AS "UPDATE_FOREIGN_KEY", `D`.`${dataFieldName}` AS "UPDATE_FIELD"
FROM `${dataTableName}` AS `D`
WHERE EXISTS (SELECT 1 FROM `${updateTableName}` AS `U` WHERE (`U`.`${updateFieldName}` IS NULL OR `U`.`${updateFieldName}` <> `D`.`${dataFieldName}`) AND `U`.`${updateForeignKeyName}` = `D`.`${dataPrimaryKeyName}` AND `U`.`${updateForeignKeyName}` IS NOT NULL)