SELECT COUNT(1)
FROM c1_permission t
WHERE T.SITE_ID = :siteId
AND EXISTS
(SELECT 1
FROM c1_dictionary e
WHERE e.id = t.type_id
AND e.code = 'authorityUrls'
)
AND EXISTS
(SELECT 1
FROM c1_role_permission e
WHERE e.permission_id         = t.id
AND instr(','||:roleIds||',', ','||e.role_id||',')>0
)
