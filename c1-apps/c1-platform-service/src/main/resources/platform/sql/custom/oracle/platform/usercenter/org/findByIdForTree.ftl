SELECT T.*
FROM C1_ORG T
START WITH <#if isSuperRole>T.PARENT_ID IS NULL<#else>T.id = :id</#if>
CONNECT BY PRIOR T.ID = T.PARENT_ID
order by t.parent_id desc,t.code