<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT count(1)
FROM C1_ORG T
LEFT JOIN C1_ORG T_ORG
ON ((sysdate BETWEEN T_ORG.BEGIN_DATE AND T_ORG.END_DATE)
AND T_ORG.id = t.parent_id)
WHERE (Sysdate BETWEEN t.begin_date AND t.end_date )
<#if StringUtils.isNotBlank(code)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>
<#if StringUtils.isNotBlank(parentCode)>
AND (instr(','||T_ORG.code||',', ','||:parentCode||',') > 0 OR instr(','||T_ORG.name||',', ','||:parentCode||',')   > 0)
</#if>