SELECT T.* FROM C1_DICTIONARY_TYPE T WHERE
<#if parentId??>
t.parent_id = :parentId
<#else>
EXISTS
(SELECT 1
FROM C1_DICTIONARY_TYPE e
WHERE e.code = :parentCode
AND e.id = t.parent_id
)
</#if>
ORDER BY
t.order_num,
t.code
