SELECT T.* FROM C1_DICTIONARY T WHERE t.code = :code AND (NOW() BETWEEN IFNULL(t.begin_date, NOW()) AND IFNULL(t.end_date, NOW())) AND EXISTS
( SELECT 1 FROM c1_dictionary_type e WHERE e.code = :typeCode AND (NOW() BETWEEN IFNULL(e.begin_date, NOW()) AND IFNULL(e.end_date, NOW())) and e.id = t.type_id
)