SELECT count(1)
FROM C1_SITE_URL T
left join C1_SITE t_site on (t_site.id = t.site_id)
where 1 = 1
<#if (siteCode??)>
and (instr(','||t_site.code||',', ','||:siteCode||',') > 0 or instr(','||t_site.name||',', ','||:siteCode||',') > 0)
</#if>
<#if (code??)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>