<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT COUNT(1)
FROM c1_user t
LEFT JOIN c1_org t_org ON (t_org.id = t.org_id)
WHERE 1 = 1
<#if StringUtils.isNotBlank(code)>
AND (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>
<#if StringUtils.isNotBlank(orgId)>
AND t.org_id = :orgId
</#if>
<#if StringUtils.isNotBlank(orgCode)>
AND (instr(','||T_ORG.code||',', ','||:orgCode||',') > 0 OR instr(','||T_ORG.name||',', ','||:orgCode||',')   > 0)
</#if>