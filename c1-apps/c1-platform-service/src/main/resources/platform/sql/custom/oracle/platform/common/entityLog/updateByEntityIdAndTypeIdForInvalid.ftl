UPDATE C1_ENTITY_LOG
SET end_date    = sysdate
WHERE ENTITY_ID = :entityId
AND TYPE_ID     = :typeId
AND sysdate BETWEEN NVL(t.begin_date, sysdate) AND NVL(t.end_date, sysdate)