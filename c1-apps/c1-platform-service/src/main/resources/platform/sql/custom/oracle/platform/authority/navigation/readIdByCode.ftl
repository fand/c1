SELECT MAX(T.ID)
FROM C1_NAVIGATION T
JOIN c1_site t_site
ON ((sysdate BETWEEN t_site.BEGIN_DATE AND t_site.END_DATE)
AND t_site.id = t.SITE_ID)
JOIN c1_dictionary t_dictionary
ON ((sysdate BETWEEN t_dictionary.BEGIN_DATE AND t_dictionary.END_DATE)
AND t_dictionary.id = t.type_id )
WHERE (sysdate BETWEEN t.BEGIN_DATE AND t.END_DATE)
AND t_site.code = :siteCode
AND t_dictionary.code = :typeCode
AND T.CODE      = :code