INSERT
INTO c1_role_user
(
USER_ID,
ROLE_ID
)
(SELECT :userId,
:roleId
FROM dual
WHERE NOT EXISTS
(SELECT 1
FROM c1_role_user e
WHERE e.USER_ID     = :userId
AND e.ROLE_ID = :roleId
)
)