<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT count(1)
FROM C1_NAVIGATION T
LEFT JOIN C1_NAVIGATION T_NAVIGATION
ON ((NOW() BETWEEN IFNULL(T_NAVIGATION.BEGIN_DATE, NOW()) AND IFNULL(T_NAVIGATION.END_DATE, NOW()))
AND T_NAVIGATION.id = T.parent_id)
WHERE (NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW()) )
<#if StringUtils.isNotBlank(siteId)>
AND T.site_id                              = :siteId
</#if>
<#if StringUtils.isNotBlank(typeId)>
AND T.type_id                              = :typeId
</#if>
<#if StringUtils.isNotBlank(parentCode)>
AND (instr(','||T_NAVIGATION.code||',', ','||:parentCode||',') > 0
OR instr(','||T_NAVIGATION.name||',',','||:parentCode||',')   > 0)
</#if>
<#if StringUtils.isNotBlank(code)>
AND (instr(','||T.code||',', ','||:code||',') > 0
OR instr(','||T.name||',', ','||:code||',')   > 0)
</#if>