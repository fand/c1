SELECT (
CASE
WHEN (MAX(T.CODE)) IS NULL
THEN ('${codePrefix!'EH'}'
||TO_CHAR(SYSDATE,'yyyymmdd')
||'0001')
ELSE '${codePrefix!'EH'}'
||TO_CHAR(SYSDATE,'yyyymmdd')
|| LPAD((SUBSTR(MAX(T.CODE),-4,4)+1),4,'0')
END ) AS T_CODE
FROM C1_ENTITY_CODE T
WHERE T.TYPE_CODE = :typeCode AND T.CODE LIKE '${codePrefix!'EH'}'
||TO_CHAR(SYSDATE,'yyyymmdd')
||'%'