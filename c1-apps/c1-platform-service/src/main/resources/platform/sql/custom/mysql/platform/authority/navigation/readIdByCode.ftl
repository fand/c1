SELECT MAX(T.ID)
FROM C1_NAVIGATION T
JOIN c1_site t_site
ON ((NOW() BETWEEN t_site.BEGIN_DATE AND t_site.END_DATE)
AND t_site.id = t.SITE_ID)
JOIN c1_dictionary t_dictionary
ON ((NOW() BETWEEN IFNULL(t_dictionary.BEGIN_DATE, NOW()) AND IFNULL(t_dictionary.END_DATE, NOW()))
AND t_dictionary.id = t.type_id )
WHERE (NOW() BETWEEN IFNULL(T.BEGIN_DATE, NOW()) AND IFNULL(T.END_DATE, NOW()))
AND t_site.code = :siteCode
AND t_dictionary.code = :typeCode
AND T.CODE      = :code