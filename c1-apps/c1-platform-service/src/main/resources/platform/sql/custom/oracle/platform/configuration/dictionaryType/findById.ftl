SELECT T.* ,
t_dictionary_type.code AS parentCode,
t_dictionary_type.name AS parentName
FROM C1_DICTIONARY_TYPE T
LEFT JOIN C1_DICTIONARY_TYPE T_DICTIONARY_TYPE
ON (Sysdate BETWEEN t_dictionary_type.begin_date AND t_dictionary_type.end_date
AND T_DICTIONARY_TYPE.ID = t.parent_id)
WHERE t.Id = :id