<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT count(1)
FROM C1_DICTIONARY T
LEFT JOIN C1_DICTIONARY_TYPE T_DICTIONARY_TYPE
ON (Sysdate BETWEEN T_DICTIONARY_TYPE.begin_date AND T_DICTIONARY_TYPE.end_date
AND T_DICTIONARY_TYPE.ID = t.type_id)
WHERE Sysdate BETWEEN t.begin_date AND t.end_date
<#if StringUtils.isNotBlank(typeCode)>
and (instr(','||T_DICTIONARY_TYPE.code||',', ','||:typeCode||',') > 0 or instr(','||T_DICTIONARY_TYPE.name||',', ','||:typeCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(code)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>