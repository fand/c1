<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT sysdate FROM dual
UNION ALL
SELECT MAX(t.BEGIN_DATE)
FROM C1_OPERATE_LOG T
WHERE (sysdate BETWEEN T.BEGIN_DATE AND T.END_DATE)
AND t.code = :code
AND t.type_id = :typeId
<#if StringUtils.isNotBlank(businessCode)>
AND T.BUSINESS_CODE = :businessCode
</#if>