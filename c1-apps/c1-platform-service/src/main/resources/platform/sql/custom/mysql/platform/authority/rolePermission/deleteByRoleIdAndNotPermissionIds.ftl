DELETE
FROM C1_ROLE_PERMISSION T
WHERE t.ROLE_ID                            = :roleId
AND instr(','||:permissionIds||',', ','||t.PERMISSION_ID||',') < 1