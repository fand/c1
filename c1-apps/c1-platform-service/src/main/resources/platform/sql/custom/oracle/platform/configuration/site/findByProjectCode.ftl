SELECT t.*
FROM C1_SITE T
left join C1_DICTIONARY project_DICTIONARY on (project_DICTIONARY.id = t.project_ID)
where ( Sysdate BETWEEN t.begin_date AND t.end_date )
and project_DICTIONARY.code = :projectCode
order by t.code
