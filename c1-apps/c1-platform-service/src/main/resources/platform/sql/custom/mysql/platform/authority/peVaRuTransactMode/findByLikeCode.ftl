<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
WITH W_PE_VA_RU_TRANSACT_MODE AS
(SELECT T.*,
ROW_NUMBER() OVER(ORDER BY TYPE_DICTIONARY.CODE, T_PERMISSION.CODE, VALIDATE_RULE_DICTIONARY.CODE, VALIDATE_SUCCESS_DICTIONARY.CODE, VALIDATE_ERROR_DICTIONARY.CODE, T.ORDER_NUM) RN,
TYPE_DICTIONARY.ID               AS PERMISSION_TYPE_ID,
TYPE_DICTIONARY.CODE             AS PERMISSION_TYPE_CODE,
TYPE_DICTIONARY.NAME             AS PERMISSION_TYPE_NAME,
T_PERMISSION.CODE                AS PERMISSION_CODE,
T_PERMISSION.NAME                AS PERMISSION_NAME,
VALIDATE_RULE_DICTIONARY.CODE    AS VALIDATE_RULE_CODE,
VALIDATE_RULE_DICTIONARY.NAME    AS VALIDATE_RULE_NAME,
VALIDATE_SUCCESS_DICTIONARY.CODE AS VALIDATE_SUCCESS_CODE,
VALIDATE_SUCCESS_DICTIONARY.NAME AS VALIDATE_SUCCESS_NAME,
VALIDATE_ERROR_DICTIONARY.CODE   AS VALIDATE_ERROR_CODE,
VALIDATE_ERROR_DICTIONARY.NAME   AS VALIDATE_ERROR_NAME
FROM C1_PE_VA_RU_TRANSACT_MODE T
LEFT JOIN C1_PERMISSION T_PERMISSION
ON (T_PERMISSION.ID = T.PERMISSION_ID)
LEFT JOIN C1_DICTIONARY TYPE_DICTIONARY
ON (TYPE_DICTIONARY.ID = T_PERMISSION.TYPE_ID)
LEFT JOIN C1_DICTIONARY VALIDATE_RULE_DICTIONARY
ON (VALIDATE_RULE_DICTIONARY.ID = T.VALIDATE_RULE_ID)
LEFT JOIN C1_DICTIONARY VALIDATE_SUCCESS_DICTIONARY
ON (VALIDATE_SUCCESS_DICTIONARY.ID = T.VALIDATE_SUCCESS_ID)
LEFT JOIN C1_DICTIONARY VALIDATE_ERROR_DICTIONARY
ON (VALIDATE_ERROR_DICTIONARY.ID = T.VALIDATE_ERROR_ID)
WHERE 1                = 1
<#if StringUtils.isNotBlank(permissionTypeCode)>
AND (instr(','||TYPE_DICTIONARY.CODE||',', ','||:permissionTypeCode||',') > 0 OR instr(','||TYPE_DICTIONARY.NAME||',', ','||:permissionTypeCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(permissionCode)>
AND (instr(','||T_PERMISSION.CODE||',', ','||:permissionCode||',') > 0 OR instr(','||T_PERMISSION.NAME||',', ','||:permissionCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(validateRuleCode)>
AND (instr(','||VALIDATE_RULE_DICTIONARY.CODE||',', ','||:validateRuleCode||',') > 0 OR instr(','||VALIDATE_RULE_DICTIONARY.NAME||',', ','||:validateRuleCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(validateSuccessCode)>
AND (instr(','||VALIDATE_SUCCESS_DICTIONARY.CODE||',', ','||:validateSuccessCode||',') > 0 OR instr(','||VALIDATE_SUCCESS_DICTIONARY.NAME||',', ','||:validateSuccessCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(permissionTypeCode)>
AND (instr(','||VALIDATE_ERROR_DICTIONARY.CODE||',', ','||:validateErrorCode||',') > 0 OR instr(','||VALIDATE_ERROR_DICTIONARY.NAME||',', ','||:validateErrorCode||',') > 0)
</#if>
)
SELECT T.*
FROM W_PE_VA_RU_TRANSACT_MODE T
WHERE t.rn BETWEEN <#if (begin_row_num??)>:begin_row_num<#else>1</#if> AND <#if (end_row_num??)>:end_row_num<#else>20</#if>
