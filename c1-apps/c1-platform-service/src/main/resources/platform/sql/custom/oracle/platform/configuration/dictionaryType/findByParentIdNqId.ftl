<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT T.* ,
(SELECT COUNT(0) FROM C1_DICTIONARY_TYPE SDT WHERE SDT.PARENT_ID = T.ID
) AS CHILD_COUNT,
(SELECT SDT.NAME FROM C1_DICTIONARY_TYPE SDT WHERE SDT.ID = T.PARENT_ID
) AS PARENT_NAME
FROM C1_DICTIONARY_TYPE T
WHERE 1          = 1
<#if StringUtils.isBlank(parentId)>
AND T.PARENT_ID IS NULL
<#else>
AND T.PARENT_ID  = :parentId
</#if>
<#if StringUtils.isNotBlank(id)>
AND T.ID        <> :id
</#if>
ORDER BY
t.order_num,
t.code