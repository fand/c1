<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT COUNT(1)
FROM C1_DICTIONARY_TYPE T
LEFT JOIN C1_DICTIONARY_TYPE T_DICTIONARY_TYPE
ON (Sysdate BETWEEN t_dictionary_type.begin_date AND t_dictionary_type.end_date
AND T_DICTIONARY_TYPE.ID = t.parent_id)
WHERE Sysdate BETWEEN t.begin_date AND t.end_date
<#if StringUtils.isNotBlank(parentCode)>
and (instr(','||T_DICTIONARY_TYPE.code||',', ','||:parentCode||',') > 0 or instr(','||T_DICTIONARY_TYPE.name||',', ','||:parentCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(parentId)>
and T.PARENT_ID = :parentId
</#if>
<#if StringUtils.isNotBlank(code)>
and (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>