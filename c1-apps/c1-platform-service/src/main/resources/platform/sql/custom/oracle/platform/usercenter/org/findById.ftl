SELECT T.*,
T_ORG.ID AS parentId ,
T_ORG.code AS parentCode ,
T_ORG.name AS parentName
FROM C1_ORG T
LEFT JOIN C1_ORG T_ORG ON (T_ORG.id = t.parent_id)
WHERE T.ID = :id