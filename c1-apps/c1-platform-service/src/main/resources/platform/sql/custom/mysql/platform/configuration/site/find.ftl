SELECT
T.*
FROM
C1_SITE T
LEFT JOIN c1_dictionary t_dictionary
ON
(
t_dictionary.id = t.project_id
)
ORDER BY
t_dictionary.order_num,
t_dictionary.code,
t.code