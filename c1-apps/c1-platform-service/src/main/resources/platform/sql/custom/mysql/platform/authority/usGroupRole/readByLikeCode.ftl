<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT COUNT(1)
FROM C1_US_GROUP_ROLE T
LEFT JOIN C1_USER_GROUP T_USER_GROUP
ON (T_USER_GROUP.id = t.user_group_id)
LEFT JOIN C1_ROLE T_ROLE
ON (T_ROLE.id = t.ROLE_ID)
WHERE 1 = 1
<#if StringUtils.isNotBlank(userGroupCode)>
AND (instr(','||T_USER_GROUP.code||',', ','||:userGroupCode||',') > 0 OR instr(','||T_USER_GROUP.name||',', ','||:userGroupCode||',')   > 0)
</#if>
<#if StringUtils.isNotBlank(roleCode)>
and (instr(','||T_ROLE.code||',', ','||:roleCode||',') > 0 or instr(','||T_ROLE.name||',', ','||:roleCode||',') > 0)
</#if>