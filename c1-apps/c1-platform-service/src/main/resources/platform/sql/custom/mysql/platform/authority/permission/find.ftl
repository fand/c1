SELECT t.*,
t_dictionary.code AS type_code,
t_dictionary.name AS type_name
FROM c1_permission t
LEFT JOIN c1_dictionary t_dictionary
ON (t_dictionary.id = t.type_id)
WHERE T.site_id = :siteId
ORDER BY t.type_id,
t.order_num,
LENGTH(t.code) DESC
