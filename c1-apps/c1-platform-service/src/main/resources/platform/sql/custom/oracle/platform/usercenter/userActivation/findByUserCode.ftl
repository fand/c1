SELECT T.*
FROM C1_USER_ACTIVATION T
WHERE (Sysdate BETWEEN t.begin_date AND t.end_date)
AND EXISTS
(SELECT 1
FROM c1_user e
WHERE e.id        = t.user_id
AND (e.code       = :userCode
OR e.MOBILE_PHONE = :userCode)
)