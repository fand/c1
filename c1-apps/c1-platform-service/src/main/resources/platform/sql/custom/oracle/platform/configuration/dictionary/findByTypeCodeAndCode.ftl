SELECT T.* FROM C1_DICTIONARY T WHERE t.code = :code AND ( Sysdate BETWEEN t.begin_date AND t.end_date ) AND EXISTS
( SELECT 1 FROM c1_dictionary_type e WHERE e.code = :typeCode AND ( Sysdate BETWEEN e.begin_date AND e.end_date ) and e.id = t.type_id
)