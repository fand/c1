<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
WITH W_USER AS
(SELECT t.*,
row_number() over(order by T_ORG.code, T.code) rn ,
t_org.code AS orgCode,
t_org.name AS orgName
FROM c1_user t
LEFT JOIN c1_org t_org ON (t_org.id = t.org_id)
WHERE 1 = 1
<#if StringUtils.isNotBlank(code)>
AND (instr(','||T.code||',', ','||:code||',') > 0 or instr(','||T.name||',', ','||:code||',') > 0)
</#if>
<#if StringUtils.isNotBlank(orgId)>
AND t.org_id = :orgId
<#else>
AND t.org_id is not null
</#if>
<#if StringUtils.isNotBlank(orgCode)>
AND (instr(','||T_ORG.code||',', ','||:orgCode||',') > 0 OR instr(','||T_ORG.name||',', ','||:orgCode||',')   > 0)
</#if>
)
SELECT T.*,
TO_CHAR(NVL(T.CREATED_DATE, T.BEGIN_DATE), 'yyyy-mm-dd') as createdDateStr,
TO_CHAR(T.END_DATE, 'yyyy-mm-dd') as endDateStr,
NVL(Trunc(MONTHS_BETWEEN(SYSDATE, T.BIRTHDAY)/12), T.AGE) as now_age,
T_SEX.code         AS SEX_CODE,
T_SEX.NAME         AS SEX_NAME
FROM W_USER T
LEFT JOIN c1_dictionary T_SEX
ON ((sysdate BETWEEN T_SEX.BEGIN_DATE AND T_SEX.END_DATE)
AND T_SEX.id = t.SEX_ID)
WHERE t.rn BETWEEN <#if (begin_row_num??)>:begin_row_num<#else>1</#if> AND <#if (end_row_num??)>:end_row_num<#else>20</#if>
order by t.rn