<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT COUNT(1)
FROM C1_PE_VA_RU_TRANSACT_MODE T
LEFT JOIN C1_PERMISSION T_PERMISSION
ON (T_PERMISSION.ID = T.PERMISSION_ID)
LEFT JOIN C1_DICTIONARY TYPE_DICTIONARY
ON (TYPE_DICTIONARY.ID = T_PERMISSION.TYPE_ID)
LEFT JOIN C1_DICTIONARY VALIDATE_RULE_DICTIONARY
ON (VALIDATE_RULE_DICTIONARY.ID = T.VALIDATE_RULE_ID)
LEFT JOIN C1_DICTIONARY VALIDATE_SUCCESS_DICTIONARY
ON (VALIDATE_SUCCESS_DICTIONARY.ID = T.VALIDATE_SUCCESS_ID)
LEFT JOIN C1_DICTIONARY VALIDATE_ERROR_DICTIONARY
ON (VALIDATE_ERROR_DICTIONARY.ID = T.VALIDATE_ERROR_ID)
WHERE 1                = 1
<#if StringUtils.isNotBlank(permissionTypeCode)>
AND (instr(','||TYPE_DICTIONARY.CODE||',', ','||:permissionTypeCode||',') > 0 OR instr(','||TYPE_DICTIONARY.NAME||',', ','||:permissionTypeCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(permissionCode)>
AND (instr(','||T_PERMISSION.CODE||',', ','||:permissionCode||',') > 0 OR instr(','||T_PERMISSION.NAME||',', ','||:permissionCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(validateRuleCode)>
AND (instr(','||VALIDATE_RULE_DICTIONARY.CODE||',', ','||:validateRuleCode||',') > 0 OR instr(','||VALIDATE_RULE_DICTIONARY.NAME||',', ','||:validateRuleCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(validateSuccessCode)>
AND (instr(','||VALIDATE_SUCCESS_DICTIONARY.CODE||',', ','||:validateSuccessCode||',') > 0 OR instr(','||VALIDATE_SUCCESS_DICTIONARY.NAME||',', ','||:validateSuccessCode||',') > 0)
</#if>
<#if StringUtils.isNotBlank(permissionTypeCode)>
AND (instr(','||VALIDATE_ERROR_DICTIONARY.CODE||',', ','||:validateErrorCode||',') > 0 OR instr(','||VALIDATE_ERROR_DICTIONARY.NAME||',', ','||:validateErrorCode||',') > 0)
</#if>