INSERT
INTO C1_ROLE_USER
(
USER_ID,
ROLE_ID
)
(SELECT :userId,
(SELECT MAX(i.id) FROM c1_role i WHERE i.code = :roleCode
)
FROM dual
WHERE NOT EXISTS
(SELECT 1
FROM C1_ROLE_USER e
WHERE e.user_id = :userId
AND e.role_id   =
(SELECT MAX(i.id) FROM c1_role i WHERE i.code = :roleCode
)
)
)