<#assign StringUtils = beansWrapperFn.getStaticModels()["org.apache.commons.lang3.StringUtils"]>
SELECT COUNT(1)
FROM C1_PERMISSION T
WHERE 1                = 1
<#if StringUtils.isNotBlank(siteId)>
AND T.SITE_ID          =:siteId
</#if>
<#if StringUtils.isNotBlank(typeId)>
AND T.TYPE_ID          = :typeId
</#if>
<#if StringUtils.isNotBlank(code)>
AND (instr(','||T.code||',', ','||:code||',') > 0 OR instr(','||T.name||',', ','||:code||',') > 0)
</#if>