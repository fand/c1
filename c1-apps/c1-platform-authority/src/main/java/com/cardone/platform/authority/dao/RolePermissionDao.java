package com.cardone.platform.authority.dao;

import java.util.List;
import java.util.Map;

/**
 * 角色与许可
 *
 * @author yaohaitao
 */
public interface RolePermissionDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.authority.dto.RolePermissionDto> {
    /**
     * 保存:角色与许可
     *
     * @param roleId        角色标识
     * @param permissionIds 许可标识集合
     * @return 角色与许可
     */
    Map<String, Object> saveRoleIdAndPermissionIds(final String roleId, final String permissionIds);

    /**
     * 删除
     *
     * @param roleIds 角色标识集合
     * @return 影响行数
     */
    int deleteByRoleIds(final String roleIds);

    /**
     * 查询角色及授权
     *
     * @param userId 用户标识
     * @return
     */
    List<Map<String, Object>> findListByUserId(String userId);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 删除
         */
        deleteByRoleIdAndNotPermissionIds,

        /**
         * 删除
         */
        deleteByRoleIds,

        findByUserId,

        /**
         * 插入
         */
        insertByRoleIdAndPermissionId;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/authority/rolePermission/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}