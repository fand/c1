package com.cardone.platform.authority.dao;

/**
 * 用户组与组织
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class UsGroupOrgJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.authority.dto.UsGroupOrgDto> implements com.cardone.platform.authority.dao.UsGroupOrgDao {
    public UsGroupOrgJdbcDao() {
        super(com.cardone.platform.authority.dao.UsGroupOrgDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(com.cardone.platform.authority.dto.UsGroupOrgDto dto) {
        return org.apache.commons.lang3.StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(com.cardone.platform.authority.dto.UsGroupOrgDto dto) {
        dto.setId(java.util.UUID.randomUUID().toString());
    }
}