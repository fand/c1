package com.cardone.platform.authority.dao;

/**
 * 权限
 *
 * @author yaohaitao
 */
public interface AuthorityDao {
    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByCodes;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/authority/authority/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = com.cardone.platform.authority.dao.AuthorityDao.SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}