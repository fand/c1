package com.cardone.platform.authority.service;

import com.cardone.context.*;
import com.cardone.platform.authority.dao.*;
import lombok.*;
import org.springframework.transaction.annotation.*;

/**
 * 许可与组织服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Transactional(readOnly = true)
public class PermissionOrgDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.authority.dto.PermissionOrgDto> implements PermissionOrgService {
    @Override
    public PermissionOrgDao getDao() {
        return ContextHolder.getBean(PermissionOrgDao.class);
    }
}