package com.cardone.platform.authority.dao;

/**
 * 用户组与角色
 *
 * @author yaohaitao
 */
public interface UsGroupRoleDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.authority.dto.UsGroupRoleDto> {
    /**
     * 删除
     *
     * @param roleIds 角色标识集合
     * @return 影响行数
     */
    int deleteByRoleIds(final String roleIds);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        deleteByRoleIds;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/authority/usGroupRole/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = com.cardone.platform.authority.dao.UsGroupRoleDao.SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}