package com.cardone.platform.authority.dao;

/**
 * 用户组与用户
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class UsGroupUserJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.authority.dto.UsGroupUserDto> implements com.cardone.platform.authority.dao.UsGroupUserDao {
    public UsGroupUserJdbcDao() {
        super(com.cardone.platform.authority.dao.UsGroupUserDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(com.cardone.platform.authority.dto.UsGroupUserDto dto) {
        return org.apache.commons.lang3.StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(com.cardone.platform.authority.dto.UsGroupUserDto dto) {
        dto.setId(java.util.UUID.randomUUID().toString());
    }
}