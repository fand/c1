package com.cardone.platform.authority.service;

import com.cardone.context.*;
import com.cardone.platform.authority.dao.*;
import com.google.common.collect.*;
import lombok.*;
import org.springframework.transaction.annotation.*;

import java.util.*;

/**
 * 许可服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Transactional(readOnly = true)
public class PermissionDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.authority.dto.PermissionDto> implements PermissionService {
    @Override
    public PermissionDao getDao() {
        return ContextHolder.getBean(PermissionDao.class);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findListBySiteId(final Class<P> mappedClass, final String siteId) {
        return ContextHolder.getBean(PermissionDao.class).findListBySiteId(mappedClass, siteId);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findListBySiteIdAndRoleId(final Class<P> mappedClass, final String siteId, final String roleId) {
        return ContextHolder.getBean(PermissionService.class).findListBySiteIdAndRoleIdList(mappedClass, siteId, Lists.newArrayList(roleId));
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findListBySiteIdAndTypeId(final Class<P> mappedClass, final String siteId, final String typeId) {
        return ContextHolder.getBean(PermissionDao.class).findListBySiteIdAndTypeId(mappedClass, siteId, typeId);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findListBySiteIdAndTypeCode(final Class<P> mappedClass, final String siteId, final String typeCode) {
        return ContextHolder.getBean(PermissionDao.class).findListBySiteIdAndTypeCode(mappedClass, siteId, typeCode);
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> readCodeListBySiteIdAndTypeCode(final String siteId, final String typeCode) {
        return ContextHolder.getBean(PermissionDao.class).readCodeListBySiteIdAndTypeCode(siteId, typeCode);
    }

    @Override
    @Transactional(readOnly = true)
    public <P> List<P> findListBySiteIdAndRoleIdList(final Class<P> mappedClass, final String siteId, final List<String> roleIdList) {
        return ContextHolder.getBean(PermissionDao.class).findListBySiteIdAndRoleIdList(mappedClass, siteId, roleIdList);
    }
}