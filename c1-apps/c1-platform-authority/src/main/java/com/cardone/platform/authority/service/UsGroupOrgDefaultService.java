package com.cardone.platform.authority.service;

import com.cardone.context.*;
import com.cardone.platform.authority.dao.*;
import lombok.*;

/**
 * 用户组与组织服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class UsGroupOrgDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.authority.dto.UsGroupOrgDto> implements UsGroupOrgService {
    @Override
    public UsGroupOrgDao getDao() {
        return ContextHolder.getBean(UsGroupOrgDao.class);
    }
}