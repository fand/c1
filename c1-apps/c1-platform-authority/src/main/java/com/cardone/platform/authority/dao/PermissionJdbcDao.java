package com.cardone.platform.authority.dao;

import com.cardone.context.*;
import com.cardone.persistent.support.*;
import com.cardone.platform.authority.dto.*;
import com.google.common.collect.*;
import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 许可
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.extern.slf4j.Slf4j
public class PermissionJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.authority.dto.PermissionDto> implements PermissionDao {
    public PermissionJdbcDao() {
        super(PermissionDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(PermissionDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(PermissionDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }

    @Override
    public <P> List<P> findListBySiteIdAndTypeCode(final Class<P> mappedClass, final String siteId, final String typeCode) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.siteId.name(), siteId);
        model.put(Attributes.typeCode.name(), typeCode);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findBySiteIdAndTypeCode.id(), model);
    }

    @Override
    public <P> List<P> findListBySiteIdAndTypeId(final Class<P> mappedClass, final String siteId, final String typeId) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.siteId.name(), siteId);
        model.put(Attributes.typeId.name(), typeId);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findBySiteIdAndTypeId.id(), model);
    }

    @Override
    public List<String> readCodeListBySiteIdAndTypeCode(final String siteId, final String typeCode) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.siteId.name(), siteId);
        model.put(Attributes.typeCode.name(), typeCode);

        return ContextHolder.getBean(JdbcTemplateSupport.class).readList(String.class, SqlIds.readCodeBySiteIdAndTypeCode.id(), model);
    }

    @Override
    public <P> List<P> findListBySiteId(final Class<P> mappedClass, final String siteId) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.siteId.name(), siteId);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findBySiteId.id(), model);
    }

    @Override
    public <P> List<P> findListBySiteIdAndRoleIdList(final Class<P> mappedClass, final String siteId, final List<String> roleIdList) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.siteId.name(), siteId);
        model.put("roleIds", StringUtils.join(roleIdList, ","));

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findBySiteIdAndRoleIdList.id(), model);
    }
}