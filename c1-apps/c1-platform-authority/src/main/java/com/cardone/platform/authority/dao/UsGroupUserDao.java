package com.cardone.platform.authority.dao;

/**
 * 用户组与用户
 *
 * @author yaohaitao
 */
public interface UsGroupUserDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.authority.dto.UsGroupUserDto> {
    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByCodes;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/authority/usGroupUser/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = com.cardone.platform.authority.dao.UsGroupUserDao.SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}