package com.cardone.platform.authority.service;

import com.cardone.context.*;
import com.cardone.platform.authority.dao.*;
import lombok.*;

/**
 * 许可、验证规则与处理模式服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class PeVaRuTransactModeDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.authority.dto.PeVaRuTransactModeDto> implements PeVaRuTransactModeService {
    @Override
    public PeVaRuTransactModeDao getDao() {
        return ContextHolder.getBean(PeVaRuTransactModeDao.class);
    }
}