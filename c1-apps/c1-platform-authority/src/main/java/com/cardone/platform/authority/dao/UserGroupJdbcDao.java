package com.cardone.platform.authority.dao;

/**
 * 用户组
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class UserGroupJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.authority.dto.UserGroupDto> implements com.cardone.platform.authority.dao.UserGroupDao {
    public UserGroupJdbcDao() {
        super(com.cardone.platform.authority.dao.UserGroupDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(com.cardone.platform.authority.dto.UserGroupDto dto) {
        return org.apache.commons.lang3.StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(com.cardone.platform.authority.dto.UserGroupDto dto) {
        dto.setId(java.util.UUID.randomUUID().toString());
    }
}