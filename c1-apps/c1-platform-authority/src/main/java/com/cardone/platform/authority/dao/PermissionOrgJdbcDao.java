package com.cardone.platform.authority.dao;

/**
 * 许可与组织
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class PermissionOrgJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.authority.dto.PermissionOrgDto> implements com.cardone.platform.authority.dao.PermissionOrgDao {
    public PermissionOrgJdbcDao() {
        super(com.cardone.platform.authority.dao.PermissionOrgDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(com.cardone.platform.authority.dto.PermissionOrgDto dto) {
        return org.apache.commons.lang3.StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(com.cardone.platform.authority.dto.PermissionOrgDto dto) {
        dto.setId(java.util.UUID.randomUUID().toString());
    }
}