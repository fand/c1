package com.cardone.platform.authority.dao;

import com.cardone.platform.authority.dto.*;

import java.util.*;

/**
 * 角色与用户
 *
 * @author yaohaitao
 */
public interface RoleUserDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.authority.dto.RoleUserDto> {
    /**
     * 保存:角色与用户
     *
     * @param userId  用户标识
     * @param roleIds 角色标识集合
     * @return 角色与用户对象
     */
    Map<String, Object> saveUserIdAndRoleIds(final String userId, final String roleIds);

    /**
     * 插入
     *
     * @param insertRoleUserList 角色与用户对象集合
     * @return 影响行数
     */
    int[] insertByRoleCode(final List<RoleUserDto> insertRoleUserList);

    /**
     * 删除
     *
     * @param roleIds 角色标识集合
     * @return 影响行数
     */
    int deleteByRoleIds(final String roleIds);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 删除
         */
        deleteByRoleIds,

        /**
         * 删除
         */
        deleteByUserIdAndNotRoleIds,

        /**
         * 插入
         */
        insertByRoleCode,

        /**
         * 插入
         */
        insertByUserIdAndRoleId;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/authority/roleUser/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}