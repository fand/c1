package com.cardone.platform.authority.service;

import com.cardone.context.*;
import com.cardone.platform.authority.dao.*;
import lombok.*;

/**
 * 用户组与角色服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class UsGroupRoleDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.authority.dto.UsGroupRoleDto> implements UsGroupRoleService {
    @Override
    public UsGroupRoleDao getDao() {
        return ContextHolder.getBean(UsGroupRoleDao.class);
    }
}