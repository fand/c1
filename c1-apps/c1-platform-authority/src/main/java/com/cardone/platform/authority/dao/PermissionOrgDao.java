package com.cardone.platform.authority.dao;

/**
 * 许可与组织
 *
 * @author yaohaitao
 */
public interface PermissionOrgDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.authority.dto.PermissionOrgDto> {
    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByCodes;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/authority/permissionOrg/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = com.cardone.platform.authority.dao.PermissionOrgDao.SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}