package com.cardone.platform.authority.dao;

import com.cardone.context.*;
import com.cardone.persistent.support.*;
import com.cardone.platform.authority.dto.*;
import com.google.common.collect.*;
import lombok.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 角色
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.extern.slf4j.Slf4j
public class RoleJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.authority.dto.RoleDto> implements RoleDao {
    public RoleJdbcDao() {
        super(RoleDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(RoleDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(RoleDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }

    @Override
    public String readIdByCode(final RoleDto readRole) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.code.name(), readRole.getCode());

        return ContextHolder.getBean(JdbcTemplateSupport.class).read(String.class, SqlIds.readIdByCode.id(), model);
    }

    @Override
    public <P> List<P> findListByUserId(final Class<P> mappedClass, final String userId) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.userId.name(), userId);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findByUserId.id(), model);
    }
}