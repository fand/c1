package com.cardone.platform.authority.dao;

/**
 * 用户组与角色
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class UsGroupRoleJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.authority.dto.UsGroupRoleDto> implements com.cardone.platform.authority.dao.UsGroupRoleDao {
    public UsGroupRoleJdbcDao() {
        super(com.cardone.platform.authority.dao.UsGroupRoleDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(com.cardone.platform.authority.dto.UsGroupRoleDto dto) {
        return org.apache.commons.lang3.StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(com.cardone.platform.authority.dto.UsGroupRoleDto dto) {
        dto.setId(java.util.UUID.randomUUID().toString());
    }

    @Override
    public int deleteByRoleIds(final String roleIds) {
        final java.util.Map<String, Object> model = com.google.common.collect.Maps.newHashMap();

        model.put("roleIds", roleIds);

        return com.cardone.context.ContextHolder.getBean(com.cardone.persistent.support.JdbcTemplateSupport.class).update(SqlIds.deleteByRoleIds.id(), model);
    }
}