package com.cardone.platform.authority.dao;

/**
 * 许可、验证规则与处理模式
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class PeVaRuTransactModeJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.authority.dto.PeVaRuTransactModeDto> implements com.cardone.platform.authority.dao.PeVaRuTransactModeDao {
    public PeVaRuTransactModeJdbcDao() {
        super(com.cardone.platform.authority.dao.PeVaRuTransactModeDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(com.cardone.platform.authority.dto.PeVaRuTransactModeDto dto) {
        return org.apache.commons.lang3.StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(com.cardone.platform.authority.dto.PeVaRuTransactModeDto dto) {
        dto.setId(java.util.UUID.randomUUID().toString());
    }
}