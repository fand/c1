package com.cardone.platform.authority.dao;

/**
 * 用户组与业务范围
 *
 * @author yaohaitao
 */
@lombok.Getter
@lombok.Setter
@lombok.extern.slf4j.Slf4j
public class UsGrBusinessAreaJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.authority.dto.UsGrBusinessAreaDto> implements com.cardone.platform.authority.dao.UsGrBusinessAreaDao {
    public UsGrBusinessAreaJdbcDao() {
        super(com.cardone.platform.authority.dao.UsGrBusinessAreaDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(com.cardone.platform.authority.dto.UsGrBusinessAreaDto dto) {
        return org.apache.commons.lang3.StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(com.cardone.platform.authority.dto.UsGrBusinessAreaDto dto) {
        dto.setId(java.util.UUID.randomUUID().toString());
    }
}