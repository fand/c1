package com.cardone.platform.authority.dao;

import java.util.*;

/**
 * 许可
 *
 * @author yaohaitao
 */
public interface PermissionDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.authority.dto.PermissionDto> {
    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param siteId      站标识
     * @param typeCode    类别代码
     * @return 许可对象集合
     */
    <P> List<P> findListBySiteIdAndTypeCode(final Class<P> mappedClass, final String siteId, final String typeCode);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param siteId      站标识
     * @param typeId      类别标识
     * @return 许可对象集合
     */
    <P> List<P> findListBySiteIdAndTypeId(final Class<P> mappedClass, final String siteId, final String typeId);

    /**
     * 查询
     *
     * @param siteId   站标识
     * @param typeCode 类别代码
     * @return 许可对象集合
     */
    List<String> readCodeListBySiteIdAndTypeCode(final String siteId, final String typeCode);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param siteId      站标识
     * @return 许可对象集合
     */
    <P> List<P> findListBySiteId(final Class<P> mappedClass, final String siteId);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param siteId      站标识
     * @param roleIdList  角色标识集合
     * @return 许可对象集合
     */
    <P> List<P> findListBySiteIdAndRoleIdList(final Class<P> mappedClass, final String siteId, final List<String> roleIdList);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findBySiteCodeAndUserId,

        /**
         * 查询
         */
        findBySiteId,

        /**
         * 查询
         */
        findBySiteIdAndRoleIdList,

        /**
         * 查询
         */
        findBySiteIdAndTypeCode,

        /**
         * 查询
         */
        findBySiteIdAndTypeId,

        /**
         * 读取
         */
        readBySiteCodeAndUserIdForCount,
        /**
         * 读取
         */
        readBySiteIdAndRoleIdListForCount,

        /**
         * 读取
         */
        readCodeBySiteIdAndTypeCode;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/authority/permission/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}