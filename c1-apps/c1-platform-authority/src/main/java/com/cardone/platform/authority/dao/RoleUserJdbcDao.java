package com.cardone.platform.authority.dao;

import com.cardone.common.util.NumberUtils;
import com.cardone.context.*;
import com.cardone.persistent.support.*;
import com.cardone.platform.authority.dto.*;
import com.google.common.collect.*;
import lombok.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 * 角色与用户
 *
 * @author yaohaitao
 */
@Getter
@Setter
@lombok.extern.slf4j.Slf4j
public class RoleUserJdbcDao extends com.cardone.persistent.support.SimpleJdbcDao<com.cardone.platform.authority.dto.RoleUserDto> implements RoleUserDao {
    public RoleUserJdbcDao() {
        super(RoleUserDao.SqlIds.ROOT);
    }

    @Override
    protected boolean isBlankById(RoleUserDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    protected void setId(RoleUserDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }

    @Override
    public Map<String, Object> saveUserIdAndRoleIds(final String userId, final String roleIds) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.userId.name(), userId);
        model.put("roleIds", roleIds);

        final Map<String, Object> returnMap = Maps.newHashMap();

        final int deleteCount = ContextHolder.getBean(JdbcTemplateSupport.class).update(SqlIds.deleteByUserIdAndNotRoleIds.id(), model);

        returnMap.put("deleteCount", deleteCount);

        if (StringUtils.isBlank(roleIds)) {
            return returnMap;
        }

        final List<Map<String, Object>> mapList = Lists.newArrayList();

        final List<String> roleIdList = Lists.newArrayList(StringUtils.split(roleIds, ","));

        for (final String roleId : roleIdList) {
            final Map<String, Object> map = Maps.newHashMap();

            map.put(Attributes.userId.name(), userId);
            map.put(Attributes.roleId.name(), roleId);

            mapList.add(map);
        }

        final int[] insertCounts = ContextHolder.getBean(JdbcTemplateSupport.class).update(SqlIds.insertByUserIdAndRoleId.id(), mapList);

        returnMap.put("insertCount", NumberUtils.sum(insertCounts));

        return returnMap;
    }

    @Override
    public int[] insertByRoleCode(final List<RoleUserDto> insertRoleUserList) {
        if (CollectionUtils.isEmpty(insertRoleUserList)) {
            return new int[0];
        }

        final List<Map<String, Object>> modelList = Lists.newArrayList();

        for (final RoleUserDto insertRoleUser : insertRoleUserList) {
            final Map<String, Object> model = Maps.newHashMap();

            model.put(Attributes.roleCode.name(), insertRoleUser.getRoleCode());
            model.put(Attributes.userId.name(), insertRoleUser.getUserId());

            modelList.add(model);
        }

        return ContextHolder.getBean(JdbcTemplateSupport.class).update(SqlIds.insertByRoleCode.id(), modelList);
    }

    @Override
    public int deleteByRoleIds(final String roleIds) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put("roleIds", roleIds);

        return ContextHolder.getBean(JdbcTemplateSupport.class).update(SqlIds.deleteByRoleIds.id(), model);
    }
}