package com.cardone.platform.authority.dao;

import com.cardone.platform.authority.dto.*;

import java.util.*;

/**
 * 角色
 *
 * @author yaohaitao
 */
public interface RoleDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.authority.dto.RoleDto> {
    /**
     * 读取
     *
     * @param readRole 角色对象
     * @return 标识
     */
    String readIdByCode(final RoleDto readRole);

    /**
     * 查询
     *
     * @param mappedClass 返回类型
     * @param userId      用户标识
     * @return 角色对象集合
     */
    <P> List<P> findListByUserId(final Class<P> mappedClass, final String userId);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByUserId,

        /**
         * 读取
         */
        readIdByCode;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/authority/role/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}