package com.cardone.platform.authority.dao;

/**
 * 许可、验证规则与处理模式
 *
 * @author yaohaitao
 */
public interface PeVaRuTransactModeDao extends com.cardone.common.dao.SimpleDao<com.cardone.platform.authority.dto.PeVaRuTransactModeDto> {
    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByCodes;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/authority/peVaRuTransactMode/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = com.cardone.platform.authority.dao.PeVaRuTransactModeDao.SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}