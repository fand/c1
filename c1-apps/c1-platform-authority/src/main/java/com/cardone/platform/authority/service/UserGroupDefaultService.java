package com.cardone.platform.authority.service;

import com.cardone.context.*;
import com.cardone.platform.authority.dao.*;
import lombok.*;

/**
 * 用户组服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class UserGroupDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.authority.dto.UserGroupDto> implements UserGroupService {
    @Override
    public UserGroupDao getDao() {
        return ContextHolder.getBean(UserGroupDao.class);
    }
}