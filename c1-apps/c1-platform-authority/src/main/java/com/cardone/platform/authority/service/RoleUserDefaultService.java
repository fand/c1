package com.cardone.platform.authority.service;

import com.cardone.context.*;
import com.cardone.platform.authority.dao.*;
import com.cardone.platform.authority.dto.*;
import com.google.common.collect.*;
import lombok.*;
import org.springframework.transaction.annotation.*;
import org.springframework.util.*;

import java.util.*;

/**
 * 角色与用户服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Transactional(readOnly = true)
public class RoleUserDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.authority.dto.RoleUserDto> implements RoleUserService {
    @Override
    public RoleUserDao getDao() {
        return ContextHolder.getBean(RoleUserDao.class);
    }

    @Override
    @Transactional
    public Map<String, Object> saveUserIdAndRoleIds(final String userId, final String roleIds) {
        return ContextHolder.getBean(RoleUserDao.class).saveUserIdAndRoleIds(userId, roleIds);
    }

    @Override
    @Transactional
    public int[] insertByRoleCode(final List<String> roleCodeList, final String userId) {
        if (CollectionUtils.isEmpty(roleCodeList)) {
            return new int[0];
        }

        final List<RoleUserDto> roleUserList = Lists.newArrayList();

        for (final String roleCode : roleCodeList) {
            final RoleUserDto roleUser = RoleUserDto.newRoleUser();

            roleUser.setRoleCode(roleCode);
            roleUser.setUserId(userId);

            roleUserList.add(roleUser);
        }

        return ContextHolder.getBean(RoleUserDao.class).insertByRoleCode(roleUserList);
    }
}