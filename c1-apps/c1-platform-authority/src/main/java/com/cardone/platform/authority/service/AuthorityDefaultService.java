package com.cardone.platform.authority.service;

import lombok.*;

/**
 * 权限服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class AuthorityDefaultService implements AuthorityService {
}