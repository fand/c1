package com.cardone.platform.authority.dao;

import com.cardone.common.dao.*;
import com.cardone.platform.authority.dto.*;

import java.util.*;

/**
 * 导航
 *
 * @author yaohaitao
 */
public interface NavigationDao extends SimpleDao<NavigationDto> {
    /**
     * 查询:导航
     *
     * @param mappedClass 返回类型
     * @param siteCode    站代码
     * @param typeCode    类别代码
     * @param parentId    导航标识
     * @return 导航对象集合
     */
    <P> List<P> findListByParentId(final Class<P> mappedClass, final String siteCode, final String typeCode, final String parentId);

    /**
     * 查询:导航
     *
     * @param mappedClass 返回类型
     * @param siteCode    站代码
     * @param typeCode    类别代码
     * @return 导航对象集合
     */
    <P> List<P> findListBySiteCodeAndTypeCode(final Class<P> mappedClass, final String siteCode, final String typeCode);

    /**
     * 查询:导航
     *
     * @param mappedClass 返回类型
     * @param siteCode    站代码
     * @param typeCode    类别代码
     * @param parentCode  父级代码
     * @return 导航对象集合
     */
    <P> List<P> findListByParentCode(final Class<P> mappedClass, final String siteCode, final String typeCode, final String parentCode);

    /**
     * sql标识
     *
     * @author yaohaitao
     */
    public enum SqlIds {
        /**
         * 查询
         */
        findByParentCode,

        /**
         * 查询
         */
        findBySiteCodeAndTypeCode,

        /**
         * 查询
         */
        findByParentId;

        /**
         * 根目录
         */
        public static final String ROOT = "/platform/authority/navigation/";

        /**
         * 标识
         */
        private final String id;

        /**
         * sql标识
         */
        private SqlIds() {
            this.id = SqlIds.ROOT + this.name();
        }

        /**
         * 获取
         *
         * @return sql标识
         */
        public String id() {
            return this.id;
        }
    }
}