package com.cardone.platform.authority.dao;

import com.cardone.context.*;
import com.cardone.persistent.support.*;
import com.cardone.platform.authority.dto.*;
import com.google.common.collect.*;
import lombok.*;
import lombok.extern.slf4j.*;
import org.apache.commons.lang3.*;

import java.util.*;

/**
 * 导航
 *
 * @author yaohaitao
 */
@Getter
@Setter
@Slf4j
public class NavigationJdbcDao extends SimpleJdbcDao<NavigationDto> implements NavigationDao {
    public NavigationJdbcDao() {
        super(NavigationDao.SqlIds.ROOT);
    }

    @Override
    public boolean isBlankById(final NavigationDto dto) {
        return StringUtils.isBlank(dto.getId());
    }

    @Override
    public void setId(final NavigationDto dto) {
        dto.setId(UUID.randomUUID().toString());
    }

    @Override
    public <P> List<P> findListByParentId(final Class<P> mappedClass, final String siteCode, final String typeCode, final String parentId) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.siteCode.name(), siteCode);

        model.put(Attributes.typeCode.name(), typeCode);

        model.put(Attributes.parentId.name(), parentId);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findByParentId.id(), model);
    }

    @Override
    public <P> java.util.List<P> findListBySiteCodeAndTypeCode(Class<P> mappedClass, String siteCode, String typeCode) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.siteCode.name(), siteCode);

        model.put(Attributes.typeCode.name(), typeCode);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findBySiteCodeAndTypeCode.id(), model);
    }

    @Override
    public <P> List<P> findListByParentCode(final Class<P> mappedClass, final String siteCode, final String typeCode, final String parentCode) {
        final Map<String, Object> model = Maps.newHashMap();

        model.put(Attributes.siteCode.name(), siteCode);

        model.put(Attributes.typeCode.name(), typeCode);

        model.put(Attributes.parentCode.name(), parentCode);

        return ContextHolder.getBean(JdbcTemplateSupport.class).findList(mappedClass, SqlIds.findByParentCode.id(), model);
    }
}