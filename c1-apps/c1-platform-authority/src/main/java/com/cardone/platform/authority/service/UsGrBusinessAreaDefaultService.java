package com.cardone.platform.authority.service;

import com.cardone.context.*;
import com.cardone.platform.authority.dao.*;
import lombok.*;

/**
 * 用户组与业务范围服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class UsGrBusinessAreaDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.authority.dto.UsGrBusinessAreaDto> implements UsGrBusinessAreaService {
    @Override
    public UsGrBusinessAreaDao getDao() {
        return ContextHolder.getBean(UsGrBusinessAreaDao.class);
    }
}