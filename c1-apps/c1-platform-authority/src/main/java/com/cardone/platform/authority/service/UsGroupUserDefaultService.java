package com.cardone.platform.authority.service;

import com.cardone.context.*;
import com.cardone.platform.authority.dao.*;
import lombok.*;

/**
 * 用户组与用户服务
 *
 * @author yaohaitao
 */
@Getter
@Setter
@org.springframework.transaction.annotation.Transactional(readOnly = true)
public class UsGroupUserDefaultService extends com.cardone.common.service.SimpleDefaultService<com.cardone.platform.authority.dto.UsGroupUserDto> implements UsGroupUserService {
    @Override
    public UsGroupUserDao getDao() {
        return ContextHolder.getBean(UsGroupUserDao.class);
    }
}